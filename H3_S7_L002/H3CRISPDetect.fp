>k141_6664 flag=0 multi=10.0249 len=1706		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                                           	Insertion/Deletion
==========	======	======	======	============================	==========================================================================	==================
      1323	    28	 100.0	    74	............................	GTAGAAAAGATAAATACTATTACATCAACAAATCAGGCGAAGAGATTATTAAACAAGGGTATGATGATGCAGGA	
      1425	    28	 100.0	     0	............................	|                                                                         	
==========	======	======	======	============================	==========================================================================	==================
         2	    28	 100.0	    84	AGTTTCTCCGAAGGATTGGCTTTGGTTA	                                                                          	       

# Left flank :   CTTGGTGTAACAGGTTTATATTTAACTTTTTATACTGATAATAATATTCCTACAATGGCGCGGAGATTCGGTGCCATGTTGTTGGGTGTATGGATAGCATGGATAGGGATTTTAATTATTTAATTTAAAAAACTAATTCCTATGATTTATTCTTACGATAAAAACAGTAATAAAAATTGGAAAAGAAATGATGATTGGAAGTTTTACACTTCTAGCAGTTTAAAAGAAAAAGAAAAAGATTATCTATTCCATGAACGAACAATCAAAGAATCAAAGCGCAAGTTTTTATATTTAGTGCTTGCTACTTGTATTCTTTTTACTGCTTTAATAACAATAATAATTATTTTAATAAAACTATGACAAACGAAATAAATATGCAAGGGTATGATGATGTATATAGTTTCTCCGAAGGATTGGCTTTGGTTAAAAGAGATGATAAATACTATTTCATCAACAAATCAGGCGAAGAGATTAATAAACAAGGGTATGATGGTGCAGAT
# Right flank :  AAAAAGACAATAAATGGTATTTCATCAACAAATCAGGCGAAGAGATTAAAAAAAAGGTTGACAAACAAAATCAAAAACCTAATTTGTAAGTTTATCAACTAATTTGTAATCACCTCCCTCTACACCCCCAAGGGGTATAGGGATGAGAGGATTATTCTCTCAATCGTTCTTTACAAGGAGTCAGCAATGAAGGTTGTTGTGAATAAAGTGTTATGCGGAATTACGGACGCAACATTTAAAAGATGGCAAGAAGC

# Questionable array : YES	 Score: 0.29
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:-2.6, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGTTTCTCCGAAGGATTGGCTTTGGTTA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.14%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.10,-0.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [65.0-71.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_5333 flag=0 multi=17.9507 len=3366		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                             	Spacer_Sequence                               	Insertion/Deletion
==========	======	======	======	============================================	==============================================	==================
       595	    44	 100.0	    46	............................................	TATCCTTGGATTACATAATTCTCTTAACATATTATGTTTCTCCACC	
       505	    44	  90.9	     0	..C....G..............................T..C..	|                                             	
==========	======	======	======	============================================	==============================================	==================
         2	    44	  95.5	    47	ATGCTTGAATTACATAATTCTCTTAATATATTATGTTTCTCTAT	                                              	       

# Left flank :   CATACTAAAACTAGTGCACTCTTGCAAATCTCTCAAGGTTGATTTTGGTGAAAGTTATCTAATATGACAAATCTTAAAGTCATATCCACTTCAGTTTTGACTTGAAGATCTCCTTTATGTCTAGAAGATCAGTGGATTATTGATGATTGTCATTGTTACTCAAAAGGAAAAGTCAATCAAGAAGGGAAAATAAAAGTCAATCAAGAAGGGAAAATAAAAGTCAATCAAGAAGGGAAAATATATCAACACTTCTTTCCGTTGCCATTAATGATATATATTCTAATACCTTCATAGTTTACTTTCATATGCTTGGATGGACCATTCTTCTCTTAAAGCATTTTCAGCTGCTTGGATGGACCATTCTTCTCTTAAAGCATTTTCAGATGCTTGGATGGACCATTCTTCTCTTATAGCATTTTCAGATGCTTGGATGGACCATTATTCTCTTATAGCATTTTCATATGCTTGGACCATTCTTCTATTCTCTTATAGCATTTCAT
# Right flank :  TATTCATTGTTATATATATGGAACTTAAGCATTTAACATGTATGATTTATTTCCCTATCGGATACTATTTCTGCAATGTTAAAGCTTATAGTTCTTAAACTCTTGATTAACATGTGCTTTGATCATAGTTGGTATTCTATTCATAATGATTCTTGTATTTTGATGATTGACTAATGCTATCCAAGTGGGAGAATGTTAAAATTTTATGTGGATTAGCATTATGTTAATAATTAAAATATTATCCTGATCATTATTATACCAATCATTTAATTATAATGATATAACACTTAGTTAATTTTAATGAATATAATTGTAATAAGGCTTTGTTATGAGCAACCAGTAATGGGTTGGTTGATTCATTTGGTAGCCTTTGAGAGGTCCCTACTCTCATTTTGGTGTGTAAAATACTGATCCCCATTTATCAGTATATAAACTCCATTTTTGGATTTTCTCTCATTACCT

# Questionable array : YES	 Score: 1.27
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.78, 5:0, 6:0.25, 7:0.04, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATGCTTGAATTACATAATTCTCTTAATATATTATGTTTCTCTAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:79.55%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-3.20,-3.30] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [4-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [73.3-70.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.41   Confidence: MEDIUM] 

# Array family : NA
//

>k141_4611 flag=0 multi=1628.0610 len=1289		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                       	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	======================================	===================================	==================
       111	    38	 100.0	    35	......................................	CCTCTAGAAGCCATTTTTAGTGACCGATTCCTTTT	
        38	    38	  94.7	     0	......G.....T.........................	|                                  	
==========	======	======	======	======================================	===================================	==================
         2	    38	  97.3	    36	TCGGTCTCTAATCTGTTGCGAACTCCAACATGAGACTT	                                   	       

# Left flank :   AAATTCGGTTGCTAAACTAGAGACTAACCAAACAAATCGGCCGCTAAATCAGAGACTGACAACAAATTCAGTCGCTAAATCAGAGACTGACAACATAATTCGGTCGCTAAATCTGAGACTTACAATAAAATTCAGTCGCTAAATTAGAGACTGACTAAAAAATCGGTCGCTAAATCAGAGACTGACAATAAAATTCGGTCGCTAAATCAGAGACTGATACCAAAAACTGTTGGCCAATTTTAGAGAGCGAAATATTAAGTCGGTCTCTAAATTTTAACGACCGACATGTTAAGTCGGTCTTTAAATTTAGCGACCGAAATGTTAAGTCGGTCTCTAAATTTAGCGACCGACATGTTTTGGCGGTCGCTACGTGTTAGCGACTTGCTTAGCTGAGACCTACCGAATTCGGTCGCTGTTTCGGTCGCTAATCAATTTAGCGACCGATATTGACGATTTAGCGACCGAAAAATCGGTCGCTAATTAGAGACCGATGTTAAAAA
# Right flank :  T

# Questionable array : NO	 Score: 2.05
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:0.29, 7:0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCGGTCTCTAATCTGTTGCGAACTCCAACATGAGACTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.26%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-4.20,-3.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_20647 flag=0 multi=53.5487 len=1630		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence        	Spacer_Sequence                                     	Insertion/Deletion
==========	======	======	======	=======================	====================================================	==================
       379	    23	  91.3	    28	AG.....................	AATGGCTTTGATGAGGTATTTATTCCAT                        	
       328	    23	 100.0	    52	.......................	ATGGAAACCATTCAACCTGTTATACCAACCGAAAACCAAAATGAAGTTTTCA	
       253	    23	 100.0	    22	.......................	GCAGAAACCTTAAACACATACA                              	
       208	    23	  87.0	     0	.G.........A....G......	|                                                   	
==========	======	======	======	=======================	====================================================	==================
         4	    23	  94.6	    34	CAAATGATGATGAAGAATTTGAA	                                                    	       

# Left flank :   TGACAAGGTTGAATATTTCTATAATTCAACCAAAGAAAAGAAAGCCTTTGAATATCATAAAGACCCAGCAAAAGTTTCTTTTAGTGATTTATCAAGACAGTCTGCAAAGGCTGGACTACAAAGACTAATGGCAGATACAACGCCACCAGATACCTTTTATGTAACTGGCCCTGACTACTTCAAGAAAAATATTTGGCTTGAATCTCAACTAATGGGATTGGAAGACAAAGGCATCAAACTTGAAGGTTATAAACCTACTCCAGCCGATTTAGAAGAATTAAAGAGAATTCAAGAGCAATACGCTTCTATGAATCCTAAATTCACACCAACAGTTCAAGCACAACCAGCCCAGGAAGCCATAGAAGCACCAGCACCATCTACCCCTATCGTTTCTATGCCTGCTGCACCTACACCGTTTAAAACCGTTGTAGAGCAGCCTACGGCATCAGGTTCTGACGAACCTAAAAAGCAGGTTGAACAGGAAGAAGTTAACTCCTTCA
# Right flank :  AGCAGAAGTCGCCCAGGAACCTGTTATACCAAACGAAATGCAGGTTGAAGATTTAACACAAGAGATAAAATCACCAGAGCAACAAAAACAAGACTTTGATGATTTAGTTGATGAGTTAATAGAAACCTATGGTGATGAGTTTAATGAGTCCTATATTTTATCTTGTGTAATGGACGCAGTAGATGG

# Questionable array : NO	 Score: 2.10
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.73, 5:0, 6:0.25, 7:-0.48, 8:0.6, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CAAATGATGATGAAGAATTTGAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:73.91%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [3-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [56.7-55.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.41   Confidence: MEDIUM] 

# Array family : NA
//

>k141_27267 flag=0 multi=10.9888 len=3082		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                       	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	======================================	=====================================	==================
       582	    38	  97.4	    19	...........T..........................	TGGACCAGTTTCTCCTATT                  	
       639	    38	 100.0	    37	......................................	ATCTCCTGTATATCCAGTGGGTCCTGTATATCCAGTG	
       714	    38	  92.1	     0	..T..T..T.............................	|                                    	
==========	======	======	======	======================================	=====================================	==================
         3	    38	  96.5	    28	GGACCAGTAACAGTTGATGCTGCACCTGTAGGTCCTGT	                                     	       

# Left flank :   TATTAGTGATAGAACTTCTAGCATAAAAAGACACTGTGACTGTTTTATTTAGACCGCACAACTTTATTGTTCCATTTTCAATGCTTTGGTCTTGAAAACCATAAGGGGTATCAGTTGCACCACTTCCAGCTCCGTCTGGGTTGATTCTATAAAAATAATAAGCCCCATATATATCTTCAGCAGATAATATTTGTCTTGAATATATGATGTTACTAGGTGGTGTATACCCCACCCCACCAATTCTAACTCGCCATCTGTCTGTAACAAATGATTCATCAATAGAATTAGTTACACTCGTCCCCCTCTGCCAGACATCAAAGTTACCGTTAATGATAGCGTTAGAGTAGAGATTACGATTAAAGGCTGGGACATTTGAAAGGGCTGTAATATATTTAGCGTCATCTGTGCCTGTGACATTTTCAAGAGCAGTGGCTTTGGTAGGAAGAGGCCCAGTTGGTCCTGTATCTCCAGTTGGTCCTGTATCTCCAGTGTATCCAGTT
# Right flank :  TAGGACCAGTTGCTCCGATTGGACCAGTGGGTCCGGTGGGTCCAATTAAACCAGTTCCTCCAGTATCAACCCATGTAGATGAATCTGAATCCCAAGTCCAAATCGTGTCAGTGCTGCCAACCATCCCGAAATATCCTGATGAACCAATAGGGTATGCTGCAGATAAGTCTCCTTCCGTTGCAAACCATCCTAAGTTATAAGGGTCAACTACTTTGTTCGATAAGTATGTCATATCTATTAATTATATTATTTATTAAAACTTTTGCAACTTCATATTACTAAATTATCCCATAAAGTAGAATAAATAACAAGGAAAAGCCCCTTGATTTCTCTCGGGGCTTTTTTGTTAGAAGTTCAGTTTAAGTTGCCGACCGTCAAGACGAATCGGATTCCACCAGTCACATTTCGTGACAGTGAAAGATTGGTGTTCAGTGCGAAACAGTCCTGCTCTGATTGAGCAACACTTCCTCGCATTGCAACCAAAGCAACATGTTCTGACT

# Questionable array : YES	 Score: 0.89
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.82, 5:-1.5, 6:0.29, 7:-0.12, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGACCAGTAACAGTTGATGCTGCACCTGTAGGTCCTGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [8,10] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-4.50,-4.50] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [1-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [48.3-43.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_33385 flag=1 multi=10.0135 len=2431		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                   	Spacer_Sequence             	Insertion/Deletion
==========	======	======	======	==================================================	============================	==================
         1	    50	 100.0	    28	..................................................	AGATGAATTTGTCCTTCCCACAAACTGG	
        79	    50	 100.0	     0	..................................................	|                           	
==========	======	======	======	==================================================	============================	==================
         2	    50	 100.0	    38	CACATACGCAATTGACAGCGATGACTTTAAAGCATACACGTTGTCATATC	                            	       

# Left flank :   |
# Right flank :  CGGCTGGTTTTGTACTTCCCACAAACTGGCACATACACGATAACAAGAGGTGACTGGGACGGGTTCGCAGCGGTAAACAATATTACCGTAGAGATGAACTTACTATCTGCTCAGGTCGATGCAATCTTACTTGGCCTTGCTGATGGCTTCGCTGCACGCACAGTTGCCGGAACGAAAGTAGACTTGCTCGGCGGTAGTAATGAAGCGCCAAATGGAACGGTAGAGGCGGCGGCAGACGCGGACATCAGTACATGGACTGGCGGTAACACCGCGTATGAACTGGTAAACGACACATATGGAAAGTCGTCCTATCACTGGGGCGCAGTAGAGGTACAAACATGATAACGGTCACGAAAGACAAGCCGATATTCTACGCGGTAGGCAATCCTATCATCGTTACCGGCATCGTCGATGTTGGCGGGTTGGTGGTGACTGCATTGCCAACCGTTACAGGCGAGAGCGAGAGCGCATTCCTGACGGCGTCCAATGGCAAAGCCACA

# Questionable array : YES	 Score: 1.17
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-0.75, 7:0.03, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CACATACGCAATTGACAGCGATGACTTTAAAGCATACACGTTGTCATATC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:60.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-8.70,-6.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

