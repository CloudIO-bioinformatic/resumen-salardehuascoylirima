>k141_55185 flag=0 multi=9.1834 len=1951		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                                    	Insertion/Deletion
==========	======	======	======	=============================	===================================================================	==================
       687	    29	 100.0	    67	.............................	CCATTTTTATCTTCTGCTATTAATGATTTTATTTTTATTCTTCCTTCATCTATATTGTGCCATAAAT	
       783	    29	 100.0	     0	.............................	|                                                                  	
==========	======	======	======	=============================	===================================================================	==================
         2	    29	 100.0	    77	CACGTTCTTGTTTTGTAAGTTTAACTCCT	                                                                   	       

# Left flank :   GATAGATTTCGATACACTTCTACTGGATGATATAGGAGAAAATGAAAATATTGAAGAAGATGATATGCTAGATGTTCCGGTAGAAACAGAAGGCGAGTTCTGTTTAAGATAATCTGATACAGATTCAGTTGTTTTTATTTTCTGTGTGATTGGAGATGAACTTGCCGAGATTGCTGTGGTAGGATTTGCTTTTGATTCTGTAGAACTAGATTTGCTAAAATCAGCAAGCTCTGCTTTAATAACTTGTTCTCTTACAGTACTTTCTGGAAACATAGATTCAAATTTCTCAATAGCTTTATTTAATTTTAATGAATAAGTAAACTTACTAGCTTCTAAAGTCTTTGCAGAAACAAAGATACTACCAATATCCTTAACTCTTTTCTCTGGTGGAAATATATCCATTTTAGTTCCAGCATCTATAATTTTATTTTTATTATAAAATTCTTTTAATTCTGATATATCTATTTTAACTTTTCCCTTTTTTATATCGCGTAATAATA
# Right flank :  TTCTTTAGTCAAATAATTTGTATGATGAAATTCTACGCCTTCTTTTATTTCTTTTTTTGTTAGCCCTTGATATTTGGAAACTTCTTCACCAGTATATTTATTTCTAACTCTAGATACTCCTTGTATTACACCACGAAGTTCTTCGTTTAATTCCGTTGTTTTCAATCCTTCTTGAATATACGGTTTTCCTTCTTTTTGAAATCCGAGAACCTGATCTGGCATTTGTTCTTCTATTCCTACAAGTTCTTTATCATTCAAATATTCTGTAACCTTTTCCCATTTTCCATTCTTCTTTATTTCTATACCATTTTTTGCAGAACCTTTTCCAGCTAATCTAGTTTCATATCCAAGTTCTTTAAGTCCTCTTTCACTAATTTTAACTTCTTCTAATATTTTTTCAGCATTCATATTAAGATGAAAGTCGATATCGTTTGGTTTTTTATTATATATAAATAGTTCATCGTCTATTTTATATGAATCTTTTAATTGCGCTGCTCTAG

# Questionable array : YES	 Score: 0.99
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:-1.9, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CACGTTCTTGTTTTGTAAGTTTAACTCCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:65.52%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [80.0-76.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_57474 flag=1 multi=4.0000 len=1568		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                    	Insertion/Deletion
==========	======	======	======	============================	===================================================	==================
       916	    28	 100.0	    27	............................	AGGCTGAAAACTCCAACAGTTCGCCAG                        	
       971	    28	  92.9	    51	.......C............T.......	CCACCCTTTCGGCCGAAACCACCATCTCAAAAATACCAACAGTTCATCAGT	
      1050	    28	  92.9	    26	........T......A............	AGGCCGAAAACTCAACAGTTCGCCCG                         	
      1104	    28	  71.4	     0	...................GT.AGAAAA	|                                                  	
==========	======	======	======	============================	===================================================	==================
         4	    28	  89.3	    35	AAGGATGGCTTGAACCACTACTCCCTTT	                                                   	       

# Left flank :   GCTAACAATCACATAGTCAAACCGCTATTACAAATTTCAATGTCTAATAAGAGGACTTGAACCTTTAAGCTTTTGGTTGAACCCACCATTACAACATTACAAATTTCAATGACCGACTAAAAAGGCTTGAACCTTTGACCTTTTGGTTGAAACCAACCTTACAACATATATAGCTTTCCTTTGCAAATAAGGAAAAAACATTTTTTTTTGGCTGAAACCATTATTACAAATTCCAACCACTTGCTAAAAGCACCTGAATGTCCAACCTTTTAGCTAAGCCACTATCACAAAATATTTTCAAACCTCCAGCTTTTGTTTGAAACAACTTGCTAAAAACGCTTAATGCCCCACCCATTTTAACCAAACCACCATCACAAAAATTCTCAAATCTCCTGCCTTTTGGCTAAAATCACTGTTACAAAAATTCTCGAAACCACTATCACAAACATCCACCCTTTTGGCCGAAACCACTATCACAAAAATACCAACAGTTCGTTGGT
# Right flank :  CCACCAAAAAATAATTCCAGTGTTAAAAAACCACAAAATTCTAAGTCGCTAGAAGGAGGGCTTGAACCTCCGACCTTGTGGTTAACAGCCACTCGCTCTAACCAACTGAGCTATTCCAGCTTTTGGCTACAAAAGAAATTTTACATATTATAGTGGCTAAGTTCTTTCTCTGCTTTTATATATCAAACCGCACATGAAAATTATTGCATTACGGTGCTGGGATTTGAAGGCAGCAACAGTTCACATTACGTCTCTGCATTACCTGATATATCTAAAACATTGCTCTTTGCTGCTGCGATTTGAACTCAGGTTGCCTGGATGAAAAGCAGTTATTCTAACCTTACTGAACAATTATACCTCAATAAATATAGGCCTTTGTTGTCGGTAAAAGTTTCCCATTAAGTTTTACAGTAGTTTGAGGAGTAATTTTCTTTTCC

# Questionable array : YES	 Score: 0.75
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.46, 5:-1.5, 6:1, 7:-0.30, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAGGATGGCTTGAACCACTACTCCCTTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.57%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.20,-2.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [2-10] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [56.7-63.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_58906 flag=1 multi=5.0000 len=1027		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	==========================	======================================	==================
       183	    26	 100.0	    38	..........................	AATGACAGGTAGCATAACCAATAGTCGAGGAATGAGCG	
       247	    26	 100.0	     0	..........................	|                                     	
==========	======	======	======	==========================	======================================	==================
         2	    26	 100.0	    48	AGGAGAAGTACTTACATCAAGGAAAT	                                      	       

# Left flank :   AAAAATTTATAATTATATCTCCTAGTACCTCAACATTTTTACCCTCAATGACTTTGATTTTGATTGATATCATTTTACCCTCGAGCGTGATAGGTTGTCTCTATGAAAAATGATTTAGAAAAGATGATATATAGCTCAAAGGTAGCCAGAGGAGATAATTAAACAATAAGAAATAGGCATATA
# Right flank :  TCCTTTTCTATTTTATTGAAATATTTTAGTTCTTCTCTTAGCATAGCCTGAGGAATGTTGCAATTGATTGTAGGGGTAATCAAGTAAAGGTTTATCAACAAAATATACAACAAAATATGTAGCATAAAAGTAATATTTCTCAAGTAGGCAAATGAGGAAGATCATTCCTACTAGAAGATCATCAGCCACCAGGTACACAAGTTCCTTTTATGTTTACTATTAATCATGCCATTATTTTGTACATAAGGCTATTGATTGCAAATCCTATTCAAATAAGTGTTTGAGTGCATTGTATGTGGATAAAAGCATGAGAAGACACAATTTTTTTCTTCCCTTTCCTAGTCACGGAGGATACTATAAATGTGACAATATTCTTGGCCACGTGGCTAAACATTGCAAGGTACCCCATTCCTTAAAAGAAACTAGACAAATGAAGAAGATAATATAAAGAAGATTACCCAAGAAGACAAAAGTCGATGCATAAATTAGAAGGCAACCCA

# Questionable array : NO	 Score: 2.61
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.26, 7:0.46, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGAGAAGTACTTACATCAAGGAAAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:65.38%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [68.3-68.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_60793 flag=1 multi=7.0000 len=1385		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                                                         	Insertion/Deletion
==========	======	======	======	==========================	========================================================================	==================
        72	    26	 100.0	    72	..........................	CACAGGAGAACGAATCATCTTGTCCACAGGAGAGTGAATCATCCTGTCCACAGGAGAGCGAATCATCCTGTC	
       170	    26	 100.0	    22	..........................	ACAGGAGAGAGAATCATCCTGT                                                  	
       218	    26	  92.3	    32	.....G................A...	TCAAATCACCATGGAGAACACATCATCCAGGC                                        	
       276	    26	  88.5	     0	.....A....A.T.............	|                                                                       	T [299]
==========	======	======	======	==========================	========================================================================	==================
         4	    26	  95.2	    42	CCACATGAGAGCGAATCATCCTGTCC	                                                                        	       

# Left flank :   ATGAGAGGTACTTCCCATGAGTCCCAGACTGCATGGGTAGGATGCCTCGACAAGAGAACTAATCGTCCTGTC
# Right flank :  CACAATCCCCACGAGGTGTCATAGGGGAAGGTTGAACGTTTTACCCTGTACCAAGTGCACCAACAGGGGATGGTGAGTCACCCTATGGTCATGTGTCACTCCAAGAAGTACCCCACGCCCCCCTAAAGCTCCTAGAGCTCAGTTCATCATACTTGACCCTCATCAGAGGGTCTCTGAATGACACCAAGTTGTTCTTGGAAAGTCAGGAATTTGACCTTTCCTAGGGGATTTGTCACAAAGTCGGTGACCTGTTCATTTGTACTGATATGTTGGAGCCTTATTGATCCTCATTGCACCATATCTTGAATATAGTGGTACTTGATATCGATATGCTTGAAGCGATCATGAAATACAAGATTTTTTGATAAACGGATCCCGCTCTGGTTGTCACATAGAATCACGGTGGTATCAAATGTAAATCCAAATAGCTCACTGAAGAGCTTTCGCAACCAGACTGCCTCACAGGAGGCCATGCTAGCAGCAATATACTCTGCCTCAGC

# Questionable array : YES	 Score: 1.42
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.76, 5:0, 6:0.26, 7:-1.00, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCACATGAGAGCGAATCATCCTGTCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [7,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-6] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [48.3-48.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0   Confidence: HIGH] 

# Array family : NA
//

>k141_61960 flag=0 multi=22.2610 len=3831		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                             	Spacer_Sequence                               	Insertion/Deletion
==========	======	======	======	============================================	==============================================	==================
       396	    44	  95.5	    46	....................A............C..........	TTGAGATAATTTTAGAGGAATTGAGGTAACAATGGATTTAGACTTG	
       306	    44	 100.0	     0	............................................	|                                             	
==========	======	======	======	============================================	==============================================	==================
         2	    44	  97.8	    47	GGTGTATGTATAAATTAATGTTGGTACTATTTTGTTAATGAAGG	                                              	       

# Left flank :   TGGTATTTTGTTGCTTTGCCTCTACATGTGTTCTTTGAAGCGAGACAAGTGAGAAAGGAGTGGCAAAGCTATATCAAAGGAGAGAGGAAACAAGCAAGGAGCCAAGCCAAAGACTGAGGTTGGTGCTCTCTGCTTCTCTCTTATCTATCATCCATAAAGAAAAACACGAGTTTCTTAAAATTGTTTGTTTCTAAGATATCTATCCGTCTTAGTGGGACTAGCAAATCAAATAGTGACAGTAGTTTTCCCAGAAACACCAACATAAACCAACTTGCCTATCCCTCAAGGGTTTACTAAAATTATAGTATTGACATCAAATTCCATATAATGTTAGTTAGATATATATATTATAATATTACCATTTCAAATGATTATAAGAAATTTAAATTCCTTTTAGCGAGTTATTATATTTATAATATGAGAATTTGAAACTATCCACCTAAAACTATTTTAATTTGAGTTTTTTTAGAGGAATTGAGGTAATGATAGATTTAGATTCA
# Right flank :  GCCAACAATATTGGATGGAAGAGCCCACCAATGGGATAGTTTTTCTAATTCCCCCAAAGATGGATTCCATTCTATAATATAGTATTAAAATGGTATATATATATATATATATACTATCCACTTAAAACTATTTTAATATGGGATTTACTTTGGAAGAATCCCATTAATGGGATAGTTTTTCTAATTCCCCCAAAAATGGTTTCCATCCTTTAATATAGTATTACCATTTCAAAATGTTATTATATTTATAATATGAGAATTTG

# Questionable array : YES	 Score: 1.38
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:0.25, 7:0.04, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTGTATGTATAAATTAATGTTGGTACTATTTTGTTAATGAAGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:72.73%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-1.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [58.3-78.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_64496 flag=1 multi=11.0000 len=1628		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                          	Spacer_Sequence            	Insertion/Deletion
==========	======	======	======	=========================================	===========================	==================
         1	    41	 100.0	    27	.........................................	AGTCTTGACTAAACTATCATTTAAAAT	
        69	    41	 100.0	     0	.........................................	|                          	
==========	======	======	======	=========================================	===========================	==================
         2	    41	 100.0	    37	AAAAAGCCCAAATGGTATAAGTTGACTTTTGGGTTAGATGG	                           	       

# Left flank :   |
# Right flank :  GGAGTCTTAACTAAACTATCATTTTCTTTATTGTGATAATATATTTATTTGCTATTGTATTTTTTTATTTTTTTTAAAGAAGGTTTCTCTTTATGTGTTTTCATTAATAAGAAATATGTTATTAATTAAACATAATATTTTATTATATGTATGATAAGTAAATATTATATATGATGATGATAATGACTATAATGATTAATATTCACTAAAACTAATATGGTTTATTGGATCTAAAGATAGTAAGTGATTAGTTGAGATTTTTAATATTTTATTTCTATGATAAAAAAGAAGAGAGATGAGAATTCCTAGATCACCATAGTTCTACTAAATTAACATGTCAAGAAGGTTGGTAGTGTGGCAAAGATTTTGAAAATTAGAACAAATGTGAGTAAGAATTGGGCAGTCTAGGAGAACGTGTTTTTCATCTTCAACCCTCTTAGTATCACAAAGGTGACATACTCTTTTGTCTCAAGGTGTTTTGAGAATTGACCAACGCCAAA

# Questionable array : NO	 Score: 2.04
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-0.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAAAAGCCCAAATGGTATAAGTTGACTTTTGGGTTAGATGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:63.41%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-9.20,-5.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_64364 flag=0 multi=74.4365 len=1534		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	===============================================	=========================================	==================
       400	    47	 100.0	    41	...............................................	TTGGGCGGGATAGGGTTTTGTTTGGGCGGGATAGGGTTCTT	
       312	    47	  97.9	     0	..................................G............	|                                        	
==========	======	======	======	===============================================	=========================================	==================
         2	    47	  99.0	    42	GAACCCTATCTACCTGATGTTATTGTTATGTGGTTTATGGGTTTTGT	                                         	       

# Left flank :   ATCGCCATTTTAATGATTGTATTGTATTGGACTGTGCTACTGTTATTTCGTTTTCTGTATTTATACAATTATTATCGTAATCTTCGCCATGCAAGGTAATTACGCAATGCACATGGTTGGGCATTATTATCCATTCATCTAAAGTGATACGTTTGTGATATTCAGGTATTTTTAAAAATTCGCTTTCGGTTATTTTGCCTGCTACAGACAATTGCATTTGTTTATCTATTACATTTCCAAAAATATGCAATCTATTATGTGTGCATATTGTAAGAAAATAGGAGGCAGGTGCAGAATAATCCCACAAATCCCAACGATTAGATTCTATGCGGTATCCTTTAAATTTTGCCATAAATTTTGATGTTTTGTTTGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTCAA
# Right flank :  TGGGTTAAATTTAAAGGCGGTTGTTTTGCCTGCGCCGTTGCAACCTGCAATGATGTATAGGTTTTTGTTGTGCATTGGTGTTTATCATTTATGGTTTTTACGTTTTAGGTCGTTGTAATTTCTAAAAAACTATCTACAACCGCACAAAACCCTATCTACAACCGCACAAAACCCTATCTCGCCCAAACAAAACCCATAAACCACATAACAATAACATCAGGTAGATAGGGTTCTTGAACCCTATCTACAACCGCACAAAACCCTAT

# Questionable array : NO	 Score: 1.56
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.95, 5:0, 6:0.26, 7:0.15, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAACCCTATCTACCTGATGTTATTGTTATGTGGTTTATGGGTTTTGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:63.83%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.20,-5.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [53.3-56.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_64364 flag=0 multi=74.4365 len=1534		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	===============================================	=========================================	==================
       400	    47	 100.0	    41	...............................................	TTGGGCGGGATAGGGTTTTGTTTGGGCGGGATAGGGTTCTT	
       312	    47	  97.9	     0	..................................G............	|                                        	
==========	======	======	======	===============================================	=========================================	==================
         2	    47	  99.0	    42	GAACCCTATCTACCTGATGTTATTGTTATGTGGTTTATGGGTTTTGT	                                         	       

# Left flank :   ATCGCCATTTTAATGATTGTATTGTATTGGACTGTGCTACTGTTATTTCGTTTTCTGTATTTATACAATTATTATCGTAATCTTCGCCATGCAAGGTAATTACGCAATGCACATGGTTGGGCATTATTATCCATTCATCTAAAGTGATACGTTTGTGATATTCAGGTATTTTTAAAAATTCGCTTTCGGTTATTTTGCCTGCTACAGACAATTGCATTTGTTTATCTATTACATTTCCAAAAATATGCAATCTATTATGTGTGCATATTGTAAGAAAATAGGAGGCAGGTGCAGAATAATCCCACAAATCCCAACGATTAGATTCTATGCGGTATCCTTTAAATTTTGCCATAAATTTTGATGTTTTGTTTGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTTTGTGCGGTTGTAGATAGGGTTCAA
# Right flank :  TGGGTTAAATTTAAAGGCGGTTGTTTTGCCTGCGCCGTTGCAACCTGCAATGATGTATAGGTTTTTGTTGTGCATTGGTGTTTATCATTTATGGTTTTTACGTTTTAGGTCGTTGTAATTTCTAAAAAACTATCTACAACCGCACAAAACCCTATCTACAACCGCACAAAACCCTATCTCGCCCAAACAAAACCCATAAACCACATAACAATAACATCAGGTAGATAGGGTTCTTGAACCCTATCTACAACCGCACAAAACCCTAT

# Questionable array : NO	 Score: 1.56
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.95, 5:0, 6:0.26, 7:0.15, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAACCCTATCTACCTGATGTTATTGTTATGTGGTTTATGGGTTTTGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:63.83%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.20,-5.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [53.3-56.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_66930 flag=1 multi=10.0000 len=1034		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                            	Insertion/Deletion
==========	======	======	======	============================	===========================================================	==================
       697	    28	 100.0	    57	............................	CAATACATATATGTTTCGGGTTTTTTGGAAGAGAAGGTCTCTATCAGTATCAGTAGG  	
       782	    28	 100.0	    59	............................	TAGGAAGTACAATACATATATGTTAGCAGTTTTTTGGAAGAGAAGGTCTCTATAGGAGA	
       869	    28	 100.0	    57	............................	CAATACATATATGTTTCGGGTTTTTTGGGTAAGAAGGTCTCTATCAGTATCAGTAGG  	
       954	    28	 100.0	     0	............................	|                                                          	
==========	======	======	======	============================	===========================================================	==================
         4	    28	 100.0	    58	AGGAACAGTATCAGTAGGAGAAGAAGTA	                                                           	       

# Left flank :   AAAAAGTTAAGAAGCCTCTACAATCATATAGATGTGATACAACTTCCGATGTCATCAATATTTGTTCATCAGCATCCGCATTTGCAAGTTCTCTTAATTTACTTAACAAATTTTCAAGTTCATCATCACTCATATCATCAAAGTCTTCATAGTCTTCAAAATCATCAATATTCATTTTGTATTTCCTTTAGTATTCAATGTAGTTTCGTTGAACTTGTTTATCAACTGGCGTTGAGCTTCGAGTAATTGTTTTTGTGACTCTATTACTCTGTCTAATGCTGTGTGTAGTTTTTTGTTGAACATACTTATGTTTATATCAGTAACACAAAAAAGACACCTCTCGGTGTCTTTTATTTTAAATTAATTTACAATGCCGTTTTAAGCCGTTTCTAGCCCGTTTAATGATAGAGTAGTGTCTTTGTATCACAAACATAAAAACAGGCTAATATGCCATAGTATGAGGATGATAAGCAATGATTGAAGTGTTAACGTATAGAGGA
# Right flank :  TAGGAAGTACAATACATATATGTTTGCAGTTTTTTGGGTAAGAAGGTCTCTAT

# Questionable array : NO	 Score: 1.95
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:-1.45, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGAACAGTATCAGTAGGAGAAGAAGTA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:60.71%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [63.3-60.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_67147 flag=1 multi=3.0000 len=1380		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence       	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	======================	=========================================	==================
      1201	    22	 100.0	    39	......................	ACAGAAACAATACAAATTCACACATCTACCATGTAGATT  	
      1262	    22	  95.5	    41	................C.....	CTAAAAACAGAAACAATCGGGATTTACATACCCACCATGCA	
      1325	    22	 100.0	     0	......................	|                                        	
==========	======	======	======	======================	=========================================	==================
         3	    22	  98.5	    40	CCCACACAATTAAACATTAAAA	                                         	       

# Left flank :   TTCTTGGTTATGAATTAAAAGCATTTTCATTCCTTCGCAGAAGTCATGGTCCCAGTTAATAGAAATTCATTTCATGTCTCTTCTTTTTGCTTTCAGAAGAGGCAAATGAGCCTGGCAGCCCTGTCAGGACCATGCTGGCGTTTGGGCACATCCTCCTGGAAATGCCTGACGGTGTCTCAGGGCCCTGGCACAGAGCTGGGCATTTAGACCTTTGCATGAGCAGCAGCAAAGTCCCCAAAACTTCCAGGAGTGGGTGTGGGGCAGCCCCAGGCCTTCCTGCCTGCACTCCCAGTTTTAGCCTGCGTCATGGTGTCTGCATTCACCTTATTGCAGCCCCAGGTGTCTGTGCTGGAAGACTGAAATTCTGGTTGTTTAAACGGCTGAGTGGAAACTGAGGCACAGAGAGGAAAAGGACTTCACACAAGCGTCCCCAAGCTGCAGGAACAAATGACAACAAACTGGGGTGTTCAAACAATCGGGATTTACACACCCACCATGCA
# Right flank :  ACAGAAACAATACAAATTCACACATCTACCATGT

# Questionable array : NO	 Score: 2.22
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.93, 5:0, 6:-0.25, 7:0.45, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCCACACAATTAAACATTAAAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:72.73%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [55.0-38.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0   Confidence: HIGH] 

# Array family : NA
//

>k141_66564 flag=0 multi=318.7441 len=3377		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	===========================	=======================================	==================
       441	    27	 100.0	    39	...........................	AGCAGACAATGTTGCTATGGGCATATGTCATTATGCATT	
       507	    27	 100.0	     0	...........................	|                                      	
==========	======	======	======	===========================	=======================================	==================
         2	    27	 100.0	    49	CATTCATTATATATGTAATTACGCTTT	                                       	       

# Left flank :   ACTGATAAGGAGAGGACCTTTAGGTGCTCGACCCTAAGGACCTTTAGGTGCTCGACTGTTAAGGAGAGGGAGAGGACCCTTTAGGGGGCTCGACCGTTAGGGAGAGGAGAGGGCCGCCATCGAGAGGAGAGGAGAGGATGCGGAGCACGCTCGACCAGCGGGAGAGGTACCTTAGTGACTTGACTGATAAGGAGAGGAGGCGGAGGTTTTAGCAGAATCAGTGATCGAATATTCCCAATTTCATGGTTTGGGGCCAGGCCTCTCAATCTCTCCTTTGGGGTCGATATTTCAGGCCTGATACCACTTTCTCTGATTACGCTTTCCTGACAAACAACCATGAGCAGCATGTATGAAATGACATCCATGTGGTAATTTCATGAACCGGTGATGCAGAGTGAGGAGTTGAGAGATGCTCCTGTTTCCAGGAGATAGATTCATGAC
# Right flank :  TCTTATCGTATGGGATCGTAGCTCTTTTCATCGATCGTACCAGAATCTCGTATACAAAGATATATATCTAATTACTAATCATTCTTTTCATAGAATTAGTTGATTAGGCTATCCTTCTAAGTTCCGGGGCAGAATTATTGTATTCTTGCAAAAAAGGGCGCCCCCCATCCATCAGTTCGGGGTGGGTAATTCCATCCGATGAACAGAGAGAGGTAGTTCTCCTCCCCGTGAGTTGATTATATTTCTTTTTTCCTTTCCCGTTGGTCAAGCCTGCTTACGCATCCTTTCTTTCCTCCGGCTATAGCTCTTGCTAGTCGATGGTAACACCCAACTAGAATAGGTAGTCATCTCCTTCTGAACTCAACTACTATCCTGGATGGCCAGCAAGCATGGAGGATTATGGTTCCAGTCGAGCCCAACTATATGGATTATGGGAGAGTAGTCCAATTAGAGGATGCTCTCCCTCAACTATTTAGAGGAGATTAGGTGGCTGGAGGATG

# Questionable array : NO	 Score: 2.47
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.28, 7:0.3, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATTCATTATATATGTAATTACGCTTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:77.78%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [51.7-60.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_67684 flag=1 multi=3.6182 len=1917		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	==============================	=======================================	==================
      1292	    30	 100.0	    39	..............................	GGTATTTGTATCAATGGTGCCAGACGTGACAATATTAAC	
      1361	    30	  96.7	     0	...........................T..	|                                      	
==========	======	======	======	==============================	=======================================	==================
         2	    30	  98.3	    40	CCACGGCGAACCGTCGGCGCTATGTTGAAG	                                       	       

# Left flank :   GTAACGCTTAACGCCCTATTCAATCCGGCATCCTGCCCCGTCAATGTGAACGTGCCCGCGTCAGCAGCTACCCTTAACGACCTGTTTAATCCGGTATCCTGGCCTGTTAATGTGAACGCGCCCGAATCCGCTGCAATGGATACGCCACGCGTTAGGTTCGCATCTTGTCCGGTCAACAGGAACGTGCCCGCGTCAGCCGCTACCCTTACATTCTTGTTTAGGTTTGCATCTTGCCCGATTAACAGGAACGTGCCCGCGTCAGCAGCTACCCTTACATTCTTGTTTAGGTTCGCGTCTTGCCCGGTTAACGTGAACGCTCCGGAATCCGCTGCAATAGATACGCCCCTCGTTAGGTTTGCATCTTGTCCGGTCAGCACAAACGCGCCCGCAAATACGGCAAACCTTAGCGCCCTAACTATATCAGCCGCTTGCCCTGTTAACGTGAACGCGCCCGCGCCAGCCGTCATGGTATACGCGCCGCCGGATGCCCCTACGCCAGT
# Right flank :  GCTTTAAAATATCGTCCTGGCTTATCATTGATTTTTATTGCCCTAATTGCTGCCGTAGTAGTTCCGCCGCCGCTTCTATTGCTTCCAGTTCATCCTCCATCGCTTGGCGGTCTTTAATCTCTTGCAATTTAGCCGCCGCCGCCGCTTCTATTTCAGCCGTTTTTGCGTTGCCTTTGAGTTTTATCATAACAGCCTCCCCATCAGCTACCTCTACCCATGCTCGTTTTTGCCCGGATTGGTCGGTTTCCACCCGGATTGTTTTGTGCGTATCAAACAGCTTTTTTTTCATGTTTAGGATATTGTAATTGACCCAACGTAGGTGTTTGAGTTGCCCGCGATGTACCAGCTATCAAACCATATTTGAACGATGCCAGTAACGGATGGGGTAAACGTTAAGTCCACCTGCTGCCAATCAGTTGAATTGATAGCAGTGTCTGTGGCGTAAGAAATACCTGCAATGTTCATTTCAGAACCATTGACACGAATCCGGCATCCTACAT

# Questionable array : NO	 Score: 2.40
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:0.99, 7:0.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCACGGCGAACCGTCGGCGCTATGTTGAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [6,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.40,-2.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [28.3-63.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_68573 flag=1 multi=95.9405 len=1705		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                          	Spacer_Sequence                         	Insertion/Deletion
==========	======	======	======	=========================================================================	========================================	==================
      1228	    73	  91.8	    40	.....A..G...........A...............T.....A...................T..........	AGAGTGTTACAAAATACTAAAACATAAAACAAAAAAAAAT	
      1115	    73	 100.0	     0	.........................................................................	|                                       	
==========	======	======	======	=========================================================================	========================================	==================
         2	    73	  95.9	    41	ATATAGTAATAACATGAATTGAAATTATTAATATAGCAATAATATGAATTAAATAATGATAACGATTAGTATA	                                        	       

# Left flank :   ATGTTTTCGTACTTCGTAGCATTACTTTGTAACACTACTACACTAATCATTATAATTATTAATCCATGTTATTATTATATTAATAATTTTAATTCAAGTTCTTACTATCTTGTTTTTTTGTTTTACTATTTTGTAAAAATATTTATGTTTTATGTTTTACTATTTTTTGAATAATATTTTGTAAGATTATTATACCAATAATTATAATTATTTAATTCATGTTATTACTATCTTAATCATTTTAATTCATGTTACTACTATATTGGTTGTTTGTTTTAAGTTTTATGTTTTACTATTATGTAACACTATGATACTAATCATTATCATTATTTAATTCATGTTATTACTATATTTATAATTTTAATTCATGTTATAACTATATTGCGTGTTTGTTTTTTGTTTTATGTTTTACTATTTTGTAACACTATTTGGTAATGCTACAAAATACTAAAACATAAAACATAAAACAAACAAAC
# Right flank :  TAGTGTTACAAAATAGTAAAACATAAAAAATAAAAAGAAACAAACAATACAGTAATAACAATTATTCAATAATAATAATGATTAGTGTAATAGTATTACAAAATAGTAAAACGTAAAACATAAAACAAACAAACGTTATAGAAATAACATGAATCAAAATTATTAATATAGTAATAAGATGAATTAAAACTATTAATATAGTAGTAACAAGAATTAAATAATGATAAAGATTAATATAATAGTGTTACAAAATAGTAAAACATAAAACAAATAAAGTGTATAGTAATAACATGAATTAAATAATTCTAATGATTAGTATAATAGTATTACACAATATTGTTACAAAATAGGAAAACATAAAATGTAAAACAAACAAACAAACAATATAGTAATAACATGAATTAAATAATGATAATGATAAGTGTAATAGTGTTACAACGTAGTAAAACATTAAAAAAAAACTATATAGTAATAACATGAATTTAATAATGTTAATGATT

# Questionable array : YES	 Score: 0.22
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.80, 5:0, 6:-1, 7:0.22, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATATAGTAATAACATGAATTGAAATTATTAATATAGCAATAATATGAATTAAATAATGATAACGATTAGTATA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         R Score: 4.5/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:84.93%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.50,-3.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-6] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [81.7-76.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,4.87   Confidence: HIGH] 

# Array family : NA
//

>k141_69958 flag=1 multi=4.0000 len=1442		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence     	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	====================	==========================================	==================
        11	    20	  95.0	    42	....T...............	GAGTTGTATGGTAGTGGGAAGCATGCACTGTGTAGTAAAATG	
        73	    20	 100.0	    20	....................	CCACATTATGAGTGACAATA                      	
       113	    19	  80.0	     0	....-.......C.G....T	|                                         	
==========	======	======	======	====================	==========================================	==================
         3	    20	  91.7	    32	TTACCTTGAACCATTAGTTG	                                          	       

# Left flank :   TAGTAAACGAT
# Right flank :  TCGATTGTATGAATATATGATTGTTTGCGTTTGTTTTCCTATCTTGTTATTAATTTAGGTGATAATAAATAATGATTAGTTTTAAGAGGAGCATGTTTAGTTGGAATTGTATGAATATATGATTGTTTGCGCTTTAAAAAGCAGGTGGTTGTTTTATCTTATTATTAATGTAGGTGATAATAAATAGTGATTAGTTTTAAGAGGAGTATGTTTAGGATTATAGTTTTTTAATTATTATTATATTTTTTAATATTATAAAATTATTGATGGGTGGATTAGTGAAGGGTGGTGGGAAGCAAGGTGCTTTTTTGGGAGAGATATTGGTGGTTTACATTCTGGCTTCGGTGAAGGTAGGGCTTGGATTTCAGAGAGGAGGCGATGGTGGTTCAAATTTGAGCTCGCCTAGTGAGTGGGTGGTCACTCTAGCTTTCTTGTTGGTAGTGATTTTTGTGGTATCGTAGGCCCCTTCAAGATGAGGCCATCCTCGTGGCATGGTCGAT

# Questionable array : YES	 Score: 0.55
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.59, 5:0, 6:-0.75, 7:-0.69, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTACCTTGAACCATTAGTTG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:65.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [1-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [11.7-71.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_9203 flag=0 multi=68.8100 len=3756		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                        	Spacer_Sequence                	Insertion/Deletion
==========	======	======	======	=======================================	===============================	==================
       139	    39	 100.0	    31	.......................................	CATCATGCTGCACTTCCTCATCACTATGCTA	
       209	    39	  94.9	    31	...........................A.G.........	CATCATGCTGCACTTCCTCATCACTATGCTG	
       279	    39	  94.9	    24	.............................GT........	ACTGTCACTGTGCACTATGCTGCA       	
       342	    39	  87.2	     0	.CAT...T......................T........	|                              	
==========	======	======	======	=======================================	===============================	==================
         4	    39	  94.2	    29	CATCTCAACATGCTGCACTAATCATCACTTCGCTGCACC	                               	       

# Left flank :   GTGTATTGATCTGTGATCTGTTTGTGAGCGGTGTCCTTTGGTGTTCTGTCTGAGGAACTTACCCTGTAAATTCCTACAACAACCTCAACAATACTACATGATCTCGTGACTACAATGCGAACCTTTATAATTGGATGGC
# Right flank :  CATTGTCACTGTGCAATATGATGCACCATTCATCATGCTGCACTATGCTGCAGCATACTCCACCATGATTTGCCCATCATGTTATCACCTATCACATCATTATGATGCACCATTGTCACTGTGCTGCACTATAGGACTGCTTATGAACACAACTCCTAAAAAGGTCTCCACTGCTGGCTATGGCCTGTGGCCTAGCCCAGCAATGAACTGCAATTAGGTCTCTCCATACAATGATGCTTCCCATACAGAGGAGCAATGTGAGTTGATGTCATATCTAAAGAAAACCAAAGCATCTTGATGCCAACCATATTATAGAGAAACAAAGTGTATCGATCAAGAGAAACAAAGAATACATACTGTGGATCGTGATACATCAAGCTGTTTGCCACGTCTACTATATCATGTCACTGGGGAGATTACCCTGTCATGGATACTATGTTTGCCATGCCATATAATATATATACTGTTTGCGATACCAAAATCCTGTTAGCCATACCATG

# Questionable array : NO	 Score: 1.90
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.71, 5:0, 6:0.25, 7:-0.15, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATCTCAACATGCTGCACTAATCATCACTTCGCTGCACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.28%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-1.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [2-7] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [61.7-55.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_12728 flag=0 multi=26.5350 len=7855		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                        	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	=======================================================================	======================================	==================
       620	    71	  94.4	    38	........G.........................................A..G.............G...	AATCTCCCTTCCAATGGGTGCTAGTAAACGATATCAAG	
       729	    71	 100.0	     0	.......................................................................	|                                     	
==========	======	======	======	=======================================================================	======================================	==================
         2	    71	  97.2	    39	GGGTTGGACCCTTTGGTAAGGGAGTTGAAGCTTCCTTCCAGTGGTATATGGTAAATTAGATCAAGGGATAG	                                      	       

# Left flank :   GTAATTCAGTGGTTCAGGATGTAACTCAATGCGTGTTTCAGAATGAAAGCGATTCAGTTTCAGGGTTCAATGTTTTAAGATCTATACGAGTGTATGCTTCAAGGAAGTCGCATATGCACATGGATTTGCTAAATTAAAGTAGTGTATGTATTCTGACATTTTATTGATTAGGTTTTTCACTATCAATTTAGTGAGTTGATGTAGTATTTGGGTGTGGTATTGGTATTATAGTTACAATGGTCTAACTATTTTGGTTGTTTGAGAATAAGTTGATTGATTTGGGTCTCAGTTTGTTCAGTGATTAATTGATTCATCTAGTTCAATGATTGAGTGATTTGTTGCAATGATCAATTTCAGTTTTGATATTTGGTATTGGGCTTAAAATGATGGTGGTGGGTCCATTCTGAAAATTTGATGAAAAGTGGCCTACAAGTTGTGATGTTTTGTATCAGGTGGATCCAATGTGAAAATTTATGAAATGTAGTCCATGGTGGTGCTGA
# Right flank :  GGAGCTCCCTTCCGGTGGGTGCCAATAAACAATATCAAGGGATTGGAGCTCACTTCCAGTGGGTGCTAGAGAATGAGAGAAGTTTGGAGCCTTCTGCAGGTGGTTTCCTACGTGTTGGTATGAGTCTTTTCCAGTAGAATATTTTAAGATTATTTCTTTATAATAAAAGATAAATATAGAGACAACAACGTCGGTGGTTTTTTCCCACATTGGGTTTCCACCTAAAAATCCTAGTGTTCATGTGTGTTGATGTGTTTCTTATGTATGATGGTTTATTCCATTCTTACTGCACACGCTTACTATTTAATTAATTCGTAATGAAGTATTGATACAGTGGTTCAAGTTAAACAGATGATGGACTAACTAAAAAGTTGGTTAACTAGATATGTGCTAGTTGTTTGCATAAAAATCACATACATACACATGTTTAATTCATTATAGTCACGTATATAAATATGGCTAAGAAAAGAAAAGTTTTAGAGAGTTTTTAAAATATTGAT

# Questionable array : YES	 Score: 0.52
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:-1, 7:0.46, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGGTTGGACCCTTTGGTAAGGGAGTTGAAGCTTCCTTCCAGTGGTATATGGTAAATTAGATCAAGGGATAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:54.93%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-16.50,-9.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [4-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [61.7-48.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.64,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_18416 flag=1 multi=8.0000 len=1453		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence      	Spacer_Sequence                                          	Insertion/Deletion
==========	======	======	======	=====================	=========================================================	==================
       644	    21	 100.0	    18	.....................	CTTTGCCACCTGCCCTGA                                       	
       683	    21	  95.2	    57	.T...................	TCAGGGATCTTTTATTTGTCTCTCTTTTTTTTTCTTTTTTTTTTAAAGACAATAGGG	
       761	    21	  95.2	    18	........A............	CCTTGCCACCTGCCCTGA                                       	
       800	    21	  90.5	    45	........A..........G.	TCAAGGATCTTTTATTTCTGCTCTTTTTTTTGAAAGACAATAGGA            	
       866	    21	  95.2	    18	........A............	TCCTACTACCTGCCTTGG                                       	
       905	    21	 100.0	     0	.....................	|                                                        	
==========	======	======	======	=====================	=========================================================	==================
         6	    21	  96.0	    31	GCGCCTCAGAGAGGTTTTCAC	                                                         	       

# Left flank :   TGCTCTTCTGCAGGTATCTTGGCTTGCATGGCGACAATGGGGGTGTGAGCCAATAATGGAGGTGTTCAGAGGTGCCATAAGTGGTATGCATGCGATTGAACCACGCTCCCATCGTTTAGATCTTCATTAAAAACTCACCTCGGCCATAGCAACCTTCTACGGGAAGTTTCCTGTAGTGATTCAGGACTTTCCTGTAGGGATTCAGGACTTTCCTGTAGGGATTTAGGACTTTCCTGTAGTGATTCAGGACTTTCCTGTAGTGATTCGGGACTTTCCTGTAGTGATTCAAATCACCAAAAAAACTTTGCACATACCTTGAAAATGTGGCTGTCCGTGGAGGAGGCATTGATGGCATTCTTTGTCATTATGACGACAGCCCCCGACTTGGAGTGGAGAGGCTTCTTCTCCTCCATAGAACATCTGCATGCAAACTCTCATGGTGCTTATTGTTTCTTTTTTTTTCTTTTTTTTTCTTTATTTTTTTTATACGGATGGCAGGG
# Right flank :  CCCAAGGATCTTTATTATTTTTTCTCTTTTTTTCTTTTTAGGGAGTGCCCAAGGTGGAGGCAGAACCCTTCTCTAAGACCAGGACAGATTTCTCCATAGTGCTGATAAATTCTTGTTTTGAGAGAGGTCTTCTATAGATCTTCAATCGGTGCCCATTGAAAAGCATGGGAATGGCTTCCTCATCAATGGTTCGAATGAGGACAGATCCATTGTCATGGCATTGTTCCACGACATATGGGCCTAGCCATCTGGTTCTCAACTTTCCCTTGAAATCTTTGAATCTGGAGTCATACAACAAGGCCCAATCTCCTGGATGGAATTGTTTGTCATGGAGATGTTGGTCATGCCACACCTTCCTCTGAAGTTGAATCACTTCTGAGTGTAGGAGGGCCTGCATGCGGTGCTCATCCAGTCCATTTAGCTGCAGTAACCTCTCTTGCTGTGCTTGGTCTTATCCACCTCAGTATCCTACTCAGCCCACTCCTCACTCTTATCATTCA

# Questionable array : YES	 Score: 0.83
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.80, 5:0, 6:-0.5, 7:-0.96, 8:0.8, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCGCCTCAGAGAGGTTTTCAC
# Alternate repeat :   GCGCCTCAAAGAGGTTTTCAC

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [1-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [75.0-61.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_17455 flag=0 multi=19.5615 len=2800		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	=============================	==========================================	==================
       141	    29	 100.0	    42	.............................	ATTTGAATACAAGATCTGCCTCTGCTTCTCAAATAATACAAA	
       212	    29	 100.0	     0	.............................	|                                         	
==========	======	======	======	=============================	==========================================	==================
         2	    29	 100.0	    52	CTCTGCCTCCACCTTTCAAAGAATACAAG	                                          	       

# Left flank :   AATATAATAAAACCATCCCATTTTAACTCCTGCAGCAAGAAAATTTAGTAGTGCTCTTGTCCTCACCACTTCTCATGATCCAAATGATAGCTCACCACCGTTCAGCTCTCCGAGGTGCCAAACTGTGTAAGTTAGCACACC
# Right flank :  GCACACCAGGCAGACAGACACACCAAACAGATGACAGCAACACAGACATAACAACATAGACCAGACCGCATTCATGCACACACAACATGGAGCTCCAGACATACATACATACATACAGGCACACGAGACTAGACCACACAATGCAGGAAACTCTTAGAGGAGTAGGAGAGGCACAACAATGCCAATGCCAATATGAATTCAAAGATTGAATCGAATTAAAGCAACCAAGGTTCTTGACCGTTGAGATAACTCGAACGGTAATTATCTCTCGAACCAAGATTCTTGACCATTGAGGTAACTTGTTTGATAAGCAGCGCTAATTATCTCTAGAACCCAGATTCTTGACCATTGAGGTAACCTGTTTGATAAGCAGCGCCAATTATCTCTCGATCGAACCAAGATACCTCACCAATGAGGTAACTTGTTTGATAAACAGCGGCAATTTTATCTCGAACTGAACTTTGCTTTCACAGACTGTCAAGTTTATTAAGAGCGGAGGG

# Questionable array : NO	 Score: 3.00
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:0.11, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTCTGCCTCCACCTTTCAAAGAATACAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.17%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,-0.60] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [50.0-53.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_19103 flag=0 multi=19.0719 len=1378		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                 	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	================================================	==========================================	==================
       206	    48	 100.0	    42	................................................	GTGTATGTATATATATGTCATGTGCCAATGTACTTCATAGAG	
       116	    48	 100.0	     0	................................................	|                                         	
==========	======	======	======	================================================	==========================================	==================
         2	    48	 100.0	    43	TTTTATGCCTAAGGGAAGTTGTCAGAAATGTCATGAGGAATGACAAGA	                                          	       

# Left flank :   ACAGAAATTTTGAAAAATTCCAAGCAAGAAAAATATGTCCATGTACCAGAGATATTGCTCTTCTAAGTAAATTTTAAAATATATTATCAATTTCATTTTCAACTTGTTTTCAGAAAATGCTTTTAGTGCCACTATGTTTACTTTTTTTAACTGTATACATCAATGGGTCATTCCACTGAAATATACCCCATATGAACAAGAGAAATTTATCTGATATACCAAAGGCTTCTATAGAAGATAAGATACAAGCTTAAGCACTCAACATTCTCAAACGTCATATGAATAGAAGGAAAAAGTAAATGTGTGAGAAAACCTCTCAGGCCAAAAAGCAGAAAAATAAAGGAAATAATTTACACATTTTCAATTTAATTTACATGTAGAAACTCCAGGAAGGAAATATAAATATAAATATATATATTTGAACAATCATATACATACATATGTATGTATGTGTATATGTATGTATATATATGTCATGTGCCAATATACTTCATAGAGTT
# Right flank :  AATAATAAAATGCATAATCCAGATGTCCAAGTCATCTACATCTCCATGTAGTCAAGATTCTTAAAAATT

# Questionable array : NO	 Score: 1.75
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-0.25, 7:0.11, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTTTATGCCTAAGGGAAGTTGTCAGAAATGTCATGAGGAATGACAAGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:62.50%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.20,-3.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [66.7-73.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_18416 flag=1 multi=8.0000 len=1453		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                                  	Spacer_Sequence                     	Insertion/Deletion
==========	======	======	======	=================================================================================	====================================	==================
       640	    81	 100.0	    36	.................................................................................	GTCTCTCTTTTTTTTTCTTTTTTTTTTAAAGACAAT	
       757	    81	  92.6	     0	............A.............C.................C......A..........G....A.............	|                                   	
==========	======	======	======	=================================================================================	====================================	==================
         2	    81	  96.3	    37	AGGGGCGCCTCAGAGAGGTTTTCACCTTTGCCACCTGCCCTGAGTGCCTCAGAGAGGTTTTCACTCAGGGATCTTTTATTT	                                    	       

# Left flank :   TTGTTGCTCTTCTGCAGGTATCTTGGCTTGCATGGCGACAATGGGGGTGTGAGCCAATAATGGAGGTGTTCAGAGGTGCCATAAGTGGTATGCATGCGATTGAACCACGCTCCCATCGTTTAGATCTTCATTAAAAACTCACCTCGGCCATAGCAACCTTCTACGGGAAGTTTCCTGTAGTGATTCAGGACTTTCCTGTAGGGATTCAGGACTTTCCTGTAGGGATTTAGGACTTTCCTGTAGTGATTCAGGACTTTCCTGTAGTGATTCGGGACTTTCCTGTAGTGATTCAAATCACCAAAAAAACTTTGCACATACCTTGAAAATGTGGCTGTCCGTGGAGGAGGCATTGATGGCATTCTTTGTCATTATGACGACAGCCCCCGACTTGGAGTGGAGAGGCTTCTTCTCCTCCATAGAACATCTGCATGCAAACTCTCATGGTGCTTATTGTTTCTTTTTTTTTCTTTTTTTTTCTTTATTTTTTTTATACGGATGGC
# Right flank :  TCTGCTCTTTTTTTTGAAAGACAATAGGAGCGCCTCAAAGAGGTTTTCACTCCTACTACCTGCCTTGGGCGCCTCAGAGAGGTTTTCACCCAAGGATCTTTATTATTTTTTCTCTTTTTTTCTTTTTAGGGAGTGCCCAAGGTGGAGGCAGAACCCTTCTCTAAGACCAGGACAGATTTCTCCATAGTGCTGATAAATTCTTGTTTTGAGAGAGGTCTTCTATAGATCTTCAATCGGTGCCCATTGAAAAGCATGGGAATGGCTTCCTCATCAATGGTTCGAATGAGGACAGATCCATTGTCATGGCATTGTTCCACGACATATGGGCCTAGCCATCTGGTTCTCAACTTTCCCTTGAAATCTTTGAATCTGGAGTCATACAACAAGGCCCAATCTCCTGGATGGAATTGTTTGTCATGGAGATGTTGGTCATGCCACACCTTCCTCTGAAGTTGAATCACTTCTGAGTGTAGGAGGGCCTGCATGCGGTGCTCATCCAG

# Questionable array : YES	 Score: 1.02
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.81, 5:0, 6:-1, 7:1.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGGGCGCCTCAGAGAGGTTTTCACCTTTGCCACCTGCCCTGAGTGCCTCAGAGAGGTTTTCACTCAGGGATCTTTTATTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [14,25] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-24.90,-24.90] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-6] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [76.7-58.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_17455 flag=0 multi=19.5615 len=2800		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence        	Spacer_Sequence                                	Insertion/Deletion
==========	======	======	======	=======================	===============================================	==================
      1302	    23	 100.0	    47	.......................	CCTTTTTTTTTTGGTTAAGGGAAGGGTTAGTTCCCAAGCAGGGCCGT	
      1372	    23	 100.0	     0	.......................	|                                              	
==========	======	======	======	=======================	===============================================	==================
         2	    23	 100.0	    48	AATTCGTGATGTGATTCGATACT	                                               	       

# Left flank :   GCAAAATGTGGAAAGATGCTCCAGCTTTTACAGCTAGACAAGAAAAGCAGAATCCATGTGATGTAATCAAAAACAGAAAGCTATAGCCAGATAGCGAACCAAAAATAGAATCCACTGCCTTTATTTAAAATAAAAATATGTAGATATAAAAGAAGGCTCAAAGTTCTTTTTCATGACATTTACATATTTTAGTTTAAGAATTAAATAATTGAATCGGATTAAAATATATATTTATGTGCTGAAAGTATAGGTGTTGTGTAGGGTGGGAAAATATATTTATGTGCTGAAAGTTCATCTACTCCAACGACGTCTTCCTACTCAGATCAGATCATATAATAAGATGCCGAGATCGAATACCCCGCCAACATGAAATCGGTTAAACATTTTTTCAGATCTTGCTTGCACAGCTGAGGAAAGAGGAAAGATTCGATCTCGTGTACAGTTAACCATGATTAAGGGTACAGTTAGTAAGGTGGTCCTACAAATCTGGTACAATATAC
# Right flank :  TTGCTCTCTCATAAGTTCACACAATTGTATAGGAAAGAAAGGACGAATTAAACGAAGATGGAAGATTATCTCCTCCGAAGATGAGATGAGTTCCAAACATTGCCTATGTTTTACATACTGGGCCACCATACAGCTAGTGTAGTTTCCTTGAAATCTATCGAAAATGGCAATGAAGGATAGGATAGTATTCAAGTAATTGAATAATTCTTGTGCTTCATAGAAAATAAGCATTGTCTCCTCATCTCATCTCGGGCTTTAGATTTCGATCCTAGGTTGGCTGGTTGAAATTCGTATCAAAATTCTATGAATTTTACACTAATATGGAGAGCTTCAAAATTTTACAAACGAGTTTATACTTGGTCTCGAAGTCTATCCTAAGGGAGACCTTCAAAATCTTATAAGTGATTTTTGTTGCACCCAACACAAATTTGAAAGTCAATGAAACTAAATCGAACCTGAGAAACTAATTATGAAAGTCAACGAGGACGGGAGCCCGGTTT

# Questionable array : NO	 Score: 2.17
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.03, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AATTCGTGATGTGATTCGATACT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:65.22%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [63.3-63.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_21663 flag=1 multi=3.0000 len=1589		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                                    	Insertion/Deletion
==========	======	======	======	===========================	===================================================	==================
      1209	    27	 100.0	    51	...........................	TTGTTATATTACATTGTGCAAAGTTGTTTTTGGTTGGGCATCTCTATTTTG	
      1287	    27	  92.6	     0	.................A......C..	|                                                  	
==========	======	======	======	===========================	===================================================	==================
         2	    27	  96.3	    52	TACTATGAATTGTTATTGATCTCATTG	                                                   	       

# Left flank :   TGGAGGAGACTTCAAAGGATAAACCTGAAGCATGGAAGACGTTTTGTTGCAAAAGAAACCCGTCAAGGTTAGATGTCCTACAAGGTTTCTTAAAGAACATTTCAAGGTATGCCTTAGACTATAAGCTTTGTTATAAAGTCCTATTTATATAAAAGGTAAGATCATTCTCTTTTCAAATTTGCTTTACATTACAGACATTGTTATGTCTAGGGTTTTGAATCTCAAATATTCTTGTTAGCATGATTAATCATTAGATTGAATCTACCATCATTATATTATCATCTGATCATTCTTGTGGAACCCTTAAAGGTTCACTATAAAACCCTAACCTAAGGGTATGTCAATATTGATTATAAAATCCTAGTCTACTTAGGGTTGTTAATGAGATCTATAAATCCAATTCTTTTGATATTTTGGGTTTTGGTATTGTTATGGAAAAATCCTAGTCCTTTAGGTTTTCTTATTGAAGGATTAATAAGGAACATGTTTTGGGCCCTAAA
# Right flank :  CTATTATATTGCATTGTATAAAGTTGTTTTTGGTTGGGCATCTCTAGTGTGTTTGTGTTCCTAACTAGGGCATAGACTAGACCTAGCGTGCAAGGAAATTTTAACATTGTATCAGAGCTACTTTGAAGGGAAGCTAGATCAAAGTGTGCATAGGTTTCAGCTCTAAAAGTGAGTTGTGATAGTTTTGTAGAAAGCAATATTATTTCCGAGTGAACTGGCTAGAGATGGAGCAAAAGGTTTAGTAGAACTAGTTTGGTTATGAGATAGGGTGGGAGT

# Questionable array : YES	 Score: 0.99
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.81, 5:0, 6:0.28, 7:-0.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TACTATGAATTGTTATTGATCTCATTG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:74.07%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [65.0-66.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_22156 flag=1 multi=6.0000 len=1585		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence            	Insertion/Deletion
==========	======	======	======	========================	===========================	==================
      1201	    24	 100.0	    27	........................	GCATAGGTTGACGGTTGACACCTCTGG	
      1252	    24	 100.0	     0	........................	|                          	
==========	======	======	======	========================	===========================	==================
         2	    24	 100.0	    37	TTTGGTGTTAAAAAAATTGGCTTT	                           	       

# Left flank :   TTAAAGTAATTAAACTAGAACAGTCCACCACAATGGCTCTTGAATTTTTAGTATTAACAGTTTGAGACATTAAAATAGATGTCGGTAGTATTACTCCAGAGAAAGAAATATTTGTTTACTCCCACTAGCGGTTGAAATATTATGGCATAGCATTGAATAAGTATTATTTTCACAGCTTCCTAGCTTAGAATATGTATATCTGAGTATAATTATTAATGACAGTGGACTAATGCAGCTTATCAAGTTTCCTGGAAAAGTTATGTTCAGTTTTTCAGTAACAATAGTTAATGGGATAACCTGGATTTTCTCAGTTGGGTCTGCAGAAGTGTAAAAATTCTGGTAAAAAAACACATGATGATTACTGTTGCGTAGTTTCCCAAAGATTTTCATGTTAACTGGAGGGTTTTCATGTTAACTGCAATTCAGTAATTAGTGGACAATGACTCATGATCGAAATGAACCTTGTGCAATTCTGCGCGGATTATTCCTGTAAAGTAAAT
# Right flank :  CGATAGGTTGACCGCATTCTTGCTTGCCTTTCCGCCATGGGAATCTTGAAGGCGATCAAGCCAACAAAAGATGCCCAGGAAATTAACGGACCAGCCGAGGCCATGACCATGAAATACACGCTTACCAATCTCACCAAAACCTACTTCGTCTCTCAAGACATTAGTCCCTTAAGCCTCGCTCCCCTGGTTCTCATGCCAACCCACGCTCTTCTGATGCAACCCTGGGACATTCACGAGCGAGCGCTCCATGTCGGCGATAATTTCAAGGACTTGTGGCCATGGCAAGGATTTTTGGTCTTATGCAGCAGCC

# Questionable array : NO	 Score: 2.04
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-0.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTTGGTGTTAAAAAAATTGGCTTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:75.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [61.7-48.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.27,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_2139 flag=0 multi=4.9914 len=1422		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                                                                                                                                                                      	Insertion/Deletion
==========	======	======	======	==============================	=====================================================================================================================================================================================	==================
        20	    30	  96.7	    53	.....T........................	GCTGCAAGTGAGAGGTTTCTAGTATCTTGCCAACCCATAGTATAGACTAACTT                                                                                                                                	
       103	    30	 100.0	    60	..............................	GAGGTTGTTGCAAGAGAGCTAGAGTGAGGTTCCTAGCCAACCGACGGTATAGACTCATTT                                                                                                                         	
       193	    28	  86.7	   181	.....C.....-.-..T.............	GAGGTTGTTGCGAAAGAGATAGAGTGAAGTTCCTGGGAAACCATGGTATAGACTCACTTAAGAGCAAGAGATAGATGGCTAGAGTGAGGTTGCTACAAGAGAGCTAGAGTGGGCCTAGAGTGAAGTTGTTACATGAGAGCTAGAGTGAGGTTCTTGGCCAACCCACAGTATAGACTCACTC	
       402	    28	  80.0	   109	....AC...T....-.-......T......	GAGGTTGTTGCAAGAGAGCTAGAGTGGACCTAGATTGAGGTTGTTGTAAGAGAGAGGTTGCTACCTAGCAGTTGTTGGTACTTGGCCAACCCATAGTATAGACTCACTT                                                                        	
       539	    30	 100.0	     0	..............................	|                                                                                                                                                                                    	
==========	======	======	======	==============================	=====================================================================================================================================================================================	==================
         5	    30	  92.7	   101	AAGAGAAAGAGAGAGAGAGAGGGCTAGAGT	                                                                                                                                                                                     	       

# Left flank :   CCACACTATAGACTCACTTA
# Right flank :  TTCTACAAGAGAGAGGTTGCTAGTACCTGGCCAACCCATAGTATAGACTCACTTAAGAGCAAGAGAGAGAGAGGGCTAGAGCGAGGTTGCTCCAAGAGAGCTGGAGTGGGCTTGGAGTGAGGTTGTTGCAAGAGAGAGGTTGCTTAGAGTGAGTTGGTTGCAAGAGAGGTAGAGTGAGGTTCCTCGCTAACCCATGGTATAGATTCACTTAAAGAGAAAGAGTGAGAGAGAAGCCTAAAGTGAGGTTGTTGGTACCTAGCTAACGCGTAGTATATACTCATTAGCGCAACGTAGTTACTCTATTCTGGAGCTACAATATGTAAGACTACGAGACTTAAACCTAAGTGGGAAGGATCCCAATACAAACTCAATTGACAAGTTGTTAAATAAATTTTATGTAAGCAAATATACCTACATTTTATTAACAGGGAAAAAATTTACATACTTTACATCCAAGAGTGCATTCTTTGCCTCTGTGTGAGGAAACCAGAGATGGAAAC

# Questionable array : YES	 Score: 0.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.64, 5:0, 6:0.99, 7:-3, 8:0.6, 9:0.92,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAGAGAAAGAGAGAGAGAGAGGGCTAGAGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [1-5] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [18.3-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_23693 flag=1 multi=3.0000 len=1164		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                             	Spacer_Sequence                                	Insertion/Deletion
==========	======	======	======	============================================================	===============================================	==================
        51	    60	 100.0	    47	............................................................	TTGCTTATTTAAATTAATAGCTCCATTCATATCGGTGGCCCAGTCTA	
       158	    60	  93.3	     0	...C...............A...C...................G................	|                                              	
==========	======	======	======	============================================================	===============================================	==================
         2	    60	  96.7	    48	TCCTTCAGCCGGGTCTAGCGGAATGACTACGTGGAGCGACTTCATTTCAAATATCATTGT	                                               	       

# Left flank :   ATGTAAAAATGAACCCATCTCCATTGTAGTCGTTCCGCTAGACCCGACGGT
# Right flank :  TCCGCTTCTTTAAATTAGTAGCTCCATTCATATCGGTGGCCCAGTCTATCCCTAACAAGGTAGGATAAGGTGTACTTTCATCCACGATTTCAATTACTTCAAAGTCTGTTCTAGTGCTCACGCCTTCGATATCTACATAATTCCCTGCAACCTTCCCATGGGCAAGACCTTCTGTTAGTTCACCATCCGTAATTGGATAGGAGACCATTATAGTGTAGGCCTACCCATGCGCTCCCAAGTTTGTTTAGGCAGTACATTTGCATCCAAACCCAAATCCAAAATGATTTGATCCATCTCATAGTCGCCAATTTGTGTCGTTAATCACATCTCTTGCCTCGTCCGTGTTGTGTGTTTTCCTAGTTTGTGGACCACTCTAGGTTCCTACATCCCTGTGCACCTATCTATTAATTCTTGCAAACCTCTAACCGCTCTATTGTCGCGCAACAATTTCATGCATGTATCTAAAAAGGTAGCTATCATCGAGGGATCCTTGTCCTGCC

# Questionable array : YES	 Score: 0.07
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.84, 5:0, 6:-1, 7:0.03, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCCTTCAGCCGGGTCTAGCGGAATGACTACGTGGAGCGACTTCATTTCAAATATCATTGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-6.90,-5.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [43.3-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.27   Confidence: MEDIUM] 

# Array family : NA
//

>k141_26026 flag=1 multi=5.0000 len=1295		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence          	Insertion/Deletion
==========	======	======	======	=============================	=========================	==================
       135	    29	 100.0	    25	.............................	ACCACCGAAGCCGCCCAAGCCGGTC	G [138]
       190	    29	  96.6	    19	..............T..............	GCCACCAGAGCGACCTACG      	
       238	    29	  96.6	    19	...........T.................	GCCACCAGAGCGGCCTACG      	
       286	    29	  96.6	    19	..G..........................	GCCGCCGGTGCGCCCCACG      	
       334	    29	  89.7	     0	...........T..T.........G....	|                        	CCA [360]
==========	======	======	======	=============================	=========================	==================
         5	    29	  95.9	    21	CCCTGCAAGCCCGACCCTTGCTACAGCAA	                         	       

# Left flank :   GAGCTTGCCCAGCACCGAGAAAGTGGGAGCGGGGCGCAGGGGGAAAGTCGACAGACACCGGCGTCTGGGACCCTCAGCCAAATCACGCAAAAAATGAAGGGCTGGTGCGGTGGGCAGCGACCGCCCAAGCCGGTC
# Right flank :  AGCCCGACCCTGTGTACTCACAGAAGAGCAACGACGATCTGGCACGCCAGCTGTTGAACAACTTCGATGCCTTCAAGGATCCGAAAACGCCGTGGTTCGTCACCACGCAGGGTCTTTCGGACATGGCCAACAAGCCGTTGACCGGTAACCCCGCCCAGGATCAGAACATCCGCCTGGCGCGTGAGCTGTTAAGGCGCCCGGAAGTGGTGAACGCATTGGATCGACACAGCTCCACCGGCGCGCTGGATGGCCTGATCGACCGGCAAAAAATTCAGATGACCCTCAGCAGCCAAAGTCCTTTGAAATACCAGGATGACTCGCGGTTGGCGCAAGAGATGCTCGAGCATTTCGAGGGATTGAAGGACCCGGCCAACCCGGGCTTCATCAGCATCGACAAGTTGCACGGGCTCGCCTCGTGGCCGACGAGCGATCCGGTTCATGGACGCCTGGCCTGGATTGCGCAGGAAGTCCTCAAGCGCTCCGAGGTGAAGGAAAACATG

# Questionable array : YES	 Score: 0.41
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.80, 5:-1.5, 6:1, 7:-1.00, 8:0.6, 9:0.51,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCCTGCAAGCCCGACCCTTGCTACAGCAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [7,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.90,-2.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [2-7] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [38.3-43.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_25955 flag=1 multi=5.4098 len=1349		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence       	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	======================	===================================	==================
       137	    22	 100.0	    35	......................	ATTGTCAAATCAAGGACTCAAACAAAATAACATCA	
       194	    22	  90.9	     0	..T......C............	|                                  	
==========	======	======	======	======================	===================================	==================
         2	    22	  95.5	    36	CAAATCCTGTTGATCCCTGATA	                                   	       

# Left flank :   ATATATATTTGTTCAGATTTCTAAACATATAAAGCAATAACAACATGAACTAAACACATTGGCACATAAATATGAAGAAGGCACTGATATATATGATGTCAAATAGAAATGATACATCAAACTTCAAAGGCGATACC
# Right flank :  AGTGATAATTGTCCAATCAAGGACTCAAACAAAATAACATCACATATATAAAACTTTAAAAGTATTTCATCATCATGAGTTATAAACTTATATGTAGCAAAAGTTGCAAAACAAATAAAAGTAGCAGCTATCTATCTGAGACCATTGGCCTCTCCCTAGCAGCTTTCTTTCTGTCTTGTTCAGCGATTGCTTCATGAGCTTTTTGTATAACCTCTGCACTAGAAACCATACAAGGCATTACATCTGCATCATATGCAGAGAGTCTAGCCAGATGGTATTTCAATCGTGTAACCACTCCTGACATCTTGGTCCCACACAAGTTGCATGATATATAATTCAAACCTACGGCACAAAAAAAACAAAAAATTGGCATTAAACCCCCCAACACAAAATAAAAAAATAATAACCACCTCATTCAAAAAAAATTACTACATTATTTGCACTTTTGCAATGTGAATGAAAAATGGCAATAAGGGGGTGCATAAGAAAGTTGAAAAAAA

# Questionable array : YES	 Score: 1.43
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.78, 5:0, 6:-0.25, 7:0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CAAATCCTGTTGATCCCTGATA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:59.09%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [66.7-75.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_27695 flag=0 multi=23.0619 len=3002		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                                        	Insertion/Deletion
==========	======	======	======	===========================	=======================================================	==================
       492	    27	 100.0	    55	...........................	GAGAACCAACCAAAAAAAAAACATTGAAAGTAACCTGTAATGACCATGTATGACC	
       410	    27	 100.0	     0	...........................	|                                                      	
==========	======	======	======	===========================	=======================================================	==================
         2	    27	 100.0	    56	AGGAAATGGCAGGGCTAGGAAATCCAT	                                                       	       

# Left flank :   ATAATAGGAACATAAATACAATGGACATATCAACCATCAAAATCCATGTATACTTCCGTACTTTTACACATGGAAATAAAGCGAATGAATTTTTCATTAATGAATGCAATGAACCAACTAGGTATGGCACTACCATGTTCCACCCAACCATATATTGGTTGAGCCAAGTCTCTCTTCAACCAGTCACCCCTGTGACATATTCGAGGCATGAATGTGGGATATCATAGAACATATCAATCAAAAACTCATCCAGGGCTTCGAGAGGACGTCCACTTCAACCCATCAACTCATCATCGGATTAGTCTCTATCTGAGCCATCCATGGCTCCAGAGCATGTGTCCAACCAAGGTTCGAAGACTACTAACTCAACCCACAATGTTTACTGGGCTCCAGAGAGTTTGTACAACCAAGGCTCCAGGACTAGTAACTTAACCCACCAAGGGCTCCAGAGCATTTGTCCAACCAAGGCTACCAGACTAGTAACTCAACCCAAGTTGATA
# Right flank :  TTATTAATCCAGGAAACCCTCTATGTGAATAACATATCAAAACCAAGAATCCACATGTATATGTCATGCATGCAAAAATGAAGAAATAATACATGCAACTCAGTACATGTAAACAAATATCTAGTTACTGATCCCATTGACTTCTTACACTTTCCTTCTCTTCTTTTTCCTTCTTGCTCCAAGAATCCAACTCTGCCACGCCCTCCAAGAGTGCTCCAAGAAAACTCTAAGTATCCAAGAGTATGAGAGAGAGAGAATAATGAGAGAGAGCTCCAACACTGAACTAACAAAAGGTTCAAACTAACCAAGCTTGCTCACTAATCCTCAAAAGCCTTTTTATCAACGGGGCCTCTATTTTCTTCATTTTCCATTATTTTCCTTCAT

# Questionable array : YES	 Score: 1.47
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.28, 7:-0.7, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGAAATGGCAGGGCTAGGAAATCCAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.85%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-1.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [68.3-51.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_28932 flag=1 multi=6.0000 len=1465		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence         	Insertion/Deletion
==========	======	======	======	========================	========================	==================
        20	    24	  87.5	    24	.A......G..A............	CTCCTACTCTGGTCTGAATGTTTC	
        68	    24	  87.5	    23	.....T.G....T...........	CTGCTGCTGAGTTCTGAGTGTTT 	
       115	    24	  79.2	    24	GT......T..A.....T......	CTGCTACGTGGGTCTAAATGTTTA	
       163	    24	  95.8	    24	.......T................	CTGCTACGAGTTTCTGAATTATTC	
       211	    24	  87.5	    24	...G.T.................T	CTCCTGCTGAGGTCTGAATGTTTC	
       259	    24	 100.0	    24	........................	TTTCTGCTGGGTTCTGTGTGTTTC	
       307	    24	 100.0	    24	........................	CTGCTAAGAGATTCTCAATGTTTG	
       355	    24	  87.5	    24	...A.......T...........C	CTGCTACGAGTTTCTGAATTATTC	
       403	    24	  83.3	    24	..TG.T.T................	CTCCTGGTGAGGTCCGAATGTTTC	
       451	    24	  95.8	    24	...............C........	TTTCTGCTGGGTTCTGTGTTTTTC	
       499	    24	 100.0	    24	........................	CTGCTACAAGTGTCTGAATGATTG	
       547	    24	  83.3	    24	.A......G...T..........T	CCCCAGGTATGGTCGGAATGATTG	
       595	    24	  95.8	    24	............T...........	CTCCTACTCTGGTCTGAATATTTG	
       643	    24	  95.8	    24	........G...............	ATGCTACGAGGGTCTGAATGTTTG	
       691	    24	  79.2	    24	...AA..AG.......A.......	CCCCAGCGGTGTTATGAATGATTG	
       739	    24	 100.0	    24	........................	CTCCTGCTCTGGTCTGAATGTTTC	
       787	    24	  91.7	    24	.......G...............T	CTGCTATGAATGTCTGAATGATTG	
       835	    24	  91.7	    23	.A....................G.	CTCCTGCTTTGGTCTGAATGTTC 	
       883	    24	  91.7	    24	........T..........T....	CTACTGTTGGGATGTGAGTGTTTG	
       931	    24	  83.3	    24	....A...G..T..A.........	CTGCTATGTGGATCTAAATGTTTG	
       979	    24	  91.7	    24	.........A......G.......	CTCCTGCTGTGGTCTGAATGTGTG	
      1027	    24	  91.7	    24	...A.......A............	CTTCTGCTGTTGACTGAATATTTG	
      1075	    24	  95.8	    24	........T...............	TTCGTGCTCTGGTCTAAATGTTTG	
      1123	    24	  75.0	    24	.T.....T...TT...G....T..	CTCCTGTGGTCGTCTGAATGTTTG	
      1171	    24	  91.7	    24	...................C.G..	CTCCTGCTGTGGACTGAATGTATG	
      1219	    24	  83.3	    24	A.......T..A..........G.	CTGCTGTTGGGTTCTGAGTGTTTC	
      1267	    24	  91.7	    24	........T..C............	CTGTTACATGGGTCTGAAGGTTTT	
      1315	    24	  95.8	    24	...............G........	TTGCTACGAGGGTCTGAATATTTG	
      1363	    24	  87.5	    24	....A...G..............G	CCCCAGCTGTGGTCTGAATGACTG	
      1411	    24	  87.5	     0	.....G.....C.T..........	|                       	
==========	======	======	======	========================	========================	==================
        30	    24	  90.3	    24	TCCCTCACATAGGATTCCAGAACA	                        	       

# Left flank :   ACAAAGGTCTGAATGATTGT
# Right flank :  CTGCTGCTGGGTTCTGAGTGTTTCTCCCTCA

# Questionable array : NO	 Score: 1.52
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.51, 5:0, 6:0.25, 7:-0.42, 8:1, 9:0.18,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCCCTCACATAGGATTCCAGAACA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:54.17%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [23-30] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [20.0-23.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_32391 flag=1 multi=5.0000 len=1355		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                    	Spacer_Sequence              	Insertion/Deletion
==========	======	======	======	===================================	=============================	==================
      1084	    34	  88.6	    29	..T........G.......-............C..	ACACTCTTTATGCCCCAGAATAAATTTTC	T [1072]
      1020	    35	  97.1	    29	.................................C.	ACACTCTATATGCCCCGAAATAACTTTGC	
       956	    35	 100.0	    22	...................................	GTTATTTGGAGCCCTTGTTGGG       	
       899	    35	  80.0	     0	...........G....A..G..T.G.T...A....	|                            	
==========	======	======	======	===================================	=============================	==================
         4	    35	  91.4	    27	TTGAGTTACTAATCCTGGAATCCTTGGTTGGAAAA	                             	       

# Left flank :   TTTCTCTCTCATACTCTTTCACACTTAGAGTATTCTTGGAGTATTCTTGGAGCACTCTATTTTTCATTGGATTTATAACAGAAGGCTACAGCTACAACAGGATTGCTTTGCAGGTATTGAGTTGCATGTATGACCTCCTTGCTAGACTTGGCATGCATGGTATACCCATGTGGTTATTTGGTTATGATCTGTCATTGGCATGGAGGATTTTATGGATCAGTTGTAGTTTTTCCTAGCCTTTCCATTTCCTAGATAGCCATGGTGTTATTG
# Right flank :  CACTCTGGAACCCTATGGAATTTGTGGCTTGGGATAATAGTCTCGGAGCCAGGTTAGAAAAAATGCTTTAGAGCCCTAAACAACTTGGGTAGAGACCGATGTGGTTGGTGTTAGTGATGTCCGACCATGAGGTCAAATCATTGGTTGAGGGTTGAGGTGGATTGGCCTCTAGAATCCCTGGTTTGGTTTTGGTTGGTTTATTCCATGATCTCCCACATACATGCTTGGAATATGGAACGAGGGTGATTCATTGAAAGAGAGATTTGGTTCAGCCCAATCTATGGTCAAGTGGTACATGGAAGTCCCTCGCCTGGTTGGAAGTTTCATATATTAATAATAGGTATTTTCTACTATGATCTATGAGCATATTCATTGATTATGATGGTGATATTATGGTCATGTTTCTACAGTTTATATGTCGATTATTTTTTGGTTATGTGATGTATATTTTTTATGGATATTAACATGTTTGATAAATATATGAATGCTTGGTGAATGCT

# Questionable array : YES	 Score: 0.29
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.57, 5:-1.5, 6:0.39, 7:-0.26, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTGAGTTACTAATCCTGGAATCCTTGGTTGGAAAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:62.86%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.40,-2.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [7-6] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [53.3-60.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_34296 flag=1 multi=20.8109 len=1590		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                         	Spacer_Sequence              	Insertion/Deletion
==========	======	======	======	========================================	=============================	==================
       117	    40	  92.5	    28	...A...............A...........G........	GCTCCTTATAGCCAGGGCCTACAGAAGA 	
       185	    40	  85.0	    28	...A...G.G...T.............T..C.........	TATCCCTGTAGCCATATCCTTGACAAAA 	
       253	    40	  85.0	    29	..GA............A........G...T.G........	TGTCTCCAGTAGGCAGAGCCTAGACAAGA	
       322	    40	  85.0	    29	.G.A..........T.......G.A.....C.........	TGCCGCCTGTAAGCAGATCCCAGACAAGA	
       391	    40	  92.5	    29	...G........C.......................T...	TGCCTCCTGTCAGCGAAGCCTATAAAAGA	
       460	    40	  82.5	    28	...A....TT..C..............T.......T.A..	TGCCAATGTAGGCAGAGCCTACACAAGT 	
       528	    40	  90.0	    28	...A.....T.......................CC.....	TACCCCTGTAAGCAGAGCGTAGACAAAA 	
       596	    40	  90.0	    29	...AG...........................G....T..	ATCCCTCTGTAGGCAAAGCTTAGACAAGT	
       665	    40	  85.0	    28	T..A........CA.C....................C...	TGTCCCTCTAGGCAGAGCTTAGACAAGT 	
       733	    40	  82.5	    28	..CA..A.T......A..........C.....A.......	TGCCCCCGTAGGCAGAGCCTAGACAAGA 	
       801	    39	  80.0	    29	...A...A...-A.......C........TG.....G...	CACCCTCTGTAGGCAGAGACTAGAAAAGA	A [815]
       870	    40	 100.0	    28	........................................	AGCCCCTGTAGGCAGAGCATAGAGAAGA 	
       938	    40	  95.0	    29	...G..................................T.	TGTCCCCTGTAGGCAAAGCCTAAGCAAGT	
      1007	    40	  80.0	    28	...A........TT..C......T...G.......G.A..	CACTCCTGTAGGCAGAGCCTAGACAAGA 	
      1075	    40	  85.0	    29	...A........A.T..T...........G...T......	TACCCCCTGTAGGGAGATCCTAGACAAGA	
      1144	    39	  87.5	    29	...GA.......-..............T.....T......	TGCCCCCTTTGGGCAGAGGGTAGACAAGG	
      1212	    40	  90.0	    27	...A........A..................T.....A..	TTCCCTGTAGGCAGTTCTTATAAAAGT  	
      1279	    40	  85.0	    26	...A........AA.....A....TG..............	AGCTCTTGTAGGCAGAACTTTGATGA   	
      1345	    40	  95.0	    29	...A......................A.............	AGCCCCCTGTAGGCAAAGCCTAGGCAATA	
      1414	    40	  82.5	    28	...A......T.............GC.....C.C.....G	TTCCCCTGTAGGCAGAACTAATACAACA 	
      1482	    40	  90.0	    29	...A............A....T.....C............	AGCACCTTGTAGGCAGATCCTAGAGAAGA	
      1551	    40	  92.5	     0	...AT............................T......	|                            	
==========	======	======	======	========================================	=============================	==================
        22	    40	  87.8	    28	GTTTCATCACCTGGGTGATCAGTGCAGAGATATGTCACAA	                             	       

# Left flank :   ACTGGAGACATTGTGACACAACTCCGCACTGATTACCCAGGTGATGTCACTTTTGTCAAGGATATGGCTACAGGGATATTGTGACAATGCCCCCTTTTGGCAGAGCCTAGAAAAGAG
# Right flank :  |

# Questionable array : YES	 Score: 0.26
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.39, 5:-1.5, 6:0.25, 7:0.02, 8:1, 9:0.10,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTTTCATCACCTGGGTGATCAGTGCAGAGATATGTCACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-3.90,-4.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [35-46] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [53.3-1.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_37472 flag=1 multi=11.0000 len=1322		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                           	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	==========================================================	======================================	==================
       966	    58	 100.0	    38	..........................................................	CGAAGGCCAAGGCATGTACATAGGCAAATACACTATTG	
      1062	    58	  96.6	     0	..........................T...........................G...	|                                     	
==========	======	======	======	==========================================================	======================================	==================
         2	    58	  98.3	    39	GAGGTGCAAACGGAGGCCAAGGCATGCACATAGGAAAATACACTATCGGAGGCGCAAA	                                      	       

# Left flank :   TTCCACAAATTTATTCAATTCATCCTAGGGGCTTTTAGCCTCCTCGTTGCCTCACGAATTCTTCAATTTATCCTGGGTTTTCGACCTCTTTTTTCCACATAATTTCTTCAAGTTTTCCTAGGATTTTTAAACTCCTTTTTTCCCGCAATTTTTGCTGGACACAGATTGTCATTATGGAAATATTCGAGGAGAAAAGAGAGCTATCGGACGAGCCAAGAGGAGCAGATAGAGCCACATATGTATATGTCATATATATATGCCAAAGAAGAGGAGCACACAGAGCAGACGAAGGAAACATAGAAACAGAGCCATTGGAGAGCTTGTATGCATGCTTTAACACTATCAACGGAGAGAAAGCCCATATATATAGGCTTAACACTATCGGTGGGAAAACTATGCATCAATGGCACATTACACTATTAGGAGCGACGATATGCCTCAAACTCATGTATCTACCCATCGACAGTAAAAACCATGCATATATGTGTTTAACACTATCA
# Right flank :  GCGAACATACAATTTTATGAACGGTAGCATTGAGTTGTATCGGGTCCGGTACATAATATTATATCACATCGGCACTACGGGAGCGGCACTACAGTGTGTCAAGTGGACCATTGCGATCACCGATAGTGATTATCGAATCGGTAGTCAGAGCAGAGGTAATATAGTGTTAAGCGGTCCTTTCTTTATGATCATTGTTATATATA

# Questionable array : YES	 Score: 0.57
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:-1, 7:0.46, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAGGTGCAAACGGAGGCCAAGGCATGCACATAGGAAAATACACTATCGGAGGCGCAAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [22,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-3.00,-8.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [66.7-61.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_34296 flag=1 multi=20.8109 len=1590		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                     	Spacer_Sequence                  	Insertion/Deletion
==========	======	======	======	====================================	=================================	==================
       121	    36	  94.4	    32	...............A...........G........	GCTCCTTATAGCCAGGGCCTACAGAAGAGTTA 	
       189	    36	  86.1	    32	...G.G...T.............T..C.........	TATCCCTGTAGCCATATCCTTGACAAAAGTGA 	
       257	    36	  88.9	    33	............A........G...T.G........	TGTCTCCAGTAGGCAGAGCCTAGACAAGAGGTA	
       326	    36	  88.9	    33	..........T.......G.A.....C.........	TGCCGCCTGTAAGCAGATCCCAGACAAGAGTTG	
       395	    36	  94.4	    33	........C.......................T...	TGCCTCCTGTCAGCGAAGCCTATAAAAGAGTTA	
       464	    36	  83.3	    32	....TT..C..............T.......T.A..	TGCCAATGTAGGCAGAGCCTACACAAGTGTTA 	
       532	    36	  91.7	    32	.....T.......................CC.....	TACCCCTGTAAGCAGAGCGTAGACAAAAGTTA 	
       600	    36	  91.7	    33	G...........................G....T..	ATCCCTCTGTAGGCAAAGCTTAGACAAGTTTTA	
       669	    36	  88.9	    32	........CA.C....................C...	TGTCCCTCTAGGCAGAGCTTAGACAAGTGTCA 	
       737	    36	  86.1	    32	..A.T......A..........C.....A.......	TGCCCCCGTAGGCAGAGCCTAGACAAGAGTTA 	
       805	    36	  80.6	    33	...A...A.A......C........TG.....G...	CACCCTCTGTAGGCAGAGACTAGAAAAGAGTTT	
       874	    36	 100.0	    32	....................................	AGCCCCTGTAGGCAGAGCATAGAGAAGAGTTG 	
       942	    36	  97.2	    33	..................................T.	TGTCCCCTGTAGGCAAAGCCTAAGCAAGTGTTA	
      1011	    36	  80.6	    32	........TT..C......T...G.......G.A..	CACTCCTGTAGGCAGAGCCTAGACAAGAGTTA 	
      1079	    36	  86.1	    33	........A.T..T...........G...T......	TACCCCCTGTAGGGAGATCCTAGACAAGAGTTG	
      1148	    35	  88.9	    33	A.........-............T.....T......	TGCCCCCTTTGGGCAGAGGGTAGACAAGGGTTA	
      1216	    36	  91.7	    31	........A..................T.....A..	TTCCCTGTAGGCAGTTCTTATAAAAGTGTTA  	
      1283	    36	  86.1	    30	........AA.....A....TG..............	AGCTCTTGTAGGCAGAACTTTGATGAGTTA   	
      1349	    36	  97.2	    33	......................A.............	AGCCCCCTGTAGGCAAAGCCTAGGCAATAGTTA	
      1418	    36	  83.3	    32	......T.............GC.....C.C.....G	TTCCCCTGTAGGCAGAACTAATACAACAGTTA 	
      1486	    36	  91.7	    33	............A....T.....C............	AGCACCTTGTAGGCAGATCCTAGAGAAGAGTTA	
      1555	    36	  94.4	     0	T............................T......	|                                	
==========	======	======	======	====================================	=================================	==================
        22	    36	  89.6	    32	CATCACCTGGGTGATCAGTGCAGAGATATGTCACAA	                                 	       

# Left flank :   ACTGGAGACATTGTGACACAACTCCGCACTGATTACCCAGGTGATGTCACTTTTGTCAAGGATATGGCTACAGGGATATTGTGACAATGCCCCCTTTTGGCAGAGCCTAGAAAAGAGTTAC
# Right flank :  |

# Questionable array : NO	 Score: 1.76
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.48, 5:-1.5, 6:1, 7:0.68, 8:1, 9:0.10,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATCACCTGGGTGATCAGTGCAGAGATATGTCACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:52.78%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.40,-1.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [26-57] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [55.0-0.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.05,0   Confidence: HIGH] 

# Array family : NA
//

>k141_3567 flag=1 multi=4.0000 len=1035		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                       	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	======================================================================	=========================================	==================
       330	    70	  94.3	    41	............G......................................G..C.......G.......	CATAAAGAAAAGGAAGGCAAAAAAGAAGAAAAAGACAAGGT	
       441	    70	 100.0	     0	......................................................................	|                                        	
==========	======	======	======	======================================================================	=========================================	==================
         2	    70	  97.2	    42	AAACTGACATAAACATCGCGGGACTCCTGCTTAAAATGAATTCATCATTATTAGTATAGCACATCTAATC	                                         	       

# Left flank :   AATATTCTTCTTGTGTTCTGACGTTTTTAAAGACGTCCACTAACTCTTCGTTAATAATTTTTCTGATTCTTTTCTCAGTCCAGAAAATCATTTCTCTCTCCTGCTCTTAATCACTTCTCTGGTAATCTCAATAGCGTTCTTATTGTCTGACATATGGTCGAAGATAACCTTACAGGCAATAATATTTTTATATTCGTTGATATAGATTTTTTTTTCATCTTCGGTCAAAAGATTCTGGAGGACCATATGATCAAGCTTGGTATTTTCGCCTCTATAGAAATAATAGACGACAGAAACGATAGCTAAGAAAGTAAAGAAACAGAGAAAGAA
# Right flank :  CTCTAAAGAAAGAAGGCAATTGGCCCTGCGATCCATGTATAGCATCTTGGTATCGCTTCTCTAGATCCTCGGGGCTTGAACCGTCACGAGTCTTAGGTAAAGAAACAGCAAGATACCTAAATGCATCACTATTGTGAACTATAGCTCCATTAGAAAGACTAAAATGGCCTTGGCCCGGAACTGTGATATCCCAAACATCATATTTTTCTTGCAGCAATCTTACGTCTTCTATAATAGGTTGCTTTACAATTTGGGTGACAAAATCTTTGAGTGTTTCCCCCACGGATAAGCCCTTGATAGGAAGTACCGCAGAGCAAACATTTTCTTTCTTTTCGCTTCCATCGCTGTCCGAAAGCCATTTTGTAAGCGTGTCGTCTATGCCAGAGAATGCCTTCTGGAGATCTGTGCCATTCTGCAGCCTTTTCCAAGGTATGCCTCGAAGGCGGTCTATGATTTGGCCTAGAAAGCTTCGCATGTAGGCTCTTGGGTACACATTCGAG

# Questionable array : YES	 Score: 0.21
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:-1, 7:0.15, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAACTGACATAAACATCGCGGGACTCCTGCTTAAAATGAATTCATCATTATTAGTATAGCACATCTAATC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:65.71%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-6.70,-5.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [4-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [68.3-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.64,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_34296 flag=1 multi=20.8109 len=1590		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                     	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	====================================	==================================	==================
       121	    34	  88.9	    34	...............A...........G......--	AAGCTCCTTATAGCCAGGGCCTACAGAAGAGTTA	
       189	    36	  86.1	    32	...G.G...T.............T..C.........	TATCCCTGTAGCCATATCCTTGACAAAAGTGA  	
       257	    36	  88.9	    33	............A........G...T.G........	TGTCTCCAGTAGGCAGAGCCTAGACAAGAGGTA 	
       326	    36	  88.9	    33	..........T.......G.A.....C.........	TGCCGCCTGTAAGCAGATCCCAGACAAGAGTTG 	
       395	    36	  94.4	    33	........C.......................T...	TGCCTCCTGTCAGCGAAGCCTATAAAAGAGTTA 	
       464	    36	  83.3	    32	....TT..C..............T.......T.A..	TGCCAATGTAGGCAGAGCCTACACAAGTGTTA  	
       532	    36	  91.7	    32	.....T.......................CC.....	TACCCCTGTAAGCAGAGCGTAGACAAAAGTTA  	
       600	    36	  91.7	    33	G...........................G....T..	ATCCCTCTGTAGGCAAAGCTTAGACAAGTTTTA 	
       669	    36	  88.9	    32	........CA.C....................C...	TGTCCCTCTAGGCAGAGCTTAGACAAGTGTCA  	
       737	    36	  86.1	    32	..A.T......A..........C.....A.......	TGCCCCCGTAGGCAGAGCCTAGACAAGAGTTA  	
       805	    36	  80.6	    33	...A...A.A......C........TG.....G...	CACCCTCTGTAGGCAGAGACTAGAAAAGAGTTT 	
       874	    36	 100.0	    32	....................................	AGCCCCTGTAGGCAGAGCATAGAGAAGAGTTG  	
       942	    36	  97.2	    33	..................................T.	TGTCCCCTGTAGGCAAAGCCTAAGCAAGTGTTA 	
      1011	    36	  80.6	    32	........TT..C......T...G.......G.A..	CACTCCTGTAGGCAGAGCCTAGACAAGAGTTA  	
      1079	    36	  86.1	    33	........A.T..T...........G...T......	TACCCCCTGTAGGGAGATCCTAGACAAGAGTTG 	
      1148	    35	  88.9	    33	A.......-..............T.....T......	TGCCCCCTTTGGGCAGAGGGTAGACAAGGGTTA 	
      1216	    36	  91.7	    31	........A..................T.....A..	TTCCCTGTAGGCAGTTCTTATAAAAGTGTTA   	
      1283	    36	  86.1	    30	........AA.....A....TG..............	AGCTCTTGTAGGCAGAACTTTGATGAGTTA    	
      1349	    36	  97.2	    33	......................A.............	AGCCCCCTGTAGGCAAAGCCTAGGCAATAGTTA 	
      1418	    36	  83.3	    32	......T.............GC.....C.C.....G	TTCCCCTGTAGGCAGAACTAATACAACAGTTA  	
      1486	    36	  91.7	    33	............A....T.....C............	AGCACCTTGTAGGCAGATCCTAGAGAAGAGTTA 	
      1555	    35	  91.7	     0	T............................T.....-	|                                 	
==========	======	======	======	====================================	==================================	==================
        22	    36	  89.3	    32	CATCACCTGGGTGATCAGTGCAGAGATATGTCACAA	                                  	       

# Left flank :   ACTGGAGACATTGTGACACAACTCCGCACTGATTACCCAGGTGATGTCACTTTTGTCAAGGATATGGCTACAGGGATATTGTGACAATGCCCCCTTTTGGCAGAGCCTAGAAAAGAGTTAC
# Right flank :  A

# Questionable array : NO	 Score: 1.71
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.46, 5:-1.5, 6:1, 7:0.65, 8:1, 9:0.10,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATCACCTGGGTGATCAGTGCAGAGATATGTCACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:52.78%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.40,-1.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [27-37] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [55.0-1.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.05,0   Confidence: HIGH] 

# Array family : NA
//

>k141_34296 flag=1 multi=20.8109 len=1590		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                         	Spacer_Sequence              	Insertion/Deletion
==========	======	======	======	========================================	=============================	==================
       117	    40	  92.5	    28	...A...............A...........G........	GCTCCTTATAGCCAGGGCCTACAGAAGA 	
       185	    40	  85.0	    28	...A...G.G...T.............T..C.........	TATCCCTGTAGCCATATCCTTGACAAAA 	
       253	    40	  85.0	    29	..GA............A........G...T.G........	TGTCTCCAGTAGGCAGAGCCTAGACAAGA	
       322	    40	  85.0	    29	.G.A..........T.......G.A.....C.........	TGCCGCCTGTAAGCAGATCCCAGACAAGA	
       391	    40	  92.5	    29	...G........C.......................T...	TGCCTCCTGTCAGCGAAGCCTATAAAAGA	
       460	    40	  82.5	    28	...A....TT..C..............T.......T.A..	TGCCAATGTAGGCAGAGCCTACACAAGT 	
       528	    40	  90.0	    28	...A.....T.......................CC.....	TACCCCTGTAAGCAGAGCGTAGACAAAA 	
       596	    40	  90.0	    29	...AG...........................G....T..	ATCCCTCTGTAGGCAAAGCTTAGACAAGT	
       665	    40	  85.0	    28	T..A........CA.C....................C...	TGTCCCTCTAGGCAGAGCTTAGACAAGT 	
       733	    40	  82.5	    28	..CA..A.T......A..........C.....A.......	TGCCCCCGTAGGCAGAGCCTAGACAAGA 	
       801	    40	  80.0	    29	...A...A...A.A......C........TG.....G...	CACCCTCTGTAGGCAGAGACTAGAAAAGA	
       870	    40	 100.0	    28	........................................	AGCCCCTGTAGGCAGAGCATAGAGAAGA 	
       938	    40	  95.0	    29	...G..................................T.	TGTCCCCTGTAGGCAAAGCCTAAGCAAGT	
      1007	    40	  80.0	    28	...A........TT..C......T...G.......G.A..	CACTCCTGTAGGCAGAGCCTAGACAAGA 	
      1075	    40	  85.0	    29	...A........A.T..T...........G...T......	TACCCCCTGTAGGGAGATCCTAGACAAGA	
      1144	    39	  87.5	    29	...GA.......-..............T.....T......	TGCCCCCTTTGGGCAGAGGGTAGACAAGG	
      1212	    40	  90.0	    27	...A........A..................T.....A..	TTCCCTGTAGGCAGTTCTTATAAAAGT  	
      1279	    40	  85.0	    26	...A........AA.....A....TG..............	AGCTCTTGTAGGCAGAACTTTGATGA   	
      1345	    40	  95.0	    29	...A......................A.............	AGCCCCCTGTAGGCAAAGCCTAGGCAATA	
      1414	    40	  82.5	    28	...A......T.............GC.....C.C.....G	TTCCCCTGTAGGCAGAACTAATACAACA 	
      1482	    40	  90.0	    29	...A............A....T.....C............	AGCACCTTGTAGGCAGATCCTAGAGAAGA	
      1551	    40	  92.5	     0	...AT............................T......	|                            	
==========	======	======	======	========================================	=============================	==================
        22	    40	  87.8	    28	GTTTCATCACCTGGGTGATCAGTGCAGAGATATGTCACAA	                             	       

# Left flank :   ACTGGAGACATTGTGACACAACTCCGCACTGATTACCCAGGTGATGTCACTTTTGTCAAGGATATGGCTACAGGGATATTGTGACAATGCCCCCTTTTGGCAGAGCCTAGAAAAGAG
# Right flank :  |

# Questionable array : YES	 Score: 0.26
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.39, 5:-1.5, 6:0.25, 7:0.02, 8:1, 9:0.10,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTTTCATCACCTGGGTGATCAGTGCAGAGATATGTCACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-3.90,-4.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [35-46] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [53.3-1.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_472 flag=0 multi=335.3883 len=6728		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence        	Insertion/Deletion
==========	======	======	======	================================	=======================	==================
        33	    32	  87.5	    23	..CAA..........A................	GGGGTGATGAGGAGAGTTCTTGA	
        88	    32	 100.0	    22	................................	ATGTCGTGAGGATAGTTCGAGC 	
       142	    32	  96.9	    23	............................G...	GGAGTATGGAGCATAGTTCCTTG	
       197	    32	  87.5	    23	...........T...A........C...T...	GGCGTCTTGAGGAGAGTTCTTGA	A [215]
       253	    32	 100.0	    20	................................	ATGTCTTGAGGATAGTTCTT   	
       305	    31	  93.8	     0	..C...............-.............	|                      	A [307]
==========	======	======	======	================================	=======================	==================
         6	    32	  94.3	    22	TCTTGTGTCAACCTCGTTAAAAAATGAAACAA	                       	       

# Left flank :   AAATGAAGCAAGGAGTCTCGAGGATAGTTTGTT
# Right flank :  ACTCCCTTTTCCAAATCAAATCTTTGATTGAGATCTCGTAAAATCCCAGTCGAATCTTTCCTGGATATCGAAAGTGAACCTTCATTACATCCACCTAGTTGAATGCAATGAAGCTTGGGGGGCATGTGCCTCGATTGAATCATTATGCTGGGGGCATGAAAAACTTATTCAAGCATAGTTTTTCATTTCAAGCTTGTGTAAACATGAAACCGAATCCAAGCAAAGTTCTCCATGGTTCGAGCTTGTGTAAAAATGAACCCATGCGCCGATGTACCTTGTTGGCTTGGTGTGTAAGCGATTTCTTGCTTATTGTGTCCTTTAGCATCTTATGTAGATGTCTGAACCCCTCATATTCTAAGGTTGTCTTTATGGGTTCGTTGATTAGAAAGGAATTTTGATTTCATCACGTATCAACACATTCCTAGAAGATTAAATACATTGACTGATGCATTAGCGAACCATGTATTAGATAGAAATTTGCACAATTTATAAAAATCATA

# Questionable array : NO	 Score: 1.97
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.71, 5:0, 6:0.7, 7:-0.73, 8:0.6, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCTTGTGTCAACCTCGTTAAAAAATGAAACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:68.75%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-1.00,-0.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [4-8] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [31.7-63.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.27   Confidence: MEDIUM] 

# Array family : NA
//

>k141_45821 flag=1 multi=9.0000 len=1498		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence      	Spacer_Sequence              	Insertion/Deletion
==========	======	======	======	=====================	=============================	==================
      1161	    21	  85.7	    29	...A....AA...........	TCCTGCCTACATGTCGCAAGGGGTATCCC	
      1111	    21	 100.0	    21	.....................	CCCTGTACACAACAAATCACT        	
      1069	    21	 100.0	    21	.....................	ACCTTATGACTGTGTCTCGGC        	
      1028	    21	  85.7	    23	.A.A.....A...........	TCCTGCCTACATAGCAAACCACT      	
       984	    21	  95.2	     0	.........T...........	|                            	
==========	======	======	======	=====================	=============================	==================
         5	    21	  93.3	    24	AGGGGAAGGCTGAACCACTCA	                             	       

# Left flank :   TAATGTTAAAAATTTAAGTAAAAGGATGAAAATGAAAATTAAAGGAAATGTCACTTTTACTTTAAGAATTTAACATTACCATTAATTGCCAACATTGTTAGGGTTGAAACACCCTTAGTTTGCCCCCCAAACCCCTGAAACATAGTGAAAAATATGCTTTTTTAACTTTATGGTGGGCGTGCCTAAAACCGGCCGGTTTAGGACAAAAAACTGGCGAAAGGGCAGGTTAAACCGGGTTTTTAGGCCGGTTCTGACCGGCGGCCTTGCTAACCGGCCGGTCAGACCGGTAACCGGTTTTTGTGAGCGGTTTTTAAAACACTGGGTAGGATGTCTCGG
# Right flank :  CCCTATGGTTACTTATCGCTCCACGATGTACCCCCAAGCCCCCAGAGCTCCCAGAGCTCAATGTCCATATCATACAAGACCCTTAGGAGAGAGTCTCTCCATGACTCCTAGTCATTCTCGGAAAGTCATAAATTTGACCTTTCCCTAGGGCTTTGACAGAATGTCGGCAACCTGCACATCAATCTCAATGCGATGGAGCCTTCTTGCTCCTCGATATACCATATCCCAGATAAGGTGGTACATGATGTCTATATGCTTGGAGCGATCATAGAATACGAGATTCCTCAACAATCGAATCCCCCTCTGGTTGTCACAGAAGATCACGGTGGTATCCAGCATGTGTCCAAACAGCTCACTAAAGAGCTTCCTCAACCAGGCATTTTCACACGAGATCATACTCGTAGCTATGTATTCCGTCTCAGCGGTGCTCAAAGCAACTGTCTTCTACTTCCTGCTCATCCAGGATATCGTAGCAGAACCCAAAGAGAACCAGCACTCAG

# Questionable array : YES	 Score: 1.06
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.66, 5:0, 6:-0.5, 7:-0.62, 8:0.6, 9:0.92,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGGGAAGGCTGAACCACTCA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [2,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.40,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [4-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [41.7-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_46688 flag=1 multi=2.0000 len=1148		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                                       	Insertion/Deletion
==========	======	======	======	==========================	======================================================	==================
       187	    26	 100.0	    54	..........................	ACCCAAATTAGGTTACTAATTTGTAACCTAATTTGGTTACAAATTAGGTAACCC	
       267	    26	  92.3	     0	..A..A....................	|                                                     	
==========	======	======	======	==========================	======================================================	==================
         2	    26	  96.2	    55	AATTTGGGTTACTAATTCTACAGGTA	                                                      	       

# Left flank :   ACGTACCCAGGCAGACAATTCCCAAAGCTGCACATGCCCCCAAGCACCTTCTCCGAACACCCAAACATTTCCCACAGAAACAATGGATCCATTTTCTGTGGACAGTCATGGTAATTCTGCAAAACAGATACTGCTTTACATCTCTTTTAAGGATGGTTTATATTTCCAAATATTTATTATATCCTGA
# Right flank :  ATTTGTTTGCCAAATGATTGTTTTGTCCAGGGAGCATTTTCAATCAAACAGATTTCCAAAAGTTACCCCCAGGTTGATGTTCTGTCAGTTGTAGAAGATGAACAGTTCAGACCACAAACCAGCTCTATCCATCCTTAGAACTCAGGCTGTAGTGAGGATATTGGCATATGGCTGAAGTATAGCATCTAAGGAGTAAATATTATTTAGCACTAACCCTCTCAACTTTTTTGAGATTCTCAAAGCTGAATCCAAATATGCTCAGTGTTAGTTCTGGTGGGCCGTCTACTAGTAAAAGATGTCTCTTCCCTCTATAAAGCAGAATTCAGCATTTGCCAGCCAGAAAATAAGCTCACTAGCAGACAGACTGAGCACTTTGATGGTGTTGCTCAAGCTTATGCAAATGTTGTCTTTGTGTAGAACGGGTACAAAAATGCAACTTTTTCTGTGAGATAACATTTCAGAGGACTTAGCTTGTGCTATCTGTCTGTGTGCCAGTCTTG

# Questionable array : YES	 Score: 0.67
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.81, 5:0, 6:0.26, 7:-0.6, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AATTTGGGTTACTAATTCTACAGGTA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:69.23%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [73.3-66.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_48169 flag=1 multi=8.2547 len=2210		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence      	Spacer_Sequence                  	Insertion/Deletion
==========	======	======	======	=====================	=================================	==================
       616	    21	  76.2	    30	..T.TA......T.T......	ACGATAGGCTTTGAGGCTGACTATTCCATC   	
       667	    21	  81.0	    30	....TA..........AA...	ACGATTGATTCGCTGGCTAACAATTCCTTT   	
       718	    21	 100.0	    30	.....................	GTAATTGACTCCCTTTCAGAATATGCAGTG   	
       769	    21	  76.2	    30	..A.TA..........AA...	TCGATTGGCTACGAGGCTTACTATTCCTTT   	
       820	    21	 100.0	    33	.....................	GTAATTGACTCGCTTTCACAATATGCAGTGATA	
       874	    21	  61.9	    27	G.A.......T....AA.TCG	ATTGGCTACGAGGCTTACTATTCCTTT      	
       922	    21	  71.4	    30	.....A..G...T.T.C...T	AAGATTGATTCGCTGGCTGTATACTCCATC   	
       973	    21	  66.7	    30	..A.TA......A.G.AA...	TCGATTGGCTACGAGGCTGACTATTCCTTT   	
      1024	    21	  76.2	     0	.....A..G.....T.C...T	|                                	
==========	======	======	======	=====================	=================================	==================
         9	    21	  78.8	    30	ATCGGGGGCGGTGAAGGCAAC	                                 	       

# Left flank :   CGCAAAGTGCTTTTATCGGAGGGGGGAACTATAATGAGATAGGCGTATATGGGTATTATGCGTCTATTGTAGGTGGTGATCAAAATTCCATTGGCTACGATGGTGAATATAGTTTTATAGGAGGAGGAGGAACAAATAGAATAGGACAAAAGTCGTATTATTCCTTTATTGGAGGCGGTTACACTAATACGATTGGCTACAATGCTGATTTTTCCTTTATCGGAGGCGGTGCAGGTAACGTTATCGACTCTCTTGCAGAATACGCCATCATCATAGGAGGCGAGGCGGATTCCGTCAAAGCTTCCTACGGCTTCATCGGAGGCGGATATCAGAACCGAGTTGACTCGATCTACGGCATCGTGCTGGGAGGTCGAGGGAACATAGCATCTAATTACGGGGCTATGGTGCTGGGGGGAGGATTTGATGCAGAATGGGATGATGAAAATGGAGAAGTGGTTATAGAAGATTATCCGAATGTCAGCTATGGTGCGTTATCGGTG
# Right flank :  TAAGATTGATTCGCTGGCTCAATACGCTTCTATAATCGGTGGTTACGGTGCAATCGCAGACAAGACAGGGCAGACTACAACTAACTCTAATTCTTTTTCGGGTGAGTCAGATGGTTCTGCTCAATCATCAATTATTCAAGTTTCCGACACCGTTTTACACGATGCAAACGTATGGCACAACCTTTATACCAATGGAACAGAAAGTGATGATGGCGCTGAATACTGCACCATCTCCGCCAACTCTGTTTGGACATTTACCGTCAGGGTCGTCGGAACCACAGAAGCAAGTGCAAAGGCTTGGTCATACACCATTCAGGGCGCAATCAAGCGGGACGCATCAAATAACACCACTCTCTTAGGTTCAACCGTTTCGGTCGTTTATGAAGCGGACAGCGATTTTGACTGTCAGGTAATCGCCGACGACACCAACGAAAGTCTGAACGTACAAGTCAAGGACGCAACCTACGGAGGCGATATAGTCAGGTGGGTAGCAACGATTG

# Questionable array : YES	 Score: 1.13
# 	Score Detail : 1:0, 2:0, 3:0, 4:-0.06, 5:0, 6:-0.5, 7:0.28, 8:1, 9:0.41,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATCGGGGGCGGTGAAGGCAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [5,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [9-27] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [58.3-53.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0   Confidence: HIGH] 

# Array family : NA
//

>k141_48116 flag=0 multi=4.9822 len=1374		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	==============================	=======================================	==================
      1008	    30	  96.7	    39	...........C..................	GACTTGTATAACTGCGCATTAGTCACAGGCTCATTAGCA	
      1077	    30	 100.0	    39	..............................	AGTTTGAGTAGTTGCTCATTAGTCACAGGCTCGCTTGCC	
      1146	    30	  93.3	    39	..............G..........T....	GGTTTGAGTAGTTGCTCACTAGTCACAGGCTCGCTTGCC	
      1215	    30	  90.0	     0	..............G...........GT..	|                                      	
==========	======	======	======	==============================	=======================================	==================
         4	    30	  95.0	    39	GACTTACAAGGTAAACTAACATATTACCTC	                                       	       

# Left flank :   CGGCGGTAACAGTTACAGTGCTACCTAATAGCAGTGTAGCTTTTGCACTAGATACAGAGATTGCGATAACGCAATATGGAGCAGGTCAAGTTAGTGTAGTGGCAGGGTCTGGAGTAACGATTAGGAGTTCTGGCTCAGCCCTAAAAACTACCGAGCAGTACGCAAGTGCTACTCTGAAAAAAATTGCGACTGACGAATGGCTTCTGGTCGGCGCAATTACAACGTAGGGAGGGTTGATTATGCTAATGACTCAATTTGGAACTATTCCTGCATCACTAAAAACTTATATCACTTTCCCATGTAAAGCAGGATTATTTGATTTTTCAATAGTAAATGCAACTAATGTAAAATGGGTGTTTCCAGATGGTACGACATCAACCGCTAGTCGACCAGCTAAGACATTGGCTAGTGACGGGGTGGTGAAATTATATGCCACATTTAATAACATCACTGAGTTACGGGACAATAATACTGATAGTCGTTATTTGGGCAAACTAAAA
# Right flank :  ACTCTCCTTAAAATTAAAGCCCCTAGATATAGTGAGTCACTAGGGGCTTGTTTGATTAATTGTTACAATCACACGTTGTTTCCAAAAATGAAGAAGAAATTCGTAAATCCTTTAGTCCAACGACCAACTG

# Questionable array : NO	 Score: 1.78
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.75, 5:-1.5, 6:0.99, 7:0.45, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GACTTACAAGGTAAACTAACATATTACCTC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:66.67%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [1-5] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [66.7-63.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_49313 flag=0 multi=5.0363 len=1049		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                               	Insertion/Deletion
==========	======	======	======	==========================	==============================================	==================
       355	    26	  92.3	    46	...T...................G..	TTTATACCTTGAAGTGTAATCCATTGTATAACTTGGATTATATGTT	
       427	    26	 100.0	    22	..........................	CGTATAACTTTCACTTGAACTC                        	
       475	    26	 100.0	    22	..........................	CGTATAACTCGCGCTTGAACTC                        	
       523	    26	  73.1	     0	..C..A..A....CT..A.......A	|                                             	
==========	======	======	======	==========================	==============================================	==================
         4	    26	  91.3	    30	GGAGTGTATCTTGAAGTGTAATCCGG	                                              	       

# Left flank :   CCAAAACAAAATAAATCCTACAAAAAAAGCAAAATCCATTAAAACAACTTCTCAAAATCAAAACCAAGCAAACTCGCAACTTGTTTATTTCCTTTTCTTTTATTAGGAAGCAACTTGATAGTTTGAATCTTAGCTTTCTTTCCACCTTTTGAATAACCAATCGCTTTCAACCAATCTGTTTTAAGCGGAAGAATCACGTTTGAAATGTTTGTCGGTTTAAATGTTCGTGGCTTTTCTTGTGAAAATGCTTTGTTATATTGTTTTACAAACTTTGATTCGTTTTTTGATTCGTTTTGTTTTCTTGAAGGATTTTTTGGCGGATTGTAAGGCGGCAAATATTTTGGTTTATAGTTCG
# Right flank :  ACAAATAACTTGTATTAAAATAATAATTCTCTTTCCTACTCAGCGTATAATTCACGGGCTTTGTAAAAGAACTTGATTTAGTTGAAGGCGAATAATATCTTGCAGAAGCATAACCCAAAGTATTTGCAGGAGTAAATAACGGATCAACAACAACACTTATTTTTCCAGAATAAAAATCACGCAAATTAACGCCTTGCTTTTCAATAAAAACAGCTTCCTTCTTTTGAGCAACACTTGAAAATTTTTCTAAAGTTTTTTTAATAAAAGATTCACTTGCTTTTACGCCGGATTGAGTAGCTTTATTCGCCGCAACGTCAGAAACATAAAACCGAATCGGAAGCGACATTGTTCCGTGAGGAGTTTTAACACTCGCTTTCGCAGTGTAACCAAGCAATTTCTCGCCACTAGTACCATAAACAAATTTATTTTGACTTGAATAATACTTTAATTGAACGTCAATTTCACTCGTCAACCCTCTTCTCACCATTGGCGAAGGCATA

# Questionable array : NO	 Score: 1.64
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.56, 5:0, 6:0.26, 7:-0.58, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGAGTGTATCTTGAAGTGTAATCCGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.85%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [2-7] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [65.0-70.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_50475 flag=1 multi=6.0000 len=1273		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                    	Insertion/Deletion
==========	======	======	======	============================	===================================================	==================
       959	    28	 100.0	    51	............................	TGTGCATCTTTCCAAAAAGCAAGTGAAGCAATAAACAAGTATTTAGTTACA	
      1038	    28	  92.9	     0	.....G........A.............	|                                                  	
==========	======	======	======	============================	===================================================	==================
         2	    28	  96.5	    52	GAGGGTGTGTTACCCTAAATTAAGCAAC	                                                   	       

# Left flank :   CATGCATTACATATCCTATCACGTCATTGCTGAGGAAAGGTAAGAATTTTGAATGGATAGAAGAGTGTGAGGATAGTTTTGAGCACCTTAAGAAGTTGTTGACACGTGCACCAGTGCTAAAGATTGTGGATCTGTACAATGAGTTTGTAGTGTGTATATATGCTTGTAAGAGAGGACTTAGTGGAGTTCTCATGAAGGAAGGACAGGTAGTGTGCTATGAGTCAGGTTTGTGAGAGTGACTTTAGGTTGTTGGTCACTCCCTTGCCCATAACATGAGAGTGACTATAACTTAAGTGATCTTGAACAATAGTGGGACCTAGGGATAGACTGTCTGAGTGGCTGACTTGTCTCTTGAGGTTTTTATTGAAGATGTTGGATAACTATACATGATGTTTTTTTGATTCCACTTCCTTAAGTTCACATAGGAGGTCAAATCGGATCATTGTAGCCTCTCATCTCATGTTTACCCTAGTTTTCATAGCTAATTTGGGTAGACCTGG
# Right flank :  CGCGCATCTTTCCACAAAGCAAGTGAAGCAATAAACAAGTATTTAGTTACTCCAATATAGAAATGTGCCACAAAATAATGAGTGGATTCAGGTGCTTTACATTTCCCTTGATCTAATGATTTTCCTTTCTCTTCGTCCCTTTCCTAGTTGATCATATCCCATGAGAGCTTACATGTGGAAGCATCATGCCCATTTCTATTACATCGAA

# Questionable array : NO	 Score: 1.72
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.82, 5:0, 6:1, 7:-0.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAGGGTGTGTTACCCTAAATTAAGCAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.14%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [58.3-63.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_51031 flag=1 multi=8.1477 len=1035		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                         	Spacer_Sequence                                                   	Insertion/Deletion
==========	======	======	======	========================================	==================================================================	==================
       282	    40	 100.0	    66	........................................	CAGCCAAATATTGAATTGGGTGCATGTACCTTGTACTTTGTGGGCAAGCTATGCAAGTCTTTCCTG	
       176	    40	 100.0	     0	........................................	|                                                                 	
==========	======	======	======	========================================	==================================================================	==================
         2	    40	 100.0	    67	GTATAATCTTATTAGTGAAGTGAGTCCTATTACAATGACA	                                                                  	       

# Left flank :   AGAGTCCACATATATCACAACTATGCAACAGGCAGTTGAAGCAATCAATAATCTCATAGAAGAGAGTAAAGATCAGGTTGAGGTGATTGAAAATGGAGAAGACTCTATGGAATGGAAGAAGGTAACAACAAGAAACGAGTCTTACAGATTGTGCAAGAGTATAGATTACACATCTAGTGAATTTAGGGAACTATCAGATGAATAACAGAGCTCATTAGAAGACATTCTTGGTGTTTGCATGTAGAGAGTGGGACCTACATTGCTAGAGAGCTGCAGCAAATGGGCAGTGGATCTGCAGGAAAGTAAACTTGTGGAAGCAGTGTATACAATGGGGAAATGGAAGGGTCAAATGGAGGAGCTGGAAAATGGAGGTTGGAGCATCACCGCAAAAACGACAAATGCATAATCCAGCACGGTATAAGTTAGTTGTATTGTTTTTTTCATATTAATTTAATTTATGTACCTTGTACTTTGTGGTCGAGCTATGCAATTCTTTCCCT
# Right flank :  ATAATTGAGTCCTATTCCAATGACATAAATATTTTAAGTTTGGCAGGTTTTTTTTAAACTTATTTTCTTGTTAAATTTGTCTTTTAATTATTGGAATATTAGGTTTGAACGGTGTAAGATATATATATATATCTATA

# Questionable array : YES	 Score: 0.34
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-1.8, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTATAATCTTATTAGTGAAGTGAGTCCTATTACAATGACA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:70.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.60,-2.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [75.0-68.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_51205 flag=1 multi=15.0000 len=6840		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                                                                                          	Insertion/Deletion
==========	======	======	======	==========================	=========================================================================================================	==================
       887	    26	  76.9	   105	..AC...GGGG...............	GGAACATATATTCAGTCGAACGTTGTCCTCACAAATATAGAGAGAAACGTGGATGGAACACGCTACCTTCATATATTCAGTATGCTCTCTCAACAAATATACAAA	
       756	    26	 100.0	    32	..........................	CAATCCTCTCCCAACAAACAAACATATATAGA                                                                         	
       698	    26	  96.2	    26	..................A.......	TGACCCTCTCCCAACAAATATATAGA                                                                               	
       646	    26	  92.3	    19	........C......C..........	CGACCTTCATATATTCAAT                                                                                      	
       601	    26	  84.6	     0	.TT.....C......C..........	|                                                                                                        	
==========	======	======	======	==========================	=========================================================================================================	==================
         5	    26	  90.0	    46	GACAACAATATTGTGGATCGAACACA	                                                                                                         	       

# Left flank :   AAAAATTATCCATAAACAATTTTTTCCCTGCATTAAAGCAACGCTCAAAAAGAGACATAAACGATATCGGTAGTTGCAGTTTTTTTTTTATAGACAACATGTTAGAACAAGTATTTTCCCAACAAAATTATCCCTAAACCCTATCCATAGTTGCAGAAAAAAGCTTGGCTTCAAATGTGAAAACAAAACTATCCCAAACCATATCCATAATTGCCTAACGAAGAAATTAAAACAAAGATCGAAGAAGAGGAGGCATGCCTCAAGAATGCTAAGATAATTTGAATGGGTGAAGAATGTTAAGATAATTTGAATTGACCGAAAGGGCCTCCTTTCCTCCAACCTGATTTGAAGTAGATCCAGTCCGACGCTCTCCCAACAAATATATAGAGCAGCTCTCCCAACAAAGATACAAAGATACACAAAGATATAGAGCAGCTCTCCCAACAAAGATACAAAGATACACAGTACCCAACAAAGATAGAAAGAACATATATTCAGTC
# Right flank :  CGACCTTCATATATTCAATGTGACTTATGGTCAAATATCCTCATATAATACGTCGTGAAACAATTGGGCACATCTGGAAACAATGGGAAACGGCTATTAATAAACTGCTAGGATTCGGATGTCTCCATCAGCTCTTAACTAAATGCTAGGGTTGAAGAGATTGGGATTGTGAAACAATTGGGACAGATTACTATTTTGAAATCCGCCTTGAACACTTAGGGAAACGGCTATTAATAAACTGCTAGGATTCGGATGTCTCCATCAGCTCTTAACTAAATGTTAGGGTTGAAGAGATTGGGATTGTGAAATCCGCTTGGAATCTGCTCGAATTCTTGTATGACCTCTTAATTGAATTCAATAAATACTAATTACAACACTTAAATTTAGAGTGATGAATATTAATTAGATTTGTTAAAGTTCCACGAACAAAAAGAAAAGGAACGGGAAATAAAAAAATAATAAACCAAATTAATTAATACTTGAAATTTTAAAATATGCAA

# Questionable array : YES	 Score: 0.46
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.66, 5:-1.5, 6:0.26, 7:-0.05, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GACAACAATATTGTGGATCGAACACA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:59.26%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [6-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [66.7-63.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.41   Confidence: MEDIUM] 

# Array family : NA
//

>k141_52173 flag=1 multi=15.6392 len=5750		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	========================	=========================================	==================
      1359	    24	 100.0	    41	........................	TTAAATCAAAAGGGGTGGACGTTTGCTTCTATTTTTTAGAT	
      1294	    24	 100.0	     0	........................	|                                        	
==========	======	======	======	========================	=========================================	==================
         2	    24	 100.0	    42	AGGTCCACCCCTAGGTCCACCCCG	                                         	       

# Left flank :   TAACTGTTTTTGTGTATAAATTCTAGAAACCCATCTGACGTTTCCCATTCTTCTATCGTGCCATCCCCAGGAGCCATTTCCATGGGTGCAATATAGCGGTCGCCGTCCTTGTCCTCCATTAACGCATAGCAGACTATTTTCCAATCAAACAGCGTGTTGTCGTCTTCGTTTCTGTATCTAGCCCACAAGTTTTCAGCGGGTATTATTTGTACTATTTTTGGATAATTATTGCTCATATTATTTGCCTTTTTTGGTTGGTAATTGTTTGTTGTTTTTTATCATGAAAGGTGGACGGGGTGGACGTGGTGGACCTGTTTGGTGGAGTGGACCTACACCAAACTCCATACACCCCATATATAGCACGTATGTACTGTATATATACATATTTACTGTTTTCTCATAAGTTTTCTAAATAGGTCCACCACGTCCACCCCTATATGCCTTTAAGCCTTATGGCTCTAGGGAAAAATCGGGGTGGACCTATGTATTTTAACTTCTTA
# Right flank :  GCTTTTTCGTACTGGAATAGGGTATTTTCGCACGCCTTTTATCAACCTTCCTTGATCACCTATAATCTCCTTTAAAATCATCCCGCACCTGGTTGCATCCGCTCTTGATGGGCGCATAAACCCAATGTCCTCCAACACGCTACTAGCGGTCATTTCTTGCACAGTTAGATTCCCCCAGTCGTATGTGTCTAGAATTTTCTCTTTCACTGGGTCGATCTTTTCAAACTCGGTGTTGTGCTCATTGACCCGATCTTGGTCGGCATTAGACAGATCGGTCTTATGCCCTGCTCGCCAAAGCGCATACACTTCTGCCCATACCTGCTTCATGTCCAGCCCATGGTTAAAGTCGATAGCAGTCACCGATACCGTCCACCACCTTCTATTGCCTGTATCGTCCACCAGGTACACATCGCTGTTAACCGATGCCGCAAAGATCGTGCGCCTGAAATAGCGACTACCTTTGTATGCGTAAGGGTTGCGAATGTGGTCTGACGATGCGG

# Questionable array : NO	 Score: 2.29
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.15, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGTCCACCCCTAGGTCCACCCCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-1.20,-1.10] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [56.7-60.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

