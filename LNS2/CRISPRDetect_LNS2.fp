>k141_3138 flag=1 multi=5.0000 len=1015		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                     	Spacer_Sequence                     	Insertion/Deletion
==========	======	======	======	====================================	====================================	==================
       294	    36	 100.0	    36	....................................	TAATTAGAAACTACAACAGGACGCACACCGTGTCAA	
       366	    36	 100.0	     0	....................................	|                                   	
==========	======	======	======	====================================	====================================	==================
         2	    36	 100.0	    37	CTCCAGGTGCTGCCAAAGTGCGCTCAACCCTTGCTC	                                    	       

# Left flank :   GCTCCAACCCTTGTTGGTGCCCAGCATCCATTCCGCCGGTAGTTTTTTCAGCCAAGCGCGACTGGGATCGACCTCGCCGACGGAAACGTAGGCATAGACCTCGCTACCAGCCAACTTCTGCAAGTCATTGGATTCGACGTGTTCCGGTTCCAGCACCACCCAGTCAAAGGCGCGCAGCTCATCCCAGGGTGGATTTGTCCCATAATAGAAGGCTACCGCCGGTTGGGCGGCCACAGCCTGTGGCACACAGATAAACGCTAGGAGACAAAGGATCAATGCACGCATGAGGAGCTC
# Right flank :  CCAGCGGGGTGGAAGGAACCACGCCCAGAGCCTGGTTCAGGCGGGTGGTGGACATGGCGGAGTGGCGAATATCGCCTTTCCGTGCCGGGGCATAGCTAAGCTGCGGTGTGATGCCGGTGACTCGGCTTAAGGTGGCAATGAGCTCTAGGAGGGTACAACTGTTGCCGGTCCCCACGTTGAGAATACCCTCTGCGGTGCCGGTCAGGGCGGCGAGATTGATGGTCGCCACGTCGCCAACATAGACGAAGTCGCGTGTCTGCAGGCCGTCGCCATGCACGATGAGCGGACGCCCGGTGCGAAGGCGATCGACAAACTCGCTGATGACGCCGGAGTAGGGGGACTTGGGGTCCTGGCCGGGGCCGTAGACGTTGAAGTAGCGCAGACCCAGCAAGGAGAGATCATGCAGCTTGCGATACAACGCGGCGTATTGTTCATTGACCAGCTTTTCCAGGCCGTAGGGCGAGGTGGGGGCGGTGGATGAAGCTTCATTTAGCGGCAGT

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTCCAGGTGCTGCCAAAGTGCGCTCAACCCTTGCTC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-5.30,-4.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [48.3-31.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.64,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_33093 flag=1 multi=5.0000 len=2724		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	=====================================	=====================================	==================
       298	    37	 100.0	    37	.....................................	CCATATCATTCAGGAGGTCAAAGATGTCGGCGGGAAA	
       224	    37	  97.3	     0	...............................A.....	|                                    	
==========	======	======	======	=====================================	=====================================	==================
         2	    37	  98.7	    38	CCTGAGTCGCCCGGAGAAGCAAATAATGTTCGTAAAG	                                     	       

# Left flank :   GGGCACTTCTTCAAAGGGCCACGAGCGGTGATAGGGGATATGGATGATGGGATATTCCACCACCCGCTGGCCGATGGCCTCCACCTCCGCCTCCACCTCGGGACGAAGGATGCGCCCGTCCTCCTGGCACAGGTGCTGATACGTCAGTACCAGACTGCGGGTGAACTCCTGGGGCGCGTAGCCGTAGATAGAGACCCGTTTGCGTTCTATCTGGGCCAGAGTCTCCCAGGCCCGTTGGGGGTCAAATCCCTCGCGGGCCATCCGTTCGGCAAGGCGTCCGTCGCCTCCTCGTAGAAGACGTTGCTTTCCCAAAGCGTGTCATCGGCATCTAAAATCAGGACTGTCTCCTCCATAGCGGGCTCTATTATACCAGAATATGCGCAATAGACCACCCACGGCTAACGGCGGACATGCTTGACGCCCGCCCAAACTATGCTACAATAATAACTCGCTGGTCCCTGACCGAGAGAGCGCAGCAGAATATGCCCGTAAGAGGCGAC
# Right flank :  GAAATGGAGGCAAGGGGCTTTCGCCATTCTTCTTCTGATACTGATGATACTGACGGCCTGTGCCCGATCGGCGCCGCGCTCGGCTCCGGAGGCAGAGGTGTTGCCGACTTTGTCTCCCGTCGCGCGGGGGGAGGGGGAACCGCTGCGCGTGGTGGCGACGACGACCATCGTGGGCGACGTAGTGGCCC

# Questionable array : YES	 Score: 1.40
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCTGAGTCGCCCGGAGAAGCAAATAATGTTCGTAAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.35%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-3.30,-0.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [51.7-45.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_33831 flag=1 multi=5.0000 len=1540		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence                                              	Insertion/Deletion
==========	======	======	======	================================	=============================================================	==================
      1466	    32	 100.0	    61	................................	CGAGACCGTCCTTATTGATGTCCTCGTGGCGTGTGTTGCCTGTGCCTGCGCTTCCGAAGGC	
      1373	    32	 100.0	     0	................................	|                                                            	
==========	======	======	======	================================	=============================================================	==================
         2	    32	 100.0	    62	GAACAGCACTGTTAGCAGGTCAGCATCGTCGA	                                                             	       

# Left flank :   GTATTGTACCTGTCCCTGTGCGGGGCAACCACGTGGGTCGCCCCTGTCCCAGGTTAACAACCGCCTCCGAAAT
# Right flank :  TGCAGCCGTCGCCATTCGTATCGCCGGTGCGCCAAGTGTCGAGCCAGTAGGCAAGGTATTGTCCCGTTTCAGCGTTTCGTCCTTGCCCGACGATGTAGCGTCCATCGTGCGAGACGGCATAAGCGTACTCCAACACGGAGTCGCCCAACAGCGCTCGGAAGGTATGGTTGAGGTTTTCCAGTCCTCGTGTCGGTGTCCAGCGGAACGCGAGGAAGCCGCCCTCGCCCACGACGGTGTTTCCATCCGCCGAGATGCCGCCGACAAAGCTGTAGTTGGGCAGTCGCCCGAGTGTTTGGGGCACGCCACCGACCCAGCGCACGGCGAAAATCTCGCCCCACCAGTTCTGCACGGTTCCCGCAATCATCTGACCATCGGCAGAGACTCCGACCGCCCAGCCACTGCGCAAGTTCGGGTAGGTCAGCGGTTCGATGGCACCGTTGACCCAACGCACAGGCTTCCAGTCGTTCCCAATCGATGAGAAACCGACGGCGACCTGTCCG

# Questionable array : YES	 Score: 0.84
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-1.3, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAACAGCACTGTTAGCAGGTCAGCATCGTCGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,9] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.50,-1.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [40.0-36.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.74   Confidence: HIGH] 

# Array family : NA
//

>k141_34478 flag=1 multi=22.7248 len=3349		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	=============================	==================================	==================
       983	    29	 100.0	    34	.............................	ACGCTAGAATTCAGTTCAGCCATTTGCAATCTGG	
      1046	    29	 100.0	     0	.............................	|                                 	
==========	======	======	======	=============================	==================================	==================
         2	    29	 100.0	    44	CAATTACCGCTTATTCTTCTCTCAAAAAC	                                  	       

# Left flank :   ACAAGTATGCATACTTTATGTAGGTGTGTGCATATCCAGTAGAGCTTCCTATCTCTTTTGAAGCGTCAAGAGTCACCCTTAGGTAGTCTTTATCGTTAATATACGCTTTTACTTGTAGGTAGTTTCTCCTCATCTCAAAACCGGCAGATTTATCGACGGTTGGACTTTTTACATCGGGATTTGCGTATGTATAACCAAGGTAGTGGACACCGCTAAACTCGATTTTTTTTGCTTTTGCACTTACTGGCACGCTTTTTACCTCTTTACTCATCATCTCGGCAAGGCTGTTTTCTTGCACGAAAGTGCCAATTTTTTGCCTGTTTTCACCAGGTTCTGTAAAGATTTGCTTTGTCGTAATATCTTCATATAGCGTAAGACCGCCAGCCGAAGCTACAGACGCAAGCGCCGCCAAAGCGACAAGAGATGCTCTTAGTTTCATTTTAGCCCTTTATAATTAGATTTTCTTTGCGGGTTAATTGTATTTTATGGGTGTTACAAAA
# Right flank :  CGGGTATTTTTTCTCCAGCAGCCGGTATTTTGCACCATCTCCATCTCTCTTTGCCTTTGCATACTTGGCATAGATATCAAACCTTCTCTCCAGCTGCTCAACTTCTTCCCTTATCATCGGCATAGCCCTAAGCGCACTCAAAGCATCATATGCCTTTTGTATCTCGCCTCGAAGCAGCATAGACTTTGCGCCCTGTATCGCTTTTTTTGCTATATCTTCAAGTGAGCGATACTCCTTTATCTCCTTGAGAAAAGGGTGGAGATTTACAAGATCAAAAGCCGATTTAAAATCTTTTTGAGAGATTTTTTTTAGAAAAAGATTTACTATATCCCGTTCGTTAATTATTTTTGTGATAATCTCCACTTTTGACGCAATACCGCGATAAGGCATAAAAAGCTCTTTCGCCTCCTCTATTCTACCTTTTGAAATCAGTGAGTTTGCACTATCTAGGGTAGAGAGCCAGCCTTCTTCAAGGGTTTTGTAGAGTGATGTCTTTTTGA

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CAATTACCGCTTATTCTTCTCTCAAAAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:65.52%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [71.7-53.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.27,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_35085 flag=1 multi=5.0000 len=1106		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	===============================	===================================	==================
        93	    31	 100.0	    35	...............................	CAATATGGCTCGATTTTGAACATAAAAAATACGGG	
        27	    27	  87.1	     0	...........................----	|                                  	
==========	======	======	======	===============================	===================================	==================
         2	    31	  93.5	    36	TCGCACGCCATGCGCGTGCGTGGATTGAAAC	                                   	       

# Left flank :   TATCCACCATTTTATTTAAAGTGAGATTATCATGATGGTAGTAGTTTGCTATGATGTTTCAACGAAAAACAAAAGCGGACAAAAGCGATTAAGAAAAGTTGCCGAAGCATGCCAGAACTTTGGTATAAGGGCGCAATATTCTGTTTTTGAATGTATTGTTGAACCCGCTCAGTGGGAATTATTAAAAAATGAGTTGCTATCAATCTACAATCCGGAAGAAGATAACCTAAGATTTTATTATCTGGGTTCTAACTGGCAGAACAGATTAGAAAATTTTGGTGTAAGTAAAAATCTCAAAGCAGATGATCCATTTATAATATGAGTAGTTTTTTAATTACGCTAAATGAAGCATACAGGATTTTACAAAGAGTTTAGCAATGCGAAAAAGAATTATTTATGTTGTTTGTTATCTCAGTTAAAATTATTCAGATAGAATTAATATCTATTAGCGGATTAAGGTTCGTATTGTCTTTTAATAAAAGGAGTTAAAGTAAATGCGG
# Right flank :  G

# Questionable array : NO	 Score: 7.14
# 	Score Detail : 1:0, 2:3, 3:3, 4:0.68, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCGCACGCCATGCGCGTGCGTGGATTGAAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [6,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         R [matched TCGCACCCCATGCGGGTGCGTGGATTGAAAT with 93% identity] Score: 4.5/4.5
# 	Secondary Structural analysis prediction:  F [-8.20,-6.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.74,4.5   Confidence: HIGH] 

# Array family : I-C [Matched known repeat from this family],   
//

>k141_4177 flag=1 multi=3.0000 len=1021		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	=====================================	==========================================	==================
       167	    37	 100.0	    42	.....................................	CTCCATTTGAAGTTCTAACAGATTGACCATAATCATCTCCTC	
       246	    37	 100.0	     0	.....................................	|                                         	
==========	======	======	======	=====================================	==========================================	==================
         2	    37	 100.0	    52	GTAGCATTCACCCCCAAGGCTGGGTGCCCATTGAAAC	                                          	       

# Left flank :   GCTCGATACACTCTTCCACCTGATCAAGCTCGCCACCATCGACCCCCATAGCGTGAAAATCGAGTGGTGGGTGCGTGAGTAAGGATGAGTGCTTCGGGGTAGTGGGGTTTGCGTCCGCGTGTGGTGGGGAGTGGGTTTTGTTGGGTGAGCTGCCTTGCGATGTGGAG
# Right flank :  CGCGGCGCAGTTATGTGGTGAGGGGGGTGACCCCGTACACTTGGAGCATTCGCTCAGGGGTGGTAGGGGAAAAAACGGGCAGTCAGGGCTTGCGTAGGATGCAGTAGAGGTTGTTGACAAAGTAGTCGCCAATCAGGGGCAGGTGGCGCAAGAGGCTAAAGACTCGCACGACAGGGTGGCGACTGCAGTTCAGCTGGAAAAACTCCACGATAAATCCCGATTCGTGGAGGATGCGTCGGTAGTCGCGGTATGACAGCATATTCATACCCAAGTCGCGGTAGGAATCCAGCGGCTTGCCCAAACGCCTCGCGGTCGCCCCCAACACGATGCGGTCGGGAATCCACAGATGCGCCCACGGCAGGTTGACGCCGATGAACCGCTTGATGGTGTCATGGTCGCCGAAAGGGCTGTTCCACAGCGGTCCAATCGCCGCGTAGAGCCGCCCTCCCGACCTAAGCCGCGCGTACATCGCACGAAGCACCTGCGGCAAATCCATCACA

# Questionable array : NO	 Score: 5.15
# 	Score Detail : 1:0, 2:0, 3:3, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTAGCATTCACCCCCAAGGCTGGGTGCCCATTGAAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         F Score: 4.5/4.5
# 	A,T distribution in repeat prediction:     F [9,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-7.60,-8.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [40.0-36.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [4.87,0.37   Confidence: HIGH] 

# Array family : NA
//

>k141_4257 flag=1 multi=4.0000 len=1077		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                        	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	=======================================	======================================	==================
        74	    39	 100.0	    38	.......................................	TACTTGTAGACCTGCGTTGAATCGGCCAGAAAATAGGA	
       151	    39	  94.9	     0	..............................C.....G..	|                                     	
==========	======	======	======	=======================================	======================================	==================
         2	    39	  97.5	    39	GCTTCCGAGCTGGAGGCTGTTGCTGCCCGCTTTGAGACC	                                      	       

# Left flank :   GCAGGCGATAGAGTAACAGATCGGGCAGGCCCTCGACCTGGCCCAAGCCCTGGCTAAAGGCGCCGGCGAAGTGG
# Right flank :  CGCTCTTCTAACATCCTTGGCCGAGCCCGGCCGGTGGAGTAATTGATGTCAAAAAAGGAGAGCAGAGATGCGCATTACCGTCATTCCAGAACAACTGCGTCAGGTGGCTCAGGGCTTCCGACAGGCGCAGAGCGGCTTAGAGGGAATGGACCGTCGTCTGCGCACTCAGCTAAGTGCGCTGGATTGGGAGATTCGCCGCCAGCAGGCGATAGAGGAACAGATTGGACAGGCGATCCGCTTGGCCCAGAACCTGGCCGGAGGTGCCGGTGAATTGGCTTCCTGGCTGGAGGCCGCTGCTGCCCGCTTTGAGGCCGCCGACCGGGAAGCGTATCAGGCGCCGGGTGTCTCCTTTGGGCGCCTGTCCGTGCCCCCTATCCCCCAGATTTTGGGAGTGTGGACGGGCGGGGGAGCCGCCCGCGATGTCCCCATCACCCGGCTCATCTCTGGCGTTCCCTTGCTGGGAGGGCTGGGGATGTTCGCCTTGAGCGCAACATTGCTCC

# Questionable array : YES	 Score: 1.34
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.88, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCTTCCGAGCTGGAGGCTGTTGCTGCCCGCTTTGAGACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,10] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-10.80,-7.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [33.3-48.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_4471 flag=1 multi=4.0000 len=1381		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence        	Spacer_Sequence                                    	Insertion/Deletion
==========	======	======	======	=======================	===================================================	==================
        99	    23	 100.0	    51	.......................	CCAAGTCGGCTGGAATATTGGTTGCAACGAAATAGCGAAATAGGAAATAGG	
        25	    23	 100.0	     0	.......................	|                                                  	
==========	======	======	======	=======================	===================================================	==================
         2	    23	 100.0	    52	AAGTAGGAAATAGAAGTTTCAAG	                                                   	       

# Left flank :   GGCGCGTCATCGGGTCGATTCTTGACTTACCTGGTTTTTTGTCTGTAGTGAGCTTGATTGGGCGCAGTATCCATGCCCCAGAAGCCTTGCATGGCGTGTTGGCTTGCCTGCAAAGATCAGCAAGCGTGATCCACGGGCCGACTTCCTTGGCAAGCCCACTCACGTAACGGGGAGGTGGCAAGAAAAAATCAACGAACATGCTGATTGGTGAGGAAAGGATGCGGCCGACACAATCTGAAGATTTTCAGGCGCTGCTAGCTTCTTGTCACCGCCTTGCGGGATGGGCTTCTCAACCTCCCGCTTCGTTTGGATTGAGACAAACCACTTGCCGCAAGAGTGGCTCACGGTGATGTTCTTCACCGCACCCAGTCCCTTTTGGCTGTTGCGGTAGCGCAGCCAGCCCAGTTTGGGCAGGAACAGGCGGCTGTTGGCTTGGTCGAGTTTTGAACCGGTATCCGGCCAATACTGGTTGCAACGAAATAGCGAAATAGGAAATAGGA
# Right flank :  GGG

# Questionable array : NO	 Score: 1.84
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-0.3, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAGTAGGAAATAGAAGTTTCAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:69.57%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [0.0-56.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.27   Confidence: MEDIUM] 

# Array family : NA
//

>k141_473 flag=1 multi=15.8561 len=1086		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	=====================================	===================================	==================
        33	    37	 100.0	    35	.....................................	TTTCAATGTTTAAATATCCAATGCCATCATTTGTT	
       105	    37	 100.0	     0	.....................................	|                                  	
==========	======	======	======	=====================================	===================================	==================
         2	    37	 100.0	    45	GTTACAACAAGACCCGATTTGATAGGGGATTGAGACT	                                   	       

# Left flank :   GGTTAGAAGTGCTATAATTCAACAATCTACAGG
# Right flank :  TCTAGTTCTATTGTATAGGCTCCGTTGCCAAGAGTTAAAACTATAATTCCCCCAAATCCCCCCTCACCACCTCAAAAAGACTTATCCCTCTAAACACCTAAAGTCTCTATGAAGTATTTATGTAGGAGATGATAAGTAAACTCCACTCATTTGTCAAGACAAACTCTCCGCTAGTTTTATTTCTCACAAATCCCATAACTCTATTGTAATTTTTTAACCTATTCAACCGCCACACATAAAACCTACTCAAAGTCCCCAAACTCATCCAGGTTATAATAACAATTTTCCCTAGCCAACTATCAATAACTCTCCTTTAAAGGCATAAAGCCATAAATCTTAAAAGTAGTTAGCTTTATATTTTTAAACCGCACAAAAAGTAAATTTAACTGGATTTATGTTTTTTTTAAGTTTAATTAAGATGGAATGAGAGCGCTAATGCAAAATATATTTGAGTCTATGGTTGACTAGAGTGTTAGCGCTAGTTGAATGTAGCTTTGAGA

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTTACAACAAGACCCGATTTGATAGGGGATTGAGACT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:56.76%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-8.00,-3.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [35.0-58.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_4843 flag=1 multi=6.0000 len=1478		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                           	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	==========================================	=========================================	==================
       125	    42	 100.0	    41	..........................................	TCATTTCGAAGGAATGTAATGAGTGAGAAATCTTTGGACGG	
        42	    42	  97.6	     0	.................T........................	|                                        	
==========	======	======	======	==========================================	=========================================	==================
         2	    42	  98.8	    42	AGTAAAAGATTCCTCGCGTTGCTCGGAATGACAGCATAAATG	                                         	       

# Left flank :   TACAGAAGGGTGAATGATTTGAAGTCATTACGAAAGAATGAAGTGACTGAGAAATCTTTTATTCAAAAACTAAATTGATTATACTCCCATATAGATACATTTTTAACTCAGTTAGTTTTGAAACAAAAATGGAATGAAGTATCATAATTACTACATATACATTATGTCGAATTGGAATAATAAAGTTATCTATGTTGGCATTACCAATGATTTAATACGAAGAGTGTATGAACACAGAAATAAATTGTTGAAAGGATTTACAAGTAAATATAATCTGATAAAACTAGTTTATTATGAATTAATTTATGATGTCAATGCTGCTATTAGAAGAGAGAAAGAAATAAAAGGTTGGAGAAGAGAGAAAAAGAATAATTTGATTGAATCGATAAATCCGGATTGGAAGGATTTATATGTTGATTTAATCAATCTTTCGCAAAGTTGAATTTGACAGGATCGATATCATTTCGAAGGAATGTAATGAGTGAGAAATCTTTGAATGA
# Right flank :  G

# Questionable array : YES	 Score: 1.40
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGTAAAAGATTCCTCGCGTTGCTCGGAATGACAGCATAAATG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.14%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-5.20,-4.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_765 flag=1 multi=23.0000 len=1574		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                  	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	=================================	======================================	==================
         1	    33	 100.0	    38	.................................	CTTTTTCAGCGGTTTTTTCTACCGCTTTTAGCGAGTTA	
        72	    33	 100.0	     0	.................................	|                                     	
==========	======	======	======	=================================	======================================	==================
         2	    33	 100.0	    48	CAACAAGACCCGATTTGATAGGGGATTGAGACA	                                      	       

# Left flank :   |
# Right flank :  ATTGTAAGTAAAGATAACTATCTCTTGGACAATATCGAAAACATCTAGCAATAAAATAACTACATTAATAGATAGAGACAATCCAAAAGATGAACTTGATATCTATCTTATTTGGAAATTTTGTACTTGATTAGAAAGAGATACTTGAATCTGCCAGATAAAAAGCAAGTTTTTGCGGCAAAGAGCTTATAGTAAGGCTCAAGACATTTTCAAAAGAGCTTCTAAAGAAGATAAAAAAATTGTAGACAAAACCTTTCTCGATGATTTTGACACAGATTTTGCTAAAATTATAGAGGAAATTTCGGCAAACTATCCGGCAATGGTCAGATAAACCTCCCACTAAATTTTGTAGCCCCTAGATACCTCCAACCCTCTATAGTTAGTTTGAATAAAAATCTAGTTAGATATAAGTAGCCATCCCTCTTAGCGGCTCTAACACTCGTCTAATCAAAAACTGTGGCTTTTGGAAGATAAATCTCATTTAAATGTTGTCTAAAAAT

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CAACAAGACCCGATTTGATAGGGGATTGAGACA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:54.55%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-4.00,-2.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_8643 flag=1 multi=5.0000 len=3691		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence          	Spacer_Sequence                                                     	Insertion/Deletion
==========	======	======	======	=========================	====================================================================	==================
         1	    25	 100.0	    68	.........................	GGGATGCATATGTCCGCTACGCCAGTGCCCAAGCTCTGGGCAACCTCCAAGACCCACAAGCCACACCA	
        94	    25	 100.0	     0	.........................	|                                                                   	
==========	======	======	======	=========================	====================================================================	==================
         2	    25	 100.0	    78	CACCTCATCCAAGCACTGAAAGATA	                                                                    	       

# Left flank :   |
# Right flank :  ACTGATTTGCTTGTCCGCTACGCCAGTGCGGAGGCGTTGGGGCGTATTGGTGAAGCCTCCGCGCTCCCCGCTCTCCAGAGGGCGCTCCAGCACGCAAAGACGGTGGAAGAGCGGGAGGCAATCCGAAACGCTATCTACGCGATTCAGATAGCCACCAAACGGGCAAAGTAAACTTTTCTCTCTCAGACACCCCCGCGCCTTGTGGCGCGCTAACCGCAGCGGCGCAAGGGCAACGGCACAAGCGAAGAGAATCGAGTGAGGTACACTAACCACCAGCGGCGATGAAGCGAGTGGAAGCCATCATACGCCCAATCCGATTCGAGGCGGTGAAGGAGGCGCTGAACGATATCGGCATCTACGGGATGACGATTACCGATGTGCGCGGCTTCGGGCGCCAGCAAGGGCACACCGAGAAGTATCGCGGCAGCACCTACACGCTGAACCTGCTGCCCAAGATTAAACTGGAGGTGGTCGTGCCCGACACGCGCGTGGAAGAGGTC

# Questionable array : YES	 Score: 0.14
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-2, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CACCTCATCCAAGCACTGAAAGATA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:56.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_12197 flag=1 multi=7.0000 len=2382		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                   	Spacer_Sequence                         	Insertion/Deletion
==========	======	======	======	==================================	========================================	==================
       308	    34	 100.0	    40	..................................	TCGATAACGGCACAATGCCAGCTTGGAACGGGTCATGGTT	
       234	    34	 100.0	     0	..................................	|                                       	
==========	======	======	======	==================================	========================================	==================
         2	    34	 100.0	    41	GCACCGGTTACCCCGATGACAAGGGGACTGAAAG	                                        	       

# Left flank :   TGTTCTGGGAGATCGGTCTGGTGCCGATGTACTTCCTTATCAACCGCTGGGGGACCAAGTACGGGGAGCGCGAACTGTGGCGCGGGTACAAAATCCCGGCGCGAACCTACGCGGCCTTCAAGTTCCTGCTCTACACGATGGCGGGGTCCCTGGGGCTCCTGCTGGCCATCCAGGTCATCGGCCTGACGGCGAAAACCTTTGACATCGTGGAGTTGCAGAAGGTCTGGCCCGCGTTCACTGGCCCCCTCTTCGGCCTGCCCATTTCGGTGATCAAGGCCATCGGGTTCTGGGCCTTTACGGTCGCTTTCGCTATCAAGGTCCTCATCTGGCCCTTCCACACCTGGCTGCCGGATGCCCACACCGAAGCGCCGACGGCCGGCTCTATGATTCTGGCCGGGGTGCTCCTAAAATTGGGGGCCTTCGGTTTCCTGCGGTTGGTGCTCCCGCTCTTCCCGGAACAGTCAGTGCAGTACGCGTGGGTGCTGGCGCTTCTGGGGCTG
# Right flank :  GGCAATCCATGAAGTGTCCATAGTAATCTGCGACACCTTTTCCCGCCGCAGTGTCCGGCAGCTTCTGCGAGTGCAAGCACGGCTTCTGTTGCCACCACGAACGTGAGAGCAAACGAAGAGTAAACTGGAAGCCGCTGCGAGAGGGCTGCCTCGATTGGGGCAGCCCCTTTTCATGAGCGCTACGGGAGGATGAGTTACCTG

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCACCGGTTACCCCGATGACAAGGGGACTGAAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,10] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-7.00,-8.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [45.0-33.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.74   Confidence: MEDIUM] 

# Array family : NA
//

>k141_13202 flag=1 multi=13.7704 len=1169		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                	Spacer_Sequence                     	Insertion/Deletion
==========	======	======	======	===============================	====================================	==================
       556	    31	 100.0	    36	...............................	TCACCAAAATCGTATGAAGCAAGCAAAGCAGTTTGA	
       489	    31	  96.8	     0	........................G......	|                                   	
==========	======	======	======	===============================	====================================	==================
         2	    31	  98.4	    37	ACTTTGCACTCTATAGGATAGGGCAGGAGAG	                                    	       

# Left flank :   TTATGCGGATGACATCGATAGAGCCTTTGGTGACAAATACTACAGGTTTCAAACTCCGCAAGATGTTATCCGTTTTTTAAAAGCGTTAAGAGAGCTGCAAAAAGAGGGCGGGGTAAGGGAGTATTTTCTCAGAGGCTACCAAAAAAACGGTTCTGTTCTGGATGGGCTGAGATGCACTATAGAGAAGCTAAATAAGATAGTAAAGCCCGATACGCAAGGGTTGCACTTTTTAATTGGTTCAGAGGATATCAAAATGTCAGGTTCTCCTCTAAAGCGGTGGAATATGTATCTTAGATGGATGGTTAGGAAAGATAGCTTGGATATGGGGCTTTGGGACGGGCTTGTGGAGAGAAGGCATCTAATACTACCACTTGACACTCACACATTTAATGTATCAAGGAGGTTGGGGCTGCTCGATAGAAAAAGTTATGATTTAAAGTCGGCACTGCTAATAACCGAAAGACTGCGGGAGTTTGACAAGGATGACCCCGTCAAATACG
# Right flank :  GGCTCGGCCAGGGAGTTTTGCCGTTTGTACTCAAGAGATCTATTGCTTTTTGGATTTAAATGAAAATAGACACCTTTTGTGCACACCAAGTCGGCGTGGTGCTGTAACGCTTCGTTTTGAGTTTAAACACTAATTGGTTAAAATCCACATAAATTTTGGTCTCAAGGGCAAAAGATGGAGGGAAGACTTTTTACTTTTTTTGGTGAATTTTTTGGTCATTCACAAGCGGCTATCCTTATCTCTCATACTGTGTTAGTGGCCATAATCGTACTGCTCGTTGCGGTAATCGCGACTCAAGGTATGAAGCTCGTTCCAAAGGGGCTCCAAAATGTGATGGAGGTCTATCTTGAGAGTGTAATTAATATGGGTGCCGGCGTATTTGGGGAGGAGAGGGCTAGAATATACCTACCTCTTATAGCAACTATTGGCTTGGTTGTATTTACTTCAAACATGATAGGT

# Questionable array : YES	 Score: 1.38
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.92, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACTTTGCACTCTATAGGATAGGGCAGGAGAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.61%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.90,-3.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [58.3-48.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.78   Confidence: MEDIUM] 

# Array family : NA
//

>k141_14051 flag=1 multi=4.0000 len=2398		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence        	Spacer_Sequence                                   	Insertion/Deletion
==========	======	======	======	=======================	==================================================	==================
         1	    23	 100.0	    50	.......................	CTATAATCTTTTTTATGGTTCGCCCATGCGCTACGTATGTCTTTCCGGGG	
        74	    23	 100.0	    50	.......................	CACAGCACCGCCAACATCCATGAGATTGCCTCCAGCCCGTCTTTCCGGGG	
       147	    23	  91.3	     0	....................GG.	|                                                 	
==========	======	======	======	=======================	==================================================	==================
         3	    23	  97.1	    50	CTCTCGCCCCGGCCCCATTGAAG	                                                  	       

# Left flank :   |
# Right flank :  GGGCCTAGGCAGCATCACGAATCTGCTGAAGCCCCCCAAGCCTCAGGTCCCCTCCACCAGTCCTTGGAGATGAATGCTGGCATCGGGAAACGCAAAAATCTGTCTCCAAACTGCGCTGTCGGCAATTCAGCCTTGACTTTGGGGCTTACGGCCTCCACTATCCTCATAAATAAGAATCAGGTCTGGGATCTTCCGCAGACCTGCGAGTCATTTCCAGGAGGTGCCGATGGCGTTCCGTATACTGGTGCTTCTTTTTTATCTTGGAGGAACTATGGGCAATGCACAAGCGACAGATCGAGAATCCATCGTTTTGGGGATGGGATGCTTTTGGGGGGCAGAGCACCGCATGGCGCAAATCCCAGGGGTCGTGGACGTCGAAGCCGGGTATGCCGGAGGGGATGCAGAAACAGTGACCTACGACGATGTCCTACGTTTGGAGCGGGAGCTCCGCTTTGGCATGGCCACGGCGCGCAACCACGCCGAAGTAGTCAAAGTCACCT

# Questionable array : NO	 Score: 1.79
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.85, 5:0, 6:0.25, 7:-0.40, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTCTCGCCCCGGCCCCATTGAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,-0.20] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_14674 flag=1 multi=6.0000 len=1338		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                       	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	======================================	=========================================	==================
       940	    38	 100.0	    41	......................................	ACACCCGCCTTCGGGCGTGGCTCAGATATCGCGCGGTGAGG	
       861	    38	 100.0	     0	......................................	|                                        	
==========	======	======	======	======================================	=========================================	==================
         2	    38	 100.0	    42	ATGTCCCTGCAGACCGAGCTACTGCCCGCGCCTGCGAA	                                         	       

# Left flank :   GTCTCCACCCACAGCGGGCGCGTCGGTGCAGGACGCACGGCAGCACGCGCTTGGCGACGCAGTGCGTCCAGGCGTGCGCGTTCCGCGACGCTCAACGGGTTGAGCGCTTGCACCCGCTCGTAGGCTGCCATCGCTTCGGTGATCTGCCCCCGTCGCTCGTGAATCAGCCCCTTGAGCGAAAGGGCGGGTGCGTAGCATTCGTCCAACGCAAGCGCACCCTCACATGCTAGCAGTGCCTCCTCCAGCCGCCCCTGCTCGAACAGCGCCACCGCCTCGCGCATGAGCTGCTCGGTGTGTGCCTTCGTGTCCGTCGTACCGTTTTTTGACATCGCTCCCCGCCCTATTGTACCTCAAGATTAAAGCCCCCGATGGGTCGGGGGCTTTTGGATAGTTCGGC
# Right flank :  GGAAGGCAGGCACCTCCAGGTCTTCTCGTGAAACGCGCGGCTCTGGTTTCGGACGCTCGGGCGTCGGGTTCGATGGGCGCGGTGGCTCAGGGGTTGCAGCGCGTGGCGGGGTCGGTGGCTCCACCCGTGCACGGGGGATGTCTAAGGGATGCGGCTCAGGCTGCGGCATCAGGTGTGCCACTGTCTCAGGCGACAACGGCTCCTCGGGGAAGCCCGTCGCCAGCACGGTCACCTGCACCACATCGGTCATGCCCGGCTTGATCACATGCCCGAAGATAATGTTTGCCTCGCTGGGGTCGGTCATCTGATAGAGCAGGTTCATCGCTTCCTGCACCTCGAAGATGCCGATATCCTCGCCTGTCGTGATATTCACCAGCAGTCGGCGCGCCCCATAGATTGAGGATTCCAGCAACTTGCTGGTGGATGCCTCCTGCACGGCTTGGATCAGGCGATTTTCGCCAGTGCCCTTGCCGATGCCCATCAGCGCAGTGCCCGCATCC

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATGTCCCTGCAGACCGAGCTACTGCCCGCGCCTGCGAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-11.00,-3.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [35.0-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_24788 flag=1 multi=10.3919 len=1616		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                           	Spacer_Sequence                                       	Insertion/Deletion
==========	======	======	======	==========================================	======================================================	==================
      1090	    42	  92.9	    54	............G.......G..G..................	CCACCAAGTGGGTGGTGGCCACCAGGTGGGTGGTGGCCAGCATAAGTAAGTTCA	
       994	    42	 100.0	     0	..........................................	|                                                     	
==========	======	======	======	==========================================	======================================================	==================
         2	    42	  96.5	    55	GGCGGTTCTGGAAAGAAAAAACCAGGGTGGGGAAGGTGGTGG	                                                      	       

# Left flank :   CGCTGATAAAAACCGTATCAATCTGACGCTGTGTCAGATACATTGGCAGAACGACCGGCTTGAAAAGCGCGGTCTGGCGTTGCCGTGGAAGATTTGGTACGCCAAAGGATTGAAGTATGGACGCCTATACCGTCGCAGTACATACCTGAAGAAGAAGGCTGGAAATCGCGATTTGTCAGCTGATTTAACAGAACCGGAGTAAGACAAAAATGTCCCTGCATGTAGGTCAAAAATGACCTAGCAGGTAGGTCAAAAATGACCTACATGTAGGTCAAAAATGACCTACCAAAAAAAAGGCTCAAAGCCGCATGAATACTGGGTTTTTGGCTGGTAAGGCACTAGTGTTTCTTCTCACTCTTATATATAATGTTCCGTTCCTGCGAAAGCCGGAAACCCGCATGAATACTGGCTTTGCCGAAACAAGCTCAAGCGATTCTGGAAGGGAAAAAAACGCCGGAAACCCGCATGAATACTGGCTTCGATGGCAGTTTTAGTTTGAT
# Right flank :  TCTTGTGCTTGAACCTGAGCTTGTGCGTCGTGCCCGCACAGCTGACCTTGTTTCGTTCCTGCGGGCGAAAGGATACCGGCTTAAGCACGAAGGTTCGGGAAACTACCGCGTGGTTGGCCACCAAGGTTTGATAGTGCAGGGTTCGCGGTGGTACTGGCATTCCGTCGATGAAGGCGGCAACGCGGTAGAGTTCCTGATGCGGGTGCTCGGGTACCCGTTCCGGGACGCGGTGCTGGAGCTGACCGGGAGCGTGCCGTGCGTGGAGAGTTCCCGCACGGAGCAGGCTGTTGCGAAGGCTTTCGCATTGCCGGAAGCCGCGCCGGATTACCGACGGCTCTACGGGTATCTAATTCGGAGCCGCAACCTGCCGGTGGTGCTGGTGCAGGATGTGGTTCGCCGGAAGCTGGCTTACCAGGACCTGCGTGGCAACGTGATTTGGGTCTGTAGAGACAGTCAAGAGGTAGTCCGTGGCGCGTTCCTGGAAGGCACTGGCGCACGGT

# Questionable array : YES	 Score: 0.67
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.82, 5:0, 6:0.25, 7:-0.6, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCGGTTCTGGAAAGAAAAAACCAGGGTGGGGAAGGTGGTGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,12] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-3.60,-4.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [45.0-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,1.01   Confidence: MEDIUM] 

# Array family : NA
//

>k141_29785 flag=1 multi=10.3594 len=1713		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence      	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	=====================	=======================================	==================
      1219	    21	 100.0	    39	.....................	TGCGTTACAGTCGTATTTTATCGAATTCCTACCAAAATA	
      1279	    21	  95.2	     0	.............C.......	|                                      	
==========	======	======	======	=====================	=======================================	==================
         2	    21	  97.6	    40	TCAAGAGGTAGTGTAAAACTC	                                       	       

# Left flank :   CTCCCAGTAGATGTTTTTATCTTCAAGTTGAAAATATGAGAGCATCGTAGCTCGTGCACCATTCCCTTTCTCATCAATGTCCCATCTGATGACTTTTTGTATTAGCTCTTCTAGCTCTTTGGCTATTTGCTTGGTTGTAATGTTACTTTCATAACCAAGTATTCTTTTAAACTCAGAAAAAGGCATCAAATACTTATCTTTCTTGCCTCTTGTCTGTATATGTTCTATTGTTTCTTTGAGCATTAGAGCAAAGGCTTTTCGTTGTAGCAGAGATATATTTCCACTACAGCGTATAAGGCTATTAGATTTGATGACTTCACCCTTTTTTTGGTATAATCCCATCGCTATACTCCACATTTTCTTTTTTGGATTTGAGCACTGAGTCTGCCTTCCTCAGTGCTTTGTTACTTCAATTTCTCCCTTTAATAATTAGACTTTAAACAGTTCTCATTGTTATCCTCCTTATTTGGTTTGCAGTAACATATGTTCCTACATATTTG
# Right flank :  CAAAAAAGGTAGGAAAATACTCCAAAAAAGGTAGGAAAATACTCTAAGGAATATACTCCAAAAAAGGTAGGAAAATACTCTAAGGAATATACTCCAAAAAAGGTAGTCATATACTCCTATCAAACCCCGATAAAATCGATATTCTCTCAAAATTTTAACTAAAGCGTTTGTTATAAAATCGAAGATATGTGCGTAAGCTATCTTTAAGCAAAAAAATTTGCTCAAAATTGAGAGCACTTGAAAAGCTTTTTGAGAGCGTAAAATTATTTAAAATACTTAAAATTATATAAAAGGCAAAAATCGCAAAAATTTGCTTTTCGCTTTTTTTATTTTTGCTTCTTTTTACGGTGCTTGCTAAATCAAAATCTCCAAAAAAAGGTAGTCATATACCCCTAATAAATGTTGGGAATACTC

# Questionable array : YES	 Score: 0.59
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.88, 5:0, 6:-0.5, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCAAGAGGTAGTGTAAAACTC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:61.90%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [66.7-68.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_31471 flag=1 multi=2.9687 len=1546		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence                                                               	Insertion/Deletion
==========	======	======	======	========================	==============================================================================	==================
      1471	    24	 100.0	    69	........................	GGAGCGGTGCGCAAGGCTCGGGGAAAAACCCCGCTACGGCGGGGACTTTCCCAGAGGACGCGCAATGGT         	
      1378	    24	 100.0	    47	........................	AGGAAAGGTCTCCCTGGGACGAGGTCTTTCCGGATGGACGTGCAACG                               	
      1307	    24	  91.7	    46	..............A......T..	AGAAGAACCTCGCCAGGACGGGGACTTTCCGGGAAGACGCGCGATG                                	
      1237	    24	  79.2	    78	......AG......A.G.....G.	GAAAGAACCTCGCCGGGACGGGAGTTTTCGGAGCCTGAGCACGACGGTCGGGAAAGAACCTTGCTGGGACGGGGGTTT	G [1220]
      1134	    24	  83.3	     0	T....A...T...........T..	|                                                                             	
==========	======	======	======	========================	==============================================================================	==================
         5	    24	  90.8	    60	CCCGGGGTCGGTGCGCAAGGCCCG	                                                                              	       

# Left flank :   GCCCGGGAAGGAACCTCGTCTGGGACGGGTATTTCTCGGATGGACGCACACAGGTCCCGGGAGACGTGCGCAGG
# Right flank :  GCGCACAAGGTCCCGGGAAAGCCTTCCCTACGGCGAGGGTTTCGGCCTGCAAGTGGTTTTTAAAATTTTATAAAAAACCCCTTGACATTATATATAAAACTCTATAAACTACAAGTAACAAAACAACCGCGTCCCGGTCCTGGCCGCGAGGTCGGGACGCGTAGGGAGGGGATAAGGATGCGGCAAGACAAGGAGATTCGGCTGCGCTACGTTGACCTTACACAAGAGGAGGTGGGGCTCGCGCTAATCGGAGAAGCCTTGTATGGTGCCTCTGTCAAAGACAAGCTCTTGAAGGCGCTCAGAATTGAGCGCCAAGAGGGAGAATCTCACTTGGCAGCATGGGTTAGAGCAATGATGGAGGAGAACAATCTTTCGTGCAAAGAGGAGGTGGCTCGAGCTATTGATAACTGCTTGGAAGCGTCAGCCATCTGGCGATTTGTTTGGACGTTAGCGTAGGCGGGGCAGTTAGCCCTGCCTCCAGTCCCCACAAGGGGGCTGGA

# Questionable array : YES	 Score: 0.82
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.54, 5:0, 6:0.25, 7:-1.69, 8:0.8, 9:0.92,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCCGGGGTCGGTGCGCAAGGCCCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [2,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [10-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [38.3-36.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.41   Confidence: MEDIUM] 

# Array family : NA
//

>k141_31927 flag=1 multi=13.9098 len=1826		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	===========================	==================================	==================
       498	    27	  92.6	    34	.............G..T..........	TAACGCGCGCCTGCTCCAAAGCCTTTCGCTCAGA	
       437	    27	 100.0	    34	...........................	CTTTTTGCAGCCGCCCTTTTTCGGGGACCGCGAG	
       376	    27	  92.6	    34	..............T........G...	TCCATTCCCGGAGCAGTAAGCCGCTGTTATCTGA	
       315	    27	  85.2	    34	.............G..TA.......G.	CTCAGCTGCCCATGCAATGAGATCGGCGTCCATT	
       254	    27	  77.8	    33	.A.........A...G.A..A..C...	ATTTCGGCCAAGCCTTTATCCTTGCTCTCGGAA 	G [228]
       193	    27	  85.2	    34	.............GTGT..........	CTGGGCCCGCCAGGCTTTCATGGCCTCAGAAATG	
       132	    27	  85.2	    34	.............GTGT..........	CGTCGAAGGCTTGCCCCGCCCTTCGGTAGTGGTG	
        71	    27	 100.0	     0	...........................	|                                 	
==========	======	======	======	===========================	==================================	==================
         8	    27	  89.8	    34	GGTCCTCCCCCGCACACGCGGGGACAC	                                  	       

# Left flank :   CGCTGCAGGTGCAGCATTGCTGTTGGCGCTGCTTTCATCGCTGGGCGCGCCATTTTCTGTCGGGCTTGCGTCTTCTGGCTTTTGGGCAGAAGCATCGCTCTTTTTGCCCTTGCTGCCTTTTTTGGGGTTCTTTTTTTGTGGCACGAAGCCCTTGTCGAGCGCTAGCAGTTGCTCCAGGGTCAAGTGTCTGCCATGCTGCGCAAGCGCAGCTTGCACCACACGCGCAATGCCAGACCGAATGACATGGCGCATGGTGGACGCCGGAATATAGCGCACCGCAGACTGCCCATAGCCAAAGGATGCCAGGCGCAAGGGTGAGCCGTCATTTTTCTTGTCGCTAGCCCAGGCAGCCCCCGGCGGCGAGACGGCCAACGGCTGGCGCGCCAAAAGTTGGCCAGTGACTTGCAGTGTCTGAATCGTGGTGTTCATGAAGTCTCCTAAAAGGTTGAGAGAAGGAAAAAGAAGTCCCCCGCGTAAACAGGGACACCCCAGACATACCG
# Right flank :  CCCTGCGTCAGAAGCTTGAAGAACTTCAATCGAAAGGTCCTCCCC

# Questionable array : NO	 Score: 2.22
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.49, 5:0, 6:0.25, 7:0.01, 8:1, 9:0.47,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTCCTCCCCCGCACACGCGGGGACAC
# Alternate repeat :   GGTCCTCCCCCGCGTGTGCGGGGACAC

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [2,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-9.30,-9.10] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-8] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [35.0-48.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,0.64   Confidence: LOW] 

# Array family : NA
//

