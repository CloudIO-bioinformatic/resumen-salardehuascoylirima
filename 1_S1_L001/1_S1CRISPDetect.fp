>k141_67853 flag=1 multi=6.9156 len=7783		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                                                        	Insertion/Deletion
==========	======	======	======	==============================	=======================================================================	==================
       230	    30	 100.0	    71	..............................	ATCGCCCCTTTGCCACAAACCAAGAAACCGGGTTTCTGCGAAAATTTTTGTCACAATACCGAGATTTATTT	
       129	    30	 100.0	     0	..............................	|                                                                      	
==========	======	======	======	==============================	=======================================================================	==================
         2	    30	 100.0	    72	AAGAAACCCGGTTTCTGAAGCCAAAAAGCG	                                                                       	       

# Left flank :   CTAATTCAAGGTGTGGAGTCTCAAGAAATGGGCTTTAATGTTAAAGGAGAAGCTTTCATGGTGCTACTTAATACGATTATTCGATGTCAAGAAGCTGGATTAATTAAAAATGACGATCCCAAAACTCAAGCACTAGCTTGTTGGTCAATTGTTCATGGGTTAGCAATGTTATTTATCGATGGACAAATTTTAATCACAGAAGAAGACTCGATCGCTTTATTGTCAGCATTAATTGTCAAATTTTTAATCGAAGGATTGTGCAAATAGCATTCTTAATTAAATTTCTTGCCACTCTCTTCGAAATCTTACCTAATAAGGCACAAACCGCTGCTAATTCCGCCTAACCTAAGAAGATACCATAAAATCTTCAAGTCGGACAACGCCCTATATAGCCCGATATAATTAAACCCCAAGTAAACCCCAAGAGCGATCGCCCCTTGACCCACAAACCAAGAAACCGGGTTCTCCGTTGCTTTTATCACAATACCGAGATTTCAAAA
# Right flank :  GGTAAGCTTTCGCCATGCCGTCAGGCTTATCGCTTATCTAAAAATAAAATAGCGATCGCAATCCTCCGGTTCCGAGTGGCTTTTTAAGGGGGGCTATGCC

# Questionable array : YES	 Score: 0.58
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.99, 7:-2.3, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAGAAACCCGGTTTCTGAAGCCAAAAAGCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.40,-2.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [58.3-58.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_68052 flag=1 multi=17.7355 len=1869		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                           	Spacer_Sequence                                                	Insertion/Deletion
==========	======	======	======	==========================================	===============================================================	==================
       762	    42	 100.0	    63	..........................................	TTCGAGAAACCCGGTTTCTGGGGATAATCTGTCGGTTTCGGGACAGAATGTGAAATCAGGTAG	
       867	    42	 100.0	     0	..........................................	|                                                              	
==========	======	======	======	==========================================	===============================================================	==================
         2	    42	 100.0	    64	TAGTAGGGTGGGCAGGGTACTGCCCACCCTACATATGGAAAG	                                                               	       

# Left flank :   CCTGGTTCCTAGTCAAATGACAAAGGACAAATGACCAAGGACAAATGACAAATGACAAAATGGCAACTGGGCATTTTTGGCGTGTTACTGGTTTCCAGCACAATTGTGGGTTTAGATGACTGTGCGATCGCGGCACAGACAAAACAGCGTGTGCCCGGAAGTCCTTTGGAGATGACAGAACCAGACCCGTTGCTGCCGAATCCGCCGAAAAAGGGCTATTTGAGTCCGCCAGAGAGCGATCGCCTGCGGTTAGCCCTAGACGAATTGAACCTCAGAGCGGCGGCGGAATTTGCCACCGGTAATCGGCGACAGGCATTTAACCTGTGGAACCGAGAACTAAGATTGCGACGCTATCTCGGACCAATAGAAGAAATCGCCGCCTTAACCAGAGTGGGAGCGATCGCCTGGACAGTAGAAGAAATCACCCAAGTGCGGGTGATTACCGGACGGTTGGATGCCATAGAACTAGAGCAATGCGGCGTTTTGAGTGATAGTAGTGA
# Right flank :  GAGGGAAAACAGAAACCGGGTTAGTCAGCATCGATACCCCGATGCAATGCAATCTGACCTTACTGCAAGCTCTGGGAAAAGCCTATGAAGTGCTACGAGCGAGAGCCCAAGCCATCCGCGTGTATCGGGAAATCCTCAGTGACGCCATTCGCCGTGGCAATGTCAGTGAGGAAGAGAAAACCATGCAGACGATCGGCCAACTGCACTTAGAGCTGCTCGAATACCGAGAAGCCGGGACAATCTACGGAGAGCTGCTCACCAGAACCGTAGCACGGGGTGACAGGGAAGCAGAAATCACCTACGTGCAGGAGTTGGTATTTATTTACAGTCAAGCCAGACAACGACAAAAAGCGATGACGATGAGAGAGCGGCTCATAGAATATCATCGCCAAAACCAAAACTTGCGGGCGATACCAGAATTGCTGATTGCCACAGCCAGCGATCGCCAAGCCATCGGACAACCCAACGAAGCCCTAGTATCCCTCCGCCAAGCCTACACC

# Questionable array : YES	 Score: 0.64
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-1.5, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TAGTAGGGTGGGCAGGGTACTGCCCACCCTACATATGGAAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-22.50,-21.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [51.7-51.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0   Confidence: HIGH] 

# Array family : NA
//

>k141_72830 flag=1 multi=7.0000 len=1164		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                            	Spacer_Sequence                                            	Insertion/Deletion
==========	======	======	======	===========================================	===========================================================	==================
      1045	    43	 100.0	    59	...........................................	TTATGGTTTCAGAAACCGGGTTTGTTGTTGGAATTCTCGGTATTGTGGCAAAAGTTATC	
       943	    43	 100.0	     0	...........................................	|                                                          	
==========	======	======	======	===========================================	===========================================================	==================
         2	    43	 100.0	    60	GCAGAAACCCGGTTTCTTTGGTTTGTGGCAAAGGGGCGATCGC	                                                           	       

# Left flank :   AGTTTAAAGTGATTGTTTGCATGGTATTTACATAGAAAAACCAAATCCTGCCTATATTGTATCATAGTTTCAGAAAGCGGGTTTTTTTTTAAATCTCGGTATTGTGGCAAAAATTGGA
# Right flank :  ACGGCAGGTGAGATCCCCCCTAACCCCCCTTAAAAAGGGGGGAACCGGAAGATAAGCCGGTGGTTCAGAAACCGGGTTTCTTTTTTTAAATCTCGGTATTGTGGCAAAAATTGGAGGCAGAAACCCGGTTTCTTTTCTGTTTAACCTTCTAGTCTTCTCCTTCCCAATCTTCTATTTTTAGCCCTTCAACCCTGACAAATTCTTTAGTATTATGAGTCACCAAAATCAAATTATGAGCCAGGGCGATCGAAGCAATTAATAAATCATTATAACCAATTGGGGTTCCCGCATTAGCTAACTGAGCGCGAATCCTCCCAGCATTTAAAGCCGCAATATCATCTAAAGGTAAAGAAACGAATACCTCTAAAAATCGCTGAACTTTTTGCCAAACCAGATCGGGATTATCACTTCTCATCGCTCCATAAAATAACTCTAATTTAACCACCGAACAAATGGCAATCTCTGCTAAATCGGTACTTTCTAATTTCCGGCGTATTCTT

# Questionable array : YES	 Score: 1.04
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-1.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCAGAAACCCGGTTTCTTTGGTTTGTGGCAAAGGGGCGATCGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-6.50,-9.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [40.0-66.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_72830 flag=1 multi=7.0000 len=1164		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                            	Spacer_Sequence                                            	Insertion/Deletion
==========	======	======	======	===========================================	===========================================================	==================
      1045	    43	 100.0	    59	...........................................	TTATGGTTTCAGAAACCGGGTTTGTTGTTGGAATTCTCGGTATTGTGGCAAAAGTTATC	
       943	    43	 100.0	     0	...........................................	|                                                          	
==========	======	======	======	===========================================	===========================================================	==================
         2	    43	 100.0	    60	GCAGAAACCCGGTTTCTTTGGTTTGTGGCAAAGGGGCGATCGC	                                                           	       

# Left flank :   AGTTTAAAGTGATTGTTTGCATGGTATTTACATAGAAAAACCAAATCCTGCCTATATTGTATCATAGTTTCAGAAAGCGGGTTTTTTTTTAAATCTCGGTATTGTGGCAAAAATTGGA
# Right flank :  ACGGCAGGTGAGATCCCCCCTAACCCCCCTTAAAAAGGGGGGAACCGGAAGATAAGCCGGTGGTTCAGAAACCGGGTTTCTTTTTTTAAATCTCGGTATTGTGGCAAAAATTGGAGGCAGAAACCCGGTTTCTTTTCTGTTTAACCTTCTAGTCTTCTCCTTCCCAATCTTCTATTTTTAGCCCTTCAACCCTGACAAATTCTTTAGTATTATGAGTCACCAAAATCAAATTATGAGCCAGGGCGATCGAAGCAATTAATAAATCATTATAACCAATTGGGGTTCCCGCATTAGCTAACTGAGCGCGAATCCTCCCAGCATTTAAAGCCGCAATATCATCTAAAGGTAAAGAAACGAATACCTCTAAAAATCGCTGAACTTTTTGCCAAACCAGATCGGGATTATCACTTCTCATCGCTCCATAAAATAACTCTAATTTAACCACCGAACAAATGGCAATCTCTGCTAAATCGGTACTTTCTAATTTCCGGCGTATTCTT

# Questionable array : YES	 Score: 1.04
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-1.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCAGAAACCCGGTTTCTTTGGTTTGTGGCAAAGGGGCGATCGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-6.50,-9.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [40.0-66.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_7875 flag=1 multi=3.0000 len=1290		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                                     	Insertion/Deletion
==========	======	======	======	===========================	====================================================	==================
      1000	    27	 100.0	    52	...........................	CTTTGGAATCTTCATGCAATCCTTGTCCAATCACTTTAATGTTCAAAATTCA	
      1079	    27	 100.0	     0	...........................	|                                                   	
==========	======	======	======	===========================	====================================================	==================
         2	    27	 100.0	    53	AAATTCCTTGTTTCTTGTTCATTATTT	                                                    	       

# Left flank :   TATATTTTATTTATTAGTGGAAGTCAAATATGACTAAAATATAATCAATGTGATATGGAAGCAAAAAATTATTTCATCATTTATAACCAAAATGGTATCGAAAAAGTAGATGAGCAAGCTTATAAAAACTGGCTTAAATACAATTGGAACAACTACAATACAGGTTTAGAAAACGTAGTTTGTATGAACGATAAAGTAAGCACTGGTTTTATTTCACACCTTAGTTCCGAAAATGAGTACAAACCATTTGTGGTAATGTATTATCCGGGGGGTAATTTAAAAAGTAAACATGTTATTAAAGAGTATTATGCTTCTTGGGAAGAAGCATTGAAACGCCACTTACAATTTATTGAATGTGGTTATTGTTCCCTAAAACAAAATGCAGCCTAAAATTTTGTTTTAATTAATCCAAAGAAGAGAGCCGCGCCACGGCTCTCTTTGTTTTTAAGTATAGCCATCAAAATAATCTTTTGAATATCAAGTTCTAAATTCATCATTTG
# Right flank :  TATTTCATAATATAATGAGAGGAGGAGAAAAATTAACCGCAAAGTCCGCAGAGAAGTCGCTGAGAACGCAAAGTTATACTATTATCATCCCCTAAAAATCATCTCATCATTCCACCACCAATAACTTTCCTTCATAAGTTTTTGATGTTTTATCAATGGTGTACTCCAACGCTACAAAATACATG

# Questionable array : NO	 Score: 1.77
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.28, 7:-0.4, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAATTCCTTGTTTCTTGTTCATTATTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:77.78%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [76.7-61.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.27,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_78726 flag=1 multi=8.0000 len=1010		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence   	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	==================	==========================================	==================
       682	    18	 100.0	    42	..................	GGGAAAAAACGCCTTTAATATAAATTTCACAATAGTTAATGC	
       742	    18	  94.4	     0	..T...............	|                                         	
==========	======	======	======	==================	==========================================	==================
         2	    18	  97.2	    43	ATATCTTAGCAATTTTTA	                                          	       

# Left flank :   CACAACTTGCGATTAGTTTCTCGAATGACTCAAGATGCCATCATTGGATCTAAATAAAGTCCACAAATAAGCAATATTTTTATTTTTTTTCAACATTTCTTTTTTTCAGATCATGATTTGAGTAGTGCAAATAATCGAATTGTACCTCCAGGAACAAAGCTCGATTTTACCTTAACTCGAACAAAAGATAAGTTTTATCTTATGTCTCTAGAAACTGATCAAGAAAAATATAAGGCCAAAATTCTAAGCTGCATTCTTTATTGTCCAATTGGAATCATGTCTGAAAGATTAACCCACGACATTTACAGCAAATGGGAGTCAATGCCAATCAAATACTTTTTTAACAGACTTCTTGTTAAAAGTCTTACAATGCCGCTCTCAAAAGCAGAATTTCTTTCAGGTAGAAAATTGTAATACAATTTATATTTAAAAAAAAATGAAAATTTCCTATATATATGAAACATAGAACATTAAAGTTTGCATAAGTACATTGAAAATTG
# Right flank :  AGAAAAAAACGCCTTTTAATGTAAATTTCACAAAAGTTCATAGACATGCTAAAAATAATATTATGTATTAAAACTATCTTATTTTTATTTCAGATAATCTTTTTCCCGAATCTGAGAATCCTTCTCGAGTTTTTATTGTGATTGTTGAGTCAAGTGCCTATCTTGGAAGTTATGCAAAATCACCTTTTACCTTTAGACGAAACTGGGAAGTAATTGGGGGAGTATTAGATTCCACACAAAATACTTTACAA

# Questionable array : YES	 Score: 0.17
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:-1, 7:0.11, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATATCTTAGCAATTTTTA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:83.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [78.3-76.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_78248 flag=1 multi=4.0000 len=2695		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                    	Spacer_Sequence                                        	Insertion/Deletion
==========	======	======	======	===================================	=======================================================	==================
        90	    35	 100.0	    55	...................................	GGAGCCTCGATCTGTCCCAGATTTGCCGGAGTTACCTTTGATCTCCCTCCAGACA	
       180	    35	  91.4	     0	.......................C....G...C..	|                                                      	
==========	======	======	======	===================================	=======================================================	==================
         2	    35	  95.7	    56	CCTCCTCAACAACTTGAGCAAGCGGTAAAAAATCT	                                                       	       

# Left flank :   AAATTGAAAGGGAAAGAGGCCAAAATCGGTCAAATCCGTTTGGGGCGCCAGGATTGAAAACACCGCCAGCCCAACAAGGGGAGTTATCGC
# Right flank :  TACAACCTCGATCTGTCCCAGATTTGCCGGAGTTACCTTTGATCTCCCTAGCACCACCCCCTGTCGCTACTCCTCCAGTCCAGACACCTCTAGGGGGCACCGGGGGGAATGTGGCGACTCCGGCGACTAACCAACGTCAGGCACAGGGCGCGGGCAATGGTCGAGGGGTGAGTCCCCCGGCGGGGAATGGCCGAGGGGTGAGTCCCCCGGCGGGGAATGGCGCTGTAGCCAGCCGTCCAGCTAGTCCCAGGCCAGTCGCTTTGCCAGAGTTACAAGCGAGATCGGTTCCTGATTTGCCCGGATTACCCACGCCAATTGGCCCCAAGCTACCTCCTGCTCCGAAAGCTGAACCGATCGCCCAAATTCCGGCTTCGGCGCTGACACCCCGTGGAGTTCCTGATTTACCCGCTTTACCCACGCCAATTGGTCCTGCCCAACTACCTCCACCCCCAGTGGCACAGGTGCCCCAGGCACCACCACCACCACCACCACCACCGGAT

# Questionable array : YES	 Score: 0.68
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.79, 5:0, 6:0.39, 7:-0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCTCCTCAACAACTTGAGCAAGCGGTAAAAAATCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.14%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.90,-1.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [45.0-45.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_78726 flag=1 multi=8.0000 len=1010		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                    	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	===================================	=======================================	==================
       225	    35	 100.0	    39	...................................	CAAGTTGTGCAATGTTCTGCCACCCGTAAAGGCGATGAG	
       151	    35	 100.0	     0	...................................	|                                      	
==========	======	======	======	===================================	=======================================	==================
         2	    35	 100.0	    40	ATGATGGCATCTTGAGTCATTCGAGAAACTAATCG	                                       	       

# Left flank :   AATTTATATTAAAGGCGTTTTTTCCCTAAAAATTGCTAAGATATCAATTTTCAATGTACTTATGCAAACTTTAATGTTCTATGTTTCATATATATAGGAAATTTTCATTTTTTTTTAAATATAAATTGTATTACAATTTTCTACCTGAAAGAAATTCTGCTTTTGAGAGCGGCATTGTAAGACTTTTAACAAGAAGTCTGTTAAAAAAGTATTTGATTGGCATTGACTCCCATTTGCTGTAAATGTCGTGGGTTAATCTTTCAGACATGATTCCAATTGGACAATAAAGAATGCAGCTTAGAATTTTGGCCTTATATTTTTCTTGATCAGTTTCTAGAGACATAAGATAAAACTTATCTTTTGTTCGAGTTAAGGTAAAATCGAGCTTTGTTCCTGGAGGTACAATTCGATTATTTGCACTACTCAAATCATGATCTGAAAAAAAGAAATGTTGAAAAAAAATAAAAATATTGCTTATTTGTGGACTTTATTTAGATCCA
# Right flank :  GGTGAAGATGTACGATATCTTCCAGCGGTAAAATCGAATGTGTACAATTGACAAAATCGGGAATCGAACCCTTAACCATTTGTGTGGGAATCTGAATACGTTCACCAACAGCTATAA

# Questionable array : NO	 Score: 2.58
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.39, 7:0.3, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATGATGGCATCTTGAGTCATTCGAGAAACTAATCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:60.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.90,-4.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [60.0-80.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.64   Confidence: HIGH] 

# Array family : NA
//

>k141_79726 flag=1 multi=8.0000 len=1156		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence          	Spacer_Sequence                                                  	Insertion/Deletion
==========	======	======	======	=========================	=================================================================	==================
       656	    25	 100.0	    65	.........................	ACTGCGGATTGGCTTGCCAATCCGCCACTCCAACCGACACGGGTTCGCAGGAACGCTTGTGAAGT	
       746	    25	 100.0	     0	.........................	|                                                                	
==========	======	======	======	=========================	=================================================================	==================
         2	    25	 100.0	    66	TGGTGACCCCTACGGGACTCGAACC	                                                                 	       

# Left flank :   CGCTGCAATCGGCTGCATGTCAAAGCGTTGGAATTGGGCCTGCCAGCCCGGGCCCCAGCCCGCCACCAGCACCTGTTCCAGTTGTTTCGGCGGCATCGGATCCGCTTCTGACCGCAGCCGCGCCAGAATGTCGGAAAGCTCGCGCGGCAATATTTCGCCGCCATCCATCGAGATCAGCTGCCCCAGCTTCATCGCCGCGCCGCGCAGACGCGCCAGTTCGTCCGCCAGGCGCTTGGCATTGGCCGGGGTGAGCAGCAGGCTGGGCAGATCGGGCCGCTGGCCTTTCAACATGTCCCGCGCGCCGCCACCCAGCACATTGCCGGCAATGCCGCCCGCCAGCCGGCCAAAACCGGCAAGCCGGGACAGGCGGGACGAAGGGACAGTGCGCGGGCGGGTGGGGAGCATGGCAGGGCAATGCCTTACCGCGCCGATCAGCGCCACTGCGGGCGATTGCCACCCCCTGAAACACAAAACGGCAGGCATCGTGCCTGCCGTCATGG
# Right flank :  CGTGTTTTCGCCGTGAAAGGGCGACGTCCTAGACCGCTAGACGAAGGGGCCATCTGCGGGAGGCCGCCTGTTATTGGGCATGGCGGCTGGCGTCAACCCCCATAACCGATTTGGCGTGGATAAGTGGCTCAGTTGGCAAAGGCCGCGTCATCAAGTTCCAGTTCAACCGCGTCGCCGCCGGTCCAATCGTCGCGCTTCACCCGGCCGGCAATGTGCACCGCGCGCCCGCCGCCCCGCCCATCGGCGCCTTGCAGCGCGGCGCCTAGGTCGCTGTCGCCGTGCCGCCAGGCCATCGCCTTGATGCGGCCGCCATCGGCGCCCGCAAGCTGCAGGCGCAGATGATTTTCACCCACAATGCGCGCATCGACGATCTGGAATGGCCCGGC

# Questionable array : YES	 Score: 0.53
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.34, 7:-1.7, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGGTGACCCCTACGGGACTCGAACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [5,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-3.60,-4.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [36.7-36.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_89083 flag=1 multi=2.0000 len=1029		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence          	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	=========================	===================================	==================
        99	    25	 100.0	    35	.........................	GGTTATCGTTTTATCAATAGGCTTGCCTATGCTTA	
       159	    25	 100.0	     0	.........................	|                                  	
==========	======	======	======	=========================	===================================	==================
         2	    25	 100.0	    45	CCCAAACAAGCGTGCTATTGTCTGC	                                   	       

# Left flank :   ATCAATATAGGTAAACTCACCTACGATTTCGCCATTGGCTAATTTTTCGTTTAGGGTTTCAGTATCTAAATCGTATTCATAGTACGCACCTGTATTAGC
# Right flank :  CTGCGGTTATCGTTTTAACGATAGGCAAACCTATGCTTATAGGCAAATCATTGCTAAGGATACTATTAGTTATTACACCTGTAACACTAACATACGTTTCAAAATCGGTTTCAAGTTTGGTAACTTCAAAACTCCTTTCTACATTTTCTATTGGGGAGATGGGTATATTCATTTTATTTTACTATTTTTATGTATAAATTAAAGTAGGTATTACAGAATAATTGCTATCTGCTGTTATGGTAACTAAGTTAGGTAGTTTGTTAGTATAGCACCCTAACAAACGCAAATTTGTTGCTGATGTATAGCCATTAGCCCCTGTTTCAAACCATTCGCCTGTGCCTACAAATATTTTGCAGTTACTCATTTCAATATACTCAGGTCTGATATAAACCAAGTTTTTAAAGTCCGAGCTAAAAAGGTTTGCCGAGCCTGTAATATTGATTATGCAGTCTTCAAATATCACCCTTTTTGCACTAAGTATGTTTGTTAAATTACCACTT

# Questionable array : NO	 Score: 2.93
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.34, 7:0.7, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCCAAACAAGCGTGCTATTGTCTGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [6,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.10,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [66.7-61.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_88005 flag=1 multi=45.2403 len=1173		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                     	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	====================================================	=====================================	==================
       145	    52	  94.2	    37	....G..T.............T..............................	ATAGTAGGGTGGGCAATGAATTGCCCACCCTACAGAA	
        56	    52	 100.0	     0	....................................................	|                                    	
==========	======	======	======	====================================================	=====================================	==================
         2	    52	  97.1	    38	CTTTTTCCTTTGTCCTTTGTCCTTTGTCCTTTGTCCTGATAGTAGATAGTAG	                                     	       

# Left flank :   CCTGCACTTGCGGTTGTGTGACAGCATTTAATGCCCGTGCAGATGCCACAACAAATGGCTGGCGCCCAGCTAAAACATCGAGTAAAACATCACTATCTAATAATACGCGCATCAACTGTATTTTTCCTCCAGGTAATCCACATAAGACTCTATGGTATCTTCGGCGTCTAACTGAATGACCCCAATTAGGCTTTGAGTCCAGGGAGCGAGACCACTCAAAGACTGCACATTTGGAGTTAGAGAATTAGATAGTAGCGTTTCTTGTTGAATTGAACTCAACAACGACTGCACTAAATGCCAGCGTTCCTGAATGGGTAACTGTAACGCCTGTTTTTGTAATTCCTGTAATGTCATTAGCTTTTTGTCCTTTGTCTTTTGTCCTTTGTCCGGGGTGCATCTCACTTTGGAAACCCCCCTTCTCCCCCCTTGGGAGAAGGGGCTGGGGGATGAGGGCACACTCGCCTAACTGAGATGCACCCTTTTGTCCTTTGTCTTTTGTC
# Right flank :  GGGTG

# Questionable array : YES	 Score: 0.83
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.85, 5:0, 6:-1, 7:0.78, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTTTTTCCTTTGTCCTTTGTCCTTTGTCCTTTGTCCTGATAGTAGATAGTAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:61.54%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-1.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [1.7-48.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_89546 flag=1 multi=43.8854 len=2079		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence       	Spacer_Sequence                            	Insertion/Deletion
==========	======	======	======	======================	===========================================	==================
      1213	    22	 100.0	    43	......................	TTTTCCTAAAGGTTGTGGTTCTGGCTTCAGCCCTTCCGGAGGC	
      1148	    22	 100.0	    41	......................	CGGATGTTTGTAGTTGGGCTTCAGCCCTCTTTTCCTAAAGT  	
      1085	    22	  90.9	     0	...........T..T.......	|                                          	
==========	======	======	======	======================	===========================================	==================
         3	    22	  97.0	    42	TCGTAGTTGGGCTTCAGCCCTC	                                           	       

# Left flank :   CGAGTTTTCTGGCAACTGGAACGACCAAGGAAACCATTATCCCGCCAATGACTTTAACCAGACCTCTCATCCCGCGCATTCAGACTGGCATCAAACCGATTATCCATATCATACCCCAGAACCGATCGACTATTCCGGTGGGCATCACCATCTAGGGGGAATGGAGCATCAACCACAGCACCTAATGTCCACTGATAACTATCATATCAGCAAAGATGATAAAAACAGTGTAGAGATTTGGCAAAATGGAGATGTTTGGTGGCACAGTGGGGGAAAAGCGGGACATATAGATGGGAATAACTTTTACAATAACGGCAACCATTATATCGGGAAACTGGGTGGAGATATGAACGTGTATAATGCCCATGATAAATGTGTCGGTTGGGTGGATGCTCACGGTCGAGCTTATACCACAAATGGGGTATTATTTGCGGAGGGTGGGACAGCTCGATGGGCGGCGGCGGTTTTAGCTTACAATACTTGCAGTCCTGATAGCTAGT
# Right flank :  TTTCGTTAGGGTGCATCTCACTTGGTAAACACCCCTTCTCCCTGTTTGGGAGAAGGGGTTGGGGGATGAGGGCAAATTTGCCTAACTGAGATGCACCCTAACGTTAAACTTCAATAGCATCTGCTATGATATAATCAGATAAAATTACACCGGGCAGGTCATATGGTGCAAATTCAACAACCAGAGATTCCCACCTTCCCGCCGGGAGATTTATGGAGTGATGAACCGCCATTGGAAACTTATCGTCATTTAGAAGAACTAATTATCCTCCTGACCAGCTTAAACCGGTTTTGGTCAGACCGCCAGGATTTTTTTGCGGCGGCCAACATCACAGTATATTATAGCAGCCAACAGCGGAAAAATAAAGATTTACGAGGGCCGGATTTCTTTGTGGTTCTCAACACAGAACCCCGGGAGCGGAAAAGCTGGGTAGTATGGGAGGAAGATGGGAAATATCCCAATGTGGTGATAGAGATACTATCCGATAGTACCTCATCAGT

# Questionable array : NO	 Score: 1.93
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.85, 5:0, 6:-0.25, 7:0.24, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCGTAGTTGGGCTTCAGCCCTC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [7,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [50.0-46.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_92911 flag=1 multi=3.0000 len=2073		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence                         	Insertion/Deletion
==========	======	======	======	========================	========================================	==================
       248	    24	 100.0	    40	........................	GACACACGGATACATCTTGGGTTTACTTCGCAGTTTCAGC	
       312	    24	  91.7	    32	............C..........T	GAAAGCAGGGTATTCCATTTACTTCACAGTTA        	AGA [333]
       371	    24	  95.8	    23	...T....................	AACACACGGATACATCTTTCAGC                 	
       418	    24	  91.7	    12	............C......C....	GACACTCAGTTA                            	
       454	    24	  87.5	     0	............C......C..T.	|                                       	TGCT [468]
==========	======	======	======	========================	========================================	==================
         5	    24	  93.3	    27	CATCTTGGATTGGTGCATCTTTAC	                                        	       

# Left flank :   AGTGCTGGGGAGGTAAAGTGCCCGTAATGAGAACTAGACGCCAACTTTCGGCTGTCAGATCGGCGTCACCCTTGGTTCGGGTACTCGGGTACGACGGCACATGATAGCCGCTACTGTAGCTATAGAATGGCTGATGGAGATGGCGTGTCTAGTTTTGGCCTAGCTCTTTCTTGCTGACCATGCGACTTATGACGAGTATACTGAAAGCACGATATGAAAGCAGCATATTCCATTTACTTCACAGTCAC
# Right flank :  CAACATACATCTTCGACTACAGATATCAGTCACATTCATTCAGTTCATGTTTACGGATACTCGACGACATAGCTTGGGTCTTCATATTGATCAGGGTCTTCAGGAGGAAAGATGCCATCCACGGTATCGCCGACGCCGTTGACGACATGGTTGATTATACCTGGTCCGCTCAAGTTCTTTGTCATGATATGGCGCAATTTGACACCAAGAGTCTCTGGCACCTCCCAGCCATTCTCAGTTACAATGGAGGGGTTGTTGCGATTGTATACATATACTCCTCCAGCCTACAACTCATGTGTCGTGACGGCATCGTTAACTTTGTAGGCAGACCAACCCAAAGTCCCATCAGGTGTAGTCCAATCTGCTTGTGTGGGTGGATCGTAAGGAAGTTCGTTCTGAAAGAAGTATACTCGTCCTCTTTCACCATTCCAAATGACATTGTGTTCTTGAAAGTGCTCCACAAATAGACCATTCGCTGTCACGTCGTTTCCATTCACCAC

# Questionable array : YES	 Score: 0.43
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.66, 5:-1.5, 6:0.25, 7:-0.29, 8:0.8, 9:0.51,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATCTTGGATTGGTGCATCTTTAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:58.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,-0.30] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [5-9] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [63.3-65.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_102202 flag=1 multi=9.0000 len=1201		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                      	Spacer_Sequence                            	Insertion/Deletion
==========	======	======	======	=====================================================================	===========================================	==================
       181	    69	 100.0	    43	.....................................................................	CAGTTTTCAGTCGTTTGGAATTGTCCTTTCGAGTGGAAATGTG	
        69	    69	  98.6	     0	...C.................................................................	|                                          	
==========	======	======	======	=====================================================================	===========================================	==================
         2	    69	  99.3	    44	CAATCAAAGACATTTATTTTTTTGCGTTTTGGTTGCACAAAAGACAATTATTTTTGAGAAAAATTGTCC	                                           	       

# Left flank :   AGGGAAAACAGCGGTGCTGCACTGCTGCACCGCTGTCTGCACTGCAATCTGTAGCTGCTCTCACGCAAAAACTCAGCGTTGCCAGATATTTTAAGTAGTAATTCACCCAAAAAAGCAACGCAGTTGAAAATGAAAAAAAAACGAATGGTCACGTCAGAAGCTGAGCAAACTTTTAACTGTTATGGCTTTCATGAAAAAAGTTTATTCATGTGTATTCATGAAATGTGAATTCATCCAATTGGGCGACATTTCGCCAAATCTGGCAACAGTAACAAAAACATAAAAGCGAAAGATGAGAAAAAACCGATCGAAAAATGCCTTGAATTATTTCCGATATAATTAAAGAAAATGCAAATCTTTATCTTATCTTTGATTTTAACAGAAGGTCTCAAATGGATAAAACAATCACAAGCAATTGATAAAGTAAAGCTTCACTTTCATATGAAAATTCGTTTTTCATATTTCATTCGTTTGGAATTGTCTTTTCAAATGGATTTGAA
# Right flank :  C

# Questionable array : YES	 Score: 0.25
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.96, 5:0, 6:-1, 7:0.09, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CAATCAAAGACATTTATTTTTTTGCGTTTTGGTTGCACAAAAGACAATTATTTTTGAGAAAAATTGTCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:72.46%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-4.50,-4.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_104802 flag=1 multi=3.0000 len=1094		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                             	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	============================================	======================================	==================
       172	    44	 100.0	    38	............................................	CTTACAGTGTCTTTGATGTTTATCTCATTAAACATGTA	
        90	    44	  97.7	     0	..................................C.........	|                                     	
==========	======	======	======	============================================	======================================	==================
         2	    44	  98.8	    39	CATGGATTATAATACACGTATATAAATGAACTTATTCATCTGGA	                                      	       

# Left flank :   AAATGGCCTCACATGCAACATGAATTCGTTTTGAGCCGTCAAGTACCACTGGACCAATATACGGCCAATCGATTGAATTGAGCTTCCTTTTCATGGCATCAAGGAAAATACATGAGCCGTATCTATTGAATGCAGCTCTCATTGCACCCGTCATCCAAACAATGCCTGTTGCATGCTGGCCCTTGGCTGCATAGGAAATCCGGTAGTCAAAGGTGCTATCTCTTCCTTTTAACTCCTTCATGTATGCATTGACTTTCCAGGTGCCATTTCCTTCCTTGAACAAGCCAACGAGCACATCACGTAGAATCTCACTAGCTTCAAATGTAGCAGCAGGCTTTTTGCTTCCCTTGTTTGTTGGTTTCATTGGCTGCACTTGCTTGAGCATGGTGTGAAAGACAAACCTGGCACTCAGATCAAAGCTGTACGGTACGTGTATGCGTACAGTACGTACAAACACTGTAGTGGTAAATGAAAGCATTAATAATCTTATGAAACATCTC
# Right flank :  AGAGATGAGATAGAGCTTGCCAAGCATTGTGATTTTTGACCAATCAT

# Questionable array : NO	 Score: 1.85
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:0.25, 7:0.46, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATGGATTATAATACACGTATATAAATGAACTTATTCATCTGGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:72.73%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-1.20,-1.30] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [46.7-68.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.68   Confidence: HIGH] 

# Array family : NA
//

>k141_105385 flag=1 multi=10.0000 len=2274		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                	Insertion/Deletion
==========	======	======	======	============================	===============================================	==================
      1306	    28	 100.0	    47	............................	TTTCCCATTTGGGCGCCAAACACAATAACCCAGTAAAAAACGTACCA	
      1381	    28	  96.4	     0	.........................C..	|                                              	
==========	======	======	======	============================	===============================================	==================
         2	    28	  98.2	    48	TCCCCACAGCGACAAAACTTTCCCCATC	                                               	       

# Left flank :   TCCTCCCCCTCCTCCTCCCTACATGATGAGCATACACCCTCTAGTGTTTCCCTGCTGCCTCTTTTAAAGTTGACCCAAAAAGAACACCATCATTGACGAAAGATACCTTTTCGGACCCGACAAACGACAATCCCAAAAAATCCGTACCAAATTCTCAAAACCTCCCCAAAATCCAACCTTTCCTCCCATAAAATGTTTCAAATAAACTAACAACATGTTACATAAAAACTGTAGCATCCATACTCCATAGACAAAAACCCATTAGACAAAAGTCACAACACCATTCCTCCTTTTGTTCATTCCCACCCCTCCGTCACTTTCCAAAAGGTAATGGGTCTACCCACCCCAACCACCAAACTTTATTCACGAGTAGAATAAAATAAAAACACCTTTGTTGTATACTCCGCGACCGTACCTCTCGTACAAACTAAATCCTCCTAACAAATAAAACTTATGAGGAAAAACCCCAACCCACAACATGTAGAATACCTACGCACACT
# Right flank :  CAAAACCACCAACAATCTCCTTAAGTATCACCCACGATACAATCTCCATGACAATCCCTCCAATACCAAACCTACAATAAAAGCTGTACAACCTACATAAAACCTACATAAAACCAAACGCGAGTTTATTTTAACAATCCTCTTCCTCCTCCATTCAAACTACCTCCCTGGTATGAAATATCACGACAAGAATTGATAAAAGGTTTCAACCAAAAATTCGCAGAGCGCGATTTAAGCGGGAATGACCACAGTACACCACTACACCCCCGGTACTGTAGTGAAAATATTTCCCTAATTACTACTGTACCAAGTACAGTACGAAGCAAATCACTAAACTCCAACAGTGACAGTGAGCCAAACATCATCACCTAACAATCCTATACCCTCCAAACACAAGGAAGTTCCTCTTAAACAAAACATCCATGCTACCAAAATAGCTCCTCAAAAGATGTCCTTCACCACAAAGGATACGAGACTAACACGACGAGCTACAGAACAGC

# Questionable array : NO	 Score: 2.14
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:1, 7:0.03, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCCCCACAGCGACAAAACTTTCCCCATC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [8,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [61.7-56.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0   Confidence: HIGH] 

# Array family : NA
//

>k141_109529 flag=1 multi=3.0000 len=1274		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                     	Spacer_Sequence                                              	Insertion/Deletion
==========	======	======	======	====================================	=============================================================	==================
       362	    36	  97.2	    61	.............................G......	ACTGCCCCAAGCTAAAACTCCCACGTGGTTGCGTCAATCGCCGATACACTGGTCTGACAAC	
       459	    36	 100.0	     0	....................................	|                                                            	
==========	======	======	======	====================================	=============================================================	==================
         2	    36	  98.6	    62	AGAAGTCTTGGGTTCCGACACACTGGATCTAGCAAC	                                                             	       

# Left flank :   TGTTGTTAGATCTGGTTTAGCGTAACGAAGTTCCGCTCGCTCTCACAGGAGACGCCTACCATAACCGAGTGGTGTTGGCCACGCAATCTTGGGGCGATATCTTTGTGTTTTGTGAAGCTGACTGGCATTAGAGCAAACCCCAGTGAAAACGGCATCGAACCGACACTAACAGCATCGAACCGGCCGCCAACGCAGCTACTAAATCAAAACGCCAGCTCTGTTTACAACATTCTGGTTGCAACTAGAAGCGACTCGTAGCTCTAGTTTTAATTGGAAGTCTTGGTTAGGTTCCGGCGCAACGCTAGAGGCCACAAGCTAAAATCCCACGAGGTTGAGTCCACCGACACACAGGTCCGACGATA
# Right flank :  CCGCTGCACCAAGCAAAAATTCCCACGAGGTTGAAAAACGAGTCGGAGACAATGAAACATGATAAGGCCTTTGATGAGCACAAGAGTGCTGGGGGACCAGAGTGGGACTCCATGGACCATCGTCGCAGCTCTGCCCGCATTGAAGCACGAGCCACGAAGACGGGCACGATCAGGGAAGAAGCCTTGAAGGACGAAGAGGATTCCTTCGGCTCACTGGGCGCCGCCCCCGACGACAGGAAGCCCACGCCAGACAGGCGCCCTAGCAAGACTACCAATCCAATGGACAACAGTGATGAGGACACCGAAGAAGAAGAGATGGGCCACGACACTGAATCGCCAATTGCCATGCCTCGTGACCTCGTGCAACGCGTCAAGGACATCACCGTGTCCTGGTCAACGAAGAAGCACAATGAGGAGGCCATCGTCCATGTCATGGAGTGGTTTGAAATCATCACGGATGACGATGAAGACAAGCTCGAACAACTCACACGTCAAGCAAG

# Questionable array : YES	 Score: 0.83
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.93, 5:0, 6:1, 7:-1.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGAAGTCTTGGGTTCCGACACACTGGATCTAGCAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [10,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-4.40,-2.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [45.0-53.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_109722 flag=1 multi=3.0000 len=1128		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	===========================	=============================================	==================
       650	    27	 100.0	    45	...........................	GAAAATGCAGTTGAAAATCCGCCGCATGGTGAAGAAGGACATGTA	
       578	    27	 100.0	    45	...........................	GAACAAGCAACAGAAAACCCACCCCATGGTGAAGAAGGACATGTA	
       506	    27	  85.2	    45	...........T..CA.T.........	CAAGAAGTAGGAGCCAATCCCGCTCATGGCGAAGAAGGTCACGTG	
       434	    27	  88.9	     0	..C..A..T..................	|                                            	
==========	======	======	======	===========================	=============================================	==================
         4	    27	  93.5	    45	CATGTGGAAGGCGATGAACACGATCAT	                                             	       

# Left flank :   TTCATCCGTTTATCAAGTGGATGCTCCCACAGGAAATATCAATTACAAAGAGGCGGAAGTTTGGATGAAGCATCCGTTTGTTGAAACGGCCATTCCTTTAGCGTTTGGTGACAATTACCGTGGATTTAAAATTGTTGGAACAACGCCTGATTACATCAAAAAATATACTGCAACCATTACAGAAGGCAAAGTTTTTGAAAAGAACTTTGAAGTGGTCATTGGAAGTGAAATCGCAAAAAAGTTAAACATAAAAGTAGGTGATAAATTTTTTGGAACGCATGGCGATGCCGAAGAAGGACACGTTCACGAAGAATATGCTTATGTAGTGGTTGGAATTGCATCCTCAACAGGAAAAGTAATCGACAACTTAATTTTGTGCACGGTTGATAGCGTTTGGGAAATGCATTCCGGACATGATCATGGCGGAGAAGCTCATTCTGAAAATGAAAATCCACCGCATGGCGAAGAAGGACACAT
# Right flank :  TTCGGAAGAGAAAGAAATTACAGCCGTTTTAATAAAATTTAAAAACAAAATGGGCATCATTTCTTGGCCAAGAATGATTGCTCAAAATACCAAAATGCAAGCAGCTTTACCAGCAATTGAAATCAATCGGTTATTTTCATTGTTTGGAATTGGCTTACAAGCATTACAGTATTTGGCTTATGGAATCATGTTGATATCGGGCATTAGTATTTTTATTGCACTTTATAATCGATTAAAAGAACGAAAGTATGAATTTGCTTTATTGCGAGTTTCAGGTGCTTCAAGATTACAATTGTTGGGTTTAGTTTTGTTTGAGAGTCTGCTACTTTGTGTGGTTGGATTTATACTTGGCACTATTGTTGGAAGGGTAGCATTGGTTTTAATATCTTCTACAACCGATGAACAGTT

# Questionable array : NO	 Score: 2.43
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.68, 5:0, 6:0.28, 7:0.07, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATGTGGAAGGCGATGAACACGATCAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.85%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.50,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [7-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [71.7-53.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_110346 flag=1 multi=4.0000 len=1329		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	=====================================	=======================================	==================
        37	    37	 100.0	    39	.....................................	AGATTGAACGATCCCTTTGTAAACCCTCTCCTCGTTAAA	
       113	    37	 100.0	     0	.....................................	|                                      	
==========	======	======	======	=====================================	=======================================	==================
         2	    37	 100.0	    49	GTTGCAATTCTGCCTTAGCCGAAGGGTGGTTTACTAC	                                       	       

# Left flank :   AAATTCCTCACTTACCCCGTTTCCAATGTAGTACATG
# Right flank :  CTCGTATGAGTTATAATATATAAGTGCCTGATTACAGGCATTTTCCAGCCGTGTCAAAGAACACGCTTCAAAAATACGCCATTTTTTTCTTTTTGAAAGGAAAAATCAGCGCCAAATGATGGAGCCGTCTTCCCTCCTGCACGACCTCTGCCCCGGGCTGCATTTTTCTGGCACGATTCTTGGTTTCTATATCACATGAAACGCATTATGAAAAAAAATAATGTTTTTCGCAAAAACAAAGAGGATCAGATGTTGATATATGATAAATGTTTATATGTTTGCGTCCGGTTCTTATTTTTTTACTAATGAAACACTATACAAACATGAAAAACAACATTTACACTTTCCGCCGCTCGCCGTAACGGCGCCCCAGGCAAGGCGGATCTTTCCGATTACATTTTGGTAACATAACGATCAGCGCGAGGAACAGCAGTTCCTCAAGGCAGTGGCATGTCTGTCTTTTGCTGAGGTTTTGAAAAGCTGCACGGCTTTTCCGGAGA

# Questionable array : NO	 Score: 3.19
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:0.3, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTTGCAATTCTGCCTTAGCCGAAGGGTGGTTTACTAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.35%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-6.60,-2.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [36.7-63.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_110589 flag=1 multi=6.1596 len=3161		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                                      	Insertion/Deletion
==========	======	======	======	==========================	=====================================================	==================
       615	    26	 100.0	    53	..........................	CGCTAGTTGAAAAAGGTTCAAATGAATTTTTTCCCAGCTTTTTCGCCCGCATT	
       694	    26	  88.5	    40	..C.....C..............T..	GGAGAAACAAAAAAGGTTCAAATGAATTTTTCCCCACCTT             	
       760	    26	 100.0	     0	..........................	|                                                    	
==========	======	======	======	==========================	=====================================================	==================
         3	    26	  96.2	    47	GGATAATTTCTGAACCTTTAGACCAG	                                                     	       

# Left flank :   GCCAGTTCTCGTCGTTGGGCTTCCGTTAAAACGCCGCGAATTTTTAACATTTGTTCAAATCGCAAATTCGCCATTTGCTGATTTAATTCTTGCAGTTGTCGATGTTGATTTCGCAGTTGACTATCAGAGGCATTAGAACTCATCAACTGACGCATTTGCTCGTGCGATTGACGACTTTGTTCCCGTAACCGATCAAATTCACCTTGAGATGATTGACGAATTTGCTCAATTTGTTGTTGTTGGTCAGCAGTCAAATTCAACTCAGAAATCCAGCGATCGCCCCCTCTACCTGAGTTGCCCGGACCGCGATCGCGACCCCCAGGACCTTGGGCGATCGTTTGCTCAAACCGATCGTAAGGCAAAGGTTCAGGAGCCGCGATCGCTGCTGTCCCACTCAGCGCCACCATCAGAATAGCCAAACTAGAAAAACGACTTAAGAACATCATAGTATCCTCTTCAAACAATTAAGTAAATCAGTGAATTGATGAATTTATCCTGCG
# Right flank :  GCGCTATTTAAAAAAGGTTCAATCTAAATCAATCAATCTGAATCAAATATCGTGGTTAAAAATCCAAATACCAATCTTGGCTATCTTGATCCAAAACCTCATCCCAAGTATTTTCAATAAAAACTTCTAAACTCGCGAGATCGGTGGTCGGGGCTGGCGGAGTTAACAGGCGACTGGTTCCCCAAGCCATCAGTAAACCAGCCGCGATCGCTGGTGGCATCAGCCATAAACTCCGCCGCCGGAACGGAATCACTGGAGACTTTCGCACCGATTCTGACAGGGGAGTTGCTGTCACCGCCGCCATAATCTGTGCTGCCAAATCCTGATTTTCTGGCGGCACCTCTGGGCGATTTTGGCGGAGAAACTCCACCAGCTTTTGGTCATTATTTGGCAACTTATTCATAATTGAACCCCCTGTTTCTCTAAAAATTTACGCATCGCCGCCCGCGCATGAAATAAGCGAGACTTCACGGTTCCCACCGGCACCGCTAAAATTTCGG

# Questionable array : NO	 Score: 1.88
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.81, 5:0, 6:0.26, 7:-0.28, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGATAATTTCTGAACCTTTAGACCAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:61.54%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.30,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [68.3-73.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_110721 flag=0 multi=28.7865 len=3270		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                       	Spacer_Sequence                  	Insertion/Deletion
==========	======	======	======	======================================================	=================================	==================
         1	    54	 100.0	    33	......................................................	TTCTCAGAAAGCGCTAGTTTTGAACCGGCTCAA	
        88	    54	 100.0	     0	......................................................	|                                	
==========	======	======	======	======================================================	=================================	==================
         2	    54	 100.0	    43	ATAGTTGAACCTGAAGTTGAGGCGATCGTTCAGGATGAGAATTTAGATGATTTC	                                 	       

# Left flank :   |
# Right flank :  CCTAGTTGCAGAGGATTCCGAATTTGCCCTTGCTAATGCAGAAGATAACTTCGCGATGTCTGCGGCTAACCTGGACTACCCAGAGGTAGATGTATTCCCAGAAACCGGAACCTTAATTAAATTAAATTCTGAATCCTCAACCTCCGAGAACCCTCCCCAAGAAAACATTGCAGAAAGTAGTCAAGACTTTGAAGATATAAATGATATCTTTGGAGATCAGGAACAGTTCTCACCATCAAACAGATTAGCCAACCCCGGATCAAACATTCTGAATCCAGAAGCAGATTTAGCCAAGTTGAAAAATCAGGAGATTGCCAGGGAATATGATGGCACAAACTGGGATGCTGATATCTATATCCAAGCATCTCCCGAAGAAAATTTATTACCAATAGCAGCACCACAAGATGAGCCAGAAGCGGCAATTTTACTTGATGCCAAAACCCTGCAACTATTACAATCTGACCTTTTCAATTTAGAAGGAATTGATACTTCAAACCCAC

# Questionable array : YES	 Score: 1.31
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:0.42, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATAGTTGAACCTGAAGTTGAGGCGATCGTTCAGGATGAGAATTTAGATGATTTC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:61.11%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-7.80,-6.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_111538 flag=1 multi=3.0000 len=1279		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	=====================================	======================================	==================
       619	    37	 100.0	    38	.....................................	CCGAGTATCATTGTCAACAGGTATGAGAGCTTGACTAC	
       694	    37	  97.3	     0	...........................T.........	|                                     	
==========	======	======	======	=====================================	======================================	==================
         2	    37	  98.7	    39	GACCTGCTGCTTGGCTGGAATTCAAATCGATTCCATA	                                      	       

# Left flank :   CAACTTGGAATGACGAAACCCCCAAAATGCAAGAGGAACAATAAGAGGACCTGTAATGAGTAGACCAGTGTCGAGTGTCTTTGATCGCGATGGCCGCATCGTTTCGTTCAAAGCTGAAATTGCATCATGATACTCTTCCGGTGTCATTAAATCGCAAAGATTCACATTGGCTGGATCATAGTCAAACTTAAAGTCAAACCCTTTGAACATGGATAGAGGTATCGTGGTAAGGGCAGGTTCCCCTGATATAAAATGGCAAAACAAGACATGCTTACCTCCATTCACAGATCTTTCGGGCACAGTAATTTTTTCTCTCTTTTGTGGTGCTTGTTTTGGGGACTTATTGCGTTCAATTCTTCCTAAACCTGCAATGACAGGAGTTACTTCTGTGCTTTGTTCTTTATTAGCCACTGCAGGTGGTTCTGAAATGTCAACTGTGACGAGAGCGTGCTCACCATTCGAAAGATTAGTATCCATAGCCACGTCGTCGAGAAGATCAT
# Right flank :  ATGGAGTATCGTTGTCAACAGGTATGAGAGCTTGACTAGTATCCGCAGCTTGGTTGTCTAGAAAATCAGGCGCCGCCTGCAGGGCTTCATTTGCGTGCACCCGATACGAATTTATTCCTCCACCAATGAAGCCTTCCATGACTGGAGCAACATCAGCTTCATTTTGTTTTGCATTGGCATACGTTCCTTCTCCTTCTAATGTTTGGTTGGTCGTGTTTGTAGTATTCATCGTTGTATCGTTTCCGACGGCGCCAACCTGATGGATAGCCGCGCCGATTACGTGCGGGCCTCTTGTAGGACCTGGAGGCCTTCCTATTGAAGTCATTCTCGCCGACTTACTGTAAGTATTGAACCGGTTTGGTACAGTATTACCTTTCCAACGTCTCAACGCAACATTTTATGCTATTTTGCCACGAGCGATGCCATGATACGACGATGAGCGAGCGACGAAAATGTCAATGACGTCACAACCGCGCCGGCAATGCTCCCGCCAGTCGCGA

# Questionable array : NO	 Score: 2.60
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:1, 7:0.46, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GACCTGCTGCTTGGCTGGAATTCAAATCGATTCCATA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:54.05%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.70,-0.10] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [51.7-53.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_115504 flag=1 multi=25.0641 len=1624		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                          	Spacer_Sequence                            	Insertion/Deletion
==========	======	======	======	=========================================================	===========================================	==================
      1311	    57	 100.0	    43	.........................................................	TTGCTCTGACTTATACCGAGAAACCGGGTTTCTAGCAAAATCT	
      1411	    57	  96.5	     0	..G........................................T.............	|                                          	
==========	======	======	======	=========================================================	===========================================	==================
         2	    57	  98.2	    44	CTCTGACAGAAACCCGGTTTCTGAGTCTAGGGGATATGGTATCGGTAACTAGGGGCG	                                           	       

# Left flank :   CTGAATCGGAAGCCGATCGGCTCCAGCACGCTCATCCCCTAGTCATCCCCATACCGCAGAAAACCCCACCAGCCAGAGAACCTCACCAGGGCGGGGGACCAAGGGGGTGGGAGAACCTATAGAGCTAATTCTTTGCTAATCAGGAAACTAATCCAAAGATTAGTATCGATAATTACTTTAATTCGCTGATTTTTTAGCATAAATTTCACTTCTGACGGCTTCCACTTCTTGAGTAATTGTATTTAGGGATAGTTCATCAGTTTTGAAACAGTCTAAAAGGGCGGTTAATTTACTGTCTAACAATTCTTTAGCAAGTTGTTGACTTAATTGTAGTTTTTCCGGTTCGGGCAGTTGTTTGACCAAGGTTAAAACTTGCTCAAAGTTTAGGGAAAGTTGATATTGAGTTGGCGTCATGGTTTCCCAGGGATGAGGGTATAGCAATATTTAAACTAATTTCTGGAAATGGGGTTTATGGGAAAATCTCTGTGAGGATGCACAAA
# Right flank :  ATCGCTGTTGTAACAGTACGAGATTGGGGCGATCGTCAGCAAAAACAGTGGTGGTGATGCTACCTATGCCAAGATTGTTGCCGTACAAGAGCTAGGAATTCCGGTTGTGATGATCCAACATCCACCCATTCCACCGGGAGACATTGTTGCAGATATA

# Questionable array : YES	 Score: 0.20
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:-1, 7:0.09, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTCTGACAGAAACCCGGTTTCTGAGTCTAGGGGATATGGTATCGGTAACTAGGGGCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [13,14] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-10.20,-7.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [66.7-50.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.05,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_118849 flag=1 multi=5.0000 len=1413		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                             	Insertion/Deletion
==========	======	======	======	============================	============================================================	==================
       648	    28	 100.0	    60	............................	CCAATTTGCTTCAAACTCAATTCCCGTTTGCTTCAACCTCAATTTCCGTTTGCTTCAACC	
       736	    28	  96.4	     0	..............C.............	|                                                           	
==========	======	======	======	============================	============================================================	==================
         2	    28	  98.2	    61	TCAATTTCCATTTGATTCAACCTCAATT	                                                            	       

# Left flank :   AAGACATTGCTTGAGAACTTCGGGTAGTATTCGCCGTCAGCCGGATTGAAAGGTCCGAATGTAAACCAGCGTTCATCAAGCTTGTTATCCATGCCGAAGGTGCGCGTTGCCAGAAGGGTTCCGCTCTTGTAGTTGCCCGTGGGACTGATGCCCTTTGCGTCGGGGTGGGTATAGCAGCCCTTGCCTCCGTATACCGAATAGGCCATCCGGGTGTCCCAGAAGTTGTTCAGCTCATCGGTCTTGCCCCCCATGTCAGGGTCATAAATCCTGATATAGAAGGGATCCTTGTAGTCTTTCGGGATTGTAAAGAAGAATGTCTGTGAGAAGTCGTCATCACCCCATGATGTTGCAGCCTTCGGTCCGAAAGTGATCAGGAAGGGCACGTTCTCATCCGGACCCGGCACCTCCTGGGCATTAACCATTGCGGATGCCGCAGTAAGAATGGCAAGAATGAGTGACAGTATAATGCGGGTAACTCTCATTTTCGTCATTAAAAATAT
# Right flank :  TTCCGTTTGCTTCAACCTCAATGTCCTTGCAGACACAAACCTCTCTCACTACACCGCCGGCTCCGGGCAGTGAACTGACAATCAGCTGTACGTTATACCTCCCCGGCAACAGATAGCTCTTCGAAACCTCCAGTCCGGTGGCGATCGTCTCATCCCCGAAGTTCCAGTAATACCTGCCCGGCTCCATCCCAGGAAGGTAGGAGTCTCCCGCATTAAGCAGCAGCGGCTCTCCCACCCTGATCCTGTCAGGCGCAGTGATCACCGGCTGTTCTATATCTGTTATCTCAAGCTCATATGAAGCCTGTCTCTTTTCTATGGCACCTGTGATGAGGTTCACTATGTCAAGGGTAACATCATATACTCCCGGTCCCTTATAGCAGTGTTCAGCCGTTATGCCTGAAGCCCTGGTTCCGTCACCGAAATCCCACTCCCACCGGAAACCTGTTTTCACCGTATCCTGTCTGAGTGCATTCTCCTCTATGAACTCGTAGCAGTAGCTG

# Questionable array : YES	 Score: 0.91
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:1, 7:-1.2, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCAATTTCCATTTGATTCAACCTCAATT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:71.43%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [66.7-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0   Confidence: HIGH] 

# Array family : NA
//

>k141_122072 flag=1 multi=2.0000 len=1218		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence      	Spacer_Sequence                                 	Insertion/Deletion
==========	======	======	======	=====================	================================================	==================
       377	    21	 100.0	    48	.....................	TTTTCTAGCCCCCCTTTTGAAACGGAATCATTTCTTAGCCCCCCTTTT	
       446	    21	 100.0	     0	.....................	|                                               	
==========	======	======	======	=====================	================================================	==================
         2	    21	 100.0	    58	AAAGGGGGGTTGGGGGGATCG	                                                	       

# Left flank :   AAAATCCAAAGAAGGATCGAGGGTTTTGGCATCCTTGAACTTGGCTAGAGCCTCTGGTAACTTACCTTCTCCTGCTATTTTTTCTCCTTCAGAAACCAAAAACAAAGCTCTAGATTTATTCGCCTTTTGTTCCGGGTCATATCCTAAAGACTGATAAAACTTTTGATCCAACTGTTTGGCTTGCTCAAACTTTTCTTTCGCTTGCTCAAAATCCCCCGTCAACCGCGCTATCGCCAAACCTTGCCTGACCCAAAACTCGGCTTTTTCCTGATTATTCCATATCGGTTCACCGTTTCCATTCTCCTGTTCTTCGCAAATTTTCACCGCTTCACGGGTTACCTCCCTATCTTCCCTATCCTCCTGTAGCCCCCCTTCAA
# Right flank :  GCCTGCCTTCACAAATACCGGATGCAGCCGCAACTTGTCACACCCAGCATCCAACCAACCTTGCCAAGTCCCCAACCACAGCCGAATCGTGTTGTCTCCACCAGAAACGATTTTTGACCCATCCGGGGAAAATGCCACTGAGGAGACACTATCCTCATAGCCTTTGAATTCAGCTAATTGTTCCTTAGACGTGACATCCCACAGGCGAATCGTTTTGTCCCTACTCAGAGAAACAATTCTGTTCCCATCTGGAGAAAAGGCTACTGAGTAGACCTCAGACTGATGGCCTTTGAACAGGGCTAATTCTGACCGGGACTTGACATCCCATAGGCGAACCGTTTTGTCATCACTCCCAGAAACAATTTTTGACCCATCCAGGGAAAATGCTACTGACATAACAGGTGACTTATGGCCTTTGAAAACCGCTAATTGTTCACCCTGAAGATTCCACAGGCGAATCGTGTCGTCATAACTCCCAGAAACAATCTTTGACCCATCGG

# Questionable array : YES	 Score: 1.41
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-0.5, 7:0.02, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAAGGGGGGTTGGGGGGATCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [4,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [46.7-45.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_128477 flag=1 multi=9.6382 len=1548		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                  	Spacer_Sequence                            	Insertion/Deletion
==========	======	======	======	=================================================================	===========================================	==================
       176	    65	  93.8	    43	..T..................................A.............G....T........	TGACAAAGGCGCAAAAGGCGACAAAGGCGCAAAAGGCGACAAA	
        68	    65	 100.0	     0	.................................................................	|                                          	
==========	======	======	======	=================================================================	===========================================	==================
         2	    65	  96.9	    44	GGCGCAAAAGGCGTCAAAGGCGCAAAAGGCGTCAAAGGCGCAAAAGGCGTCAAAGGCGCAAAAGG	                                           	       

# Left flank :   ACCATGACCATACAGTTTTGACGTGGAGTGGAATTGACGCCTTTCTGTCTGTTGTTCAGCAAGATGCAGCCCATTCTTAAGCATTCTATGGTGACTCAGCTTTTCGGGGAGGTTTTCAGAACATTCGGTCCAAGCATCGCCCAACTCCATTATTTCCATTAACTCAACGGCAAGAACACGAGAATGTTGCAATGAAAAAAGCTCGTGAAGCTATTGAGTGGTCCTACTGTTTGTTGACTAACTTGTGGCATCTTTCAGAAAACAGGGAGGCATTCAAGCTTGAGACAGACCCCAATGCTGTCATGGCGCAGATCCGCGTCATGCACCTTTTGACTAATTGCTATTCGTGTATGCATGGCAACCAAGTGAGCAGCCGCCATACCTTTGCCTGTCCACCTCCTACACTAGAAGCATACCTAGCTCTCTAGGTGTAATGTACTCACTAGAGATTCATTGTTACATTTCTTGCTATGCCTCATTTGTCCCGTCCCCTGCCATTC
# Right flank :  GCGT

# Questionable array : YES	 Score: 0.14
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.85, 5:0, 6:-1, 7:0.09, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCGCAAAAGGCGTCAAAGGCGCAAAAGGCGTCAAAGGCGCAAAAGGCGTCAAAGGCGCAAAAGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,25] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-8.10,-22.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [1.7-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,1.01   Confidence: MEDIUM] 

# Array family : NA
//

>k141_131919 flag=1 multi=3.0000 len=1401		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                                                                                                                                                       	Insertion/Deletion
==========	======	======	======	=============================	======================================================================================================================================================================================	==================
       120	    29	  72.4	     7	A.CC....T..T..AC...A.........	GGGTTTG                                                                                                                                                                               	Deletion [156]
       156	    29	  82.8	    20	..............A....AGG.A.....	TGTAGTTGCCCTAGTTGCTG                                                                                                                                                                  	Deletion [205]
       205	    28	  93.1	    37	..-...........T..............	TGAAGCTTGTGTTGTAGATGATCTTGTTGTAGGTTTG                                                                                                                                                 	CCC [212]
       273	    29	 100.0	    37	.............................	AGTCGACAGAGTTGTTGTTGAAGATTGAGCTGTAGGC                                                                                                                                                 	
       339	    29	 100.0	    40	.............................	TGAAGATTGTGTTGTAGTTGCCCTAGTAGTTGTTGTTGAT                                                                                                                                              	
       408	    29	  75.9	    17	..T.....T.....T.A.TT.G.......	AGTTGCCCTAGTTGTTG                                                                                                                                                                     	Deletion [454]
       454	    28	  86.2	   182	..-...........T.....C..C.....	TGATGTTGATTGTGTTGTTGATGTTGGCCAATATATTGTCGAATAATCAGCAGCAGTCAAAAAAACAAACCACTCTGTTGAAGACATCTTTGTTGTTGGACTTGTAGAATGTAAAAATAGTTTATTTTGTTGAGTAAACATAGACATTGTTGTCATCCTTGTTGTTGGCTTCGTTGAAGTTG	CCC [461]
       667	    29	  79.3	    47	........A.....A....AAA......C	CCTAGTAGTTGGCCTTGTAGTAGTAGTTGAAGTTGTTGCCCTAGTAG                                                                                                                                       	CC [671]
       745	    29	  82.8	    26	........A.....A.....C...A.A..	AGTTGTAGTTAACGTTGTAGTGGTTG                                                                                                                                                            	CC [749]
       802	    27	  72.4	     0	........A..G..A....TC..C...--	-                                                                                                                                                                                     	CC [806] Deletion [829]
       831	    28	  75.9	     0	G.......T...C.T..A...-......A	|                                                                                                                                                                                     	A [849]
==========	======	======	======	=============================	======================================================================================================================================================================================	==================
        11	    29	  83.7	    41	TTGGTTGTGGTAGTGGTTGCTCTTGTTGT	                                                                                                                                                                                      	       

# Left flank :   TAGTCGTAAAATCAGCAGCAGTCAAAAATCCAAACCACTCTGTTGAAGACATCTTTGTTTTTGGAGTTGTAGAATGTACAAATAGTTTATTTTGTTGAGTAAACGTAGACATTGTTGTCA
# Right flank :  ACATTTTCGTTTTCCTGTTAAAAATAAGAATACATTTTAAGCAATGAGAACATTACTTTGATCTTTTTACATTTTGTAAAAAAAGTTGTTGGAGCCAAGACTCGAACTCGGAGCCTATATAATTGTAAGCAGTGATGCTAACCACTAGTCTATCCAGGTTGATGATCAAGGAGAGAAAAAAAATCTTTTTTTTTCTTTTCACGTAAAAATATTTTGCTCACTCAATTAAAAATTTTTTTTCTAACGGATGACTCGCAAGCAATAAAAAAACCATTAAGTGATCTTTTAACATTTTGTAAAAAATAATAAACAAAAATTGTTTGAGCCTCTCAAGATTTAAACTTGGATTTTTTTAAGTTTGAAAGCACAATGATGCTAACCACAAGTTAGCATCCAAGTTAATATAAATTGCAGAAAAAATCTTTTTTTTTGTTTTTACGTAAAAATATTTTACATAGTCAACTGAACAAATTTTTTCTAAAAAACAAATAACTAGCAAG

# Questionable array : YES	 Score: 0.46
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.19, 5:0, 6:1, 7:-2.05, 8:1, 9:0.32,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTGGTTGTGGTAGTGGTTGCTCTTGTTGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.17%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.70,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [18-39] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [70.0-75.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_135140 flag=0 multi=10.9984 len=6844		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	===============================================	==================================	==================
        29	    47	 100.0	    34	...............................................	TCATCACTGATTTCATCAATGCCACCGAAACCCT	
       110	    47	  95.7	    34	..G............................G...............	TCATCGCCACCAAGGTCATCCATGCCACCGAGGC	
       191	    47	  78.7	     0	................C..........TAC..C....GT..GGC...	|                                 	
==========	======	======	======	===============================================	==================================	==================
         3	    47	  91.5	    34	CATCATCATCGCCACCGAGGTCATCCACGGCATCGAGACCATCATCG	                                  	       

# Left flank :   CCGATTTCATCCATGCCACCGAAACCCTC
# Right flank :  GGACGCCTCTGGGCTCATATCATCTATGCTGCCGATGTCTATGCTGGGTTCTTCGTCCTCGTCGGCGAAAGACATAGCAGCGGCACCAACGGCGCCCACGGCTGCCACTCCGGCGGCGGCGGCGGCTGCTTTGGCAGCAACACCCATTTTGGGCTCTGTAGTTGCTATCGCCGACTCATCACCCCACACGTCGGCAGCAATATCTGGGGCAGTGCCGCTAAACACTTCATTCACCGTTTCCACGGCTTCGGGCTGGAGTTTTTCTTCTGGCTCCGATTTTGGTGCTTTAGCGGCGGCGGCTGGGGCTCCCAGTTTTGCTTCTGGAGCTGCTACTTCCGGTGGTTTAGCGGCGCTTGCGGGCTGTTCAGTTTTAGCCGCCGGTGGGATTTCCCTAGCCTCGCCTTTGCTCAGAGCCATTTCTGGGTCAAAATGCAGTCCCAAAACTTCAATCAGCTTGTCAGCATCGGGATTGAGCTGGGAAAATGGGAGCATCAACTGCG

# Questionable array : NO	 Score: 1.63
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.57, 5:-1.5, 6:0.26, 7:0.90, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATCATCATCGCCACCGAGGTCATCCACGGCATCGAGACCATCATCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [12,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-7.70,-12.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-12] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [21.7-45.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_137858 flag=1 multi=18.8824 len=1076		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence                           	Spacer_Sequence                                                	Insertion/Deletion
==========	======	======	======	==========================================	===============================================================	==================
         1	    42	 100.0	    63	..........................................	TCCCTTTAGCCCTCTTTCATAGCAGTAGTAGGGTGGGCAGTGCCAAAACTAATAACTCTTCCC	
       106	    42	 100.0	     0	..........................................	|                                                              	
==========	======	======	======	==========================================	===============================================================	==================
         2	    42	 100.0	    73	TGACAGTAGCAGGATTTTCGGCACTGCCCACCCTACGAAATC	                                                               	       

# Left flank :   |
# Right flank :  CGTTGCGGATTGTCCAATTTAGGAGTATAATAAAAATGGCCAGTAACTAAAATGATTATGACTGGACAAATGATAACAGCAAATGCCCCGGATGTCAAGCCAGAAGTTTCTCCTAGTGCTGGCTTTATCCTAAAATCGATTAAATGGGCGACTTTTCAAGCCTTGATGGCTGATGTGGGAGAAGATAGAGCCTGGAGAATCGCGTATAATCAAGGACTGTTAGAAATTAGGATGCCACTCATACAGCATGAAGTACCTAAACGGATGTTAGAATGCTTCATTACTTCCCTAGCTGACGAGTTAGAGATGGAAGTGATGAGCCTTGGCTCTCTCACCCTTGAGCGGGAAGATTTGCAGCGATCGGTAGAGCCAGATAGCTGTTTCTACATTCAAAATGAAGCTGTAATTCGGGGCAAAACTGAAATTAATTTAACTATTGACCCTCCCCCAGATTTAGTGTTAGAATCTGATTTTACCAATTTATCAGTCAACAAAGATGT

# Questionable array : YES	 Score: 0.64
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-1.5, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGACAGTAGCAGGATTTTCGGCACTGCCCACCCTACGAAATC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,9] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-5.00,-9.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_140757 flag=0 multi=8.8188 len=2977		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence          	Spacer_Sequence                                   	Insertion/Deletion
==========	======	======	======	=========================	==================================================	==================
       423	    25	 100.0	    50	.........................	GGGAAAATACCGATTCTGTAGGGGCGAAGCATTCTGGCGAGCAATCTCTC	
       348	    25	 100.0	     0	.........................	|                                                 	
==========	======	======	======	=========================	==================================================	==================
         2	    25	 100.0	    51	GCTGGCCAAAAGCCAGATGAAAAGC	                                                  	       

# Left flank :   TTCCTGGGGAAAATTTACCAGAAAATTCCTCTATAGGACACCGCCCTTTATTTACAATGGGTTATGGAGCCACCATGAGTATGGGAATTGTGGTAGCCCATAAGAGCGTCCCCCTACCTACTGTGTTAGAAACTTTGTGGGAGGCGGAAAAAGAGCGCGCCAAAAAGCTGCCTGGGACAAAGGAAATTCCCGCTAAAGATGGTTTGTGTTTCCGGGTTATCTATGGCGGTGGTAATGTGCTGGAAGCGTTGATGAAAGGTCATTTGCTCCCCAGTTGGTGGCAGTTGATTAAAGATTACCCAACTGCGGCGGAGGATTTATCGCCAGTGTTCTATCGCTTGGCGGAAGAGCTGCCGAAACACGCTTTCGTTACTCCAGAGCATCGCCTGTTTTCTAAAGCGGCGAAGGTGATTTTAATGCGGCGGGATAATCCCCTACCGGAGGACCAGCAACTGGCGATTATGGCATGGCTGGATGAATGGGAAGATTGGGCGAAAAAT
# Right flank :  CAAATCGGAATCAGCGTGCAAGATTTAAGCCGCCTGTTGCGGTTTACGGCGTTTTGGTTGGACAAAATGGCGCTGTTGAGTAACTGGATCGAGGAGGAGTAAAGCTGATGACTTATTATTGGTATGAAATCGAACCCCTGGATGTGTTGCTGTTTCGGGAGGCAAAACCTTTCAGTCCGGGGGAGGGTTCTTGGGCAAAAGGGCAATTTCCCCCACCGCCGATCGCCCTGTTCCACGCCTTGCGTTCTGTCTTGCAAGAAGTGCCAGATTTACAACAAAACAAATCTCGAACCCAGGAATTTCTCGGCCCATTTTTAATGGATG

# Questionable array : NO	 Score: 2.03
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.34, 7:-0.2, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCTGGCCAAAAGCCAGATGAAAAGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [2,10] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [50.0-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_144362 flag=1 multi=2.0000 len=1033		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                	Spacer_Sequence                                        	Insertion/Deletion
==========	======	======	======	===============================================	=======================================================	==================
       153	    47	 100.0	    55	...............................................	TAGTGGAGTTAGTTAGTGAATAACGCCCTTCGGCTTCGCTCAGGGTAAAAATAGT	
        51	    47	  97.9	     0	...T...........................................	|                                                      	
==========	======	======	======	===============================================	=======================================================	==================
         2	    47	  99.0	    56	GAAGAGTTTACCCTGTGGAATTAAGCGAAGCTATTCAACAGGGTGAA	                                                       	       

# Left flank :   TTTTTGACAAACTTTCTGACTGCTTTTTCATCCAGGTGATTGATGTTTGCTCCATCTGCAGGGAGTTCATCAAAGGTTTTACCCATTTTTTTTATGATAAAATGCTGGAGTTCCGCTCCCTTCATTTCCTGTTTTGTACTTCCCGAACGATAATGATAGGTTCCTTTGTATGAAATGGGTACGCTGGATGGTGATACAATGATTTCAATGTATTGATTCCCTTCTGAATCCAATAAATTCACGTCTGAAATAATTCCCAAATAATTAACAATTTTGTTGGGAATATCTTCCATGAGCCGCTTATTATCGTCAACACCAATTACTTGCCCTTTGTCGTTAACTCCAATCAGCAATTTCCCTCCATTCGCATTAGCAAAACCACAGACCCATTTAAGGTACTCATCGCGCCAGGCAGATTTATATTCGATATTTTGTGATTCGGTCATTTTAACCAGAGTATTAGCTTATTTGCCTCATTATTTAAGTGAAAAACGAAAAAC
# Right flank :  ACTTA

# Questionable array : YES	 Score: 0.71
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.95, 5:0, 6:0.26, 7:-0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAAGAGTTTACCCTGTGGAATTAAGCGAAGCTATTCAACAGGGTGAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.45%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-10.20,-10.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [5.0-70.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.68   Confidence: HIGH] 

# Array family : NA
//

>k141_151100 flag=1 multi=9.0000 len=1671		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence       	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	======================	======================================	==================
       445	    22	  72.7	    38	..CA.......TCG......T.	GCGGGTGGGCGTGTTGCAGACGCCGCGCCTGCGGGTGA	C [450]
       506	    22	  90.9	    38	..C..T................	GCAGTTGGAGAACCTGCAGACCGAGGACCTGAGCGTCA	
       566	    22	  95.5	     6	..............A.......	CCGCGG                                	Deletion [594]
       590	    22	  63.6	     8	...GC..........CGCG.AA	GCTGGCCA                              	Deletion [620]
       623	    22	 100.0	    38	......................	GCAGCTGATGGACCTGAAGAGCGACGAGGTCTCGGTGC	
       683	    22	  90.9	    32	..A.C.................	CAAGGTGCACGCGGCACGCACGCTGGAGATCA      	
       737	    22	 100.0	    32	......................	CACCTACGCCGGTGACGCCACCGTCAGCAGCC      	
       791	    21	  81.8	    33	..A.C.............-.A.	CCGCAAGCAGTGACCGGCGGGCAGTGACACACA     	G [805]
       846	    22	  77.3	     0	.....T...CA...A.T.....	|                                     	A,T [848,852]
==========	======	======	======	======================	======================================	==================
         9	    22	  85.9	    28	GCGTGGCCGGCAGCGGCGACGT	                                      	       

# Left flank :   GGCGTTCCCGCTGCCAGGAGCCCTGCGATGACCCCCCTCCGCCACCCGGCTGCTCGCCCGGCGCCGCCCGCTCGCCGCACCTTCGCCGCGTGGATCGCCGCCGCGCCGCTGCTGCTCGTGGCGCCGGTAATGACGCTGTCGTCCACCCCGGCCGCGGCCTGGGGCGAGCGCCTGGTGGGCTCGGGCCGCAGCGTCACGCAGACGCGGGCGCTGCCGGCCTTCACCGCGGTGGCCAGCCGCGGCTCGTTCGACATCGCGGTGCGGCAGGGCAGCCCTTCGCAGGTGGAGATCAGCGCCGACGACAACCTCATCGACGAGATCGAGACCGTGGTCGAGGACGGCCGCGACGGCGCGACGCTGACGGTGCGCGTCAAGCCAGGCCGCAGCCTCGACACGCGCAGCCCCGTGCGCATCACCGTGACCACGCCCACGCTGACCGCGCTGG
# Right flank :  TTGCACGGCAGGCGACAATCGCCCACGTGCAAACCCTGAGCCCGCAGCAGCCGCATTGGCGCCACCCCGAGTTCCGCCGCGGGGCGCGCGACATGCTCGCCACCAGCCTGGGCATCGCCGCCTGGGGGCTGATCACCGGCGTGGCCATGGGCAACACCGGGCTCGGCCTGCCGCTGATGCTGATGATGTCGCTGCTGGTCTACGCCGGCAGCGCGCAGCTGGCGGTGCTGCCGCTGCTGGTCAGCGGCGCGCCGGTGTGGGTGGTGTGGGCCACGGCGCTGTGCGTGAACCTGCGCTTCGTCGTCTTCAGCCTGCAGTGGCGGCCTTACTTCGGCCACCTGCCGCTGGCGCAGCGGCTGCGGCTGGCCTACTTCACGGCCGACCTCAACTACGTGCTGTTCATGCGCCGCTTCCCCGAGCCGCAGCCCGAGCCGGCGCAGCTGCCGTACTTCTGGGGCGGCGCGGCCACCAACTGGCTGGCCTGGCAGGTGCCGTCGATC

# Questionable array : YES	 Score: 1.31
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.45, 5:0, 6:-0.25, 7:-0.25, 8:0.8, 9:0.56,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCGTGGCCGGCAGCGGCGACGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [2,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-6.20,-4.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [3-7] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [23.3-38.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.27   Confidence: MEDIUM] 

# Array family : NA
//

>k141_153101 flag=1 multi=4.7954 len=1177		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	===============================================	===================================	==================
       724	    47	 100.0	    35	...............................................	GGTACTGATATGAAACATTACACCGGGTACGAGGC	
       806	    47	  95.7	     0	.............G..........................A......	|                                  	
==========	======	======	======	===============================================	===================================	==================
         2	    47	  97.8	    36	ACTTCAGGGGGCTTATGTGAAATATTTCGCCCGGGGATTTGCTTCAT	                                   	       

# Left flank :   CCGGCAGGCTTCGTGGACTGAGATTACGGCGAGGTGGAAGGAGACTCTTGACCGCCTTGCGGAACTGAACCCGGGCCTGAAGGTCTGGTTTACCGTCAGTCCTGTCAGGCACCTGTCAGACGGCGCCCATGCCAACCAGCTCAGCAAGGCTCACCTGCTCCTGGCAGTTGAGGAGCTCCTCGCTCATCCTGCCGTCGGGGGCTATTTCCCGGCCTATGAGATATTCATGGATGAGCTTCGTGATTACAGGTTCTATGCCTCCGATATGGTCCACCCGTCGGAGACGGCCATCGATTTTATCTGGGAGAAGTTTACATCTCTCTTTATCGAAAGCCCGGTGCTGCGCCTCTGGAGCGAGGCTGCGCGGATAACCCGTGCCATGGCTCACCGCACCTCCGGCGGGAAGCGGGAGACAGCCCCGTTCGCCGAAGCGATGCTTCAAAGGATCAGGGATTTACATGAAAAGGCACCATATATTGACATGACAGCGGAGGAGAGTT
# Right flank :  TGGACCGGGATGAAATACTTCACTGAGGGCTAGTAACTTCGGGGGCTCCTGTAGAATAATTTTTTTGGTGAGTGAACTATCTGTATATTGTATTGTTTTTTTAACAAAAATATGAACAAAATAAAACCTGCATGAGCAATAAATCAATTAAGGATTCGAAAATATATTTTCATGCAAGTTGCATTGGACCAGTGAAATAGATTAATTACATACAAATGAAAAAATCTTCCCTTGTTTTATTGTTAACCCTTATGTTCGCCGATCTCTCCGGTCAGTCGGAGGGCGGGATCATCCGCGGCCAGGTTGTTGACGCAACCACCAATGA

# Questionable array : NO	 Score: 2.05
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:0.26, 7:0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACTTCAGGGGGCTTATGTGAAATATTTCGCCCGGGGATTTGCTTCAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.19%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-6.00,-6.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [58.3-51.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_154459 flag=0 multi=7.1450 len=3679		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                                                 	Insertion/Deletion
==========	======	======	======	==========================	================================================================	==================
        91	    26	 100.0	    64	..........................	AAGTCTTCGAGCATTCTCACAATTCTTTAGAGGTTTTTCGAAATCTCTGAACAGCTCACAAGTC	
       181	    26	 100.0	     0	..........................	|                                                               	
==========	======	======	======	==========================	================================================================	==================
         2	    26	 100.0	    74	CTTAAAAACTCTTTAGAGGTTTTTCG	                                                                	       

# Left flank :   ATCTTCATCATCCTCGAGATTCTTCACAAAATCTTCAAGGATATCTTCGAATCCGTCAAGATTCGCAATTTCCCCAAGCTCACAATTATTC
# Right flank :  GGACATCTTCGAGCTATCTTCTCTTGAAAGTCGAACTCGCTTTTCATCATGGTTGGCCCTAGACGCACTCAAGTAGCAGAGCAAAGGGACCAGCAACGTGGACCTTATCAGCCACCGACAATTAAGGAGTTCAAGACCTTGGGAGATGCAGCGCACCTCAGAATCCGGCTGAATTCTCCAGAGAGACGAGAACATCATCGTCTCCCTCCAGATGACGAACATCTAGCAGCTGCGAGACTGCCAGCCATACAGCGAGATCCTCCGGAACAGTACGCAGTAAGCATTGCTCAGCATTTAAATTACGCTGCTCTCGCTCTGGATCCTAACACTACAATGGAAAGGCCATTTCTAACTCATCATACTCGCATGATACGTGAGCCTTCTCACTACTCACTGTACGCTGGTCAAATGGCGTCTGAGCTTAAAGCCGTTCCCCTCTTCAGTCTTGACTTTGAGAAAAGATTGATGACAGCCAGGACCAAGCAGTACTATTCAAACCT

# Questionable array : YES	 Score: 0.55
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.26, 7:-1.6, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTTAAAAACTCTTTAGAGGTTTTTCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:69.23%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-1.30,-0.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [61.7-55.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_158923 flag=0 multi=8.0260 len=2681		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                          	Spacer_Sequence                               	Insertion/Deletion
==========	======	======	======	=========================================	==============================================	==================
       135	    41	 100.0	    46	.........................................	GCTTATCCACTTTTCACTTGTATGAGCAAACAAAGGACAAATGACC	
        48	    41	  90.2	     0	..T......G.....................C...T.....	|                                             	
==========	======	======	======	=========================================	==============================================	==================
         2	    41	  95.1	    47	AAGGACAAATGACAAATTATATGAAAATTTGTTATCTGTCC	                                              	       

# Left flank :   TAATCCCGATACAGGCGGTAATCTCGGAGAATCCGATCGTGGTCAAAGCACAATGGTGTAGGCAAGCGCCACAACTCAAAAATGCCTACGGTTTTCGCATCATCTGCCGCTTTTGGCTCGCCGGTGGCGGTGGCCAAAAATACCACGCTCAGGGTATGGCCTCTGGGATCTCGTTGGGGGTCGGAGTAAACATGGAACTGCTCGATCAGCTCCACCGGTAAGCTGATTTCTTCTAATGCTTCTCGCACGGCGGCGGTTTCTACCGATTCGCCGTAATCCACAAAACCTCCGGGAATCGCCCAACCTAAAGGGGTATTCCGCCGCTCGATCAGGACGATCGGGCGGTGGGGGCGATCGACCAGTTCGATAATAATGTCAACCGTAGGCGCCGGATTGCGATAGGTCATTTGTCCTTAGTCCCTTGTCCCTTGTCCCTTGTCCCTTGTCCTTAGTCCCTTGTCCCTTGTCCTTAGTCCCTTGTCCCTTGTCCCTTGTCACCA
# Right flank :  CAGTCTTC

# Questionable array : YES	 Score: 1.24
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.75, 5:0, 6:0.25, 7:0.04, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAGGACAAATGACAAATTATATGAAAATTTGTTATCTGTCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:73.17%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-3.60,-6.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [4-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [6.7-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.05   Confidence: HIGH] 

# Array family : NA
//

>k141_172755 flag=1 multi=14.3608 len=1330		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                   	Spacer_Sequence                                          	Insertion/Deletion
==========	======	======	======	==================================	=========================================================	==================
       110	    34	 100.0	    57	..................................	CAAAAAATCTTTGCATAAAACCGATAACTGATCGGCCCGAATGCTTCGCCCTTTCTT	
       201	    34	 100.0	     0	..................................	|                                                        	
==========	======	======	======	==================================	=========================================================	==================
         2	    34	 100.0	    58	TATTTAATTTACCTGTAGGGGCGAAGCATTCGGG	                                                         	       

# Left flank :   ACGAAACCCAACAAAAACCATTACTAATGTTGGGTTTCACTTCGTTCTACCCAACCTACAAATCCACTACAAATATCAAATATATAAGCGATCGCCTTGAAGGAAAAAAT
# Right flank :  GGCAAAAAATCTTTGCATAAAACCGATAACTCATCTGCCCGAATGCTTCGCCCCTACATTAACGCAAACCAGAACAGATAAAACCAGCCCAATAAAAAGGATCGGCAAAGGGATCTTCTTTACCACAGAGAATTAACAGGTTTTCTTTAACCCTGAAAATGCGCTTTCCTATCTTATTCCACTGTTGATACTCTTCATCCCATCGCTGATAATCTGGGGTGCCTTCCCGTAAACGGTTACGATTTTTCTTGGCTTTTTGGCGGTTTTCTTCTGCTAGTTGTAATTGCTGCTGATAATGCTCTTGTAGTTTATCCTTGTAATTACTTACTAGGGTTTTACCCGATAAATTTATCAGGTCAAATTGCGCTTGTTTCAAGGCTTCAGGACGGGTTAAACCTGACTGGCGATACTGGTAATAAAAGATGGAAAATAACGCGGTGGCTAAATCATCAACTGCCCATAAGGTACTGACCACACTCCGCGCCCCAGCACATAAAAAG

# Questionable array : YES	 Score: 1.25
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.26, 7:-0.9, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TATTTAATTTACCTGTAGGGGCGAAGCATTCGGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.88%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.50,-1.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [65.0-58.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_18276 flag=1 multi=8.8580 len=1120		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence              	Insertion/Deletion
==========	======	======	======	========================	=============================	==================
       961	    24	 100.0	    29	........................	ATTATGCCTGTCATCCGCCAGTTGGCGGA	
       908	    24	 100.0	     0	........................	|                            	
==========	======	======	======	========================	=============================	==================
         2	    24	 100.0	    30	CTAAACCAACTGAGCTAACGTCCC	                             	       

# Left flank :   TCAGCCTGGCTTGGATGTCAGAGGTATATCCTATGTAAATCTTATGATACTCCACGGAGTATAAGACATATACATAAAACATGATATAATAATTTGCAGCGAAAATTGTGGGGCGTACTGGAATCGAACCAGTGACCTCTTGCCTGTCAAGCAAGCGC
# Right flank :  GGGCGGGGACAAATATAATAAATTTTTGAATTTCCGGTTACTCGATCATCACGGGAGTGCTGAACGAGAAGAGAAACGACGATCCCTTGCCTGGCAGTGACTCCACCCAGATACGCCCCTTCATAAGCTCTGCCAGGCGATTGCATATAGCCAGCCCGAGACCGGCCCCCTGAACCTTCGGCACATCCTCATTCCAGTACTGTACGAACGGCAGGAAGATCTCTTTGAGCCTGTCAGGCTCGATACCCTGCCCCGTGTCGGTGACAAAATAGGTTATCGAATACCTGTTATCCGATTTAGAGATCCGGTACCCGTATTTTATCTCTCCCTTGTCGGTGAACTTAATAGCGTTGCTGACGAGGTTGCTGAAGATCTGGCTCAGCCGCAGCTCATCAATATACTGGTAAACGGCATTTTTATTCTTATCGGTGACTGGGGAAATCTTAATATGCTTCTTCCCTGCAGTCTCCAGGTCATAAGAGAGCGACGAGGCCATGGAG

# Questionable array : NO	 Score: 2.18
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.04, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTAAACCAACTGAGCTAACGTCCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [56.7-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_192605 flag=1 multi=5.2707 len=3111		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	==============================	===================================	==================
         1	    30	 100.0	    35	..............................	CTTCGGCTTGCGCCCATATCATAGGATGACAGTTT	
        66	    30	 100.0	     0	..............................	|                                  	
==========	======	======	======	==============================	===================================	==================
         2	    30	 100.0	    45	ATTAACTGGTTATTAACTGGGGTAGGGGCG	                                   	       

# Left flank :   |
# Right flank :  GATCGAGGATGGAAAGAAATTTTCCCTCGAAAGTCAAGAGTATCTCCGTTGACCATTACTATAGAGTAGCAAGCTATTAGGATAGAGCATCCATGTCCGAAACTCAACTTACCCTCAATCTCTTACAGGGCTCAGTGAGCTTTCGCTTCACCCCCGAATCCGCAAAACAACTACAAAAACACATTGCTGAATTGATGGATAAATTAAAAGCAGTGGCCGCCAACTCTACACCGGGGGGTGGGGGTGGCAAACCGCGTCCCCAAAAACCGATGGAATATCAATACACCGGGGATGTGTTTCTGGAAATTTTCTGTAATCCCAATATTTACCCCAGTCCTTTTGCGGCTAAGGTGCTGATTACCCTGCGCGACGATCGCATTCGCCTGAGTAGCGAAGCAGAATTGACTCGTTTGATTGAAGATATTAACCAGTATCTTGAACAAGTGGGTTAAGATTAGTTAAATTTAGATCGGGGCAACCACCCGCGATCGATTGCCCCT

# Questionable array : NO	 Score: 3.58
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.99, 7:0.7, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATTAACTGGTTATTAACTGGGGTAGGGGCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.10,0.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_194876 flag=0 multi=15.6712 len=10235		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                    	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	===================================================	=======================================	==================
         1	    51	 100.0	    39	...................................................	AGTCCCTCATCCCCCTACCCCCTTCTCCCAAAGCGGGAG	
        91	    51	 100.0	     0	...................................................	|                                      	
==========	======	======	======	===================================================	=======================================	==================
         2	    51	 100.0	    49	AAGGGGAATTAAGTCCCTCTCCCTCCTTGGGAGAGGGATTGAGGGTGAGGG	                                       	       

# Left flank :   |
# Right flank :  GTCAATCTGCAATCTCGCAAACTGGTAATTTTTACCGAACATCGCGATACATTAAACTATTTAAAACAGCGGATTACCACCCTCCTCGGTCGTCCCGAAGCCGTTGTCACCATTCATGGCGGGATGGGACGAGAAGAACGCAAAAAAGCCGAAGAAGCATTTAAGCAAGATATCGCCGTGGAGGTGTTGATTGCTACCGATGCAGCGGGAGAAGGAATTAACCTGCAACGGGCGCATTTGATGGTAAACTATGATTTACCCTGGAATCCCAATCGGTTAGAACAGCGGTTTGGTCGGATTCACCGGATTGGCCAAACTGAGGTTTGTCACCTGTGGAATTTGGTGGCGGGAGAAACTCGCGAAGGTGATGTTTATCTGTCTTTGTTGCGGAAATTAGAAATTGAGCAAACCGCTCTCGGTGGCAAAGTGTTTGATGTGTTGGGGAAGGCGATCGCCGGGAAAGAACTGCGGGAATTACTCATCGAAGCCATCCGCTACGG

# Questionable array : YES	 Score: 1.19
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:0.3, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAGGGGAATTAAGTCCCTCTCCCTCCTTGGGAGAGGGATTGAGGGTGAGGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [11,11] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-23.30,-20.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_18868 flag=1 multi=4.0000 len=1122		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence     	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	====================	=============================================	==================
       793	    20	 100.0	    45	....................	GGTAGGCAGTGGGCAGTGCAAAGCGTAGAGTTGGCGGTGCAATCT	
       858	    20	  95.0	     0	................T...	|                                            	
==========	======	======	======	====================	=============================================	==================
         2	    20	  97.5	    46	TGAATCCGAAATCAGTAGGC	                                             	       

# Left flank :   TGTTCCCCATTGTGGGACTCATCCTCACCACGTTCATTTCGCCCAGTGCATTGCCGCTGCTCGGTATGCTGTTTTTTGGCAATTTGCTCAAAGAATCAGGCGTCACCAAACGTCTGGCAGAAACTTCCCGCACCGTACTCATTGACATCGTTACCATTATTCTGGGGCTTACGGTGGGCGCCTCCACGCAGGCCGATGTTTTCCTCACGCCGCAGTCCGTCATGATTTTTGCACTTGGCGCGTTTGCATTCGGCATCTCCACTGCCGGCGGTGTACTTTTTGCCAAAGCGATGAACCTGTTTTTGAAACCTGCCGACCGCATCAATCCGCTCATCGGCGCAGCAGGCGTTTCGGCAGTGCCCGACAGCGCCCGCGTGGCACAGTTGGAGGGACTGCGCGAAGACCCCAACAACCACTTGCTTATGCACGCTATGGCGCCCAACGTGGCCGGAGTAATCGGCTCGGCGATAGTGGCCGGGCTTCTGCTCAGTTTTTTGGGG
# Right flank :  CTTAGGCAGTGCAAAGCGTAGAGTTGGCGGTGCAATCTCGAATCTTGAATTCGGAATCTTGAATCCTGAATGTATCTTTGCCGCACATTCATCAATCTTCTTCTACTTATGTCACACTCAGTCATCATCGGTACCGGCGCCTACCTGCCCACGGAAGTTGTTCCTAATGCTGCGTTTGAAAAAAATGATTTTTACAATCCTGACGGCACCCGCATTGACAGGCCCACCAAGGAAATTGTAGATAA

# Questionable array : YES	 Score: 0.38
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.88, 5:0, 6:-0.75, 7:0.05, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGAATCCGAAATCAGTAGGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,-0.20] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [40.0-53.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_196513 flag=1 multi=2.0000 len=1046		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence            	Insertion/Deletion
==========	======	======	======	=============================	===========================	==================
        15	    29	 100.0	    27	.............................	TCAGAAGACCGAAGTCAGAAGACGGTA	
        71	    29	 100.0	     0	.............................	|                          	
==========	======	======	======	=============================	===========================	==================
         2	    29	 100.0	    37	GTCGGAAGACGGAAGACGGAAGACGGAAG	                           	       

# Left flank :   ATGGTATATAAAAGG
# Right flank :  GAGGGTGCCAATTTGAGATTTGGTTTCTCCGAATAGGAAATACCTTACTAAATGTAATTGGTCAATAAAATCATAATTAAACAGATAAATGAAGTTTTTGGGGAACTTCAGACTTCCGTCTTCGGACTTCCGACAAAATGATTACTTACAAAATTAGCAGTAAAACCAATTAAAAATAATCGGTATGACATTTTTTCAAATTTACCTGCTTGCATTTGCAGCCATTATGGGGATGATGACCATCCTGTGGCTGATCAGCATTAAAATCACGAACGTGAGTATTGTTGACCTGTTCTGGGGTTTTGGATTTGTAGTTGCCAGTTCGGTTTACTTCATTTTTACCGAAGGATTCGAAACACGTAAAATTCTTTTGATGACTCTGGTTGCCATCTGGGGACTGCGTCTTTCCATTTACCTGGCCTGGCGTAACCTGGGTAAAGGCGAGGATTTCAGGTATCAGAAATTCAGGAAGGACTTTGGTGAAGACCGGTATTGGTGGT

# Questionable array : NO	 Score: 2.79
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:-0.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTCGGAAGACGGAAGACGGAAGACGGAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,1] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.60,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [18.3-63.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_198995 flag=1 multi=4.0000 len=1340		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	==============================	=============================================	==================
       846	    30	 100.0	    45	..............................	TACGCATTCGATGCGTGGGCGACGACGCGCATTCGATGCGTGGAT	
       921	    30	  96.7	     0	...A..........................	|                                            	
==========	======	======	======	==============================	=============================================	==================
         2	    30	  98.3	    46	GACGACACGCATTCGATGCGTGGGCGACGA	                                             	       

# Left flank :   AGGAACCGATGTAGTCGATGTAGCGCTTGCCATTGGCGTCCCAGAAATACGGGCCCTGCGCCTTGCTGATGAAGCGCGGCGTGGCGCAGCGGCGAATCGGTGTGCTCGAGCCAGGTCTCGTAGAGCAGCGCGGCGGCATCGATCTGGCCGGCCTGTTGCAGCCGGCTGGCGCGGTCCATCAGATCGGCGAGCGCCAGCGCGCCCTGGCGTGCGCTGGCCAGAACCGCCTCACTCTCGCCCAACGCACCCACATCCAGATTCATCGCGCCCATGCGGCATCCCCCAAACGGCTTGCACCGTCTCATGCTAGGGGGCGGTCCCGGCTGCCAAGCCGGCATCAAGGCGGCTTTCCCGGTCTTGTTCGCAGCCCGGCCCGGCGGCGCGCCAGAGGCGCACGCGCAAAGCCCGCTTCAGGCCGGAAAGACGTGGCCAACTCCACGCATGCAGGCCGCGCAGGGCGGCTATGCTGGCCGCAGACGGCATGCATTCGGTGTGTCGGC
# Right flank :  CGCGCATTGCACGCGTGGGCGCCGGCGCAGGGCCGGCAGGAAGGAGCACGACGATGGAAACAATCGGAGGGTGGCTGGCGCTGCTGGTGCTGGCGGTGGGGGTCGGCCTCATCGCACGCCGAGTCATGGCGCAGCGCCGCGCGCCGAACCGTGTCGAGGCCGACGCACCGGCGAGCGTGACCGAGTTCGAGACCGAGAGCGCGGCCCCGACCGAAATCGACGCGCAGGCAGCGGCGCTGGCTGCTGCGGTGCACGCGTCCGAACTCGAGGAGCGCACCCGGGCCTCGGCCCTGGCCCGGCAGGCCCAGGCCGAGGCCGAAGCGCATGCGCGCGCCGAGGCGCTGCGGGCCGAAAAACAGGCTGAAAAACAGGCCGAAATACACGCCGAAG

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:0.99, 7:0.05, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GACGACACGCATTCGATGCGTGGGCGACGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [7,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-8.40,-8.20] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [30.0-28.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0   Confidence: HIGH] 

# Array family : NA
//

>k141_209829 flag=0 multi=58.2199 len=3074		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                       	Spacer_Sequence                                	Insertion/Deletion
==========	======	======	======	======================================================	===============================================	==================
      1463	    54	 100.0	    47	......................................................	CGAAAATACCCCCGTCTCCTTTTCCTTACAAGTGTAGGTTTTTTATA	
      1362	    54	  98.1	     0	..................................................C...	|                                              	
==========	======	======	======	======================================================	===============================================	==================
         2	    54	  99.0	    48	TTGCCCTCTATCCCCCAACCCCTTTCTCCCAAAAAGGGAGAAAGGGGAGTAAGA	                                               	       

# Left flank :   AGTCGAGGCATATTATGATGCCAGCAGTCCCATGCAGATTTTGCCAGATTTGCAAAAACCCACTTTGATAGTGTATGCTGAAGATGACCCCATGTTTTATCCCGGCATCGTGCCAGAGGTAAAAGCCATTTGCAGCGCTAATCCCGTAGTAGATTTAATTTCCACCCGTTACGGGGGTCACGCGGGATACATTAGCAGTAAAAAATGCCAGCATCAAACGGGAGATAATGACCCTTGGTGGGCGTGGAATCGGATTTTAGCCTGGTTTAATCAACAGGTAGGTGCTACTACTGAAAGCAGGCAAACAGCCGAAGTTTTTTAGCAAGTAAATTGATGTTGGTGGAGGCACGGGATATTTAACCTCTCGGTTGAAGAATAAGGGCGAAGCATTCCGATCGACATATTATCGGGTTTAACCAGATATTATCCGCCGGAATGCTTCGCCCCTACAATGAATATAGTCAATTCAAATAACAATAAGACGCCAAGTGCCTGCATAG
# Right flank :  GAATAACCGTTCTCCAATGCCCCCCACCGCTCCAATGCCCCCCTCTCCCCACCTGGGAGAGGGGTTGGGGGTGAGGGCTGCTCGGCAAGTGGGGTTTAACCAAAAAACTATTCTCATTCTTAATTGAAATGACTATACCTCCAGAATATCCTCCCCTCACCGTAGTCCCAGCTCTCGTAACAAACCATCCGCCCACAGTGCCTGCGCCTCGGATAATGGCGGCACCGATTCCCTGGTATAATCTATTACTAAATCATAACCAATTAGGTCATAAACTCCACCTAACAACGCCTGTAAATCTATCAACGGCTCTTTATCTCCTGGGCGGAGTGGTAAAGAGAAGGAGGGGATTTCATCAGATAAATTAAAAGCATACAAATCGGCGCAGGGGCGGGATGCGCTCTGACTTACCAAAATCCGATAATCGCTGTGAATGGGGCTAAAAATTGGCATGGACTGCCAGCTCCGTAACAAATCAATTTCCACTAAATTGGTTAAAC

# Questionable array : YES	 Score: 0.18
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.95, 5:0, 6:-1, 7:0.03, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTGCCCTCTATCCCCCAACCCCTTTCTCCCAAAAAGGGAGAAAGGGGAGTAAGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [10,16] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-24.90,-24.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [36.7-60.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,1.05   Confidence: MEDIUM] 

# Array family : NA
//

>k141_20384 flag=1 multi=11.8901 len=3189		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence    	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	===================	======================================	==================
      1119	    19	 100.0	    38	...................	GTCGGGGCGAAGCATTCGGGTCATAAATTTTGGGTTTT	
      1176	    19	 100.0	     0	...................	|                                     	
==========	======	======	======	===================	======================================	==================
         2	    19	 100.0	    48	TTGCGAGAGATGACTCGCC	                                      	       

# Left flank :   GGTAGATGGCGGCGCCGGGGATGATTTGGTGCGCGGCGGTCAGGGGAATGATTTGTTAATTGGCGGTGATGGGGATGATGTGTTAATTGGCGATTTTGGTACTGATACCCTTACCGGGGGCGCTGGTGCCGATAAGTTTATCATCCGCGCCGATACGGCGGCTGGCCAAACCAGTGCCAGTTTAGCCGATCGGATTACCGATTTTGGCACTGGCGATGAAATTGTGATTGTGGGCAACCAAGAACCCATCCAGTTAGTAGCCAGCGGTAGTGATACGCTGATTCAGTTGAGCGGCGGCGATATTGTGGCAGTGGTGGCTGGTGTGACAATGGAGAATTTACAGGGGTCAATTTTGGCGGCTGGTAGTGGCGATGGAGGGATGAGTATCGGTTAAGGCCATGATTTGAGCCGGTTGGTGGGGACAAGGCATCAGCAAAATTAGGATTTAAAATAAATCTGAATGATGCCTTGCCCCTAACGATGTTTGGGTGGTGGGGCTG
# Right flank :  CCGAATGCTTCGCCCCTACATGCGGCGGGATGCTACCGGGGTAGGGTGATTAATCTGCCTGCCCCGGGGGGATGTATTTTGTCGATGACCAGTCTCAAACCACAGGAGCAGGGATTATGGCAAGTCTGACAGGGACGGAAGTGGATGATTTATTGGAGGGAACGGAAGCAGCAGACCAGATTTTGGGACTGGCGGGCAATGATGTGGCGATCGGCTCTGATGGGGATGACTTGCTCAGTGGCAACCAGGGGAACGATCGCCTATTTGGCGGTCCCGGTGAAGATACCATCTACGGGGGACAAGACAACGATTTAATCCAGGCTGGGGAAGATGAGGACTTGATTTTTGGCGATCGGGGGAACGATCGCATCTGGGGAGACGGCGGCGATGACACCATCTTTGGCGGCTTAGGCAGCGATATCTTGTCCGGTAATACCGGCACTGATGAACTCTACGGCAACCAAAGTGACGATACCATCTATGGCGGTCAAAATAACGAT

# Questionable array : YES	 Score: 1.35
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:0.46, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTGCGAGAGATGACTCGCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [4,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [55.0-40.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.27,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_223227 flag=1 multi=2.0000 len=1606		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	================================	=====================================	==================
       904	    32	 100.0	    37	................................	GGGGTTATGCCACGCTCAAAACTCCTACGCCCCTGAC	
       973	    32	 100.0	     0	................................	|                                    	
==========	======	======	======	================================	=====================================	==================
         2	    32	 100.0	    38	GGGCTTAGTGAGTGGTATGGTCTATCTCTTAC	                                     	       

# Left flank :   CTTTTATAGATTCCATTGACAATGAGTATAAAAAATCTATAGAATCAACTTTCCTGATGCGCTTAATAAATCCAACTTCATTTGCAAGAGCAGTTAACTTTTTTTCATTGATTTCCAACATGTTAAAAATGTGGACAAATTCTTCTTTTTTCATTGTAGTGATTTTTAGTGTATTATCAATGTAATAATAGTATATATACTATTATTGATAATAATTAAAAATCTATTAACAATCCAATATATTTATTAGCTTTAGCTTAATTAAAGGCCATTAAACAATGGGGTACAATGCTAAAATTCAGAGGGTGGACAGAGGTAAAACCAAATCCTTTTATATAAATCTTCCTGCAGCAGTAGCTGAAGCTTGTAAAATCGAAAAAGGCGAGGAAATGGAATGGCTGATTGAAGATAAAAATACTTTTGTACTCAAGCGAATTGCACCTCTTAAAAGCTTTATTAAAGAATCAAAAAAATCTTAAGTTGACGCGTATGCCCTGACG
# Right flank :  CTGAGCGATGCCCCGCCATAAATTATTGTTGTGCCTACGGGCCAACGCGGAATAAGTTACCACTAGACTCTTTCCGCCACCAGATTCCCCACCTCGGTGGTTGAGAATCCCATCCTGCCGGCTCCAAGGTCTTTCAGCTTGTTTGCCGTCACATCCATAATAGCCTTCTCCACAGCAATTGCGGCCTCCACCTCTCCAAGATGCTCAAGCATCATGGCTCCGCAGGCAATTGCCGCCAGCGGATTGATTACATTCATACCGGTATATTTCGGAGCAGAACCACCGATAGGCTCAAACATCGAAACTCCCCCGGGATTGATGTTTCCTCCGGCTGCGATCCCCATTCCTCCCTGGGTTATGGCACCGAGATCAGTAATGATGTCTCCGAACATATTAGTGGTAACAATTACATCAAACCATTCCGGATTCTTTACCATCCACATGCATGTTGCGTCGACATGATAATATTCCCTCTTAATATCGGGATATTCAGCCTGCCC

# Questionable array : NO	 Score: 3.37
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.7, 7:0.78, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGGCTTAGTGAGTGGTATGGTCTATCTCTTAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.12%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-1.20,0.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [65.0-48.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.64,0   Confidence: HIGH] 

# Array family : NA
//

>k141_223646 flag=1 multi=10.0000 len=1313		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence         	Insertion/Deletion
==========	======	======	======	================================	========================	==================
      1089	    32	 100.0	    24	................................	TCAGCCCTCCTCTGTTTGTAGTTG	
      1145	    32	 100.0	    19	................................	ACCCTGCGGTTCGCCTTAC     	
      1196	    32	  68.8	     0	.............T.TC.......TAGCA.TA	|                       	
==========	======	======	======	================================	========================	==================
         3	    32	  89.6	    22	GGCTACAGCCCTCCTCTGTTTGTAGTTGGGCT	                        	       

# Left flank :   TACTTTTCCCATACCCCGCCGCTCCCAAAATCGGTACTACCCGCTGTTTCCCCCCCAGTCCCAGGCGAATTTGCTGCAGGCACTTTTCCAGCACCCCCTCCCGGCGAATCGCAGTTAAAATATAAGGGTCCAGAGCAAGGCGCGATCGAGCCCCACACCGCCACACCGCCTCCACCAGCTCCCCAGTCGGCTCCTCCGGTTCCGGGTCCGGTTCCCATTCCTCAGATACCCCCGCAATCTCCTCCCAGTCCAAGTCCAGAACATGGCAAATTTCCTGAAAAAAGATGCGGTCAACCGGTTTGCCATTAAAAAACTTGCTCACCGTCGTCCAAGAAGCAATTGCCCGCTCGTTCACCAGAGCCGTTTTCGTAAGTTGTTTACGTGTTAACGCTAATTCTGCTTTCTCCTGACCCGCCTCGCAAGCACGAAGGGAGCGACCCGCCATATCCCAACCCCTGAAGTGGAAACCATTTCCTTATCCTAGCAAAAGTTTGTAGTTG
# Right flank :  GCCAAAACAGTTAGGACAAAAATTTTAACCACCAGGAGTTCTGTAGGGTGGGCAGTGCCTACGACTGCTACTGGTTATGAACAGAC

# Questionable array : YES	 Score: 0.97
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.48, 5:0, 6:0.7, 7:-1.30, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCTACAGCCCTCCTCTGTTTGTAGTTGGGCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,11] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,-0.90] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-10] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [56.7-53.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_225952 flag=1 multi=7.0000 len=2107		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                               	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	==============================================	=============================================	==================
       363	    46	 100.0	    45	..............................................	GTGGACGAGACGAGTGGATGATGCATGCCCTCGATCGACCCTCCT	
       272	    46	  93.5	     0	.............................G...T.........G..	|                                            	
==========	======	======	======	==============================================	=============================================	==================
         2	    46	  96.8	    46	GATACTCGAGCGACTGCCGGTCTCTTTTTCACTCTGTATTCCTTGA	                                             	       

# Left flank :   CACTTCCAGAAGAGCACGAGAATGGTCGAAAAGTTGAGCCCTTCCATTACGTGTATTCCTCCATTGCTGGTTTTGGGTCTCCACATTTCCACATTTTGTTGTATCTTGAATCGTACTGCGGCCTGGATGAGTGGTACTGAACATTGGACCCATAGAGGTACCAACGTGCGATCCAAGTGGTGACGATATTGCGACCGAGCTCGGGTCTGAAATGGCCAAGAACGGCAATGTTGTCAGCGATTCTGACTCAGAGTCAGAATCGCAGCCACTTGGACAGAATGCCCAACTTGAGATCACGGACATACGGTAGTTCAAGAGGGCCCTGGCGGGTTAAGCCTCTTGTTCCCGTAGCCCTCGTCATCCCCCTGCAATGTCGTGTGTTCGTCAATTACAATCGTCTCTTTCACGTTTTCAAACAACAAACGGTGGTGTTCAATAATAAGTAATAATGTATTTGAATGATGAATAATGCAATGGTGCATCGATATATTTCTCGGTGG
# Right flank :  AAGTGGCGTATCAAATTTTGTTAACAGCTCCATCTTGTCCATTATTTTGGGATCTGTGTCGTCGAGTCCATAATTCCGGTACCCTTTCCTCTCCTGTATTGGAAATTTATACATGATACTTGTTGACTGTACATGCAGAAATGTATTGCTCCCGTCATTGCGATTGCAATCAATGATACCACAAGTACCCATTCTGTTTCTATTTTTGCTTTTACAATCAAAACATG

# Questionable array : YES	 Score: 1.34
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.84, 5:0, 6:0.25, 7:0.05, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GATACTCGAGCGACTGCCGGTCTCTTTTTCACTCTGTATTCCTTGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:52.17%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-5.50,-4.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [3-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [60.0-68.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_230680 flag=0 multi=11.0629 len=1541		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence                                                   	Insertion/Deletion
==========	======	======	======	================================	==================================================================	==================
       250	    32	 100.0	    66	................................	CCTTTGTCCTTTGTCCTTTGTCATTTGTCATTTGTTCTTGGTCTTTAGTAGCACAGCAAATATCAT	
       348	    32	 100.0	     0	................................	|                                                                 	
==========	======	======	======	================================	==================================================================	==================
         2	    32	 100.0	    67	GTAGTAGGGTGGGCAGTGCCCACCCTACAGAA	                                                                  	       

# Left flank :   TCGCGGCTAACAGGCTATCGGTTTTACCGAGATATTCATCCCGCGATGCTTCGCCCTTACTCTCGCGGCTAACAGGCTATCGGTTTTACCGAGATATTCATCCCGCGATGCTTCGCCCTTACTCTCGCGGCTAACAGGCTATCGGTTTTACCGAGATATTCATCCCGCGATGCTTCGCCCTTACTCTCGCGGCTAACTTGGCTAAGAAACCCGGTTTCTGGCACACAAGTAGTAGTAGGGTGGGCAGTAG
# Right flank :  AATTTGATAATCATCAATCATCAATCATCAATTATCAATTATCAATTATCAATTATTTAACGCGATAATTGAATGCCATGAGGGAGACATTTGGCTACCCAAAAATCTAAATCAGGGTCATTTAGGGTAGTTTGCACTTGTTGACGCACTCGCTCCGCTTCCGACAGGTCCGGCGTGAGGGCAAATACTGTGGGACCACTGCCAGACATCATGGTGCCCAAAACACCTTTCTCGGCGAAGGCTGCGCGCAAGCGAGCTACGGGAGGATGGGCGGGGAGGACAACTTTTTCTAGGTCGTTGTGGAGGAGTTGGGCGATTTTGTGGCTGTCTTTGCGGGCGATCGCGCCTACCATCGGGCCAGAGTGCAACCGTTGCCGCCGAGAAAGGTGTAACTGTTCTGGCGCTGCAGCATAAGTGCTGCCAAACTGTTGGCGGTAGGTTTGGTAAGCCCAAGCGGTGGAAACCTGTAAATTCCGATATTTCGCCAAAACCACATACAA

# Questionable array : YES	 Score: 0.79
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.7, 7:-1.8, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTAGTAGGGTGGGCAGTGCCCACCCTACAGAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [8,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-17.70,-17.70] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [48.3-81.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_247220 flag=1 multi=10.3345 len=3510		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence        	Spacer_Sequence               	Insertion/Deletion
==========	======	======	======	=======================	==============================	==================
        76	    23	 100.0	    30	.......................	AGGGCGGCGTTACGGGAGTTTTGCGTAAGT	
        23	    23	 100.0	     0	.......................	|                             	
==========	======	======	======	=======================	==============================	==================
         2	    23	 100.0	    31	TAGGCGGTACTCTAACCGCCCAG	                              	       

# Left flank :   CGGGATCTAATTGGGCGTTAGCATTAGTGTCGTTGAATTTGAGTCCGCTGATAGTACCGGCAAAGAAGTTATTACCAAAGTTAATGCCTGTAGCGTTTGTACCACTGACAATGCTAATTGGGGCCGGGTTGGGTGTAGTTTGGCGAAAATTGGGTTGTTGAACTTCCCTAACTGGATAATTGCCTGGTGGTAAGTTAACAAAACTGTAGTTACCTTGGGCATTAGTTGAGGTTAAAGGTTCGTTAGAGTCCGGCGTACCATTATTGTTAATATCAAGGAAGATTTGCCAGTTGGGGAGTGTAGGTTCTCCGGGATCGAGGATACCATTGGCATTAACATCATTAAATTTTAAGCCGCTGATTGTCCCGACTTGGACGTTGCCAAAGTTGACATTGCCGATGTCTGTTTGGCTACTGGTTCTGGTATTGCTGATTGATTCTGGGATGCTGGCCATAGCACTTTCAAGATGATTAAAGGTTTGTTTACGTAACGCCGCCTTC
# Right flank :  G

# Questionable array : NO	 Score: 2.41
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.27, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TAGGCGGTACTCTAACCGCCCAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-1.50,-1.70] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_256102 flag=1 multi=10.0000 len=1094		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                          	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	=========================================	===================================	==================
      1001	    41	 100.0	    35	.........................................	GTTGTTATGGACAACTCTATTTCATCAGGCATTCT	
       925	    41	  92.7	     0	........C..........................A..T..	|                                  	
==========	======	======	======	=========================================	===================================	==================
         2	    41	  96.3	    36	TGTTGTACTGAGAGCAATCCTTGTCTTCCTTGTACGTTGCC	                                   	       

# Left flank :   GTTACGGTCGGACTTGATTTGGTTTGATGTTGTTCGGGACAGCACTGACGTTACGGTCGGGCTTTCCATTTGCTGGTATTTATTAGGCATGA
# Right flank :  AAAGCACCGACGTTATGGTCGGGCTTTCAACTTCATTGTGTTCTGTTATGTTGAGATCATCATTAATCTCAACTTCGACTACGTCCTTGTTTGTTGTGGGGTTTTCATTTCCATCCCCAATCACTCCTGGGAGCTCGACATGTCCTTGAGTCGGCTGATCCCAGGTAACTTGGTTGTGGGTCCAAGTTCCAGCTCTCAAGGCTGTACATTCCGATGGACTTTCCACACGGCTCTCTAGGCCATTCTTCCTTGTTGACGTCATCTGCTGCGTTACTTGTATAGCGCTCTTTAGCGCTGCAGCAGTCATTGCCATCTTCGATACTTGTATAGCGCTCTTTAGTGCTGCGGCAGTCCTTGCCGACTTCAACACTTATAAAGCGCTCTTAAGCGCTGTGGCATGCCACTCCTTTGAGCAACATCCTTGCAGTGTGGAGTGTGGAGCATCATTCTGTGACATCTCCTATGCACTCCTGTAATCCAACTCAATTTGTTGACTGTGG

# Questionable array : NO	 Score: 1.96
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.81, 5:0, 6:0.25, 7:0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGTTGTACTGAGAGCAATCCTTGTCTTCCTTGTACGTTGCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.66%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.30,-2.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [3-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [58.3-51.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_262324 flag=1 multi=3.0000 len=1702		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence                            	Insertion/Deletion
==========	======	======	======	================================	===========================================	==================
      1269	    32	 100.0	    43	................................	AAAAGATTTATGTTTACCTAAAAAAAATCCGTCAAAACTTCAT	
      1344	    32	 100.0	     0	................................	|                                          	
==========	======	======	======	================================	===========================================	==================
         2	    32	 100.0	    53	CCCTAGACCTGTAGTAAGCCCTTCAGGGCTAA	                                           	       

# Left flank :   TAAATTAGAAGAAATAATCCGCTATCCTTGGACATCTACACCCATTAGGGAGCGAAAAATTCTGTCCGATCAGTTTTTAATTCAATATAGTCATGCTCGCTGTTGTTCTTTATTACGTTTAGGCGATCGCGATCAATTAATTACTTTAGAATCATCTAATTTAATAGCAACATTACCCTTAAAAATTAGCCCCAATTCCCCCTCAATTCCCTGGTTAAATCCCCAGGGAAAACTACAATTTACCCATAGTTATGAATATCAGCTACTAACCCAATCTGTGAATCTGGTTGATATTTTATTTTGTTGCGAAAATCATCAAAATTGGCAAAAAATTGCCGAACAATTTAGCCAAAATTTTCAACAATTTTATCGTTACTGTCAAATTTGGGGAGAAGTCAAACAAAGACAACCGGAGTTAGCTCAAGCTAGACTAGGTTTAGTTTTGATTACCCAATTTATCCTAAAAATTTTATTAGAAGAAAAATTAGGAACAATGGCTC
# Right flank :  CTAATCAACTAAAAAATCCGTCAGAATTTCATCCGGTTCATCCTGATCATTATCATCGGTTAAAAGTTGGGAAAAAGTAGAACTAGGGGAAAGAGAAAGTTCCGGGATAACTATTTTTTCTAAAGCAACTATTTTGGCTTCTAGTTTAGCAACTTGATTTAATAATGGTTTTAAAGATTCAACTTTTCCCTGTAAATTTAATAACTCTTTTTGGAGATTAAAATATTCTAAATCGACGGACTCACTACTCAAAAACACTCTAGTTTTATCTAAATACTGACCAATTGCTTCTTGAACTAATTCTTCTAAGGAATGTCCGGTTTCTAG

# Questionable array : NO	 Score: 2.68
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.7, 7:0.09, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCCTAGACCTGTAGTAAGCCCTTCAGGGCTAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [8,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-5.40,-6.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [75.0-63.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.64,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_259036 flag=1 multi=4.0000 len=1069		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                                        	Insertion/Deletion
==========	======	======	======	==========================	=======================================================	==================
       721	    26	 100.0	    55	..........................	TAGCCTCGTTGCCAGTGATAGTGACATTGGTGAGATTGAAAGTGCCATCGTAAGC	
       802	    26	  96.2	     0	..C.......................	|                                                      	
==========	======	======	======	==========================	=======================================================	==================
         2	    26	  98.1	    56	ATAAATGCCACCGCCGTTGGTATTTG	                                                       	       

# Left flank :   GTAGCACCATTATAGATGCCAGCGCCGCTGCTGGATGAGATATTATCGGCGATGGTGCTGCCGTTGATGGTGACGGTGGCATCCCCATCGTAGATGTAAATACCACCACCATAATCAGTAGTAGCTTCATTCCCAGAGATGGTGCTGTCGTTGATTTCTACCGTAGGACCATCTTCAATATAGATGCCGCCACCATAGGTTGCCGTATTCCCAGTGATGGTGGTGTTATTGATGGTCAGGAAGGCATCACTATCGTAAGCGTAAATGCCACCCCCATAGGAGTCCGCATTATTCCCAGAGATTTCACTATCAGTGATGGTGAGGGTGCCAGATTCTAGGTAGATGCCACCACCATAGGAGTCCGCCTGATTGTTAGTGATAGTGGTGTTATTGATGGTGACAACAGCAGTGTCTTCGTAGATATAGATACCACCACCATCTTCACCCGCCGTATTCCCGGAGATGTCGCTATCGGTAATGGTGACAGTGCCATCGCTAAT
# Right flank :  CCTCGTTGTCAGTGATAGTGACGTTGGTGATATCGAAAGTGGCATAGTAAGTGCTGATGCCACCACCGTAATCGCCCGATGTATTCCCGGAGATGTCGCTATCGGTAATGGTGACAGTGCCATCATAAACATAGATACCACCGCCATCACCATCACCAGCACCATCGTCCGCCGTGTTGCCAGTGATAGTGGCCTCACTGATGGCGAGGTCGCCAGCCTCCATATTGATACCACCACCATCC

# Questionable array : YES	 Score: 0.66
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.90, 5:0, 6:0.26, 7:-0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATAAATGCCACCGCCGTTGGTATTTG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.85%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-2.20,-2.10] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [46.7-53.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_2580 flag=1 multi=17.6618 len=4284		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                            	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	===========================================	=======================================	==================
         1	    43	 100.0	    39	...........................................	TGGGGGGACGGGGGGCGATTCCCCCCTCTCCCCCCCTGG	
        83	    43	  95.3	     0	...................................T....G..	|                                      	
==========	======	======	======	===========================================	=======================================	==================
         2	    43	  97.7	    40	GAGAGGGGACGGGGGTGAGGGCTTTAGCGGGGAGACGGGGAGA	                                       	       

# Left flank :   |
# Right flank :  ACCAGGATAAGAGGCTCTGGACTCAAGGAATCACATCGGTGACGACTTCCCCACCACTGAAAGTAATTGTGCGGTCTTGTAGTTTGCCAAAAGCCTTTTTTCCCCTGGTGGTCATTGGGGGATTATTTTGGCGGTAAACTACATTGCCCTCGGCTTGGATTTGCTCTTTGGTGATATCCCACATCATCTGGTCAGATGCCAAATCGAATTGGCCCTCGGTGCTGGTGCCGCTGACGCCTTCTGATAAATAAAAGATGTTGCTGGTTATATCCAGCTTCCCTTTTTGGGCTTTGATAATGGTTTTTTGCTGCTCGTGAACTATGGCAATGGGTTCGTCGGAGGTGATAGTTTTCTCCTCTAGATTCCACTCAATTAAATTCCCAGCCACATTAACTGGGGGTTTGCTCAGATCCAGGCTGGCGTTTTTCTCCAGGGTAGCGGTTTTCTTGGCTAGATTCATTTTGCCTTGGTTGGCAGTGGCTCGGTCAGTTGGGGTACAT

# Questionable array : NO	 Score: 1.64
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:0.25, 7:0.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAGAGGGGACGGGGGTGAGGGCTTTAGCGGGGAGACGGGGAGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [9,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.30,0.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.15,0   Confidence: HIGH] 

# Array family : NA
//

>k141_268134 flag=1 multi=10.0000 len=2923		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                            	Spacer_Sequence              	Insertion/Deletion
==========	======	======	======	===========================================================================	=============================	==================
       909	    75	 100.0	    29	...........................................................................	TAATGGTTTTGTTATGATCTATATTTTGT	
      1013	    75	 100.0	     0	...........................................................................	|                            	
==========	======	======	======	===========================================================================	=============================	==================
         2	    75	 100.0	    39	GCGGAATGGACGGGACTCGAACCCGCGACCTCCTGCGTGACAGGCAGGCATTCTAACCAGCTGAACTACCACTCC	                             	       

# Left flank :   ATGCTAGTTTCCGAGACTTTGGATAGGGGAATATCAACTCCCTCGGTGGTTAGGTAGTATTGCCCTAAAATGCGTGAAACAGAGAAACTGACGATGTTTCCTTGGTAATTGTAAGAGCCCGAAAGAATGCCAAGAGCCGGAAGAGCTTCTCCGATGTAGTGTTCGTTGTTGATGAGAAATTGGACTTCAAAGCCGATTTCCAGTTTTCGGACTGACACTGCCATTTCGACAATATCGCTGCTTTCATAGGTGCCAATGTATTTGGTCTCTGCGGAAGTTTGTGCATGGTTGCTTTCTACAAACAAGAAAAAGACAAAAGTAAGAGAGAATAAATTTTTCATAACTGATTTTTAAAAAGGAAAATAAGCACTTAAGCAACAACATCTTACAATCGAAGTTTTGCCTTGAAGACTAGATAAAAGTAATTAAATCAGGACGAATTTGATTGAAATCGGGAACAGTAAAAAACAAAAAAGCCTTAGATTTCTCTAAGGCTTTAA
# Right flank :  CGTTTTATTCCTTCTCGTTAGAAGTGGTGCAAATGTAGGTATTTGATTATTTTAAGCAAGAAAGCCAAAGAACATTTTTCCATTTATAATTTATCTGATTCTTTGTCAGCCCACTAATTATTGGAAGCATACTTTCCTTTGCCCCTTTCAGCAAAGTTTGTCACTATGGTATAATCCTTATCTTTGCCCTTCCCAAAAATCACTTAAATGGTATTAGAATTCGAGAAACCTATTGCTGATTTAGAGCAAAAACTTCAAGAGATGAAAGACCTCGCCAAAGGCCGAAATATCGACCTGAGTAGTGATATTCAATCTTTGGAAGAAAAAATTCTGGCTTTGAAGAAAGAGACTTTCGAAAACTTGACTCGCTGGCAGCGTGTGCAGCTTTCCCGGCATGCTGATCGACCCTACTCGCTGGATTATATCTATGAAATTACCAATGATTTCATTGAAATACATGGCGATCGAAGCGTCAAAGACGATAAAGCGATGGTAGGAGG

# Questionable array : YES	 Score: 0.93
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:0.04, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCGGAATGGACGGGACTCGAACCCGCGACCTCCTGCGTGACAGGCAGGCATTCTAACCAGCTGAACTACCACTCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [18,11] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-15.20,-18.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [70.0-68.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_273164 flag=1 multi=3.0000 len=1494		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	==============================	==========================================	==================
        19	    30	 100.0	    42	..............................	GTCCAAAGATGGCAATGAAGATGGCTCCAAACTTGGTAATCC	
        91	    30	  86.7	     0	...T..C.....A..A..............	|                                         	
==========	======	======	======	==============================	==========================================	==================
         2	    30	  93.3	    43	TGAAGGTAAGCTCGACGGACCTGAGCTTGG	                                          	       

# Left flank :   GTCGACACTTGGAACTTTT
# Right flank :  GACCTAGAGATGGTGATATCGATGGCTCAACACTTGGTAAGATTGTCGGCAATCTTGATGGATGAAAGCTTGGGCCCTGCGAGGGATCACCTGTTGGACTAATGGAAGGCCTTCTACTTGGAGATACTGTTGGGAGTCTTGATAGTTGTTCACTTGGAAATATTGAAGGCTGCATTGATGGTGAATCACTTGGATCAAGTGAACGAGTTGGTTGGATTGATGGCTGATCGGATGGGGCCACAGATACAGAAGGATCAGTAGATGGTTCATCACTGGGGGCCTTTGATAGATCATCCGAGGGCTCCAAACTTGGTTGAATCGATGTTGTGGGCCGCTCTGAAGGCAAAATGCTGGGAATCGTCGATGAACTTGGTTCATTTGTTGGAGCATTGCTCGGTTGAAGCGACAGAGAGGGTTCCCCACTAGGTTCTTCAATTGGCACTGGCGATAATGATGAAAAGCTTGAAGGATCCTCGCTTGGTACTGAAGACAATGACGGA

# Questionable array : YES	 Score: 0.46
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.66, 5:-1.5, 6:0.99, 7:0.11, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGAAGGTAAGCTCGACGGACCTGAGCTTGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [7,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-1.80,-1.60] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [16.7-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.27   Confidence: MEDIUM] 

# Array family : NA
//

>k141_275378 flag=1 multi=3.0000 len=1081		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence        	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	=======================	=====================================	==================
       117	    23	 100.0	    37	.......................	GCACTGCTAACAATGTCTTTGGAAATGCTCAAAGCAC	
       177	    23	 100.0	     0	.......................	|                                    	
==========	======	======	======	=======================	=====================================	==================
         2	    23	 100.0	    47	TGCCCAGCAGCGGGTCAACACTG	                                     	       

# Left flank :   GCCCCTACACAGTGACAGTGAAGCTTGGTGGCACGCCAATAACAAAAAATGTGACCGACCGCACACAAAAAGACAACAACCATGAGCGACGAAAGTGCTGCCCCAGCCAGCAGAGGT
# Right flank :  GCTAACAATGTCTTTGGAAATGCTCAAAGCACTGTTGATGGGAAGAACACCGCAATTCGTGTGTTCAATTTGTACCAAACTGAGGTGCAAAGTGGTGACCATCATTTTCATGATCTCGTTCTTGAGGATGTGGAAAATGATAACCTTGAAGATCTGGTGACAGGTCTTGCTCACTGGATGGGCCATGTTCCAATTCCACAAGACAATGGCAGGGGACATTTCAAAGAGCAATGCCCTTATTGGAGAATTAGTTGGCATTATTCGAGATAAAAGAGGTATTCCAGATGGTTTCTTTGCTGCCTCTCCTCCGAAACGACAATGTGTCACACGAAACCCTGCAGAATTGGTGCAGTTGACACCGAGTGTCACCAATGCCCTGGAAGGACAGTCTGAGGCAGTTCCTTCTGTATTGAAACAGTTGACTACTGAAACTGAATTGTTCAAGGCTGGCGAGGTTGGAAGTAGCAAGAAGATACTTGTTAGCACTCTCTTGCGGGACT

# Questionable array : NO	 Score: 2.92
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.78, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGCCCAGCAGCGGGTCAACACTG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [5,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [43.3-56.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_27308 flag=1 multi=13.7361 len=1350		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence       	Spacer_Sequence                                  	Insertion/Deletion
==========	======	======	======	======================	=================================================	==================
       934	    22	 100.0	    49	......................	GGTGCGATCGGGCGATCTGGGCAACGACGGTGCGGTCGGCTTGAAGAGC	
      1005	    22	  86.4	    47	..................CTA.	GGACGCACAGGCGGCCCGTACCCCGCGACCGAAGCGCAGCTTCGTAG  	
      1074	    22	 100.0	    49	......................	GATGCGGTCGGACGTTTCGGCCAACGACGATGCGCTTGGCTTGGAGAGC	
      1145	    22	  81.8	     0	........G.........CTA.	|                                                	
==========	======	======	======	======================	=================================================	==================
         4	    22	  92.0	    48	AGCCGAGCATGCTCGGCTGCGC	                                                 	       

# Left flank :   GCACCGAGGAGGAGAAGATGACGCCGTGGATGGGCGCGCTGACCGACAACCTCGAGGTGCTGGCCGACCCGCAGGAGGGCGGCTCCTGGGGCCGCGCGGCGACCAACGACCTGCTCGCCTCGCGGATCAAGATCCGCTCGCTGAACTTCATGCGCGGGCGCACCTTCCTGTCGCGCTACGTGATCATCGACGAGGCGCAGAACCTGACGCCCAAGCAGATGAAGACCCTGCTCACCCGCGCGGGTCCCGGCACCAAGATGGTCTGCCTGGGCAATGTCGAGCAGATCGACACGCCGTACCTGACCGAGACCACCTCGGGCCTGACCTACGCGGTGGACCGCTTCAAGGCCTGGCAGCATTCGGCGCACATCACGCTGCGCCGCGGCGAACGCTCGCGGCTGGCGGATTTCGCTTCGGAGCAGTTATAGGCGCAGGTGCCGGGGCGGGAATCAGCGGCGCCGCATTCGCTCAATACGTGCAGACTGCAGTGCAGCCAGTAG
# Right flank :  AAAAGCAAAAAAAACGGGGCTCCTTTGGGAGCCCCGTAAGGGTTGCCTTGAAGCCCTGGAGTGAATCAGAAAGCCATCGACCACACGGCCTCGACTTCCACCTGCTCGCCGCCTGCATTGCGGTTGCCGAAGGAAGCGTTGCCCCGGAAGGATTGGGCCGGTGCGTAACTGGCGGCCATCCGCA

# Questionable array : NO	 Score: 1.86
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.60, 5:0, 6:-0.25, 7:-0.09, 8:0.6, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGCCGAGCATGCTCGGCTGCGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [3,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [3-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [36.7-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_277010 flag=1 multi=7.0000 len=1462		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                   	Spacer_Sequence    	Insertion/Deletion
==========	======	======	======	==================================	===================	==================
       352	    34	 100.0	    19	..................................	AGTAGGGTGGGCTTGCATC	
       405	    34	 100.0	    18	..................................	AGTAGGGTGGGCAAAACT 	
       457	    34	 100.0	    19	..................................	AGTAGGGTGGGCTTGCATC	
       510	    34	  70.6	     0	..................GATT..C.AGC.G..A	|                  	
==========	======	======	======	==================================	===================	==================
         4	    34	  92.7	    19	TTGCCCACCCTACGAATATGGATAATTTTAAAAC	                   	       

# Left flank :   AACACCAACTGGTCTTTAGCATCATCCTGGGGCAACTGGGGCAACTGGGCCACAACCGCATCAAAAACTTGGGGCTTAATTGCCAACAAAAGCACTTGATTTGCCAAGGCGATCGCCTGATTATCAGATGTCACCTGAACCTGATATTTTTCACTTAAATATTCTCGTCGCTGAGGTAGCGGATCGCTAACGATCACCTCTTGAGGCTCATAGACTTTTTGGGCGATTAAGCGGGATAAAAGCGCCTCTCCCATGACCCCACCGCCAATCATGCCTAATTTTATGGACAATTTTCTGTAATCCTTTCTGCTAATTATGGATAATTTTAAAACAGTAGGGTGGGCTTGCATCT
# Right flank :  AGCGAGAAACTTACTGAGAAGCCATCCGCGCTTGGTCATCATTCCAGTTTCCGGTCGGCGCTATTGGCTGACGAGAAGAGCGGATATGATGTTGCAGCACATCATGGACAACACCAGACTGAGTGCTGACTTGAACACAATTCGGTGTAAACAAAAAGATGCTTTCACCAATTCTTTCTTGGTGGCCATCCAGGGCATAAGTGCCACCGGCAATAAAATCGACGGCTCTTTGAGCTTGGTCGGGATCCATCATGGTGAGATTCAAAACCACAGATTTGCGTTCCCGCAGACCTTGGATGGCTTGGGGCATTTCTTCAAAAGAGCGAGGTTCCATCACCACAACTTCGCTGATCCCATTAGCGGCACCGGGCATTCCAATCACATTATTCATCCCCTTTAAATTTGTATTCATTCCTGTATCTGCTCCACCCATACCAATATTTGAAGAACTCATGCCAACAGCACTAACTGTTCTTTCTCGAAGTCTACTACGGGTCTGG

# Questionable array : YES	 Score: 0.90
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.64, 5:0, 6:0.26, 7:-1.40, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTGCCCACCCTACGAATATGGATAATTTTAAAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:64.71%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-2.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-10] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [65.0-43.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_280252 flag=1 multi=3.0000 len=1054		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence       	Spacer_Sequence                               	Insertion/Deletion
==========	======	======	======	======================	==============================================	==================
       206	    22	 100.0	    46	......................	TGACAACGTTTGTATTTAATATCCCTTTAGGGCCAGCCGACATGTA	
       138	    22	 100.0	     0	......................	|                                             	
==========	======	======	======	======================	==============================================	==================
         2	    22	 100.0	    47	CATTGAATCAGCCCCAGAGGGG	                                              	       

# Left flank :   GGAAGCGGAAAGAGCTGTCTGTACTCATCTCAATGATGCCGTACAAGGTTTGGCCCATCTGTGTTCCTGCATTGAAATTGTTCATATCAATTGCCGTAGGGTCATTTTTACAATCAATCAGGTAGCTGAAGGTAATGGTATCGGAGGTTGAACCAAAACGTGTGAGCCAAAGACCTTTTCCGTCGGGATTAAATACCAGGGATTGTCCTTCGCGGTTGGTCCATGCGCCGGTGGGTTGGGTGCAGTTTTTTTCGCCACAACCCAGGAGGCAGATCATAAAAATTGCGGCAATGACCGCCTTTGACGAATGGGTATTCATGGTTTTCCTGTTCATTGGCTTTCGTAATGTCTGTATAAAAGACAAGGTTTGCATGGAAAGGTTGAGAGGAAGAGGTAAATCGTATGTTTCAACTAAAAACACCGTCACGTTGTCGCCCCGCTGGGGCTCAGGATCAGGGTGGGTATAACTTGGCTACAGACGTTGTCGCCCCTCTGGGGCT
# Right flank :  GCGGCAACGTCTGTAGCAAATCATACCCACCTCACTCCTGAGCCCCAGCGGGGCGGCAACGTCTGTAGCAAATCAAACCCACCTCACTCCTGAGCCCCAGCGGGGCGGCAATGTGTT

# Questionable array : NO	 Score: 1.68
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-0.25, 7:0.04, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CATTGAATCAGCCCCAGAGGGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [36.7-40.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_282803 flag=1 multi=17.7731 len=7066		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                             	Spacer_Sequence                                                      	Insertion/Deletion
==========	======	======	======	============================================	=====================================================================	==================
       272	    44	 100.0	    69	............................................	CCAGATAGATGAGCAATCTGCTGTATAGTCATTTCAAATAAGAATGAGATTTTAGTCTGCCCTCCTCCC	
       159	    44	 100.0	     0	............................................	|                                                                    	
==========	======	======	======	============================================	=====================================================================	==================
         2	    44	 100.0	    70	CCCCTCTCCCTCCCTGGGAGAGGGGTTTGGGGTGAGGGCTTTAG	                                                                     	       

# Left flank :   CCCCACCATCCCAAGAATATGAATCAATGAAAATTACGCCTCTCTCATCTATCTGGCTAATTTTTATCGCCAACCTTAACCTAGGATGGGTTTTGTCCCAATCAAAATCCACTGAATGGGTCTGGCTAGGATTCACATTTTTTGTCTTGATAATGGCCGAATCTTTAGCCTCACCTTGGGCGCTCATTAGAAACGTATTTTTTCGCTGGATGGAATCAGATTTAAGAGCATTTTTGACCGTAATATTTACTTCATTTGTCGCGGTAATCATGCTCTCCTGGTTGCACGTATCAACCCATTTATTAATGTTAGTTATCTCCGCATCCATTGCCAGACTAGATTTACAAACAGCCAATGTAAGCGAATTAAATGCTTTCTTGATTTTAGCCAGTTTATCTTTATTCGGCTTAGGGGGGGGTTGGCTCATGTATCAATGGCATTTTTTATTGTTCAGCCATTAGATACAGCACTTTGTGAAATGATGTAGTACATCTTCAAAG
# Right flank :  GGGCAATATCGAGTGTTTCATTGGGTGCATCTCAGTTAGGCAAATTTGCCCTCATCCCCCAACCCCTTCTCCCAAACAGGGAGAAGGGGAGGCGACAAAGTGAGATGCACCCGTTT

# Questionable array : YES	 Score: 0.04
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-2.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCCCTCTCCCTCCCTGGGAGAGGGGTTTGGGGTGAGGGCTTTAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-10.30,-15.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [51.7-68.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_286822 flag=1 multi=5.0778 len=1220		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                            	Spacer_Sequence                                            	Insertion/Deletion
==========	======	======	======	===========================================	===========================================================	==================
        22	    43	 100.0	    59	...........................................	ATAGTTTGCCCGAATGCTTCGCCCCTACAGGACGCGGACACCTAATAAAAATTAGGTTT	
       124	    43	 100.0	     0	...........................................	|                                                          	
==========	======	======	======	===========================================	===========================================================	==================
         2	    43	 100.0	    60	AGGGCGAAGCATTCGGGGCGGAAATTTTCCTGGTGATTCAGAG	                                                           	       

# Left flank :   ATTCTCTGATGTTTCCGGAAAA
# Right flank :  GGTTATTTGCCCGAATGCTTCGCCCCTACAGGTTATTTGGCAGCCTTTGCTTCTTTAATATGCTTCTTAGCATAAGCTTTTGCCTCATCATCCGCTTGCTTTAACAACGCTTTCAAACTTTCCTCATTGCCAATAATTCCCTTAACTTCTTTCAACAACGGCATAAAACTTTCGCCAATAAAATCCGTATGCACAATAAACTCCTCTGCCATCAATGCTGCAATAGTTTTGGTGGCAGCGGATATCACCTCATCCTCATTTAATGGATCGGGAGAATGCAGGGTTTCTGCTGCTTTCATCTCATCATAAATTGCCTGAGTCCATTGCAAATTACTGGTAATTTCTCCATCAGCAAATATCACCCCAATTAACTCATTGCGAACTCGTCCGGTTCGGGTAGTTTGTGCCCCATAATGCCGCGTCCGCAAGATATTATTAAACACATACAAGAAACTTGCCTCAGTCGGGTCTTTCAAAGTCACAATACTGGGGAAAAATAC

# Questionable array : YES	 Score: 1.04
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-1.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGGCGAAGCATTCGGGGCGGAAATTTTCCTGGTGATTCAGAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [10,10] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-4.70,-2.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [21.7-56.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_288339 flag=1 multi=5.0000 len=1024		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence      	Spacer_Sequence                                            	Insertion/Deletion
==========	======	======	======	=====================	===========================================================	==================
       579	    21	 100.0	    59	.....................	TGATGCCACACCTTGGCACCTTTCCTTTTTCTTGAGCATGGCAAAATTTTGGTGTCAGC	
       659	    21	 100.0	     0	.....................	|                                                          	
==========	======	======	======	=====================	===========================================================	==================
         2	    21	 100.0	    69	GGCAAACTGTTATTGGCATTT	                                                           	       

# Left flank :   AGGTGCCTTACTGCTAATTTGTGTTCTCTTTCCATTTCCTTTTTCCAACATTCACAGTATTCCTTCCACTCCTTGTGTACTTGGTCTGAATTGAGTTCCTCATCTGCCTCCTCTGGATCAATACGACGTTCACCTATAGGTGAGAGAAATGCACTCAATATTGCATTGCAGTCATTTTTTTGTGAACCATCCTGCAAAAAATCCATACCATACATTGATTTTGTACCAGTGCCCCATGTAGGATTTTGTGAGGCATGACGCCTCTCTGAGAGGCTGCCATTTTCAAATGAATGGTTATGTTTTATGTTCCTTGAATTTTCAAGTTCATTCATGTATTCATTATATTTTTGGTTTTGAATTTAATGATCATTCATTATATATATACGAATTGTCCACCTGAATCTCTTCATAGCTAAATAACTCTTTTCATCCACCATAACTAATTATCATCTTCAGACTTTAAAAATTTTGAGTCTTGGTGCAAAATTTTTTGCGTCGGT
# Right flank :  ACAAGCTGCACCTTGGCACCTTTCCTTTTTCTTGAGTGCATTTTTTCATTAATCACCATGTACCAGCAAAAAGGCACTGTGGTTATATTTCTAGGAATTGAGTCCGCAATATAGACTGGGCTGAGTATTTATGGTCCTCACACAGGAAGGCTTCACCATCATGATCCAGACAATGGCGGCAGCAGCAGCATCGGAAGATATGCTTTGTGCGATGATGACGAAGACGCCAGTGATGATGTCCTGTGTTCAGGACATCCTATTTGGCATGTGCCTGCTGATCTCACAAGAGACTCAGGTATTATGTCGGTAGGTGCATCCCTGCTTTGAGGGTCACACTGTGGTAGC

# Questionable array : YES	 Score: 0.29
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-0.5, 7:-1.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCAAACTGTTATTGGCATTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:61.90%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [70.0-60.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.27,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_291460 flag=1 multi=5.0000 len=1237		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                	Spacer_Sequence                                     	Insertion/Deletion
==========	======	======	======	===============================	====================================================	==================
      1051	    31	 100.0	    52	...............................	TGCACCGAACCAGCAGACGCGCGCAGAAGCCTACGTGCCCCACGTGTCCTAC	
       968	    31	  96.8	     0	...C...........................	|                                                   	
==========	======	======	======	===============================	====================================================	==================
         2	    31	  98.4	    53	GTGGTTCAATCCGTTCGAGCGGTTCCCGCAC	                                                    	       

# Left flank :   CGGCTCTCCGCGAGGGTGACGGGAAGCGCCACGGTGACGCGCGGCGCGTAGAACGGCAGAAGAACAGAAGAACAGAACCATCCCAGACGACGAGAGCGCACATCCACGACATGGTGGAGGGCGTGCGCTCTCGATGATTTCGCCGTGCCCCTGTGCCGGGGCCCGCAGGGCCGCGCCGTGTCCCC
# Right flank :  GAACCGGACGCGTCAGGCGGAAGCCGCGGTGCGGGCGAGGGTCATGAGCTCGGCGGCGCTGGTGCGGAGCGCGCGGGCGATGCCGAGTTCGGCGACGGCGGGGGCGACCATCGTGTAGCCGAGGTAGTAGCCGGCGCGTTCCGGCACGACGTGGCGCTCGATCGTGCGGGCGTCCTCGGAGACGCCGTCGGTGAGCCAGCGGAGGCGCAGCCCGAGTCCCGTGCGATCGAGGTCGGGGGCGACGGCGCGGGCCAGCAGCGGCTGGAGCTCGCGGATGCGGGCGTACTGGCGCCGCCCGTAGCCCAGGTACTCCCACGGCGCGTAGCCGCTGCAGACCTGTTCGGCCGCGTGCACGGCGAGCCCCTCGTTGACGAGCAGTTCGCGGAGTGTGGCCTGCTTGCCGGTGTCCCAGAACGAGTAGTAGCCGTCGCGTTGCGCGACGAGCGCGGCCAGGTCGGCGCGCGAGTCCGGGGCGCAGTAGCGGACGCCGTGGGCGATCT

# Questionable array : YES	 Score: 1.06
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.92, 5:0, 6:0.34, 7:-0.4, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTGGTTCAATCCGTTCGAGCGGTTCCCGCAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [8,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.90,-4.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [25.0-25.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.78   Confidence: MEDIUM] 

# Array family : NA
//

>k141_297907 flag=1 multi=6.0000 len=1149		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                               	Insertion/Deletion
==========	======	======	======	==========================	==============================================	==================
       233	    26	 100.0	    46	..........................	AATTGGTTTGCATTATCTGGTAGTTTAAACATTACTTGGAATATAG	
       161	    26	 100.0	    46	..........................	GACTGGTATGCGTTGTCTCAAAATCCAAACACTACTTGGGATTTTG	
        89	    26	  80.8	     0	.T......A...T.......GA....	|                                             	
==========	======	======	======	==========================	==============================================	==================
         3	    26	  93.6	    46	TCAAAGACCACCCAGACAAACCTTGG	                                              	       

# Left flank :   TCGAATCTCTGCATGTTCTGCTCGAATCTCTGCATGTTCTGCTCGAATCTCTGCATGTTCTGCTCGAATCTCTTCATGTTCTGCTCGAATCTCTTCATGTTCTGCTCGAATCTCTGCATGTTCTGCTCGAATCTCTTCATGTTCTGCTCGAATCTCTTCATGTCCTTTCTGAACGTCTGCAATCATTTCCTCTAATTGCGCTACTAAAACCCAACAACGCTTTTCTTCTTCAGTAGTCATAATAATGGTGTTTGTTAAACTTTTTATCATCCTTTATTTGATAAAAAATTTTCGAGCGTATACCGTTAAAGATTACGAGAGAAAAGTCTCTCTACCCTATGATATTCGAGATATAATTTTGTGGTCTTCTGAGACGTAACGAAAGTTGTGGATATTGAGACCATTAGATTTTTTAAAATGAAATTTACAAGTTAAAAAAGACCACAAAATAAAAATGTCTAAAATTTTAAAGGATTATGCACATTACTATTTTGAATTTA
# Right flank :  GAACTGGTTTTGGTTATCTCAACACCCAAACATTCCTTGGGATATTATCAAAGTCAATCCAGAC

# Questionable array : NO	 Score: 2.11
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.68, 5:0, 6:0.26, 7:0.08, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCAAAGACCACCCAGACAAACCTTGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,10] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [5-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [63.3-80.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.05   Confidence: HIGH] 

# Array family : NA
//

>k141_29694 flag=1 multi=12.2242 len=1305		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                                         	Insertion/Deletion
==========	======	======	======	===========================	========================================================	==================
         1	    27	 100.0	    56	...........................	TATTCTTATGACATCATTTCCTCACCGGGTTTATGAATTTCCTTGCCGGGTTTATG	
        84	    27	  92.6	     0	.................C.....G...	|                                                       	
==========	======	======	======	===========================	========================================================	==================
         2	    27	  96.3	    57	AATTTCCTCACCGGATTTATGACATCA	                                                        	       

# Left flank :   |
# Right flank :  AGCGTAAAACGGTGGCATAAACTTGCTCGTGGCGATTTTGTTTTTCACTCCACCCTCCAAGCATTGCATTCGAGACTGAGTAGTGGCGAAGCATGAAAAATTTCACAGGACCGTCTCTGGGTTTTCGCCAATAGGTTCATAGAGCATTCAAAATGACAGTGATAATGACCGGACGGATGACCAATGTCAAATGACGATTTGACGCCCTACTCCTCAGCGCGCAAATTTGGCGAAATAATGATGACGAGCCATGCAAAAGCACTGAAAACGACACGCGACAGAGAAATGCATCATGCGCTCATGAATTTGCTCCTTCCATCTTCCATGTGCAGAAGCTTTTGCTTCTTCCATGTGCAGAAGCTTTTGCTTGTTGCTGGAAACCAGTTGTTGTTATTAGTAGCTTTTATGTTGGCTTGCATTTCGTCGTGTGAAAATGGCGGGAAAATCAGTCCAGAATCGAAACCAGAATCATGGCAGTACATCACGAGAGACAGCTAGAG

# Questionable array : YES	 Score: 0.49
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.81, 5:0, 6:0.28, 7:-0.8, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AATTTCCTCACCGGATTTATGACATCA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:62.96%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,-0.90] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_298187 flag=1 multi=6.3800 len=1578		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                                       	Insertion/Deletion
==========	======	======	======	==========================	======================================================	==================
       106	    26	 100.0	    54	..........................	ATTAGCGTTAACTGTGTAAGTAAGAAAAGAAACCCGGTTTCTGGGTTTCCAAGC	
        26	    26	 100.0	     0	..........................	|                                                     	
==========	======	======	======	==========================	======================================================	==================
         2	    26	 100.0	    55	AAAGAAACCGGGTTTCTGTGGTGGAT	                                                      	       

# Left flank :   ATGTTCTAAATCTTGGGGTAGTGGTGACATCAATGTTGCGTCTTCTTAATCCCTCGGCAATGGATCTTTCTACTTGTTCATCTAGATGAAATCTAATCCTGACCAAGATTTTGCCCTCTTAATTTTTGCTGGACTTTAGAAGGAATTTTTGCTTGCAATTCTTGAACAAACGCTTGACTTTCTTGAATCTGCTGGCGAATTTCCTGAAAATGATCGTGGTAATAAGCCAATGCTGCATAGACATCGGCTAAAGTAATCGTCGGATACCGCGAGACAATTTCATCGGGAGACATTCCCATTTGTTCATGCCAAACGGCGATATCTTGAACTCGAATTCGATGTCCAGCAATCCGAGGTTTCCCTCCACAGATTCCCGGAGTAATTTCAATATGTTCCGCAATCACTGCTGTCATTTTATATAATCTCCTGAGTATGCTAACAATTATCTATATTTAGACTATCCTCAATTCTGCTAATTTTACCAGGCGATCGCACCCAAG
# Right flank :  T

# Questionable array : NO	 Score: 1.55
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.26, 7:-0.6, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAAGAAACCGGGTTTCTGTGGTGGAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.85%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-1.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_29088 flag=1 multi=11.7277 len=3520		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                               	Spacer_Sequence                                         	Insertion/Deletion
==========	======	======	======	==============================================	========================================================	==================
      1400	    46	 100.0	    56	..............................................	AGCTGATTCCCTAATTAAGTTAGAAATATGGGAAGAGGCGGTTCCCGCATACCTCA	
      1298	    46	  95.7	    56	....T...A.....................................	GGGTGATGCTTTGGTGAAGTTGGAAAGATGGGCGGAAGCGGTTGAAGCATATAATG	
      1196	    45	  69.6	    56	....A.A.A......GAAT-...TT..TC.....G..G........	AGGCGATGCTTTGATGAAGTTAGAAAGATGGGAAGAAGCAGTAATAGCTTATCAAC	C [1189]
      1094	    45	  73.9	    56	.G..G......C.T...AAT..C....-...A.......G...T..	AGCAGAAGCTTTGGTAAAGGTGGAAGATTGGGATGGTGCGATCGCGGCTTATCAAC	T [1062]
       992	    45	  76.1	     0	.G..G..C...C.T.....T....C..-......GT.......T..	|                                                       	A [971]
==========	======	======	======	==============================================	========================================================	==================
         5	    46	  83.1	    56	GCGCCATTGAGTTAAATCCCGATCATTCTTGGTCCCATAATAACTT	                                                        	       

# Left flank :   GAAACCCGATCCGGCAATATATAAGTTATTAGGTAATGCTTTGCAAGCAACCAGCAAACTTGAGGATGCTAAAAATTGCTATATCAAAGCTTTGGAATTAAACCCAAATTTTGCCGAAGGTTATGCTAATTTGGGAAGTCTTTATGCAGGTCAACAACAATGGCAATTAGCAATTAACGCTTACCAAAAAGCCATTGCTATTAAACCAGATTTTGCCGGCGCTTACCGCAATTTTGCTAAACTTTGGACGCAGGTAGGCAAGCCCCAAGAAGCGGCAGAATGTTGGTATAAAGCTTTTAATCTCGATCCTAGTAAAGCAACGGCTGAGGAATATTTAAGTTTAGCTAAAACCTTTTTAGAACAAGGAAAAACAGACGATGGGGTGGCTTGCTATCGCCGTACCTTAGAGTTAAATCCTACATCTTCTTGGGGTTATCACGAATTAGCAGAAATTCTGAAAAATCAGGAGAAATGGGAAGAAGCGATCCCGCTTTATCGCC
# Right flank :  AGGGGAAATTCTGGTCAAGTTAGAACAGTGGGAAGAGGCGATTCCCGTGTATCAACGGGCGTTAGAATTGAACCGGGATTTTGCTTGGGGTCATTATAACTTGGGACAGGCTTTGGACCATGAGGAAAGATGGGATGAGGCGATCGCGTCTTACCATCAGGCTTTGAAGATTCAGCCGGATCTGCCTTGGCTACCGCGCAATTTGGCGGATGCTTTACGTAATCGGGCTCAGGCTGATTTAGAGATGGCGAGTAATTTGTATCTGACTGCGATCGAGCATAACCCGGATGATGTGCAATTGTATCATAAGGCTTTGGAGATTCGGCCGAATGATGCTGATTTATATGTGGGGTTGGCTGATACTTTGGTGAGACATAATTGGCTGGATGGGGCGATCGTTTTTTATCAGATGGGGTTGCAGATTCGGCCGGATGATCGGGGGATTGCGGGGCGGTTGGAAGAGGTGTTAAAAAAAAAGGGGGTAAGTAAGGCTGTATTAG

# Questionable array : YES	 Score: 0.64
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.15, 5:0, 6:0.25, 7:-1.07, 8:0.8, 9:0.51,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCGCCATTGAGTTAAATCCCGATCATTCTTGGTCCCATAATAACTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:58.70%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.80,-3.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [25-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [48.3-58.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.05   Confidence: HIGH] 

# Array family : NA
//

>k141_303374 flag=1 multi=4.0000 len=2236		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence                                                                      	Insertion/Deletion
==========	======	======	======	================================	=====================================================================================	==================
       387	    32	 100.0	    40	................................	AAAGGACGGAAAAGACGGTAAGGACGGCAAAGACGGTGCC                                             	
       315	    32	 100.0	    22	................................	TGCCAAAGGTGAAAAAGGAGAT                                                               	Deletion [262]
       261	    32	  81.2	    85	........C.....A..T......A.A..T..	TAAAGACGGCCATGACGGTGACAAAGGTGAAAAGGGAGATAAAGGAGATAAAGGCGAAAAAGGAGATAGAGGCGACAAGGGAGAT	
       144	    32	  71.9	     0	.....A..T.....C....AG..AG..A.A..	|                                                                                    	
==========	======	======	======	================================	=====================================================================================	==================
         4	    32	  88.3	    49	AAAGGCGAGAAAGGTGACAGCGGTCATGACGG	                                                                                     	       

# Left flank :   GACATAACAGAAAAGGAACAATCATGAAAGTGTTGCTTGAGAAGGTTGTGCTTTTAGCGCTTTTGCTTGTCCCTTGCGCGGGAGCGGGACATGAAAATGGAGGCGTTCGTGGATCGGCTCTCCACACCTACGGTGGCATTGAGCTCCAAGATCGCAGCTTGGGCGAGGGCGGCTCGATTCATGGCTGTGTTGAAAACAACAAGGATATTCGCTTCGTCGAAAGTAAAGACTTGTGCAACTCCTACGAGCGCTATGTCCACTTGAATGCTCTCGATCTTACTGGAGAAGTCGGACAACCTGAGGCGAAGCGAGGCATTCGTGGAATCGTTCTAAGTTACGGTCGCGTTATCACTGATCGCGTACGAGTCCACTACGATCGCATTTTGACGGAGAGTCACAGCAGCGAAATCCGTGTCTGTATCAAAGACGACATGTACATTCGCTTGGTCGTGAGTTCAGACAAGTGCGAACATTCGGAAACCTACCTGAGCTGGAATGTT
# Right flank :  GCGACAAGGGAGATAAAGGCGAGGAAGGCGAAGACGGAAGGCATGGCAATGACGGAAGGCATGGCGAAGACGGAGAGGATGGAGAAGACGGAGAGGATGGCGAAGATGGAAAT

# Questionable array : YES	 Score: 0.47
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.41, 5:0, 6:0.7, 7:-2.04, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAAGGCGAGAAAGGTGACAGCGGTCATGACGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,11] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.20,-4.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [15-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [43.3-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.15   Confidence: HIGH] 

# Array family : NA
//

>k141_303374 flag=1 multi=4.0000 len=2236		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence                                                                      	Insertion/Deletion
==========	======	======	======	================================	=====================================================================================	==================
       387	    32	 100.0	    40	................................	AAAGGACGGAAAAGACGGTAAGGACGGCAAAGACGGTGCC                                             	
       315	    32	 100.0	    22	................................	TGCCAAAGGTGAAAAAGGAGAT                                                               	Deletion [262]
       261	    32	  81.2	    85	........C.....A..T......A.A..T..	TAAAGACGGCCATGACGGTGACAAAGGTGAAAAGGGAGATAAAGGAGATAAAGGCGAAAAAGGAGATAGAGGCGACAAGGGAGAT	
       144	    32	  71.9	     0	.....A..T.....C....AG..AG..A.A..	|                                                                                    	
==========	======	======	======	================================	=====================================================================================	==================
         4	    32	  88.3	    49	AAAGGCGAGAAAGGTGACAGCGGTCATGACGG	                                                                                     	       

# Left flank :   GACATAACAGAAAAGGAACAATCATGAAAGTGTTGCTTGAGAAGGTTGTGCTTTTAGCGCTTTTGCTTGTCCCTTGCGCGGGAGCGGGACATGAAAATGGAGGCGTTCGTGGATCGGCTCTCCACACCTACGGTGGCATTGAGCTCCAAGATCGCAGCTTGGGCGAGGGCGGCTCGATTCATGGCTGTGTTGAAAACAACAAGGATATTCGCTTCGTCGAAAGTAAAGACTTGTGCAACTCCTACGAGCGCTATGTCCACTTGAATGCTCTCGATCTTACTGGAGAAGTCGGACAACCTGAGGCGAAGCGAGGCATTCGTGGAATCGTTCTAAGTTACGGTCGCGTTATCACTGATCGCGTACGAGTCCACTACGATCGCATTTTGACGGAGAGTCACAGCAGCGAAATCCGTGTCTGTATCAAAGACGACATGTACATTCGCTTGGTCGTGAGTTCAGACAAGTGCGAACATTCGGAAACCTACCTGAGCTGGAATGTT
# Right flank :  GCGACAAGGGAGATAAAGGCGAGGAAGGCGAAGACGGAAGGCATGGCAATGACGGAAGGCATGGCGAAGACGGAGAGGATGGAGAAGACGGAGAGGATGGCGAAGATGGAAAT

# Questionable array : YES	 Score: 0.47
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.41, 5:0, 6:0.7, 7:-2.04, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAAGGCGAGAAAGGTGACAGCGGTCATGACGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,11] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.20,-4.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [15-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [43.3-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.15   Confidence: HIGH] 

# Array family : NA
//

>k141_304427 flag=1 multi=8.0000 len=1176		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                        	Spacer_Sequence               	Insertion/Deletion
==========	======	======	======	=======================================	==============================	==================
      1176	    38	  94.9	    30	-.....................................A	CTTAGTTGGTTGGTGTAAAGCGAGAGTCCG	
      1108	    39	 100.0	    30	.......................................	CTTAGTTGGTTGTTGTTAAGCCCAAGTTGT	
      1039	    39	  94.9	    28	.................A..A..................	CTTAGTTGGTTGGTGTCAAGCATGAGTT  	
       972	    39	  84.6	    28	........T........A.CA.....C......C.....	CTTAGTTGGTTGGTGTGAAGCACGAGTT  	T,A [968,970]
       903	    39	  76.9	    30	........T..T...........G.CC.....ACT...A	CTCAACTGGTTGCGGGAAAGCCCGAGCTTT	T,A [899,901]
       832	    39	  87.2	    30	........T.................C.....A.TG...	CTTAGTTGGTTGCCGTAAAGATAAAGTATT	
       763	    39	  87.2	    30	............................CG..A.TG...	GTGAGTTGGTTGTTATTAAGCGCGAGTCCG	
       694	    39	  87.2	     0	..............C..A........C....C...G...	|                             	
==========	======	======	======	=======================================	==============================	==================
         8	    39	  89.1	    30	GTCAAATTCGGCAAATTGCTGAAATTTGGAATCTCACCG	                              	       

# Left flank :   |
# Right flank :  CTCAGTTGGTTGTCGTAAAGCCCAAGTGTTGTCAAATTCGGTAAATTAAGATCAGGGATTTGCCCTGTCAAACCATTTCCATAGACCTTATCTGAGGTGTTACCATCATCCCCTAAATCTATCCCTGTAACGTGTCCCGAATCGCAAGTGATACCAAACCAACTACACGGAGTATTCGTTTGATTCCAGCCCGTATTATTTTTCCAATTCGGCCCGTCGGTGCTGTTATATAATTCCAAGAGTGATTGACACTCGCTCGCAGAAACTTCTGTCACTGCGTTACAATCGGTCGTAGCGTAGACAAACTCTGACCCCGTGGCTAAAGCTAGAAAAGCTATAGAACAACGAATGCGTTTAGAAACGCCCATTTTTAATCTCCTGTAGATGAATTGTTGATCAAAAAATAATCAGACTTGACAGGTTTTCAAAACTTGCCAAGTCTTTAACTGTATTGAGGACTGTCTTTCATTAAAAATTTTGGCGTTGGTCAGTTTATGAAA

# Questionable array : YES	 Score: 0.53
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.45, 5:-1.5, 6:0.25, 7:0.24, 8:0.8, 9:0.29,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTCAAATTCGGCAAATTGCTGAAATTTGGAATCTCACCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:58.97%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.40,-6.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [15-12] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [60.0-0.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.78   Confidence: MEDIUM] 

# Array family : NA
//

>k141_305572 flag=1 multi=5.0000 len=4858		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                	Spacer_Sequence                                 	Insertion/Deletion
==========	======	======	======	===============================================	================================================	==================
       606	    47	 100.0	    48	...............................................	CTCAACCGATATTTCTATAGAAACCCGGTTTCTGTCACCCCAAGAAGC	
       701	    47	  93.6	     0	........T..C..........................C........	|                                               	
==========	======	======	======	===============================================	================================================	==================
         2	    47	  96.8	    49	GCGATCGCCTTTTCCGAGAAACCGGGTTTCTATGATAAATTTGGTAT	                                                	       

# Left flank :   TATACGGAATGTTAGACGATCTCACCCCGGAACAGATGGCAATGTTTGATGCAGCGGTAGAAGGGAGATAGTAAAATTGGGTTATCTACTGGATACTAATATAATCACGGGATTTGTGAAAAAAAATCAAATACTAAGCAATAAATTACGGCAAGTAGAAATTGAGGGTGAAGATATTTTTATCAGTGCGATTAGCTACTATGAGGTAAAAAGAGGACTTTTATATGTGAATGCTACTCGACAGTTATCAAATTTTAATGCGTTTTGTAACCGAGTGAATCTACTATTCGTGGACGATCTAAAAATTGTCGAGACAGCCGCAACAATTCACGCAGATTTAAGAGGCAAGGGTACTACTCTGGAAGATGCCGATGTTTTAATTGCCGCAACAGCAATTTCCCATAATTTAATTTTGGTTTCTCACGATTCGGATATGCAAAGAGTTCCCGGTCTAAAGTTACAAGACTGGCTTTCAGCATAAGTCTCCCATCCACAAATAA
# Right flank :  TTCCCACCGATATTTCCGTAGAAACCCGGTTTCTGTTATCCCAATAAGCGCGATCGCTCTTATCATTCGTTACTACTGGTAGGGTGTGTTAGGCGGCGCGGATATTTTTGCTCACCACAAATAACCTTCAACTGTGCCGTTGTAACGCACCAGATGAAAGCTGATAAAAAAAAGACGATCGCCTTTCGGGCTTACTCAGGCGTGAGGGGCGATCGCTGGTTTCTTAAAATAAAATCTTATCTTACTCGACAAATAAGCCAAGAGTGAAACCACAGACACTTGTGCGACAAGAGTACATTTTAACTTGAATACTATAACCAGCGGCTAAGGGGGTAAATACCTGCTTAGGAAAATCATCAACCTCTGTTTTTGCTATAATTTCTTGATCATCGTAAAGTATAGCAAAATCGATATCCGAGCAGTCTGAATCACAAGCTGCCATGAAAGTGTACTCAACTCCAGGAGTTAAATCTAAATAGTATAATTCAGATTTTCCTGCG

# Questionable array : YES	 Score: 1.32
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.84, 5:0, 6:0.26, 7:0.02, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCGATCGCCTTTTCCGAGAAACCGGGTTTCTATGATAAATTTGGTAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.45%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-3.80,-4.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [56.7-51.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_310834 flag=1 multi=2.0000 len=1540		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                  	Spacer_Sequence                             	Insertion/Deletion
==========	======	======	======	=================================	============================================	==================
       921	    33	 100.0	    44	.................................	ATGAACTTCAAACCTCAAACCTTAAACCTCAAACCCGCAACCAC	
       844	    33	  93.9	     0	....C.......................G....	|                                           	
==========	======	======	======	=================================	============================================	==================
         2	    33	  97.0	    45	AAACTATAAACCCGCAACCACAAACCATAAACC	                                            	       

# Left flank :   CACAGGAATTTTTGGATCTCTCCCTTTCCATGGGCAATGATTAAATTACGCCGAGACCTGAATTCCGCTTTCCTGGGCTTAAACCCGGAGATAAATGGTGTCTATGCGCCCTGCGTTGGAAAGAAGCGTTTGAAGCAGAGGTAGCACCGCCGGTGTACCTTGCGGGTACGCATGTAAAGGCTTTAAATTATGTTACTTTAGCACAGTTGAAAACGCATGCTTTGAATGAGCAATAGGCAATAAGCAATAAGCAATAAGCAATTTGCAGTAGGCAATAAGCAATTTGAGGGTGACCTTAGTTTTTTGAGATGAATCAGGTTAATTGAGGAATCAGGTTAATTGGGTTAATTGAGGAATTGAGGAATCAGGTTAACTGAGTTAATTAAGTAATTAGGTTAATTGAGGAATAAGGATAATTGAGGAATTGAGTTAATTATGTTTTGGGTTGTTAAATTGAATAATTGATAGAGGAGCAACAGGATAATTTTATTCCATTAAAA
# Right flank :  CGCAACCACAAACCATGAACCTCAAACCTAAAACCTAAAACCTCAAATGCCTTACGGTTAACCCTTCGTTGATCAACTGTTTCAGGGAGTCAATCCCAAATTTAAGGTGGCGGTCAACAAATTTGCTGGTTACTTTTTTGTCGGACGCGGCAGTTTTTACGCCCTCCGGAACCATCGGCTGATCACTAACAAGCAGCAATGCACCGGTAGGAATTTTATTGAAAAAACCAACGGTAAATATGGTTGCCGTTTCCATATCAATGGCATAAGCCCTTATTCGGGTAAGGTATTCCTTAAATTTTTCATCGTGTTCCCAAACCCTGCGGTTAGTGGTGTACACAGTGCCGGTCCAATAATCACAACCCGAATCGCGGATGGTGGTACTGATGGCTTTTTGCAGTGCAAAGGCCGGTAATGCAGGCACTTCGGGAGGAAAGTAGTCATTGCTTGTTCCCTCGCCACGAATAGCCGCAATAGGAAGTAAAAGATCACCAAGTTTA

# Questionable array : YES	 Score: 1.43
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.85, 5:0, 6:0.31, 7:0.07, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAACTATAAACCCGCAACCACAAACCATAAACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:60.61%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [58.3-73.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.68   Confidence: HIGH] 

# Array family : NA
//

>k141_343569 flag=1 multi=4.0000 len=1449		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                                                  	Insertion/Deletion
==========	======	======	======	=====================================	=================================================================	==================
      1198	    37	 100.0	    65	.....................................	CTCTTTCCTTTCTGCTTCTTTCCGCCCTGTTTATTGCCACCGTCAATTGGCTTTCCATCTGGCTT	
      1300	    37	  97.3	     0	...A.................................	|                                                                	
==========	======	======	======	=====================================	=================================================================	==================
         2	    37	  98.7	    66	GTCGTTCCTGTCATTATTCTTTTGTTTCTTGTTCGAA	                                                                 	       

# Left flank :   CAGTCATTCCACAACACGATAAAGTCCACTCCACGTGCCTCGTCCTGTAAGTGTTTCACAACTGATCCTTTGCACGGAGTCTTTTTGATTGGGGCATCAAACAATTCGGCCGGATCTACTGAATCCCATGACTGATACTGCGAAGGGAAGTCAACGTTGAATACATGACCTGCGACTGATGTGACCTTATGAGTACATTTCGATGCGTTTGGTGCTTTGGGAAACGACGGGTCCGTAAACATGTGTACTGGGAGACCCTTTCCACCAGAGTCAAAGCTTCCTTTCGCCAAGCCTTTGGCGATTGCGAGTCCAATCGATGGTTTCTCAGCGATATGTAAAACCTTGATTCGTTCGCCTTTTCCGTAGTTAATGTCACTAGTCTGTTGAGGCACATTTGGGGGAATTGAAGGGGCTAATGGAGTCGCATCTACCGACTTGGATTTATTTGCAGGCTGTTTGATATCCTGCTTCGGCTTGTTGCTCAAAGTGGACTGCTTGTC
# Right flank :  TCCATTCCTTTCTGCTTCTTTCCACCTTGTTTGCCACCTTCGCTATGCTTTCCGTCTGGCTTGTTTGACTTCTCACCTCCCTTCTTCACAGACGTGTTTGCACCCGTCTTTCC

# Questionable array : YES	 Score: 0.44
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:1, 7:-1.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTCGTTCCTGTCATTATTCTTTTGTTTCTTGTTCGAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:64.86%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [55.0-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_346108 flag=1 multi=8.9183 len=1022		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                          	Spacer_Sequence                                                 	Insertion/Deletion
==========	======	======	======	=========================================	================================================================	==================
       292	    41	  92.7	    64	.............................G......C..A.	CCTTGCTTCTGTAGAGCCGAGCATGCTCGGCTGCTCCCCAACCCGCGCACCGTCGCCTCCTGGG	CCC [283]
       184	    41	  95.1	    25	..T.C....................................	CTGCGGTTTTCTCCTGATGCGTGGC                                       	GG [176]
       116	    41	 100.0	    25	.........................................	TCGCGGTTGTCTCCTGATGCGTGGC                                       	
        50	    41	  97.6	     0	............................A............	|                                                               	
==========	======	======	======	=========================================	================================================================	==================
         4	    41	  96.3	    38	GTGGGGCAGCCGAGCATGCTCGGCTCTACAACATCCTGCGG	                                                                	       

# Left flank :   TGGCCTTGCTGGCGGGTGGCGCCCGGCTGCTGCAACTGCGGGCCAAGCGCTTGTCCGGGCCGTCGCTGGCACGCGTGCTCGAGGCGATCCTGCCGGCTGCCCGTGCGTCGGGCGCGCAGGTCCTGGTCAATGCGGATTTCGCGCTGGCGGCACGCCATCCGGAAGTCGGCGTGCATCTCACCGCTGCGCAGTTGCGCGTGCTCGAGCGGCGTCCGCTCGAGCGACCGCGCTGGGTTGGCGCGTCCTGCCACGACGCCAGCGAGGTCGCGCGCGCGGTCGCACTCGACGTCGACTTCGTGGCCATCGCGCCGGTGTTGCCGACCATGACCCACCCTGGCGCGCCAAGCCTTGGCTGGGACGGCCTCGCCGCGCTGTGCGCCGATTGCCCGCTGCCCGCATTCGCCCTCGGCGGCATCGCGCCACGGGACCTTGAATCCGCCCGCGCCGCCGGCGCCTTCGGGGTCGCCGGCATTCGCGGATTCGCCTGACCGCGGCGTGGT
# Right flank :  GCCGCGGTCT

# Questionable array : YES	 Score: 1.05
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.81, 5:0, 6:0.25, 7:-1.10, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTGGGGCAGCCGAGCATGCTCGGCTCTACAACATCCTGCGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [7,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-11.50,-12.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [1-16] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [3.3-21.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_3376 flag=1 multi=13.0000 len=1884		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	===========================	=========================================	==================
         1	    27	 100.0	    41	...........................	TACGCAAGGGTTACAATTAGCAGGGTGCGTCAGACTGACGC	
        69	    27	 100.0	     0	...........................	|                                        	
==========	======	======	======	===========================	=========================================	==================
         2	    27	 100.0	    51	ACCCTACAAGTGGATTAAAGTGTGACT	                                         	       

# Left flank :   |
# Right flank :  TCGAAGTTTATACACACAAATAACAGTTAACAGTTAACAGTTAACAGTTAACAGTTAACAAATAACAAATAACCAATAACAAATAACCAATAACCAATAACAAATAACCAATAACAAATGACTATTATAAAATTTGGAATGATTGGCGGCGGCGTGATGGGAGAGGCGCTTTTATCCCGCTTAATTGCTGAAAAAATATATCAATCAAAAGAAGTTTTAGTTAGTGAACCCCAACCGGAACGTCGGGATTTTTTAATCGAGCGTTATGGTGTGGGTGTGACGGCTAATAATCGAGAAATTGCGGTGGCAAAAGAAGTAATATTTTTAGCTATTAAACCCCAGGTTTTTCCACAGGTGGCCGCAGAATTGGCAAAAAGCGATCTAACTGGCAATTCTGATTTAAAGCCGATGATTGTGTCAATTTTGGCTGGTGTACCTCTGAGTAAATTAGAAGCGGCTTTTCCTAGTTTTCCAGTGGTGCGGGTGATGCCAAATACGCC

# Questionable array : NO	 Score: 2.32
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.28, 7:0.15, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACCCTACAAGTGGATTAAAGTGTGACT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:59.26%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_355115 flag=1 multi=23.7874 len=1063		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                               	Spacer_Sequence                             	Insertion/Deletion
==========	======	======	======	==============================================	============================================	==================
       566	    46	 100.0	    44	..............................................	AGTTGATTCCAATGTTCTGGATTCAACCTCAAATGATGGAGACG	
       476	    46	  91.3	     0	................T........A.........G.....A....	|                                           	
==========	======	======	======	==============================================	============================================	==================
         2	    46	  95.7	    45	AGGGTGAAGGCGGGGAAACGGAACAGGCAAGTCACACTGAATTGGA	                                            	       

# Left flank :   ATTGTATTGTTCTGGATTATCCAGGGACCTTGTACATTTTCTTTCTGGATTGTGTAACTTTGCCTACGATCAGCAACTGAAATGCAATGAATCGGATACCGTTGATCCGTATCAAGTCCTGAACATGGTACGAAACGGCTCTGTGTTGCTTAATCCTGTGCGTAGGGAACTTCACACCATGTCAAAGTATTTGTTTGAAGATGTGCAAGGAGCCGACTTCATCAGCAATTCAATTGCTCATGAAGAAGCTGTCCTTCTTTCCAAAAAGACCGAAGCAGAAGAAACAACTAAAGCTTCGTATGTTAAGATGGATGTGTTTTGCAAATCGCGAATGAAGAACGTGGAAAAGAGTCAAGAAAATACATTTGCTACCAATGCTTTGGATTCAACCTCGAATGATGGAGGCAGGGGTGATGGAAGCAATACGGAACAAGAAAGTCTCACTGTTACGGAATTTGATTCGAATGTTCTGCATTCAACCTCTCATGATTTAGGC
# Right flank :  AGAGTAAGAAAAGTGATGTTGCAGACAATGATTTTGCTTTTGAAGAGACGAATTATAATACCTCCTCCGACATTAGTCAACACGAAGATGAAGGCGAGGGCGACAAAATGAATTTGGAACAAGCGAGTGAGTCAGATTCAAGCGATGAGGAAACATCACATTCAAAACAAGTGAGTCAGTCGGAATCAATTGACAAGGACACAGCACAGGAAAGTCAGCCTCGAAAGAGGAAAGCAGTTAGTGATCCTCTAGATACAAGTGATTCTGAAGAAAATGACAATGAAGAAAGCAAATCCAAACAGGATCCAAGTGATTCTAAAGCAAATGTCAATGAAGAAAGCAAATCCAAACAGAAAGTCGCCCAAGGAAAAAGACTTTCGCCCATGGAACGCGAGAGTATAGCTTCACAGAAACTGGTGGTGAAAAAAACT

# Questionable array : YES	 Score: 1.31
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.79, 5:0, 6:0.25, 7:0.07, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGGTGAAGGCGGGGAAACGGAACAGGCAAGTCACACTGAATTGGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [5,16] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.00,-2.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [4-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [68.3-60.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.15   Confidence: HIGH] 

# Array family : NA
//

>k141_359329 flag=0 multi=26.8202 len=2310		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                           	Insertion/Deletion
==========	======	======	======	=============================	==========================================================	==================
       176	    29	 100.0	    58	.............................	CGTAGGGTGGGCAGTGCCTCACAGAAAAGTCTGTTCATAACCAGTAGCAGTATTGGGC	
       263	    29	 100.0	     0	.............................	|                                                         	
==========	======	======	======	=============================	==========================================================	==================
         2	    29	 100.0	    59	ACTGCCCACCCTACAGAACTGAGCGAAGT	                                                          	       

# Left flank :   CCCAGACCTCAACAAAATTGAACGGTGCTGGTCCTGGCTAAAAGCCCGAATTCGTAAATGGATTAACCAATTTGATTCTTTGCATAACGCGATGGATCATGTTTTAGCTCTTGTGTCCTAATAACCTTGGCTACTGCTATAATTTCCTGAATGTCGAGTTGTAGGTTTCTGGTGAA
# Right flank :  TGCCCTCGGGAGAAGCCCGGATCCGAAATGTCCTAATCATTTAAACGCGCTATAGTAAAACAGTTACCTGTTTGACCCAACCCAATGCACCTTGATTTGGCTTGCGGTCTGTGCATTTCGTCTCCAATTTGGTTGCCAGCAATCGATCCGCTTTTGCTAGGGGAGGCTCAGTCCACACTGCTTTCTATCAGCGCTGTAGGTGATTTCCTGGTTGCAAAAAGTCTGCTCTCTCTACCTATGAATATTCAATTGGTTGATTCCCTCGTTCAAATTATCGAAGCTCTCACCCCAGAGGAAAACGCCCTGCTGCAAGAAAAACTACAGGTGCGAACTATTCAAGTAACCCCAGGGGTTTGTGGAGGAGCCGCCCGCATTCGCAATACCAGAATCCCGGTGTGGACATTGGTTTCTTTCCGCCAACAGGGAGCTGATGAAGCTGAAATTATTCGCAACTATCCCGCCCTCACTCCGTTGGATCTCAGCGCGGCGTGGATTTACTA

# Questionable array : NO	 Score: 1.89
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:-1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACTGCCCACCCTACAGAACTGAGCGAAGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [9,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-1.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [60.0-51.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_3458 flag=1 multi=6.0000 len=4725		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence       	Spacer_Sequence                                             	Insertion/Deletion
==========	======	======	======	======================	============================================================	==================
      1347	    22	 100.0	    60	......................	CCCAGGGACAAGAAACCGGGTTTCTCTGAGAAATGTCTGCCACCCCACCGGCCCCTACAT	
      1265	    22	 100.0	     0	......................	|                                                           	
==========	======	======	======	======================	============================================================	==================
         2	    22	 100.0	    61	AGAAACCCGGTTTCTGGGCCGC	                                                            	       

# Left flank :   TCCTTGATTTTCCATAAATCGGCGCAGAGAAAATACCATTGCTGAAGCATTAAATGGGGTGCGATCGTGAAAAATTATCCCTTGACGGAGGGGAATTGTATAACTTAATCCATCGGCGCTAATTTTCGGTAGTTCCGTGGCTAGTTGGGGGATAATTTCCCCTTGTTCGTTGTAGCGATAAAGGGGATCGCCTAAGTTATTCAGTAAATAACCGGCAGTTAAATCATAAGCATCAGCGGGGTCAAGGGTGCGTAATTTGGCAGTGGTGCCCAAAGTGATGCGATTTTCACTATTTACATTACCAGAATTACCGGAATTGTTGGGGGAATTTTGTAATCCTTGCCCACAACTAACCGTTAAAAATAAACAGACCGCAAATAAGCCGAAAAACCGTTGCCAATGGATCATTTAGATTACTCCTTTTTTGGGGTTAACTGACTTCGGCAAAAAAGAAACCGGGTTTCTATGATATTTGTTGGTTGGTGGCAAAGGTTGTCTGA
# Right flank :  AAGGAAGAAACCAGGTTTCTATGATAATTTATGTATCCTAACCGAGATTTTCGTCAATTACCCGGTTTCTCAATTCTTCAAGAGGCAATGATATGCAGTTGAGGTTTGAATGGCATGAAGAAAAAGCCGAAAATAACCTCAAAAAACACGGAATTAGTTTTGAGGAAGCAAAAACCGTTTTCAATGACCCATTATCGATAACGATCGCTGATTCCCAGCACTCGGATAATGAAGATCGCTATATTGATATTGGTCGGTCTAGTCAAGGTCAGTTACTTGTTGTAGTTTACACAGAACGTCAAGCAAACATTCGGATTATTAGTTGTCGTCCAGCCACTAAAACAGAGAGGAAAATCTATGAACAATCATAATTTTGACCAGCCAGATATGACTGATAATGAAATGCTTCCAGAATATGATTTTACCGAGGGTATGCGAGGCAAGCATTACCAAGCCTATCGGCAAGGACATACTGCGACCATTCATCAAACCGATGGCAA

# Questionable array : YES	 Score: 0.44
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-0.25, 7:-1.2, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGAAACCCGGTTTCTGGGCCGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [4,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-2.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [68.3-58.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_378430 flag=1 multi=6.0000 len=1240		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                  	Insertion/Deletion
==========	======	======	======	===========================	=================================	==================
       549	    27	 100.0	    33	...........................	CAAGCAGGATCCCCGGCAGCAGGGCAATCAGAG	
       609	    27	 100.0	     0	...........................	|                                	
==========	======	======	======	===========================	=================================	==================
         2	    27	 100.0	    43	CGCGCAGCAGGGCGCCCAGCAGCAGGG	                                 	       

# Left flank :   GAGGCGCAGCAGTCCATGCAGCAGCTCGCCGAGCAGCAGAAGCAGATGCGCCAGCAACTTGAGAAGAGCGCCGAGATGCTCAAGCGGGCCGCGCTCGAAGGGGCCATGCAGACGCTGCGCGACGACGCCAAGGACTTGGCAAGGGACCAGCAGCAGCTGGCGAACCGGCTGGATGGCCAGAAGCGCGACGGCAGTCAGGGCAAGCCGGGGGAAGGCAAGCCCGGCAACGGCGGGGAGCAGCGCCCGTCGCAGGACCCGAAGACGCTGGCCGACCGCTCACGCGATCTCGAGCGCGAGGTGCAGGCGCTGGCGAAGCGGCTCGAAGAGGCCGGCGCAAAACCGGGCGCATCCAAGACCCGCGAGGCGCAGCCGCTCGCCAAGCAGGCGGCGGACGCCATGTCGCAGGCAGCCAAGGACGCCGCCCGCGAGGCCCAGCAGCAGGCACAGCGCGAGGCGCAGGAGAACGCCAAGCAAGATCCCCAGCAGAACGCCCAGCGCGA
# Right flank :  GACAGCAGGGGGCCCAGCAGCAGGGACAGCGTGGCGCGCAGCAACAGGGGCAGCAAGGCCAGCAGGCAGGCGGGCAGCAAGGAGGCCAGCCGCAGGGGCAGCAGGGCACGCCGCAGCAGGGCGGGCAGGGAAGCCCATCGGAACAGGCCCGTCAGGCCGCGGAGGCGATGGACAAGGCCGCGCAGCAGTTGGCGCAGGCGCGCGACGCGCAGGTCGACGCCTGGAAGGGCGAGCTGTCCGACCAGCTCGACAAGTCCATCAACGAGACGATGCAGCTGGCGCGGCAGCAGGCCGACCTCGAAAAAAAGATGCGTCAGCAGGGGGCGCAGGGCCAACCGGGCATGCAAGGCGAGCAGAGCGCCCTGCAGCAGGGCGTGCAGCAGGCCGCCGAACGCCTCGAGAAGGCCGGACGCGAGTCGTCGCTGCTGTCGCAGCGCTCGCAGAAGGCCATGGGCGAGGCGCAGCGCCGGGTGTCGCAGGCCACGCAGGCCATGCAGCAG

# Questionable array : NO	 Score: 2.59
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.28, 7:0.42, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CGCGCAGCAGGGCGCCCAGCAGCAGGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [5,0] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-2.80,-2.90] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [31.7-28.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_388027 flag=1 multi=9.4104 len=2992		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                   	Spacer_Sequence                                                     	Insertion/Deletion
==========	======	======	======	==================================	====================================================================	==================
       136	    34	 100.0	    68	..................................	GGCAGAGATGGTCTAAAGAAACCCGGTTTCTCAACCGCGAACCCTACCAAGTCTATCGCACCCGAACC	
        34	    34	 100.0	     0	..................................	|                                                                   	
==========	======	======	======	==================================	====================================================================	==================
         2	    34	 100.0	    69	AGAAACCGGGTTTCTATGATGATTTTGGTGGGGT	                                                                    	       

# Left flank :   AAAAGATACTCAGTTAATTACTCTGAGTGCTTGTCAAACGGCGATGAATAGTTATCTGGAAGGGCAAGAAATTTCGGGAATGGCCTATATTTTGGAACGGGCAGGGGCAAGGGCAGTGATGGCGACTTTGTGGAATGCCGATGATGCTAGAACGGCGAATTTAATGGCTGATTTTTATCAGAATTTAGGGGCAGGTGATTCCCAGTCTGCGGCGTTACGGGAGGCAAAAGTTAAGGCGATAGGGCAAAATTTGCATCCGTTTTTCTGGTCGCCGTTTATTTTGATTGGTCGGGGTTCGTAGGGGGTAGGGTGTGTTAGGCGCTGCTTTTATGTTGGCTACCTAACCGATAATTTTTTTTCAGCGCCGTAACGCACCATTAAAATTAATGTGGTTAATGGTGCGTTACGGCTGACATTTAAAATAACTGTTTTCAGCAAAGGTTTTCTGTCAGCCTAACACACCCTACCACTACCGCTACCAAGTCTATCGCACCCGACCT
# Right flank :  T

# Questionable array : YES	 Score: 0.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.26, 7:-2, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGAAACCGGGTTTCTATGATGATTTTGGTGGGGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.88%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-2.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_388749 flag=1 multi=6.0000 len=2585		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                              	Spacer_Sequence                                            	Insertion/Deletion
==========	======	======	======	=============================================	===========================================================	==================
       175	    45	 100.0	    59	.............................................	TGACGACCTGCCTACGTCAGGCAAGCGCCAAGCAGAGAACTTGTCAACGCGAAGCGGAC	
        71	    45	  93.3	     0	...C.................................A..C....	|                                                          	
==========	======	======	======	=============================================	===========================================================	==================
         2	    45	  96.7	    60	AACTTGACAACCTGCCTGCGGTAGGCAGGCGCGAAGCGGATAACC	                                                           	       

# Left flank :   CAGTTTCTCCTTCTCAGGGATCAAGACAGCAGTGCTGTATTTCCTCCGAGATAACCTGAAACAAAACCCGGAATTCATAACCGAGAACCTAAACGATATCTGTGCGTCTGTTCAATATACACTGATCAAAATGCTTTCTCAAAAATTGGTGAAAGCGGCCAAAGAATACAGAATCAAAGAAATCGCCATTGCTGGAGGGGTCTCGGCCAACTCAGGACTCAGAGAAGAATTGAACAATCTTTCTATCAAATATGGGTGGAAAGTATATGTTCCAAAATTCGAATACTGTACCGACAATGCCGCAATGATCGCCATGGCTGCACATTTCAAATACCTCAAAGGAGAGTTTTGTGGCTTGGATGTTGTACCGGAAGCGAGAATGAAAATTTAGAATCTAGTATCAAGTATTTAGTATCAAGTATTTAGTATCAAGTACCAAGTACCAAGATACGAGAAGCGAGAGGCAAGAAACAAGAAGCGAGAATCAAGAAACAAGAAAA
# Right flank :  CACTACAACCACTACAACAATTACAAC

# Questionable array : YES	 Score: 0.19
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.84, 5:0, 6:0.25, 7:-1.1, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AACTTGACAACCTGCCTGCGGTAGGCAGGCGCGAAGCGGATAACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,12] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-13.90,-14.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [3-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [28.3-60.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.42   Confidence: HIGH] 

# Array family : NA
//

>k141_389652 flag=1 multi=11.8988 len=2423		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                                                                           	Insertion/Deletion
==========	======	======	======	==============================	==========================================================================================	==================
      1219	    30	 100.0	    90	..............................	GCTGGGGAGGGAAGGGCCAATGGGAATTTGGGCAATGCCTACCAATCCCTGGGAGACTACCCCCGTGCCATTAAATTTTACGAGAAACAC	
      1099	    30	  96.7	    90	...G..........................	GAGGGGAAGGGTGCGGCCAATGGGAATTTGGGCAATACCTACTATTCCCTAGGAGACTACCGCCAAGCCATTGACTATCACCAGAAACGT	
       979	    30	  96.7	     0	...........T..................	|                                                                                         	
==========	======	======	======	==============================	==========================================================================================	==================
         3	    30	  97.8	    90	TTGCAAATAGCACAAGAAATAGGCGATCGC	                                                                                          	       

# Left flank :   GCAATGGCTAAGTTAGCTTGAGCCACTTCTTGACCTAGTTTTTTTCCCCAAAATGCGTGCTTAATAATATAGCCATGCTGCAAGTAATTGCTGCGATCGCTATAGTAATGGCCAGCGCGGCCAATTAGTTCCCCTGAAGCTTTATCAATCACTGCCATTGGGCCGCAATTGTGGGTTTTCCAGTGATTAATTGCATTGCTGACATATTCCCAAATTTCGGGCGGGTTTAAAGCTCCTTTGCCGATGTAGCGCATCTCTAAGGGTTGGCCGTAGATGGTTAGTAAGGGGTGGTAATCTGTGCGCTTAAAGGGGCGAAGAATTAGGCATTTTGTTTCAAAGAAAGTAGGTTTCATTTGTGATACACTTGGGTTGGTTTTAATTATCAATGTCTGGCTTCAGTCGGACTGAAGCTAGATATCTTTTCTCTCTATGGGAATTTGGGCGTTGCCTACCAATCCCTAGAAGACTACCGCCAAGCGATTGACTATCACCAGAAATAT
# Right flank :  CGGGGTGAAGCGATTGCCTGGTTTAATTTAGGTTTTACTTTAGCCAGAGTTAATGAAAAATGGCAGGCTATTACTGCTTATAAAAATGCCCGCAAATTGTATCAAGCAATGGGACTCGATCAAGACGTTGAAAACTGCAATAATTCTATTCGAGATATCGGGATGAAAGTCGTAGTTGAGCCTCAAAGATACCTAGAACTTCCTAGCACACCTGCATGGATGAAAAGCCGTAAATTTAGTAGGAAATGGCAACAGCTAAGGGCCGACATTGGCAAGTCATTTTCTATTTTTTTCCGTTGGGTTTCGCGATGGGTGCGCTCCGTCTTCATCAGGTAGGGAAAAATCTGATTTGTAGAATCACAAATAATATTGTATCATGTTAATTGTTGTAGGGGCGGGTTCGCCTAGATATCCAATAATAATCGACAATATAGATAAACCCGCCCCGGCCAGATTCCGTTTTTTTATGCCTCTTTTGGTGACATCATAGATCGGGTCGG

# Questionable array : YES	 Score: 0.28
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:0.99, 7:-3, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTGCAAATAGCACAAGAAATAGGCGATCGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:56.67%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [1-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [61.7-51.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_398217 flag=1 multi=5.0000 len=1398		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                  	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	=================================	=============================================	==================
       509	    33	  90.9	    45	...C....A............C...........	AATGCTTCCTGTGGCACATAACCCAGTTGAGTCCTCTCCGTGTTC	
       587	    33	 100.0	    32	.................................	CACCAAAATTCTTGAACACAAATTACATGGTG             	
       652	    33	  87.9	     0	........A....................GCT.	|                                            	
==========	======	======	======	=================================	=============================================	==================
         3	    33	  92.9	    39	ATAGAATCGCCCAAAAATGGGTGATTCTATGAA	                                             	       

# Left flank :   GTTTTCTACTAGCCATGGGTGCCTGGTGGTCTTGTTGCTTCGCCAACTGCTCTATCCCTTCCATCATGTGCTGGTTAGTATCATCACAAGGTGTGCATATTTGGGAATATACAAAGGATTCATCATCAGGATTCATCACTCCACAGAGTGCACCACCGGTGAGTCCTTCATAATGCACTCCACACCAGCAGCACACCAATCCACATGGCCATGTACAGCACTTGTTGGCACTATGAAACAATCATGGGGAATAGAATCCATGGTTGTCTACTGCTCATTCTTGTGAGGCAGCAACAATAATTTGCTTGGCAGCACTTTTCACTCTGCTTTGAGCTTGTTCAATGGACCGCTGCACTTTTCACTCTGTGTTTTTCAATGGACCCATTTTTTGGAATTGTTCCACATGGTTTCTATGTAGCATAAATGAGGCCAAGTGCCCCTCAATCAGTAATTTTTGGCACCATAGTCACAGACATGCTGGCTCTGCCCGGCTCGGCTTC
# Right flank :  ATCACCTGGCACAGCCATAAGGGTTAATCAAAAGCAATAACTGAACAATGTAAGCAATTATTTGGCAGTGCTATCCCTTTTAAGCAATCATTATGTGAAAAGTAGATCTAGATCTTTTATTGCATGCATCTTTGGAGATTTTGAAAAGTTGAAATAAGTGATTGAAAATGGAGCCAACTTTGAGGCCAAAAGCAAGATGAAGTGTGTGGAGGATCAATGCCACTTCATTGGGCTAGCAAGAAAGGACACAAGGGTATTGTTTCATTATTGTTTGAAAAGAATGCCAACAGGATTGACACCACTTCATTGGGCTTGCAGCAATGGAGAAGTAGCTATCATTTAGTTATTGGAAAATGGTGCAGATCCAGCAACCATTACCAACAACAAAGGCAAGACTCCATTGCAAATTGCTCAAGAATGCAACAAACATAATTTTCCAGCTTCAAATGAGAAATCTCTTCAATGAAGTCAACAAGTAGTAGAAGACTAATTGCAACTTT

# Questionable array : NO	 Score: 1.92
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.65, 5:-1.5, 6:0.31, 7:1.06, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATAGAATCGCCCAAAAATGGGTGATTCTATGAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:63.64%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-11.00,-8.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [3-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [48.3-63.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.27   Confidence: MEDIUM] 

# Array family : NA
//

>k141_395943 flag=1 multi=4.0000 len=1287		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence      	Insertion/Deletion
==========	======	======	======	==============================	=====================	==================
       484	    30	  96.7	    21	.........................A....	CGGCAGCAACTCTACAGGCAG	
       535	    30	  93.3	    21	..........T........G..........	ACGCCTTAACTCTACAGGCAG	
       586	    30	 100.0	    21	..............................	TACCACCAACTCTACAGGCAG	
       637	    30	  73.3	     0	.AA.........A..T...TT.AA......	|                    	
==========	======	======	======	==============================	=====================	==================
         4	    30	  90.8	    21	TTCTAATTCAGCGTTCGGAAGTTTTTCACT	                     	       

# Left flank :   GATGATTTTCATAAGCTTAATCAGCTCGTTATGAAAATAACCTGCAGTTCAAAAACAAGAATTCAGTTACGACATTACAATAAAATTCAAATTACAGGAAAGTAGTTATTAAATGAAAAACATTATTTCAACAATCGCTATCAGCATAATGCTGGTAAACTTTGTATCCAACGCACAGGATATTACAAACACACTTGCGCCAAACGGCAGCTTTAAAATAAAAGATGGCTCAACAGATTTTTTTATACTAAATCAGTCCAATGGAACAATAACACTACCTTTACAGCTTGCTGGTAATCAGCGGGGTTCAATTTTCAAAGGTTCAGACCGGTTTCTTCACACATATTATGGGACAGGAACTGATGGCTTTAATCTTTTTCTTGGAATAAATGCAGGTAATTTCACAATGGGTGGTTCAGGAATAGAAGGCAGCTATAATACAGGTGTTGGTTATGGCACACTTTCTTCCAGCAGCACTAGCTTT
# Right flank :  TTTATTCAAATACAACAGGCAACTTTAATTCCGCTTTCGGGACACGTTCTCTCTATACCAACACAACAGGCTACTCCAATTCCGCTTTCGGGTATAGTTCGCTTGCCTTCAACACAACAGGCTACGAGAATTCTGCGGTTGGTATTCAATCACTTGAGTCAAATACTTCTGGTTATCAAAATTCTGCTTTTGGCTACCGGAGTCTTTTTAATAATAGAACGGGCCATTATAATTCCGCTTTCGGGTCACAATCGCTTTTCGACAATACAACAGGTGGTGCTAATTCCGCTTTTGGCTATCAATCGCTCTTCTTCAACTCAATCGGTAACAGAAATTCTGCTTTTGGATTTGAGGCACTATATACCAACACAGAAGGTGAAGAAAACTCTGCTTTCGGGTTCAATTCGCTTTACAAAAACACAATTGGTAACTATAATTCTGCTTTTGGACTTCTTTCACTATATAACAATACATCCGGCTCTGATAATTCCGCATTCGGG

# Questionable array : YES	 Score: 0.07
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.54, 5:-1.5, 6:0.99, 7:-1.05, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTCTAATTCAGCGTTCGGAAGTTTTTCACT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:63.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.10,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [3-8] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [55.0-61.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_403667 flag=1 multi=7.0000 len=1479		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                          	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	=========================================	=======================================	==================
       124	    41	 100.0	    39	.........................................	TATAGTAAAGATGTTACCTCCATGAATGCTGGGTCCTCT	
        44	    41	  97.6	     0	...C.....................................	|                                      	
==========	======	======	======	=========================================	=======================================	==================
         2	    41	  98.8	    40	CCCAGTCCCCGCTAGTCGGGGAAACCAGTTGAATGGAAACA	                                       	       

# Left flank :   TTATAGGGCCCCTCATTGGGTATCACCACCCCATTGTCTATGTCTACTTCCGCCCCTCCCTCCAGGGCATTGCAAAACATTACTAGGTCCAGGGGTTTCCCCTTTAACCCACTAGCGGAGTGAAGGACCACCGCCTCTTTGAAAAGGTCCTCCATTCCGGGGGTCTCCACCGCCAAGGAGAATGGAGAGACGGGTACAAGGGTGGACTTCACCCCGTATCCCCGAATCTTCTTCCACTCACTCTTGGTAAGCAGGCTCATGGAGAGCTTTAGGAAGAACCCCCCCACTTGGAATGGGGCGTCCTCAATGGAGACGCTATCCACTCGAAGGGGTTGGTTCATCTTTCGATGACGTAGTAGGGAGCCGAAGCCATCCCATATGGAATCCCCATATGAGAAGGTTTGGCCGATTAACCCTTTGGCCTGGGCCATAACCTCGGCCTTGAGGTTCTCCGTAGCCTTGAGGAAGGCCCCCTCATTACCCCGTCCCCCCTCGGGGAA
# Right flank :  ACGG

# Questionable array : NO	 Score: 1.69
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:0.25, 7:0.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCCAGTCCCCGCTAGTCGGGGAAACCAGTTGAATGGAAACA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,12] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-11.40,-12.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [0.0-36.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.42   Confidence: HIGH] 

# Array family : NA
//

>k141_408571 flag=1 multi=4.0000 len=2474		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                             	Insertion/Deletion
==========	======	======	======	=====================================	============================================	==================
       118	    37	 100.0	    44	.....................................	GGGAAATGTCAACTTCCACTCATGACGAAGTTATACCTTCTAGG	
        37	    37	  94.6	     0	..C..T...............................	|                                           	
==========	======	======	======	=====================================	============================================	==================
         2	    37	  97.3	    45	GATATGTCAACTTCTACTCATGATGAGCCTTATCACA	                                            	       

# Left flank :   TACCACCCACAATTTATCATGAAACCGATGTACCTGCATCAGTTGCTTCTACTATAGTACATGAAGATGATCTTCCATCCACAACTCATGAAACCGATGTACCTGCATCAGTTGCTTCTACTATAGTACACGAAGATGATCTTCCACCTACAACTTTAATCGAATCGCCTCCTCGACTAACTGAATTGGAACATCCAGACTCATCATCTAAAGAATTAGAAAATACGTCGGATGAAGATAGTAGAAGTAGATCTTCGTCTAATTTGTCAAATTCTACTATGTCTACAGATTTATCAATGCTTTCTACGACAGACCATGATGAAGATAGTAGAAGTAGATCTTCGTCTAATTTGTCAAATTCTACTATGTCTACAGATTTATCAATGCCTTCAATTCAATCATACAAAGAAGAACATTTGCCATCTTCACAAAAATCTGTAGCATCTACCATAGACAGTGTTGATTTAGAAACCATGGATTCACAACCTGATAAAATATAT
# Right flank :  A

# Questionable array : NO	 Score: 2.13
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:1, 7:0.07, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GATATGTCAACTTCTACTCATGATGAGCCTTATCACA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:62.16%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-3.30,-2.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_410798 flag=0 multi=8.0000 len=2244		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                 	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	================================	=====================================	==================
         1	    32	 100.0	    37	................................	TACGGCAGTGTCCTGTCTTGAATCAAACATCACAGAT	
        70	    32	  96.9	     0	.............................G..	|                                    	
==========	======	======	======	================================	=====================================	==================
         2	    32	  98.5	    38	ACGGCAGTGCCGTCTCCCTACGGACAACCAGA	                                     	       

# Left flank :   |
# Right flank :  AAGGCAAAACTCTACAGGATATGGATTTTGACACAGCGATGATGCGTCGCTGCATTGAATTGGCCTGCTGTGCTTTGGGCAAAACAGCGCCTAATCCCTTAGTTGGTTCAGTAATTACCAAAAATGGTGTAATTGTCGGTGAGGGTTTTCATCCCGGTGCAGGCCAGCCCCATGCGGAAATTTTTGCCCTCCGACAGGCCGGCGAACAAGCTCTGGGAGCCACTATTTATGTGAATTTAGAACCCTGCAATCACTATGGACGTACACCACCCTGTTCGGAGGCTTTAATTGCCGCTGGAGTGGCTAAAGTCGTGGTGGGAATGGTCGATCCAAATCCGCTGGTAGCTGGTGGCGGGATTGCTAAATTGCAAGCAGCGGGTATAGAAGTGGTGGTAGGCGTGGCAGAGGAAGCTTGTCGAAAATTAAATGAGGCTTTTATCCATCGGATTCTCTATCATCGACCTTTTGGTATTTTGAAATATGCGATGACTCTGGACGGT

# Questionable array : NO	 Score: 2.61
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.93, 5:0, 6:0.7, 7:0.78, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACGGCAGTGCCGTCTCCCTACGGACAACCAGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [8,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-4.80,-4.80] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0   Confidence: HIGH] 

# Array family : NA
//

>k141_413383 flag=0 multi=16.9073 len=1231		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                    	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	===================================	=====================================	==================
       127	    35	 100.0	    37	...................................	AAAGAACAAAGGCAGACCTGATGAACCAGGCTCAAAA	
        55	    35	 100.0	     0	...................................	|                                    	
==========	======	======	======	===================================	=====================================	==================
         2	    35	 100.0	    38	CTTTAGACTTCACCCGAAGTCGAATTAGTGGAAAC	                                     	       

# Left flank :   TATATTACGGAAAGTTCACTAATCTTTTCATTCAGCAATAGACTGAGTTGTTACTCATATAATCATTAATGATTCCAGCTAGCTGGTAAAAATACTCCTCATATTTTTGCTTCTGCTGCACGATTCGGTAGTGGGTGGTGTGATTTATCCTCTAAGGGATTGCTTGCGTCTAATCGCTGTTAATCCCTTAGTTTGTGATTTTTGCTAGATTACTGGCTGTAGGTTGTAGCTTTTTTGTACCTACTAGCAAATACCTGCCCCCCTCCTCCCCTGCTCCCCCGTATTTATTTTCTCTTTCCAGAGTTTTTTCGAGGGGGTGAGGCAGTGGCTGAAACCCTCATTCTGTCGTTGACCCCCTCGAATTACGCTCTGGGATAAGGTTTCAGGCGACGATTTGTCCCGATTTTCGCAAAAATTCTCTGTTTGTCATTACAAAAGCTTGACCCCCCCGAATTCACTGGGGTATAATTCAGTCATGGTAAGGCTCCCAGGAGCCAAGC
# Right flank :  CCGTCGGTGGCACACGTTGTG

# Questionable array : NO	 Score: 3.06
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.39, 7:0.78, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTTTAGACTTCACCCGAAGTCGAATTAGTGGAAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.14%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-6.40,-7.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [11.7-45.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.64   Confidence: HIGH] 

# Array family : NA
//

>k141_415307 flag=1 multi=24.5094 len=11955		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                       	Insertion/Deletion
==========	======	======	======	============================	======================================================	==================
         1	    28	 100.0	    54	............................	TTGATAATTGATAATTGATAATTGATAATTATTAATTATTAATTCAAATGACAA	
        83	    28	 100.0	     0	............................	|                                                     	
==========	======	======	======	============================	======================================================	==================
         2	    28	 100.0	    64	AGGACAAATGACCAAGGACAAAGGACAA	                                                      	       

# Left flank :   |
# Right flank :  AAAATAAAAATTATCTTAAATTATTTGGGCAACCAGGGCTATTAATAGGGTTAGCGTTATGCTAAGTATAAAAAACTAAGCCCACTGGCGTTAACTACGGAAATGCAACATCAAACTGCTACGGAGTCCATTGCTACGCTACAGTCCTACACCCTGCCACCCCTACAGGTTTCGCCGTCACGGGTGATGCGGGGAGTGGGGATATTGGGTGATGCTGGGGAGGCGAACCGACAGGTCAGCAAAGCGATCGCCCGTTTGGGGAGCCGTCCGGTTATTGTTGGCGGTAACAACTCCCTCGCTGTCACCCTCCCCCGACTGGAACCAATATTGCCACAACATCGCCTCATCGCGGTTAGCTCTTCATACCTCCCCGACTGCAGCGACAACAGTTTGGCGAAACTTACGGAAGCTGTCATCGCCCACCAAGGGGATATGATTATTGGGACTGGCGGGGGTAAAGCTCTGGATGCGGCTAAACTTTTGGCTTATCGCTGCGGTCT

# Questionable array : NO	 Score: 2.29
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:-0.6, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGACAAATGACCAAGGACAAAGGACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.14%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_414770 flag=1 multi=5.0000 len=1360		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	============================	=============================================	==================
      1003	    28	  96.4	    42	...................A........	TTCCGCGAACGTAGAGCGGCAATCCTGAACCGCGAATTTTTG   	
      1073	    28	 100.0	    45	............................	TTCCGCGAACGTAGAGTCAGAAATCCGGAACGCGAAATCTTTTGT	
      1146	    28	  96.4	     0	............A...............	|                                            	
==========	======	======	======	============================	=============================================	==================
         3	    28	  97.6	    44	AACGCGAATTTTGGCGAAGGACACGAAT	                                             	       

# Left flank :   CCGTCCAGTGTTTGGCGCTGGTGGACATCGCTTGTTTGGGTATATTTTGCAAAGGCTTGCTGAGCAGGGAGCCTATACCGAAAATGACGAGCAAGAGGCCGCCGATGATGCCCAGGGTGAGTACAAATGCAGGGCCAGACACAAAGTTTTTGACGTAAGACAGACCGTAATACACGGCTACGATGAACAGCACATCGCTGATCCAGATGCCGGCACCGACGGCCAAACCTGCGCGAAATCCTTTTTCGATGCCGGTTTGAAGCAATGTGAAGAGAATGGGGCCTGCCAGCAGACTGAGGAGGAGGCCGACTTTGATGCCTTCGATGAAATAAACTGTGTCTGTTAAATCCGGCATGGCAGATCGAGTGAAATCAAAAACACAACTTGGGTACAAATGTAACCATTCGCTACATATGTGCGTCTAATACCGTAACATTGTGTTGGGTTTGCCGTGGGCGGTTTGCAGTGGGCGGTACAATCCGGAACGCGAAATCTTTTGT
# Right flank :  CCGCCTCAGCCGGCGGACAGGTTTCCGCGAACGTAGAGCGCAGAAATCCGGAATTAAGAATTATGAATCAGCTTCCAATAAGATCATTCAAAAATCGAAATATGAAAACGCTTTTAAAATTTTTGCTCGTTGCTGTGGTATTGCACAGCGCTGTTTCCTGCGCCATCAATCCGGTGACAGGCAAAAA

# Questionable array : YES [Potential tandem repeat]	 Score: 0.04
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.88, 5:0, 6:1, 7:0.16, 8:-3, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AACGCGAATTTTGGCGAAGGACACGAAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [12,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.40,-1.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [1-8] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [45.0-51.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.15,0   Confidence: HIGH] 

# Array family : NA
//

>k141_413383 flag=0 multi=16.9073 len=1231		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                    	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	===================================	===================================	==================
       800	    35	 100.0	    35	...................................	GTCTTTAGGTGTGACAGAAGGCGGGTTAAAATAAA	
       870	    35	 100.0	     0	...................................	|                                  	
==========	======	======	======	===================================	===================================	==================
         2	    35	 100.0	    36	GTTGTGGGACAGCCACAAGCCACAGGCGGAAGAAG	                                   	       

# Left flank :   CCACTGCCTCACCCCCTCGAAAAAACTCTGGAAAGAGAAAATAAATACGGGGGAGCAGGGGAGGAGGGGGGCAGGTATTTGCTAGTAGGTACAAAAAAGCTACAACCTACAGCCAGTAATCTAGCAAAAATCACAAACTAAGGGATTAACAGCGATTAGACGCAAGCAATCCCTTAGAGGATAAATCACACCACCCACTACCGAATCGTGCAGCAGAAGCAAAAATATGAGGAGTATTTTTACCAGCTAGCTGGAATCATTAATGATTATATGAGTAACAACTCAGTCTATTGCTGAATGAAAAGATTAGTGAACTTTCCGTAATATACGGTTTAGGATAAAAAAATTCAGATATATAGTCATTTCAGATAAGAATGAGACAATCCTTGCAGTTGCCCTCTATCCCCCAATCCCTTTCTCCCAAAAGGGGAGAAAGGGGAGTAAGAAAAGGAGGGGGGGAAGAGGGGGAAGGGTGGGAAGATGGGGAAGATGACTCTGGG
# Right flank :  AGGGGGAGGGGTGGGAAGATGGGGAAGATGACTCCCCACACTACACCTCAAACCGCACACTCCCCCGTCTCCTTTTCCCCCCTCTCCCTACCTGGGAGAGGTTAGGGGGTGAGGGCAATATCGAGTGTCTCACTCTTATTTGAAATTACTATAGATTTGAATTAGCTCCCTATTCAACAACTCCCAACCATGAAAGCAATTGAAACCACAGCCACGATTAACGAAAGTGGAGAACTCACCCTCGATCGCACTCTAGATGTCACGAAGCCTCAGCGAGTCCGCGTCATCGTCTTAATGATTGAAGAAGGCGAAGAAGACCCGGATGAG

# Questionable array : NO	 Score: 2.98
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.39, 7:0.7, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTTGTGGGACAGCCACAAGCCACAGGCGGAAGAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-3.50,-3.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [41.7-40.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0   Confidence: HIGH] 

# Array family : NA
//

>k141_419797 flag=1 multi=3.0000 len=1131		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence          	Spacer_Sequence                                                   	Insertion/Deletion
==========	======	======	======	=========================	==================================================================	==================
       490	    25	 100.0	    66	.........................	CACGAGTTTTAAATTTTAAATTTTAAATGTTAAATGTTGAATGATGAATCATGCCTTCGGCTTGAC	
       581	    25	 100.0	     0	.........................	|                                                                 	
==========	======	======	======	=========================	==================================================================	==================
         2	    25	 100.0	    67	AAGTGTTGAATCATGCCTTCGGCTT	                                                                  	       

# Left flank :   CAGCTTGAAATGCTTGTTGCATGTTTAATATTCCATCGGGTGTTTCCCTGTTTATCCAAAAGCCAGCAGGGGTAAGGTGAATGGCTAAATACACAGCTAAGAAAGAATAAGCAACCAATGCCGAGGCGATATACGTAAAGAGTTTCACGCCATTGCGGCCAAACTATTCGTTAATGATATCTGTCATCACAAAAACTACCGGCCAAAGCAGTACACCTGCAGTAAGGTTGAGTGATAAACTGTTGCCAAATAACTCAATATCCATAGGAGTAAATCCCAATGACGATTCCATTGAAAAGATTTTTACGCCAATAAATTCGGCAATCAACGCATTGGCAATAAAAAATGAGCCAAGCACAAAAAGCAATACGTTTCGCTTTGATTCTTTTACCATTTATCCTTCAACATTTGCATGCAATATATGTTTTTTAGGTAATTGTATGTAGCCGTTTTCGAGGAGTTAGTTTGATGTTCGATGTTCGAAGTTCGA
# Right flank :  TGATAAGTTTTAAGTGTTTAGTTGTGCGGCTGTGCCCTTTAGGGTAAGGGCGTGGGAGGGAGTTAGTTCGAAGTTATTCGTTCGATGTTCGAAGTGTTGAATGTGTACGTCCCTGATATAGGTTGACCACAAAAGTCAATTTATATGAAAGGAAATTTAAAACAAATTCAAAAACACCGGCATTTCAGTTTAGATTTCAAAAAGGAAATTGTATCCTTGTTTGAAAGAGGTAAATTCAGTGTTAGTCAGCTATCTCGGCTTCACAGTATTTCGACTACAAGTATTTACAAATGGATTTATCGCTATTCAACAGTTAACGAAAAAGGTTTTCGAGTTGTAGAACATAAAAGCAGCGCAATGAATAAAGTTAAAGAGCTAGAAAAGCGAATAAAAGAGCTTGAACAAGCGGTTGGCCAGAAGCAAATAAAGATTGACTATCTGGAGAAAATGATTGATATCGCTAAAGATGAACTACAAATTGATATCAAAAAAAACTTCAA

# Questionable array : YES	 Score: 0.43
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.34, 7:-1.8, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAGTGTTGAATCATGCCTTCGGCTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:56.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [60.0-50.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.27,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_40471 flag=0 multi=6.0000 len=5125		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                	Spacer_Sequence                         	Insertion/Deletion
==========	======	======	======	===============================	========================================	==================
         1	    31	 100.0	    40	...............................	AGAAGCTTATAGGGTTTACTATCTTGCTTCCGATCGTGCT	
        72	    31	 100.0	     0	...............................	|                                       	
==========	======	======	======	===============================	========================================	==================
         2	    31	 100.0	    50	CAACGCCTTACGGCATCAGAGGTAAGAGCAA	                                        	       

# Left flank :   |
# Right flank :  AGGCATCAGCCAAAACCCTTAAACAAACTGCTTTGCTGTCCAATTTTACAAGCACCTGCCAAAAAACGCAATCTAATTCGCCAAATCATCACAAAACTCAACAATTCTCATCAATTAACCAACTCGAAATCCCCACAAATCATCGCTTTTCCACCCCTGACTCAAAATTGCAAGCATCAAAGGGCAGGAAAAAAATCCTCAAATCCCTGCCCCTTGAGCATTTGCGACTTTAAATCACCGGGTCATTTCTTCACAAGCACCAATGCTTGCATCATCATAAAATCCGAAAACATTCCGGTTCTTCTGGCCAAGCCCCCTGTCGATTATAGGCGGGAATCCCTTGCACGCATTGGTTTGAGAGCCGAACTAGCAGCAAATCATCCTCTGCGGTCAAAATCTTCTCCAACTCCCAGCGCAGCTTTTCCCGTCCTCGCTGGGATAACCAACAACGGAAAATAGAATATTGCATCCGTTCCCCATATCCCTCCATCAGCTTATAG

# Questionable array : NO	 Score: 2.45
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.34, 7:0.22, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CAACGCCTTACGGCATCAGAGGTAAGAGCAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-3.20,-2.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0   Confidence: HIGH] 

# Array family : NA
//

>k141_425783 flag=1 multi=3.0000 len=1015		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence                  	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	=================================	=====================================	==================
         1	    33	 100.0	    37	.................................	TCTACCGTTAGTAGTAACGACTTCAGTCGTTTCTTTC	
        71	    33	 100.0	     0	.................................	|                                    	
==========	======	======	======	=================================	=====================================	==================
         2	    33	 100.0	    47	TACCGTTCGTAGTAACGACTTCAGTCGTTTCTT	                                     	       

# Left flank :   |
# Right flank :  TATGTTCGTAATAGTGTGTAGGAATTGATGAAGGGTTTTGATTCATAATAATTGGGGAAATTATTTCTATTTTTTATAACAATGAATCAAGAAAATAGATGTTTAATTTCGCACATTTTCCTTAACCCTGTAAAAAACAAGCCTAAATCAAGGCTTCTTTCCCTCTCTCTATCCCAACAATTCATCCAACGCAATCTTAAATCCCTCCAGGACTACTTGCGTGCTGACAATATCTCCGACTTGAAAAGCGAGGGTCTGATGGTGAGTCAGTATAAAAATTCGCCGTCCTTCGGGAAAGACTAACCAAACTTCTTGACAAGTTGAGTCTAAATATTCTTGAGCTTTTGCTAATAATTCTTCCGCCACATCGGTGGGAGAAGCAATCTCAGCAACTAACGGCGGACTCTGGGGTAAAGTGGGTTCATTTCTATAGCGTTCTAGCAGTTCTGGGGTCAGGTAAGAAACATCGGGTCTGCGTCCCTGTTTTTGGGTACGGCAAG

# Questionable array : NO	 Score: 2.98
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.31, 7:0.78, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TACCGTTCGTAGTAACGACTTCAGTCGTTTCTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.58%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-4.90,-4.80] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_425527 flag=1 multi=5.6296 len=1399		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                       	Spacer_Sequence                             	Insertion/Deletion
==========	======	======	======	======================================	============================================	==================
      1246	    38	 100.0	    44	......................................	TGACTTGGGTATTATTGATGCCGCCACCCCATCGGCGATTAAGG	
      1328	    38	 100.0	     0	......................................	|                                           	
==========	======	======	======	======================================	============================================	==================
         2	    38	 100.0	    45	GTTTCCATTCAACTGATATCCCCGACTAGCGGGGACTT	                                            	       

# Left flank :   TTGTCGGTCGGCTTTTATCCGTAAAGGTCAATAAAAGCGACCTCGTGCCCACTTTCCCCCCGGTCCCGTCTTTAGGGGGTTCCCTCCTCCCCGTAGTCGCCACCGCCGCTGCTCTCAACCAAGGTATTGAAGACCTGGAAAATGCCATTCTCGCCGCTGTCAAATCTGGAGATTTACAAGCGGCTAATTTGGAATTAGCCATTAACCAGCGTCAAGCTGCCCAGTTAATTCGCGCCAAAACTTCTATCGAGTTGGTTTTGGGGACGATCGCCGATGCCAATCCCCTGGATTTCTGGACGATCGACCTGCGGGCCGCCATTAGCGCCCTCGGGGAAATCACAGGCGAAGAAGTCACCGAGTCCATCCTAGATAGGATTTTCAGCCGCTTTTGTATCGGGAAATAATTGATAATTGATAATTTAAATGAATTTTTGCCTAGAACCCCACTTGATTGAAATTAGTTACAAAAATCAAGTTTAGTACGCAAAAATCGCTTGCCA
# Right flank :  CAATGAAGTAGTAGGAGAGGCTTTTATAAGCCTG

# Questionable array : NO	 Score: 2.25
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.29, 7:0.07, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTTTCCATTCAACTGATATCCCCGACTAGCGGGGACTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [8,11] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-5.40,-4.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [65.0-33.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.64,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_426228 flag=1 multi=4.0000 len=1153		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	===============================	=======================================	==================
       280	    31	  96.8	    39	............T..................	TCCGAATAATATTTCCGAAGATGAAAAAATTTAAAAACC	
       210	    31	 100.0	     0	...............................	|                                      	
==========	======	======	======	===============================	=======================================	==================
         2	    31	  98.4	    40	CCGAAGATTTTTATGAAAAAGTTTTGAAAAC	                                       	       

# Left flank :   TTTCATCGTATTTTCGATATAGTTATTACATGTTTTTACTAGATTTATTTATAAAATTTATATTAGGATTTGGAGGTTTACAGCCTTAGCTGTAAACCTAGAATAGAAGCTTAGCGGAGCGTCAGACTATGGATGTCTATTTGCTCCACAAGCAATGAGAGGTCCCAAATTCTCATACAGATGTTATCTTTTCCCAACGATTTTAAGAAAGTGACGTTGAGAATTTGGGATTGCACTACCTCTTAAATTATTTGTAGTTTATTTTAAGAAATTAAAATAAACATTTAGAATTTAATTCGATTATTTGAAGAGTTTTTGTATTTTGAAATCGAAAATTGTGTAATAAATATATAAAAAATATTTCTGAAAGAGATACAATTAAAAAAGTCTAAAAAATATTATTGAAATAAATAATATTCACAGAATCAAATAAAAGTTACTAACTAAAATAGCGACATCTTGTGTTGTGTTAAAAAATTAAAAAATTATAAATAATATTT
# Right flank :  CAATAAATTAAACACCATAAGAGTACTATATTCAATGAATTCGATAAGTTTGTGTTAGGTGTTGGATAGAATTCGTATTTTAACATAGTTTTAAAAGGTTTTAAGAATTTTAAAAGAAATCTGTCAAAGAATAAAGAGGTTACTAAAGTAACCAAATAAAAAAGATAGTATTATCATTAT

# Questionable array : NO	 Score: 1.76
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.92, 5:0, 6:0.34, 7:0.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCGAAGATTTTTATGAAAAAGTTTTGAAAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:74.19%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-1.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [73.3-83.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_428155 flag=1 multi=5.0000 len=1355		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                                                                                                                 	Insertion/Deletion
==========	======	======	======	===========================	================================================================================================================================	==================
       465	    27	  77.8	    45	.T..AT.............TTG.....	CTAAACATCCAAACATTACATCTGATTTTGTCGAAGTCAATTTAG                                                                                   	
       537	    27	  74.1	    45	....AT.....A......CTTG.....	CTAAACACCCTAAAATTACTTGGAATGTAGTTAAAGCCCATCCAT                                                                                   	
       609	    27	  85.2	    45	....A............T.C.C.....	CTGGCTCTGTTAACATTACATTTGATATTGTTGAAGCTAGCTTAC                                                                                   	
       681	    27	  88.9	    45	....A...............GC.....	CTAAAAATCCAAACATCACTTGGGATGTGGTAAAAGCCAACATAC                                                                                   	
       753	    27	  85.2	    45	....A......A........GC.....	CTAAAAATCCAAACATCACTTGGGATGTGGTAAAAGCCAACCCAG                                                                                   	
       825	    27	 100.0	    45	...........................	CTTTAAACCTTAACATCACATTTGATATCTTCAAAGCCAATCTAG                                                                                   	
       897	    27	 100.0	   128	...........................	GTAAAAACCACAACATTCGCCGAGATATCATTAAATATGTTTTTTCCAATCTAGATATTTTTTCCCCATATTTTAAAAATTTGTCTATGCATCCTAAAATTACGTTGGATGTGGTAAAAGCCAACCCA	
      1052	    27	  77.8	   117	.A.........A.A...G..G....A.	CTTCTTGCCCCAACATTACTTGGAGTGACATCGAAGCCAATCCAGACAAACCTTGGGACTACAACTATATGTCTAGAAATCCTAACATTACTTGGAACATTGTTGAAGCTAACTTGG           	T [1053]
      1197	    27	  74.1	    45	....AGA....A.......TC....A.	CTAAACATCCTAACATTACATCTGATATCGTTGAAGCCAACCCAG                                                                                   	
      1269	    27	  88.9	     0	..T........A.......T.......	|                                                                                                                               	
==========	======	======	======	===========================	================================================================================================================================	==================
        10	    27	  85.2	    62	ACAACCCTTGGGATTGGAAAATGTTGT	                                                                                                                                	       

# Left flank :   GATTAAGAAATGGATCAAAAATTGTAGGTACAAGATACCAATGTTTAAAAAAAGGTATTAATATAGGTTTAAATGAACCTTTAATTAATTACAATGAAGAATATGAACCTATTGAAGATTTAAAAACATTTTGTGGAAATGGTACTACTTTACCACGAAATAAACTTAGATTTGGAACACGAGACGAATGCTTAAGAAAAGGCTTTGCTGTTGGACAAAATTTACAATATAATAGAAATTAAATTTAATGGTGTTTTATACCTTAGCGCAGAAAATTGAACTTTAAAGTTAAAAATAACCATAAAAATATACTTTGTGAAATTGTAAGCAAAACTACTATTTTGGCATGTCTTATGCAAACTACTATTTCGACTTCATCAAAAACCGTCCAAATCTAAATTGGAATTGGCATAAATTGACTAGAAACCCAAACATCACTTGGAATGTTATTAAAGCCAACCCGAA
# Right flank :  CTAGCAATCCTAACATTACATTTCTAGACATGGTTATTCAAGTCGACTTGGACGGGTGGA

# Questionable array : YES	 Score: 0.08
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.26, 5:0, 6:0.28, 7:-1.82, 8:1, 9:0.36,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACAACCCTTGGGATTGGAAAATGTTGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:59.26%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.90,-0.30] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [17-17] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [63.3-58.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_428155 flag=1 multi=5.0000 len=1355		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                                                                                              	Insertion/Deletion
==========	======	======	======	=============================	=============================================================================================================================	==================
       465	    29	  82.8	    43	.T...T.............TTG.......	AAACATCCAAACATTACATCTGATTTTGTCGAAGTCAATTTAG                                                                                  	
       537	    29	  79.3	    43	.....T.....A......CTTG.......	AAACACCCTAAAATTACTTGGAATGTAGTTAAAGCCCATCCAT                                                                                  	
       609	    29	  89.7	    43	.................T.CA........	GGCTCTGTTAACATTACATTTGATATTGTTGAAGCTAGCTTAC                                                                                  	
       681	    29	 100.0	    43	.............................	AAAAATCCAAACATCACTTGGGATGTGGTAAAAGCCAACATAC                                                                                  	
       753	    29	  96.6	    43	...........A.................	AAAAATCCAAACATCACTTGGGATGTGGTAAAAGCCAACCCAG                                                                                  	
       825	    29	  89.7	    43	....C...............AT.......	TTAAACCTTAACATCACATTTGATATCTTCAAAGCCAATCTAG                                                                                  	
       897	    29	  86.2	   125	....C...............AT.....G.	AAAAACCACAACATTCGCCGAGATATCATTAAATATGTTTTTTCCAATCTAGATATTTTTTCCCCATATTTTAAAAATTTGTCTATGCATCCTAAAATTACGTTGGATGTGGTAAAAGCCAACCC	
      1051	    29	  79.3	    43	.A.........A.A...G...T...A...	TCTTGCCCCAACATTACTTGGAGTGACATCGAAGCCAATCCAG                                                                                  	T,C [1055,1057]
      1125	    29	  72.4	    43	.............C.AC..CTATA.....	AGAAATCCTAACATTACTTGGAACATTGTTGAAGCTAACTTGG                                                                                  	
      1197	    29	  75.9	    43	.....GA....A.......TCT...A...	AAACATCCTAACATTACATCTGATATCGTTGAAGCCAACCCAG                                                                                  	
      1269	    29	  79.3	     0	..T.C......A.......TAT.......	|                                                                                                                            	
==========	======	======	======	=============================	=============================================================================================================================	==================
        11	    29	  84.7	    51	ACAAACCTTGGGATTGGAAAGCGTTGTCT	                                                                                                                             	       

# Left flank :   GATTAAGAAATGGATCAAAAATTGTAGGTACAAGATACCAATGTTTAAAAAAAGGTATTAATATAGGTTTAAATGAACCTTTAATTAATTACAATGAAGAATATGAACCTATTGAAGATTTAAAAACATTTTGTGGAAATGGTACTACTTTACCACGAAATAAACTTAGATTTGGAACACGAGACGAATGCTTAAGAAAAGGCTTTGCTGTTGGACAAAATTTACAATATAATAGAAATTAAATTTAATGGTGTTTTATACCTTAGCGCAGAAAATTGAACTTTAAAGTTAAAAATAACCATAAAAATATACTTTGTGAAATTGTAAGCAAAACTACTATTTTGGCATGTCTTATGCAAACTACTATTTCGACTTCATCAAAAACCGTCCAAATCTAAATTGGAATTGGCATAAATTGACTAGAAACCCAAACATCACTTGGAATGTTATTAAAGCCAACCCGAA
# Right flank :  AGCAATCCTAACATTACATTTCTAGACATGGTTATTCAAGTCGACTTGGACGGGTGGA

# Questionable array : YES	 Score: 0.17
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.24, 5:-1.5, 6:1, 7:-0.77, 8:1, 9:0.20,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACAAACCTTGGGATTGGAAAGCGTTGTCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.17%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.90,-0.30] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [14-33] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [63.3-56.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_445636 flag=1 multi=5.0000 len=2058		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                    	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	===================================================	=====================================	==================
         1	    51	 100.0	    37	...................................................	ATGAGGCCGCCAATCGATTTATCCCCCGGCGGTGTTG	
        89	    51	 100.0	     0	...................................................	|                                    	
==========	======	======	======	===================================================	=====================================	==================
         2	    51	 100.0	    47	CTGAAAGCCTTATTGGGTGGTATTTGTAGTCGGTTTTAACCGACTTTAGCT	                                     	       

# Left flank :   |
# Right flank :  TGTTAGGCGGGGGATTAATCCCCCGCCGGTTGTTGCTATGGGATAAATCCCCCGCCGGTGTTGCGAGGGGTGCTTCAAATGTCAGGGTTTTGTGACAGGTGGATACTAAATTTGCAACTTTTGGTCCTCCAGAGATTGAAGACAAAAATCGACGTGAAATTTAACATTTCAGACCCACAAAATTTGACAAAATAACAGTGCCACCAATCTAGCCTCAAGACCTTGTTGTCTAGGCAAGAATCATTATGTAAATTGGAGATGAGACCGTGGCTATTGCCAGTATCAACCCGGTGACGGGAGAAACGATACAAACTTTTACGCCCCTAACCGATGGCGAAATCCAGGCTAAACTGGAAAAAGCGCAACAGGCTTTTGAAAAATATCGGCGGATTCCCCTGGACCAAAGGGCAGTCTGGATGAATGCCGCAGCGGATATTTTGGAAAAAAATAAAGCGCACTATGGGAAAATCATGACCCTGGAAATGGGCAAAACCCTCGCT

# Questionable array : NO	 Score: 1.67
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:0.78, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTGAAAGCCTTATTGGGTGGTATTTGTAGTCGGTTTTAACCGACTTTAGCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:58.82%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-13.00,-9.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_428155 flag=1 multi=5.0000 len=1355		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                                                                                              	Insertion/Deletion
==========	======	======	======	=============================	=============================================================================================================================	==================
       465	    29	  82.8	    43	.T...T.............TTG.......	AAACATCCAAACATTACATCTGATTTTGTCGAAGTCAATTTAG                                                                                  	
       537	    29	  79.3	    43	.....T.....A......CTTG.......	AAACACCCTAAAATTACTTGGAATGTAGTTAAAGCCCATCCAT                                                                                  	
       609	    29	  89.7	    43	.................T.CA........	GGCTCTGTTAACATTACATTTGATATTGTTGAAGCTAGCTTAC                                                                                  	
       681	    29	 100.0	    43	.............................	AAAAATCCAAACATCACTTGGGATGTGGTAAAAGCCAACATAC                                                                                  	
       753	    29	  96.6	    43	...........A.................	AAAAATCCAAACATCACTTGGGATGTGGTAAAAGCCAACCCAG                                                                                  	
       825	    29	  89.7	    43	....C...............AT.......	TTAAACCTTAACATCACATTTGATATCTTCAAAGCCAATCTAG                                                                                  	
       897	    29	  86.2	   125	....C...............AT.....G.	AAAAACCACAACATTCGCCGAGATATCATTAAATATGTTTTTTCCAATCTAGATATTTTTTCCCCATATTTTAAAAATTTGTCTATGCATCCTAAAATTACGTTGGATGTGGTAAAAGCCAACCC	
      1051	    29	  79.3	    43	.A.........A.A...G...T...A...	TCTTGCCCCAACATTACTTGGAGTGACATCGAAGCCAATCCAG                                                                                  	T,C [1055,1057]
      1125	    29	  72.4	    43	.............C.AC..CTATA.....	AGAAATCCTAACATTACTTGGAACATTGTTGAAGCTAACTTGG                                                                                  	
      1197	    29	  75.9	    43	.....GA....A.......TCT...A...	AAACATCCTAACATTACATCTGATATCGTTGAAGCCAACCCAG                                                                                  	
      1269	    29	  79.3	     0	..T.C......A.......TAT.......	|                                                                                                                            	
==========	======	======	======	=============================	=============================================================================================================================	==================
        11	    29	  84.7	    51	ACAAACCTTGGGATTGGAAAGCGTTGTCT	                                                                                                                             	       

# Left flank :   GATTAAGAAATGGATCAAAAATTGTAGGTACAAGATACCAATGTTTAAAAAAAGGTATTAATATAGGTTTAAATGAACCTTTAATTAATTACAATGAAGAATATGAACCTATTGAAGATTTAAAAACATTTTGTGGAAATGGTACTACTTTACCACGAAATAAACTTAGATTTGGAACACGAGACGAATGCTTAAGAAAAGGCTTTGCTGTTGGACAAAATTTACAATATAATAGAAATTAAATTTAATGGTGTTTTATACCTTAGCGCAGAAAATTGAACTTTAAAGTTAAAAATAACCATAAAAATATACTTTGTGAAATTGTAAGCAAAACTACTATTTTGGCATGTCTTATGCAAACTACTATTTCGACTTCATCAAAAACCGTCCAAATCTAAATTGGAATTGGCATAAATTGACTAGAAACCCAAACATCACTTGGAATGTTATTAAAGCCAACCCGAA
# Right flank :  AGCAATCCTAACATTACATTTCTAGACATGGTTATTCAAGTCGACTTGGACGGGTGGA

# Questionable array : YES	 Score: 0.17
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.24, 5:-1.5, 6:1, 7:-0.77, 8:1, 9:0.20,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACAAACCTTGGGATTGGAAAGCGTTGTCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.17%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.90,-0.30] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [14-33] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [63.3-56.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_447551 flag=1 multi=3.0000 len=1394		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence   	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	==================	=======================================	==================
       608	    18	 100.0	    39	..................	CTGAGTAAACCCGAAGAAGTGGAGGTGCAACCCCTGGAG	
       665	    18	  94.4	     0	...............G..	|                                      	
==========	======	======	======	==================	=======================================	==================
         2	    18	  97.2	    40	TTGCCGGAGTTGCAGAAA	                                       	       

# Left flank :   TCCGCTTGGCTGTCGAGGCGGGTGCTGAAACTGCCGAAAAGTTTGGTCAAGTGGTTTCTAAGCTCGTTATCCCCCGTCCCTTGGCAAATTTGGAAGCCATCCTCCCCATCGGTGCGCGGTTGATGGATTTGGCCCAAGGCAATCATGGACAACCCAGCCATCAAGCGATCGGACTCTTGGAAACCCGAGGATTTCCGGCGTTGGTGGGTGCCGCAGATGCCATGCTCAAGTCTGCTGATGTGGATTTAACCGGCTATGAAAAGATTGGCGACGGGTTATGCACGGTGATTATTCGAGGGAGCGTGGCGAATGTGGTAGTCGCGGTGGAAGCCGGGATGTTTGAAGCCGAACGGATTGGAGAACTGACCTCGGTGATGGTGATTCCCAGGCCCCTGGATGACTTGGAGCGGGCTCTACCCCTGGCGAGTTGTTTTGTTGAGACGGTCCAACCCCTGCAAATGCCCGTTGTCCTTGAGGAGAAACAGCCTGAACGGTTAGCT
# Right flank :  ATTAGAGGAATTACAGGAATTACCGAAACTGGAACTGGAACTGGAAAACCCGGATCAAGAATAAATTGAGGTCGGGTTCCAGGGGATTCGGGTGGCGAGGCGATCGCCGATGGGTTCCGTTCATCAGGAGTCCCGGTGATTTTTGTAGGCTAAGATTCAGGAATTTCCGAGGGAGACTGGCGATCGAGAATCCCTTCAGCAATCCAGCGCGCAAACGCTTCATCCCCCATCCGGGATAATGCCTCTAGGAGGATCTCTAGCGATTCTTCCGGCGCGTCCCGAGTTAATTCTACAGGTAGAGCCAACAAGGGCATATTCTGGATCAGGTCCAAGAGGTCCTCGCGGATGCGTTTGCGCTTGGTTTCCAGGACCATCACGGTTCGACGAATCGATCGCTCCCGGGTGACTTCGAGTAATAAAACATCCTGTTCATAAAAGTTATTCATTAAACCAACTCCTTTGTGGATACCCCTGACTCGAATGGGGTTGCATTTTCATAC

# Questionable array : YES	 Score: 0.36
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:-1, 7:0.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTGCCGGAGTTGCAGAAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [5,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [45.0-58.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.27   Confidence: MEDIUM] 

# Array family : NA
//

>k141_449902 flag=1 multi=4.0000 len=1649		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	===========================	==========================================	==================
      1336	    27	 100.0	    42	...........................	GAATTCGCATTCTTCGCTAAAATTCGCGTTTCGGGTTTCGCG	
      1405	    27	  96.3	     0	.......A...................	|                                         	
==========	======	======	======	===========================	==========================================	==================
         2	    27	  98.2	    43	TTCCGGGTAGCAGATATACGTTCGCGA	                                          	       

# Left flank :   AACTCAGGAAGTATCCAAAAAGCTACAGGCAAGGCGGCCACAATGCCCAACAACAAACTCTTCATTCGGGAAAACGATCGCAAGCCGCTGCTTTCTACCTCCATCAGCAAACGATGCACCACTACAAACAGGACCTGAAGCCATACAGCGATTTTGAGCATTATCAACAGCCCTGTTTTACCCGCACTGAAGATGGGGTGAAATCCCAGGGTTGTATCCAACTGGTAGGTGATGGTTTTCAGGATGAACAGCAAGCCCAGCACCGCAAGTGATGCCACAAAATAGGTCAGCGTGGATCGTAAAACGGGGTTTTTATTCGGCAGTGAACGTGGGGGAAACTTGCGGTACCACAACAGAACCACAACCAACGCCGACAGCGCAGCAGCTAAGCCTGATACCCCATTCCGCAGCCACCATTCGGGCAGGTCTCCCATGTTCGGATATTTGAGGCGAAGGTAAGGATTGTCAAGGATTTGGGATTCCTGATTTCGGATTTCTGA
# Right flank :  AGACTGGTCCGCCGGCTGAGGCGGATTCGCATTCTTCGCTAAAATTCGCGGTTAATAATTCGCTGTTTCGGATTCCGCTCAGACGATGAACTGCTAAGCAGATTACAGATTCCGGATTCCGCCCAGACGATAAACTGCGAAGCAGATTACAGATTCCGGATTCCGCCCAGACGATAAACTGCGCAGCAGATTACAGATTCCTGCTTCCGCCCAGACGA

# Questionable array : NO	 Score: 1.50
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:0.28, 7:0.11, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTCCGGGTAGCAGATATACGTTCGCGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.10,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [60.0-50.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_451487 flag=1 multi=2.0000 len=1473		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                   	Spacer_Sequence              	Insertion/Deletion
==========	======	======	======	==================================	=============================	==================
       709	    34	 100.0	    29	..................................	AGATGGTTTGACTGCACTAGTGTTGTCCA	
       772	    34	 100.0	     0	..................................	|                            	
==========	======	======	======	==================================	=============================	==================
         2	    34	 100.0	    39	TACGAGCACTTGGTCTGGGAGGAGAAGTTGCAGC	                             	       

# Left flank :   TGCTCCTCGGGGCCGTGCTACTTCTTTGCTGCCTACACCAGTACCACTAGAACTTTGCGTCGACTGAGAGGATGATCGATTGGAGGATCGTCCCATCATGACACTTCCCTTCACATACGAAGTGTTTCCGAGATCGGGTCGTTTGGAAGGAGTCGATCGTTTGACGCCGACACTGTCGGATGTTGGTTTTGTAAATGTCGTTGTGGGCGGCACAGTCGAAGGTCCATTGCTACCACTAGATGGTGGTGTTGTATTTAAATTGCAATGGGAAGACGGTGCAGGAGCAGTACCGCTGGGCGTAGTGGATCCTGTACTGCTGCTACTTTGTTGACGGCCAACGGTTGATCCAGTAGGTGAAGACTTGTTCGACGTATTATACATCGACGAAAATGATCCAGAGGTACTTTTTGGCATTGAAGCATGTGTTTTGGGAGAAGTGGCAGCAGGAGGTTTGACTGTACTTGCTGGTTTCGAAGTAGCGAAATTTGTGCTGTTGACTG
# Right flank :  CTGGCGACTTGACTGCGCTGGCTGGTCTCGAAGGAGCAGGTTTCGGTACGTCCACTTTTTTGTTGAACGACGATCCAGTAGTAAAATCAGGCACCTTTGGCTTGCCTGAGGTAGAATACGCATATCCATTAGAAGACTTGGTACTAGCAGTGTTGCTGACACTAGTCTTTTTGGCACTCTCATACGACGACGGTGGCGGCGACGATGCAGTATTGGAACGATTGGGTGGCGTCGCAGTTTGAGTCGCTTTGGGAACTTGTCGTTTTGGACCATTGGCATGGCTTGTATTGCTCGTGGGAGGTTTGACACTATTGTTTTTCTTGGGTGGTGTAGAAAAGGGAGACTTGATGGTCGTATCACCTGAATACGTGGTAGAATGATCATAGGATGCATCACTGACTTTGCCAGTGTAAAACGACTCTGCCGTCTTGCCACCATAAAATGACTTCGTTTCTTCTTTTGGAGATTGCACACTTGCTCTTTTACTACCAACAGTTGAA

# Questionable array : NO	 Score: 2.19
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.26, 7:0.04, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TACGAGCACTTGGTCTGGGAGGAGAAGTTGCAGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [8,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.60,0.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [53.3-45.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0   Confidence: HIGH] 

# Array family : NA
//

>k141_454951 flag=1 multi=10.4016 len=1767		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                             	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	============================================================	=========================================	==================
       661	    60	 100.0	    41	............................................................	CACTTCGGCTTCGCTCAGTGTAAAATCGTAAATCGTAAACA	
       560	    60	  96.7	     0	...........................A.............A..................	|                                        	
==========	======	======	======	============================================================	=========================================	==================
         2	    60	  98.3	    42	AAAAAAGGTACGATTGACGAGGTACGAGGTACGATTGTGGGGCAAATTCGTAAATCGTAA	                                         	       

# Left flank :   CGCGGGTATAATAAGTTCCTTGAAATATCGGACAATGTCAAAGTTGCCATTAGTAAAGACAAGATCAAACAAGATGAACAATGGGATGGATTGAAAGGATACCTGACCAACACCAGCCTGGATGCCAAAGAAGTATATGAACAATACAATGGCTTGTGGGCAATCGAACGGGCATTTAGGATTACCAAGGGAACGCTTGAAATGAGACCGATGTTCCATTTTACCCCAAATCGGATAGAAGCTCACGTATGTATCTGTTTTGTGGCCTATAAGATTTACAAAGAACTTGAAAGGGTAATGAAAATAAGCCACATCAATATGAGTGTCGATAAGGCTTTAGCTATTGCCAAAACTATTACGACATTAAAAATCAAACTACCCAAAAGCGGAAAAACAATGACCAAGACTATGATCCTGACACATAGGCACAAATCAATTGCCCATTTGCTCGATGAAAACTTCTGGAAAAACTTTTAAGGTGTCCCAGTGTCGAAGTCAGG
# Right flank :  ATCATAAATCGTAAAGGGTATGACTGATTTAAATTCACTCAGGCTAAACCTCATCCGTGGAGAAACCCGACAGGAAAACCGGGCTTTACAAATAGATATCTTTCCTGTGCCCAAGGGCTAGAATATTCACAATAAGTTCGTTATCAAATACTTCGTAGATAATACGGTAGTTACCAACTCTAACCCGGTAACCGCCTCTGCCTTTCAGCTTTATATAGCCTTTTGGTCTTGGATTTTCTTCTAAATCAGCAATGGCTGCCAGTATAGGTTCGGCAATATTGTCAGAAAGTTTATCTAATTGTTTCTGAGCCTTTTTAGATAAGCTGGCAGTATAATCAGGCATTTTTCTTTTTTCTGTTTTTCAGGTAATCAGAAAATAGAATACGTTCGCCGGTGTCCTCTTTTTTGGCTTCATCGTAAAGGCGGATATCTTCCAGTTCTTCCAACTCTTCCATAATAGCATCAAACTCTTTGGCAGGAAGAATCACCACCGACTTCTT

# Questionable array : YES	 Score: 0.26
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:-1, 7:0.15, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAAAAAGGTACGATTGACGAGGTACGAGGTACGATTGTGGGGCAAATTCGTAAATCGTAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:58.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-4.00,-8.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [61.7-56.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_464864 flag=0 multi=14.1431 len=1678		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence      	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	=====================	=========================================	==================
       696	    21	 100.0	    41	.....................	GCGTCGTGGATAGCAGCTTGCATCCGCAACACCCTAAACTG	
       758	    21	 100.0	     0	.....................	|                                        	
==========	======	======	======	=====================	=========================================	==================
         2	    21	 100.0	    42	GCGATCGCCTCCTGGGTTCCC	                                         	       

# Left flank :   CAAAGAAAGACTTTTGTGTGGAGACATTTCGGGAGCATTAGTCATCGTAGAAGAACTAGAAGAAATGAGCCGCGACGACAAAATCAATAACATTAGAAGTTATGCGGTTATCCTGATATTACACTTAATCAAACGAAAAGCCGAAAATCGCACAACTCGCTCGTGGGATGTCTCAATTCGCAACAGCGTTCGGGAAATTCAGGAGAAAAACAAGCGCCGGAAAGTGGGTGGCTATTATCTGACACCAGAAGATTTGCTCGATGCGCTAGAAATAGCCTATCCAATGGCAATCGATCGCGCATCCTTAGAAGTAGCCGAAGGAATTTACGATCCTGACACATTAGAAACCCTGGTCAACCGGGAAGAAATCATTCAGCAAGCACTGACATTGATTGCCCCAGGAACCTAATTCGCATAGGGGACCGCTGGGGTGTAGCAACAGGTTTCTCGGATATCGCAGGAAGGACGATCGCTTCATAAGTGCGATCGCCTACGGCAAC
# Right flank :  CTGGTCGTGGATAGCAGCTTGCATCCGCAACACCCTAAACTGGCGATCGCCATATACTATAGATGAGGGGTTCCAGCAAAGCGATCGCGCCATAACTCCCCACTGTTCCTTGGGCAGCGTAAAATAAAATCAGAACATTGTCCCGTATTCTAAAATAACTTGTGGGATAAAAATGAAACCAACATTACCCTTATACGACACAGACTACCAACTTTGGTTAGAACAAACTATATCCCAGTTGCAAACCCAGGATTTCAGCCAGATCGACCTAGAGAACTTAATTGAGGAAATAAAAAGTTTGGGAAAAAGAGACAAACGGGCTATTTCCAGCTACCTCATGCGACTGTGCGAACAATTGCTGAAAGTCAAATATTGGGAACAAGAACGAGAAACTTGTATCCGAGGCTGGACATTAGAAATTAGAAATTTTCGCTTGCAGATTCAACTCATCCTCAAAGATAGTCCCAGTCTAAAAAATTATCTCCAGGAAAACTTTGTTC

# Questionable array : NO	 Score: 1.54
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-0.5, 7:0.15, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCGATCGCCTCCTGGGTTCCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [1,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-1.40,-0.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [46.7-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_471695 flag=1 multi=8.0497 len=1349		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                       	Spacer_Sequence                	Insertion/Deletion
==========	======	======	======	======================================================	===============================	==================
         1	    54	 100.0	    31	......................................................	TGCTACAAGAAACTAGGTTTTGGGCAGCTTA	
        86	    54	 100.0	     0	......................................................	|                              	
==========	======	======	======	======================================================	===============================	==================
         2	    54	 100.0	    41	CCATCCAGAAGAAACTGGGTTTTTTCATCCCGGCTTCTGGTTGGGTTACAATGC	                               	       

# Left flank :   |
# Right flank :  CCACCACAAGAAACCGGGTTTTTGGCACAAAAGCGTGAATCAAATTCCTCCCCTCGACCTCGGGTTACAATACCAAAGCATTGCCGCTGAAGTGGAAGCGGCGGTTAGAGATGTGATGGCCTCGGGCCGTTATATCGGCGGTACGGCGGTAGAGGCATTCGAGGCCCAGTTTGCCGCTTATACTGGCACCTCGGAATGTGTCGCCTGCGCTTCCGGGACCGATGCCCTTTATCTGGCTCTGCGCGCCTTGAATGTGGGACCAGGAGATGAGGCGATCGCCCCATCATTTACCTTCATGGCCACCGCAGAAGCCATCAGCCTTGTGGGTGCCACCCCAGTATTTGTGGATATCGAGGCAGATAGTTTTAATCTCGACGTATCCAAAATTGAAGCTGCCATTACCGATAAAACCAAAGCTATCATCGCCGTCCACCTCTTCGGTCAGGCAGCGGACATGACCCGGTTGATGGCGATCGCCGAAACCCACAATTTACCCGTTA

# Questionable array : YES	 Score: 0.94
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:0.05, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCATCCAGAAGAAACTGGGTTTTTTCATCCCGGCTTCTGGTTGGGTTACAATGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.85%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-13.30,-9.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_479338 flag=1 multi=3.0000 len=1168		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	==============================	=============================================	==================
       735	    30	 100.0	    45	..............................	AGAATAGAACAGCACGAGACGAATCCGAAGCCGAATCGGACGACA	
       660	    30	  93.3	     0	..T..G........................	|                                            	
==========	======	======	======	==============================	=============================================	==================
         2	    30	  96.7	    46	AACCGATGACGAAAAAGCGAATCCACAAGA	                                             	       

# Left flank :   TGTTTTGGAACCAAGTGCCCACCATTTGATGTCGATACGCTTCCAATGCTGAAGGCAATTTTGAATGATTGTGAATGTGAATTGGCTGACAATGACGAAACTAAATCTCGACGTAAAAGATCTTCCGGCATTATCGATTTGGTTGATGACAAAGACGACGAAGATACAAGCAAGAGTACCCATGGGAAAATAACTCAAAGGATTCATGAATTTCACAAAACCATGGCAGCAATGGTTAAAGAGTGTAAAGTCATCAAAGGAATGGTAGATTGACTTGAAAAAGAATCAAATGTGAACGCACCAATTGAAGAAATGAATATGCAAACTTTTTTCAAAAAATTAAAATTCATGGCGGCGAGGCAATCGGAGGAAACGACGGAATCAGAGAAGAAGAGGGCAGCACGAGACGAGTCAGAACACGAATCGGACGAT
# Right flank :  CAAGTACATCTGATCGTGGAAAGAAGAAACAAACGAAATGATTGTGAATCGTCTGTGTATTGTACATAGAATGGTTAAAGTAGTGTATTAGGTATAAGTGACAGTTCGAACGATGGTCTCGAATTATTAGTAAAAGAATTGATTTGGTCATAGCTGTATATATATTAACGTAAGTTCTAACAGTAAGTGTGTTAAAAGCGAAAGAATGTTTATTCGTGCATTAATATGTATAGATATTCTTCTTGGCGATTGATATTTACTGGATCGCCAAATTTAGATAGTTGAGTCATCCATGTGCGCACTGCGAACTTCGCGCTTCACCAGTATATAAAGTGTATATTGTAACGCCATGCCACTACGTATTGAGATATACATATATATATAACCTCGTGTTAAATAAAAATAACGTTGATACCGCTATGTTTATGTAAATCTATCTCGGACACACATGATTTTGAACCCGTCACAGAAAGTGTGATGCAGAATATATAAGTACTGAC

# Questionable array : NO	 Score: 2.08
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.84, 5:0, 6:0.99, 7:0.05, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AACCGATGACGAAAAAGCGAATCCACAAGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:56.67%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [63.3-45.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_481209 flag=1 multi=11.0000 len=3287		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                 	Spacer_Sequence                                 	Insertion/Deletion
==========	======	======	======	================================================	================================================	==================
       144	    48	 100.0	    48	................................................	CCTATTATTCATTTCATTCTGAAAATTCTAAAATTCTGCAAATTCTGA	
        48	    48	  97.9	     0	.........................................G......	|                                               	
==========	======	======	======	================================================	================================================	==================
         2	    48	  99.0	    49	TTCAGAATCAGAATTTGCAGAATTAAAGAATTAACAGAATAATGTTAT	                                                	       

# Left flank :   CAACTGAAATGATAGAGATGTTGCATGTTTGTTTAGATATTGCGAGTACTGCGATTACTGAAGCTGGTGGAGTTGTTGTTAATTTTATGGGCGACGCAGTGATGGCGATTTTTAACGCGCCAGATGAACAAGCAGATCATGCTCATCGTGCGGTGCGGGCGGCTTTAAATTTACAATTTTTAATGAATCAGCATCAACAAACGACTGTAGATAATAAACCCTTGCTTTATTTTGGCGTGGGGATTAACACTGGGCCGGCGTTGGTCGGTAATATTGGCGCACAATGGCATTACCAATACACCGCAGTAGGGGATACGATTAACGTCGCTTCGCGGATATGTAGTCATGCGCAACCAAGAGAAATTTTAATCGGTGAAAATACTTATGCACATTTGGAAGGTCAGGTCGTAGCACAGGCATTGCCGCCGATTCAATTTAAAGGAAAAAGTGAATCTAGTACAGTTTATCGAGTTTTGGGGTTATAAGTATATACTATCCAC
# Right flank :  T

# Questionable array : YES	 Score: 0.92
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.95, 5:0, 6:-0.25, 7:0.02, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTCAGAATCAGAATTTGCAGAATTAAAGAATTAACAGAATAATGTTAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:77.08%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-0.20,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.41   Confidence: MEDIUM] 

# Array family : NA
//

>k141_481285 flag=1 multi=3.0000 len=1150		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                               	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	==============================================	======================================	==================
      1121	    46	 100.0	    38	..............................................	TGCACAACCTACAGCACAACCTGCCGTACAACCTACAG	
      1037	    46	  97.8	     0	..................................C...........	|                                     	
==========	======	======	======	==============================================	======================================	==================
         2	    46	  98.9	    39	CACAACCTGCCGTACAACCTACTGCACAACCCGCAGTACAACCTAC	                                      	       

# Left flank :   TGAAGGAGCAACGGTTCCTCAACCTATC
# Right flank :  CCCACAACCTACTGAGACAATGGCAAAGAAAAAATCCGAATCCCAGCCTCCAGCTACCGCTCCCAAAAAATCCGAATCCCAGCCTCCAGTCACTGCCCCCAAAAAATCCTACGTCCCGACAACAATGACCGGGCTTCAGGATTATTCCTACTGGTGGGAATATGCCCGAGAACACGCAAAAATCGCTAAACCCAATAGCAAAACTTTTCGCCGGGGGCGGATTTGGGCATAGTATTTCAAGTCAAAAGTAGGGTGCATTAATGTAATGCACCGCCTTAGCTCCCAGTCTTAGCTTGCAATTGGTGGTGGGTTACGCTGGCACTAACCCACTCTACGAATGCCCTACGAATGTACAAAAATCCATTTTTATTGTTGATGTTGTTCGTTAATTAAAATGACATTTCAAATCAAACCTCACTACCACATCGAAATTTGTCCCCCCAAACAGGTTTACCTCCTGGGGGAACAGGAAAACCACGCCCTCACGGGACAGCTTTACT

# Questionable array : NO	 Score: 1.86
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.95, 5:0, 6:0.25, 7:0.46, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CACAACCTGCCGTACAACCTACTGCACAACCCGCAGTACAACCTAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,15] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-9.20,-6.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [48.3-23.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.64,0.78   Confidence: LOW] 

# Array family : NA
//

>k141_45135 flag=1 multi=14.4770 len=4973		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                                    	Insertion/Deletion
==========	======	======	======	============================	===================================================================	==================
       351	    28	 100.0	    67	............................	AAGCTAAAACCTGATGATCTCAATTGATTCCTGTGTCTGACGCACCCTACCAGTCGATTAAAGTGTG	
       446	    28	 100.0	     0	............................	|                                                                  	
==========	======	======	======	============================	===================================================================	==================
         2	    28	 100.0	    77	ACTGGCATAATCAGCTTTGGTGCGTCAG	                                                                   	       

# Left flank :   ACCGGGTTTTTTGCGTAAGTCCTATTATAGCTGTAACGTAAGCATAAGCAGAAGGTTTGGCCACACCATAATCATCCTCTTCTTCTTCCTCTAAAAGCGCTTGCAACCTAGCAATAGTAATGCTTACTTCTGCATTTTCAAAGGTAAGTGTTTTGTTTTCAGATTTATTATTTAAACTGGTTTTTATTGTCCCTAAACGTATATATTTCAACTGACTTATACTTGGCTCAGTTTGCTTTGCAAACCAAAATGGATTCTCAGATATAAAGACGCACCTTTGCGCGTCTTTACTACAATTATATTACAACACTAATACACAGGTCGCATCTAACTTCAACTGGACTTACGCAA
# Right flank :  GTGTCTGACGCACCCTACCAGTCGATTAAAGTGTGACAATTGCGTAAGTCCTAACAGATAAGCTTCGCCCTCATTAGCCTAAAGTGTATGCTGATTGTATTGCCCACCAAAGTAGGAAACTGATACCACCCAGGACTGCGATCGATGAAATGACGACTGCAATTGGGCTATCAGCCCCTTTAAATTTCATTATTCCGCGATTTAAGTCAGACATAGTACATTTATCCCAATTCTTACGCTGATTTTAGCGAATACTCTATCTTTCTAACATCTAACTAACGTCATATTTATAAAATTAAAGTTAAGAATTGAGGCAATCAAGCTGGTTTCGATCGTATTTTTTCATAAATTACACATTTCTTAATCAGGCTTTCATTATTTCCTCAAGACAGTTAAAAGCAGGCAAAATGCCTGCTCTACAAGAGTTATGGAAAATGTATAATAATTATTGCTGATGATTTTCCAACCAAGCAACTAAATCATTAACATCAGAAAAGTCA

# Questionable array : YES	 Score: 0.99
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:1, 7:-1.9, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACTGGCATAATCAGCTTTGGTGCGTCAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-1.20,0.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [63.3-56.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_499358 flag=0 multi=26.9200 len=5431		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence                             	Insertion/Deletion
==========	======	======	======	========================	============================================	==================
       957	    24	 100.0	    22	........................	GATTAGCACTCTTCATAACCAG                      	
      1003	    24	  75.0	    44	...T..AT......C....C.C..	TACATCACCTGATTTGATTGGGGGTTTAATTATGTGGATTTACT	T [1009]
      1072	    24	 100.0	    26	........................	TTACTGGTTCTGGTTATCTAGAGAAG                  	
      1122	    24	  83.3	     0	...T..........C....C.C..	|                                           	
==========	======	======	======	========================	============================================	==================
         4	    24	  89.6	    31	TAGGAGGGTGGGCAGTGCCTAACC	                                            	       

# Left flank :   GTTTCGCCTTATTCCTCGGCGATCGTCCTGGTAAATGACCAGCAAAAGCAGGCATTGAAAGCCGCCGAATTGCGCGCCGACAGGTTCGATCGAGAAGTAGAAACCGGCACCGAACAGTTGACCAACCCGCTCAATACTGTCACCGCCACCGTTCCCGAACCAGAAATGTGGTTAAGCGGTATGATCGCAGTTACCTGTTTACTCATCCTCAAACGCCGCCGCCGCGTTAAAATATAGTCAATTCAAATAAGAATGAGACGCCAAGTGCCTGCACAGTTGCCCTCTATCCCCCAACCCCTTTCTCCCAAAAGGGGAGAAAGGGGAGTAACCGTAGTAACCGTAGGTTGGGTTGAGGAACGAAACCCAACTTACAGCTTGTTCTCAAAAAATGTGGGACATTTTCAAAGTCCCTCTCCCTCTCTGGGAGAGGGATTTAGGGTGAGGGAGCTGTACTAAGTTAATGTTGAACAAGCTGTACTGTTTCTGGTTATCTAGAGAAG
# Right flank :  CTACATCACCTGATTTGATTGGGGGTTTAATTATGTGGATTTACTTAGGAGGTTCTTAGTTAGGATGATATAGAACAAGGGCGAAGCATCCGCGTACAAGGTTCTTAGTTAGGATTATATAGGTTAATGGCGCGGATGCTTCGCCCCTACAACCCATCATTTAAACCGCCAATTACTCATACTCCTGGCGGGATTTGAACCCGCAAAAAAACAGATTTTAAGTCTGCCGCGTATGCCAATTTCGCCACAGGAGCAAATTTTGTTTCAATTAGATTTGGGCTAGTGAAAGGTGGCAACCAAACCACCTTCACCATACCCAAAGGGGAGCTGTGGAATCCTGGCAGATATCCTCGCTGTTTCCCCATAGCCCCAACGGGGTTTGAACCCGTTTTGCTGCATTGAAAGTGCAGTGTCCTCACCAATTAGACGATGGGGCCAGAAACTGGGGGAAATACCCCAGGGGAAGAAACCGGGTTTCTAAAATCAAGGGTCTTGTCACT

# Questionable array : NO	 Score: 1.97
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.48, 5:0, 6:0.25, 7:-0.36, 8:0.6, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TAGGAGGGTGGGCAGTGCCTAACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [5,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.70,-2.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [7-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [60.0-65.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_499466 flag=1 multi=4.0000 len=1927		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                       	Spacer_Sequence                         	Insertion/Deletion
==========	======	======	======	======================================	========================================	==================
       996	    38	 100.0	    40	......................................	CTATACGGACGAAGCCTGCCTACGCAGGCTGAATAGTCCA	
       918	    38	  97.4	     0	...................................C..	|                                       	
==========	======	======	======	======================================	========================================	==================
         2	    38	  98.7	    41	GTCAAAGTCTATCAAAACCCACAGGCTGAAGCCTGGGG	                                        	       

# Left flank :   CCCCTAACAATGCACAACCGGATTGTTTACATCCATCGGTAATTCCTTGGACTACATTCGCCAGTTGTTCTTGATTGAGTTTTCCAGTGGCTAAATAATCTAAAAAGAATAGGGGTTCGGCACCGGAGGTTAGAACATCATTCACACACATTGCTACTAAGTCAATGCCAACCGTATCGTGGCGATCGAGCAACATGGCGAGTTTGAGTTTCGTTCCCACCCCATCGGTCCCCGAGACTAGGATGGGTTGCTGATATCCGGTGGGTAACTCGAAATAACCGCTAAATCCACCAATCCCCCCAAGGACTTCCGGTCGATGGGTCCCTGTAACCATATTCCGAATTTTCTGGACAAAGGCCCGACCCGCCTCAACATCCACGCCTGCTTCTCGATAATCCATGTATCAACCCATCTGAACGGCTATTTCCACCTTGGCTATTTTACAGGGGAGGTAGAACCAATGGATCGCGGGAGACAGGATGTTTTTATCCAATCCGGTT
# Right flank :  GTTTTTGCGTGGCAATTGAACAAAGCGCAAATTTTGTTTTATGAATGTAAAGTTTTGATAAAGTATTCTTTCCCGCAAATAAATCCAAAACCCTGATTCTATCTAGGTTTAACTAGGCTAGGCCCCCTTGAGAAATTGTAAAGTTTTAATAAGGTTACACTGTTTTAATCGACCATACCGATCCCCAAGCTCAAATCTTTGTGATACTCTGTTGCAGTTCAACACACCAAGCCAAAAACGTATCGACTATGAGAAAGGTTAAGCAAGCCCCAAGGAAGGGGCCGCCTTGGGTCGTTCCAAGGGCCTAATCCCACTCATCGATTGTTCTTTATCAGTTCCTCACAGCAAAATATATGAAGCAAAGATTTTGGTGGACAACAGTTGTGCTACTCACAACAGCCTGCGGGACAACCTCAACCACTCATGCTGAACCGAATAGGCCCGTCAATTTGAACTCGGATCCCGACCAAATCCAGGTCAGTCCTTGGTCCGAAGTTATT

# Questionable array : NO	 Score: 1.65
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:0.29, 7:0.22, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTCAAAGTCTATCAAAACCCACAGGCTGAAGCCTGGGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,12] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-4.20,-3.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [70.0-51.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.64,0.78   Confidence: LOW] 

# Array family : NA
//

>k141_503139 flag=1 multi=22.0000 len=1169		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                       	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	======================================================================	=====================================	==================
       944	    70	 100.0	    37	......................................................................	ATGACATCAGCGGGTCGGTCGCTCGTAGCAGCACATG	
       837	    70	 100.0	     0	......................................................................	|                                    	
==========	======	======	======	======================================================================	=====================================	==================
         2	    70	 100.0	    38	GGCACTATTGTACAGTACGTACAGTAACACCTGTTGTTCAGTGTGGCGTTTTGCGACAGTTGCAAATTTT	                                     	       

# Left flank :   AGGTAGTTGGCGTGACACGAACGGGGTTTGATTCACTGGACTAATTTTTTTTTCTTGCATTTTTGCGAAATGTTGAACCAAATTTTTTTCAGTATTGTATTATGTGGATTCTACATAAAGATAAATAGAAGAAAGTGTCTAGTACATCGTATTACGTAGAGTGTACATAAAGATTATTATTACGAAGAGTATTACGAAGAGTACAGCACCCGGATGTTTGCAGA
# Right flank :  GCGACAGTTGCAAATTTTATGACATCAGCGGGTCGGCTGCTCGTAGCAGCACATTGGCAAGTTGTACAGTAATTAGTGGACAGTGACAAACAGATCATTCTCAACTATTGCTAATTAGTGGACAGCGACAAACGGATTATCTCAACTGTTGCATTGACTCCTTTCAATTCAAGTCAAAGACGTCATTGTATTGTCCATCTGTCCAATAAGTGGGAGTCTTGGTGCTGCGGAACTGGTGCAGCATTCTTTGCAATGCTTCTGGTACTGCGTATTTGGTCAGGTCGTAGCTAAGAGGCTTTCTGCAGAAAAGTCCTGCACGGGTCTTTACACGGGAAAGCATGACATACACCCAGTTCTGCACATAGCTCCAATTATGAACAAACAAGCTATCAACTCCTGAACCCTGTAATCGGTGTCCTGTGGTGGCGTTGTTGATTAGCAGGGGTAACTGGCATGCCTTCATCTGCAATGTCTCATGTTCGTCACTCTTTACTTGAAGC

# Questionable array : NO	 Score: 1.67
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:0.78, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCACTATTGTACAGTACGTACAGTAACACCTGTTGTTCAGTGTGGCGTTTTGCGACAGTTGCAAATTTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:57.14%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-11.20,-15.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [48.3-63.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.64   Confidence: HIGH] 

# Array family : NA
//

>k141_505136 flag=1 multi=4.0000 len=1046		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	==========================	======================================	==================
       865	    26	 100.0	    38	..........................	AATATTAAAACAAAGAGTCCTGAAAGACTCACTACAAT	
       929	    26	 100.0	     0	..........................	|                                     	
==========	======	======	======	==========================	======================================	==================
         2	    26	 100.0	    48	AGTAGTAAGCCCTTCAGGGCTATCTT	                                      	       

# Left flank :   CATTGGGAATCGAGCTATGGTCAATTACCACACCATTGCCACATTCAGTACCTTCTACGGTCTTTTTATCGGCTTCGGTTTGAATTCGTCGATCTGCTATCCCGTCCCGTACACGCAACACTGTCCCCCCCTGGGCTGCTAATACGGGCACTCCTCTGGCCATGGTTTGTTCATCGGGTATGGCAAAATCCGTGCCTTTATGACCATCGTAGGTTTGCCGTCCACAACCAAAATCAACAGCATTGGGAGTTGGATCTCGATCAGGATATAACAAGATAAAGCAATCCTGACCCAATTGACAATCAATTGGTTGAGCAAAACGGGGATTTTGGGTTTGGCTGATTAGTTTTTGGGATATGCCTTGAGCCATGGTCTGAGCTTGCTGTAGGGAAGACTCAGCCGTTTGGGTGACTTGAGGACTCCAACAACTAACTAATACCGTTAACCCCAAAGTTAGGCAGAATGCGATCGCATATTTTTTAGCCGCAGAAATTCTCATA
# Right flank :  GAAAGAAGGAGTCCTGAAGGACTCACTACGATTTATGAAAGAAGGAGTCCTGAAGGACTCACTACGGTTTATGAAAGAAGGAGTCCTGAAGG

# Questionable array : NO	 Score: 2.61
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.26, 7:0.46, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGTAGTAAGCCCTTCAGGGCTATCTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.85%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-7.80,-7.70] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [60.0-55.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_503133 flag=1 multi=3.0000 len=1018		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence   	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	==================	=============================================	==================
       215	    18	  72.2	    18	AT..A........C..T.	GATTTGCTCCTGATGGCA                           	
       251	    18	 100.0	    45	..................	CCTTCACTATAGAGGTTGGACTCAAAGCTGTTGGCTGAGCTTGTG	
       314	    18	  94.4	     0	..............G...	|                                            	
==========	======	======	======	==================	=============================================	==================
         3	    18	  88.9	    32	TAGGGCTCTGAGGTATAG	                                             	       

# Left flank :   ATCGGAGCCGGCCCAGTCAAAGGCATTGGAGCACGTCCAGTTCCTGATGTCAGAGCCAGCCCAGATGAAGGCATTGGAGCAGGTCCAGTTCCTGATGTCAGAGCCAGCCCCATTGAAGGTGTGGGAGCAGGTCCAGTTACTGGTGTTAGTTTGGGTCCAGTTCCTGGTGATGGAGTAGGAGGTGGCACTGGTGCTGACGTTGGAAGCTTTTGAGA
# Right flank :  GGGTTTGTTGCTGGCATCAGTTTAGTGACCACCGTAGGACTCGGGGATGGCACTGTCACAGGAGCTGGTGATGGCTCTTGAATGCCACAATTCTTGCAGACCTTGAAGGAATCTAGCTGGGCATTACATTTTTCTCTGGCCTCATCTGTGCAGCAGTTGATATTACCTTTATGCCATTCCTCAAACTCATCACATTGTTTGCCAGAAATTATACCTGGAGACCTGCGATTGCACATAGAGCAGCATACAATGTTCACAATTGAACAATCTATGAACTCCCCATATGCCTCATTGCACATTGCCCCTGCAGCCAGAGTGGGAGAGGTTTCTGGTGTAGGCTTTTGACCACCACTGGGAGTGGGAGAGATTGCTGATGTTGGAGTTCCAGGTGCCGGTTGGTTGCCGGGAGTTGGTTCAGGACCAGTTCCTGGGGTAGGAGAGGGTCCAGTCAAAGGCATTGGAGTCGGCTCCAATGCTGGCTTTGGAGCTGGTTCAGTCAA

# Questionable array : YES	 Score: 0.11
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:-1, 7:0.05, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TAGGGCTCTGAGGTATAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [50.0-45.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_4806 flag=1 multi=8.0000 len=2641		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                            	Spacer_Sequence                                     	Insertion/Deletion
==========	======	======	======	===========================================	====================================================	==================
        34	    43	 100.0	    52	...........................................	GGCAACACCAAAGTTATCATAGAAACCCGGTTTCTCGGAGAAGGCGATCGCA	
       129	    43	  95.3	     0	.....................................A..G..	|                                                   	
==========	======	======	======	===========================================	====================================================	==================
         2	    43	  97.7	    53	CTTCTTGGGATAACAGAAACCGGGTTTCTACAGAAATCTCTGT	                                                    	       

# Left flank :   ACCAAGAAAAAAGAATGATAAGAGGCGATCACGC
# Right flank :  TAATATCGGTGGCAATAGTTATCATAGAAATCCGGTTTCTCGGAGAAGGCGAGATCGCCTGAAATTTTCGCGAGTAATTTTGACGATAAATTTTGGCTATTTTAGCTTGACAAATTGGCAAATTTTTTCTACAATCAAATTATTCGATTAGATTTAGGATAATGGGAATCCGTGTCAGCCTGTGAGGTGTCCCTAACTTCTTTTATCTAAAATAGTTTTCTGGGCTAAGTGCTGACAGAAAAGTGGATGAATGAAGCGATAAGCAACACCCGAATAACTCCGCTGATTTCCCCCGCTAATTCCTTTGACTTGCTGGAGAAATAAACAGCTTTTGCCATAATTCAGCAGTTGCCGATAGTTCCAGGGAATGCAGCCTTGAAAGCAGAGAGTTAGATGCAGAGAAAATTGGTGAATTTCTGCCAGTCCACCGAGGGCGATCGCTAATAATCCACCGATGATGATGCCCCCAATCAACAGTTGCCACGCTACAAATATTTTAC

# Questionable array : YES	 Score: 0.94
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:0.25, 7:-0.4, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTTCTTGGGATAACAGAAACCGGGTTTCTACAGAAATCTCTGT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:58.14%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-4.70,-4.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [33.3-53.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.27   Confidence: MEDIUM] 

# Array family : NA
//

>k141_4855 flag=1 multi=2.0000 len=1075		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                	Spacer_Sequence                                          	Insertion/Deletion
==========	======	======	======	===============================	=========================================================	==================
       795	    31	 100.0	    57	...............................	TATATATCGAGAAAAGAAATTTAAAAAGAAAAATTAACAATACATTCTCCTAAAGCT	
       707	    31	 100.0	     0	...............................	|                                                        	
==========	======	======	======	===============================	=========================================================	==================
         2	    31	 100.0	    58	TTTGCGTCTCTGCGTCTTTGCGTGAAAAAAA	                                                         	       

# Left flank :   GGTTTATTGGGATGAATTAGTTATGGATATAGGTTTCAGAGCGGATTTAATTGTAGAAGAAAAAGTAATTGTTGAACTAAAATCAGTCGAACAACTATCAACAGTTACCCCAAAACAAATTTTAACCTATTTAAAAATCACAAATTGTAAATTGGGTTTGATTGTGAATTTTAATGTTCCATTAATTAAGAACGGCATAACAAGAATTGCAAATGGACTGTAAAAGAAAACTGTAAAAATAAAATGCTTAATAAAATTTAACTAGTAAAATCTTTGCGT
# Right flank :  ATATTGCGAGAAAAAAGATAGAAAATGAAATATACCAGACTAACCAAAGAACAATTCGAAGCCCTCCATCAGGAGTTTGCTAACTTTTTAGCTACCCAATCCATTGATAAAAAAGAATGGGATAATTTAAAAGTAACTAAACCTGAGGTTGCTGAACAAGAATTAGACGTATTTTCAGATTTGATTTGGGAAGGTGTATTATCGCAAGCCAATTTTTTAGAACATTTTTCCAAAAATCATATTTTTCTGTTTCATTGTTTGGAAACAAACGTGCATTCCATTGTTTTGAAAACATTAGAACCCGAAGTTGACTTTATGACTTCCAAAGGATTGGAATGGATGGTGAAGAATTTATTTACCGACGCTATTGATATTCATGTTGGGAAAAAAGCTTATGAATCCGATCGAAATAAGTCCATTTTTGACATCATTCAACAAGGAGCGATATTAAGTGACGGACAATTATACCAACAAATTGATCAAATAATCAACTCAAAATA

# Questionable array : YES	 Score: 1.33
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.34, 7:-0.9, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTTGCGTCTCTGCGTCTTTGCGTGAAAAAAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:58.06%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.60,-2.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [71.7-80.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_47478 flag=1 multi=31.3981 len=34575		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                                            	Insertion/Deletion
==========	======	======	======	=============================	===========================================================================	==================
     15748	    29	  93.1	    75	.......G..C..................	CGAAACGAAATAAGACAACCTATTGACAAACCCGGAACTGAAATGCTAATTTTGGATCCGGGATAACGCAACCAG	
     15644	    29	 100.0	     0	.............................	|                                                                          	
==========	======	======	======	=============================	===========================================================================	==================
         2	    29	  96.5	    76	TTTACCCAAAACCCTTTCAGGGATTGAAA	                                                                           	       

# Left flank :   GGATTTATTCGACTGTGTAATTCCCACCCGCTGGGCCCGCCACGGTGCTGTGATGGTACGCGGTGAGCGGTGGAATATTAAAAATGCCCAGTTTCGGGAAGATTTCACCCCCCTAGATGACACTTGTCCTTGTTACACCTGCCAAAATTTTTCTCGCGCTTATTTGAATCACTTATGGCGGGCGAGGGAAATCCTCAGCCACACTTTACTATCGATTCACAACGTCACGGAGTTGGTCCGGTTCACCAGCCGCATCCGGGAAGCGATTTTGTGCGATCGCTTTACCGATGAGTTTGGCCATTGGTTAACAGGTGCAGGCGACCCACACACAGGAGGAGGGGGCGGCTAGGGGAAAAATATTTCTCGCCACACCTCTGAAACTATAGGAAGTTGTATTCCCAATTTCCCCAGTGTCCTCGGCGTTTAAGACAACCGATTGACAACCGATTGGCAAACCCGGAACTGAAATGCTACTTTTGGTTCCGGGAGAACGCATCCCT
# Right flank :  TTTTCAATTATGATAACTTTGTTGAAGAGGTAAAAAAATACATGGAAGCAACAATGCTGTTGGCAAAACTGCCCGAGGCTTACTCAATTTTCGATCCCTTAGTGGACGTTTTGCCACTCATCCCCGTCTTTTTCCTCCTGCTGGCTTTCGTCTGGCAGGCAGCGATCGGCTTTAAATAAGTGAAAAGTGAAAAGTGAAAAGTGAAAAGTGAAAACTTAATTTTTCACTTTTCACTCAAATAGTTAAGCAAAAAAATTCACTCTTATTTATTCACTCTTCACTCTTCACTCCGTCCAGGCGGTCTGGGATAGTGAAGAAAAACGTAGCACCCTTTCCTGGCTCCGACTTGACCCAGATGCGGCCCCCGTGACTTTCGACAATCTTCTGGCAAATGGCTAAGCCAATGCCTGTACCGGGGTACTCGGAATGTCTGTGTAGGCGCTGAAACACGACAAAGATGCGCTGGAAGTAATCCGAATTGATACCAATACCGTTATCTG

# Questionable array : NO	 Score: 2.32
# 	Score Detail : 1:0, 2:0, 3:3, 4:0.82, 5:0, 6:1, 7:-2.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTTACCCAAAACCCTTTCAGGGATTGAAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         R Score: 4.5/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:62.07%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-4.00,-4.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [71.7-48.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.68,4.87   Confidence: HIGH] 

# Array family : NA
//

>k141_48542 flag=1 multi=4.0000 len=1314		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence       	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	======================	===================================	==================
       795	    22	 100.0	    35	......................	AAGTTTGAGGTTTGACGTTTGAAGTTTGAAGTTTG	
       852	    22	  95.5	     0	.................T....	|                                  	
==========	======	======	======	======================	===================================	==================
         2	    22	  97.8	    36	AGGTTTGAGGTTTGAAGGTTTG	                                   	       

# Left flank :   ATCCTTACAGCAGTTACGAAGATTTTGATTTTGAAATTCCGGTAGGTACTACAGGTGATAATTACGACCGCTTTATGGTGCGCAATGGTGAAATTTGGCAGAGTTTGCGCATTATTGAGCAGGCCTACCGTAAAGTGCAGGCATTTAAAGGAACAGAAGCGGAGGTATTTCATGCCGATGTGCCTGAATATTATTTGCCGGAGAAAAAGGATGTATACACCAAAATGGAAGCCCTCATATGGCATTTTAAGATCATAATGGGCGAAATTGACATTCCAAAAGGTGAAGTTTACCATTCAGTGGAAGCGGGTAATGGAGAACTTGGATTTTACCTTGTAAGTGATGGAGGCCGTACGCCATACCGGCTACATTTTAGAAGACCTTGTTTTATATATTACCAGATTTATAAAGAAATAGTAAAGGGATCTATGCTAAGCGATGCTATTGTTACGCTTAGCAGCCTCAACCTGATTGCCGGCGAAATGGACGCTTAGGGTTGA
# Right flank :  GAATTTGACTATTTGGAATTTGGCGTTGCCCTGAGCTGGTTGAGGCTCTCGAAGCCGCGAAGGGAATTTGACTATTTGGAATTTGATTATTTGGAATTTGGAATTTGACTATTTGGAATTTATGATAAAATTCAACGAAGAAAAACTGGCAAAGGCAAATGAAATTATTGCCCGGTACCCGGAGGGGAAGCAAAAAAGTGCTTTAATGCCTTTGCTTCACCTTGCACAGGAAGAGTGGGGATGGCTGAGTGCCGAGGCTATGGATTATGTAGCTTCCCTGTTGCAACTTGAACCGATTGAAGTGTACGAAGTGGCAACCTTTTATACCATGTATAACATGAAACCGGTAGGCAGTTTTGTGTTTGAGGTATGCCATACCGGCCCCTGCATGCTGAGAGGAAGCGATGATATTATTGCCTACATTAAGGAAAAGCTGCACAT

# Questionable array : NO	 Score: 1.54
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:-0.25, 7:0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGGTTTGAGGTTTGAAGGTTTG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:59.09%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [48.3-48.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_537691 flag=1 multi=9.0000 len=2588		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                             	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	============================================	==========================================	==================
      1077	    44	 100.0	    42	............................................	GTTGGAAAAAACGATTGGAAAAAACGAATGGCATTCATGAAA	
      1163	    44	  97.7	     0	........................................G...	|                                         	
==========	======	======	======	============================================	==========================================	==================
         2	    44	  98.8	    43	CGGCGCAACCACAAATTGGGGCAACCACAAGGGATTGCCCCTAC	                                          	       

# Left flank :   ACCGGCAGGGGCGAGGCGTTTATTTGAGGCTACAATATGATTTTTGAGGAAATCGGGGCAACCATAAAGCAACCATAAAGCAACCATAAAGGGGCAACCATAAAGGGGCAACCATAAAGGGGCAACCATAAATAAAGGGGCAACCATAAAGGATTGCCCCTACGTTGAGCCGGGTCTTGACTGCGCCTCCAAAAACAGGGTATACTTTGGCGTAAGCAGTTGGATTTCCTATCCTATTCACATTCCTAAAAGTCTTATTGAGCAAAATCAAATTGTTCAAGGTATTGAGAAAATAAAGCAAGAAACTCAACGCCTCGAATCCCTTTACCAGCGCAAACTTGCCGCGTTGGAGGAATTGAAAAAAGCGTTGTTACATCGGGCGTTTAACGGGGAATTGTAGGGGCAAACCCTTGTGGTTGCCCTATTCATGGCACAACGGGGCAACCGTAATTCGGGGCAACCGTAATTCGGGGCAACCGTAATTCGGGGCGATTATAATT
# Right flank :  CAAATATCCCGCATCATAGGGGCAATCCCTTGTGGTTGCCCCCAGCATTGGATAACCATGAAATACAATCCTAACATTCATCACCGTAAATCTATTCGCCTGCAAGGATATGATTATTCACGTGCAGGGGCGTATTTTGTAACGATTTGTACGCAAAATCGGCAATGCTTGTTAGGTGAAATCATCCAAAATGAAATGATTTTAAATAACGCGGGGAAAATGGTTTTGCAACAATGGGAGGCTTTAATTCAACGCTTTTCTTCGTTACAATTAGATGCTGTTGTTGTAATGCCTAACCATCTGCATGGAATTTTGGTTCTTGATGGCAATTCAACAGAAACGCTTGGTGATATTATTGGCGCGTTTAAATCCATCACCACCAATGAATATATCAAGGGTGTGCGGCAATATCATTGGCAAGGATTTGATAAAACATTTTGGCAACGTAATTATTATGAACATGTTATTCGTGATGAAACCGCTTTAAATAAGATTCGGGA

# Questionable array : NO	 Score: 1.50
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:0.25, 7:0.11, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CGGCGCAACCACAAATTGGGGCAACCACAAGGGATTGCCCCTAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [13,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-9.80,-9.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [50.0-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.15,0   Confidence: HIGH] 

# Array family : NA
//

>k141_556980 flag=1 multi=25.4605 len=1331		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	========================	======================================	==================
       129	    24	  95.8	    38	......C.................	AGATATAGCAACCGCCAAGGTGGTTAAGGCATAAATAT	
        67	    24	 100.0	     0	........................	|                                     	
==========	======	======	======	========================	======================================	==================
         2	    24	  97.9	    39	GGGGTAGGGGCGAAGCATTCGGGC	                                      	       

# Left flank :   CCTAGAAGAAATTAGCGAAATTGTCAAAGATGTGCGTGGCGATTTATGGTCTAAATAATGATAATAGTCCTTGATGTAAACGTTTGGATTTCTGCTTTACTTTGGGGAGGACTACCGAGTAAAATTCTGCGCCTGGCCAGAAATAAAGAGGTGACTATATTTGTTTCTGAAGCTCTATTACTCGAACTAGAAACTACCTTAAAACGTAGCAAATTTCAGTCAAGATTACAGCAGCGAGGTCAGAAAGTTGAGTTTTTGATATCGTTGACTCAAGAATTAAGTGAATCTTGTGGGACAATTGCCGATATTGACGTACCAGAATTGCGCGATCCCAAAGATAGTAAAATTCTGGCAACTGCTTTAAGGGTTGAAGCTGATGTTTTGGTCACAGGTGACTTAGATTTGCTAGTAATGAAGGAATTTAGAGGAATTCCAATTCTAACTATTGCTAATTTTCTGGAAAGCTATAGCAGCCGCCAAGGTGGTTAAGGCATAAATCC
# Right flank :  CGAAAAATCCTGAGTAAAAACGATAAATTATTTGCCCGAATGCT

# Questionable array : NO	 Score: 1.81
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.90, 5:0, 6:0.25, 7:0.46, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGGGTAGGGGCGAAGCATTCGGGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [48.3-58.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_566630 flag=1 multi=4.0000 len=1037		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence     	Spacer_Sequence               	Insertion/Deletion
==========	======	======	======	====================	==============================	==================
        50	    20	 100.0	    30	....................	ATGAGAATGTTGGTTGAATCACAGAGATTA	
       100	    20	  95.0	    27	............C.......	CAACATAGGTGCGATCGCCCCCAAGCC   	
       147	    20	  90.0	     0	C.C.................	|                             	GG,G [151,156]
==========	======	======	======	====================	==============================	==================
         3	    20	  95.0	    29	TATAAAGAAACCGGGTTTCT	                              	       

# Left flank :   AAGGATAAAACCGGGTTTATATGAATTTATTTTAATCGCCCCACCAAGCT
# Right flank :  TACGATCGGGTTAGTTGAGTGACCGAGATTATACAAATGCTGAAATAGCTTGCTTTAGTCGGACGCAGCTTATAGAATAAAGTTAAACTGTCAGGAGGAACTGTTTAATGCCTGAACTCGCCCGTTTTTTTGGCATCATTATCTCAATGTACTATAACGACCATCCCCCACCGCATTTTCACGTTCGATATGGGACACAAAAAGCGATTATTGATATTGAAACGCTATCGGTGTTAGCAGGAGAACTTGCACCAAGAGTTCGGGGAATGGTTATCGAGTGGGCGACACTTCATCAAGCCGAACTTTTACAAAACTGGGAGTTGGCCAGACAAAATATGCCGTTAGAAAAAATTATACCATTGGAGTAATGTTATGCTGAAGGATATTATATCAGTTAAAACCCTAGAAGGATATGAACTATATTTATGTTTTGAAGATGGAATCGAGGGCATAGTTGATGTCAGCGAAATCATCGAATTTACTGGAGTGTTTGCACCGCT

# Questionable array : YES	 Score: 0.07
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.75, 5:-1.5, 6:-0.75, 7:0.17, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TATAAAGAAACCGGGTTTCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:65.00%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [0-6] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [51.7-60.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.41,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_576516 flag=1 multi=5.0000 len=2277		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                    	Spacer_Sequence                	Insertion/Deletion
==========	======	======	======	===================================================================	===============================	==================
       165	    67	 100.0	    31	...................................................................	TAAGGTAGCTAAATGTAGCAGCCTGTCCAGG	
        67	    67	 100.0	     0	...................................................................	|                              	
==========	======	======	======	===================================================================	===============================	==================
         2	    67	 100.0	    32	GTAAAACTGTAGGTTGGGTTGAGGATACGAAACCCAACAACAACCAAAAGTAATTTTGGGTTTCCTT	                               	       

# Left flank :   GGGCTGGGTTGCCGAATGCCCTCGGTTTAGGCGCGATCGCCTTAATTATGTTCCTGCTGGTTTTAGGGGTGACTGGCTTAAGTTTGGGTTTAGCTTTTGCCTTACCCGGTCACATTGAATTAATTGCCGTAATTTTTGTCACCAACCTGCCATTATTATTTGCCAGTACCGCCTTGGCGCCGCTTTCTTTTATGCCCAAATGGTTGGCGGTCATTGCCACCTTAAACCCATTAAGTTATGCGATCGAACCCATTCGCTATCTTTACCTGCATCAAGATTGGACTTTGGGCAATATTGTCATGCAAGCCCCTTGGGGAAATATCTCCTTTGGCGCTTCCCTGTTAGTATTACTGGGTTTTGCGGTCTTGTCCCTAGTCAGCATTCAACCCCTGCTTAAGCGCACATTCGCTTAAGGTAGCTAAATAGACAAAAAAGCCCCCCTTTTTAAGGGGGGTTGGGGGGATCGATCCGGTATGAAAGGACTCTTGTCTAATGTAGTA
# Right flank :  T

# Questionable array : YES	 Score: 0.94
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:0.05, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTAAAACTGTAGGTTGGGTTGAGGATACGAAACCCAACAACAACCAAAAGTAATTTTGGGTTTCCTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:61.19%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-15.10,-17.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_573403 flag=1 multi=7.0000 len=2160		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                          	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	=========================================================	=============================================	==================
       247	    57	  94.7	    45	.................C.....G...................A.............	TTTAACCGGGGTAATGCGCTGTATAACTTAGGGCGAATTGAAGAA	
       145	    57	 100.0	     0	.........................................................	|                                            	
==========	======	======	======	=========================================================	=============================================	==================
         2	    57	  97.3	    46	GCGATCGCCTCTTACGATAAAGCCATTGAAATCAAACCTGACTTCCACGAAGCTTGG	                                             	       

# Left flank :   AAATGCTAAACAAAATTTGGCAATTCTTCAAGCAATTATTTCAACGGCTACTGATTATTTTTCCCCTATTTCCCAAGGATGAAAGCAAAAAGCATCCTGTAACGCCAACGGATTTTACCCTGACTGATGAAGCTTATGAAGCGATTTTTATGGAGCTTTTAGAAGAGGTGGATCGAGGCACTAGCAGTGGGAATGTGGCTGGGTTTTTGATGGTGAAAAATGTTAAGGAGAAGGAGTTAGCGTTTTGGTTGCGGCAGTTTGGTGAGAGGTTGTTAGGGGTTGAGAATCCATCGGTGGCTTCGCATCAGGAATTGGCGCGGCGGTTGGTGTTATTGAGTGAGGTTCGCGGTGGGGAATTGGGAGGTGTTGCGGGGGAATTGGGAAGAGAAATTAACCGCAGAGGCGCAGAGGACGCAGAGGGAAATGAGGAGGTTAATCAAGAGGCTGAGGAGTGGTTTAATCGGGGATTTGATCAAGTTATGACTGGTGATTTCGTAGGT
# Right flank :  GAATAACCGGGGTGTGGCGCTGGGAAACTTAGGGCGAATTGAAGATGCGATCGCCTCTTACGATAAAGCCATTGAAATCAAACCTGACT

# Questionable array : YES	 Score: 0.11
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:-1, 7:0.05, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCGATCGCCTCTTACGATAAAGCCATTGAAATCAAACCTGACTTCCACGAAGCTTGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         R Score: 4.5/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:52.63%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-10.60,-1.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [46.7-55.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.78,4.5   Confidence: HIGH] 

# Array family : NA
//

>k141_577298 flag=1 multi=2.0000 len=1664		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                             	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	============================================================	======================================	==================
       327	    60	 100.0	    38	............................................................	CGTCAGTTATGACACGTAAGGATGTCGGAACCTGACGA	
       425	    60	  91.7	     0	...C............................T...............T....G...G..	|                                     	
==========	======	======	======	============================================================	======================================	==================
         2	    60	  95.8	    39	TGGTCGCAGACCGTCAGTTATGGCAGTTGCGGATGGCGGTGACTGACAATGGTTGCAAAC	                                      	       

# Left flank :   AAACCTGGGCAAATTGTTCATGCAATAATCAACCAATGCCATCAGGAGATCCCCCGGCAACGTCTATCTGGGCCTGGCGGCTGTGCTGATATGGATTGGAAGAGGGGGTCGAATCGCTCCGATCCTGCCAACAGAAACACTCTCGCCTGTTTGGGATACACCATGGCGGAGGAATGCATTTATGAAACCTAAAAAAAAAATTCTCTTGGTCGCAGGTTGGGTTTACTGGCCGCAGGTTGTTAAATAATTCCGGGAAGCGCATTATGCTAATTCCAATTGGCCGCAGCCCATAAGATATTGCAGATGCAAGTCCCGTTACTTATCTTT
# Right flank :  CTATAAACTATTTCAGATCCGGATATGAAATTTCAATTTTATTTTTTTTTATTTTTCCTGACCAGCCTGCTTCCGGCCCAGCACCTCCCGGGATTCAAGCCATCCGGGTCATTTGATGAACAGCAACTGGTAATCGAAAACTCACCTCCGGGAACGCGGATACTTATCAATGCCCCGCTGACGGGCTTTGGTCGTGATGACCGGGTTCTGCTGGTTTTCTATGCCCTGCCCAACGGCAACACCATTGAGCAGACCTTTGGGAAGAGGCTGCAGGAGGGCGACGACTGGCACTATGATATACAGCATATTGGCGCTCAGACGAGATTCATCAGGAAGGTCATCACTGACCGGACGGTTGTTGTGGCATACCTTGAGAACAGCCTTAAAAGCTGGCCGGCATGGAACGCCGCAACACCTGATTACCGGCATCAGGTAAAGGGAATGGTGGAGAAGATCACCTCGGTCTTCGCTTCCTGGCAGCCCGAATTGGTCCTTAACGG

# Questionable array : YES	 Score: 0.45
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.79, 5:0, 6:-1, 7:0.46, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGGTCGCAGACCGTCAGTTATGGCAGTTGCGGATGGCGGTGACTGACAATGGTTGCAAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [13,14] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-19.00,-9.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-5] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [56.7-80.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_581393 flag=1 multi=3.0000 len=1330		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence                    	Spacer_Sequence                         	Insertion/Deletion
==========	======	======	======	===================================	========================================	==================
       314	    35	 100.0	    40	...................................	CTCGGTGGTGCGCGGTGAATCTTCACGACGAGGATCGGCG	
       389	    35	 100.0	     0	...................................	|                                       	
==========	======	======	======	===================================	========================================	==================
         2	    35	 100.0	    41	CTGCGCGGTGCTTCTATCGGCTGCTGGGTGCGTGG	                                        	       

# Left flank :   ATTTGATCAGGCCAAAGGTAAATCAATGTCCTTGGAATTCGCAAGCCCTTCAGCAAAAATTATTTTAACAATGTACTGAATGATTATGGGAAAGTGAGGAAATTACCAAAGCAACTCCCTCACTTTCAACTCATTTCGCTATTCTTCGCCCCCCTTCGATTTGCTCCGCTCCCGAGGTGCCGGAGTGGTTGTTTTGGCAGGCGTTGTATCCTTACGCGGTTCGGCGCTGCGGCTCGGCGAGGTTTCCTTGCGAGGCTCTACGCTGCGAGGCGCTTCCGTGCGGGTGGGTGCGGTTTCCCGGCGAGGTTCAACAC
# Right flank :  GTGCAATTTCCCGGCGCGGATCGTTTGATCTTGGTTCGCCCGGGCGATTTTCGGCGGGAGTCGGATCGGGCAGTCGTTCTGTCAGACGTGGATATTTGGTCGTATTCTGAACGGCATATTCCCTTTGCGGAATCTCTCTGCCCGGATTGTCGCGATTGAAACGCTCGCGGTCATCCTCCATTCTGCCGAACTCGCGGAAGCGGTCTATTCTGCCGTTTCGGTCACGGAGCCATACATCACCTACCACAGCATTGTTGCGGCGGCGCCAGCGATCTACACCGATAATGATACCGCAGGAAGAACGTCGGTAGCCGTAGTAATAGTCCACATACATGCCGGCTAAGTACGGGTAGTTGTAGATGTGAATGGGACGACGGTAGTACCAGTCGACGAAGTAGTAAGACGGAAAGTCGAACACGACGACGCCGCCGCCGGGGCGAATGTAGAACCCCCAGTCGTACCAGTACGGGCGGGGCCTCCAGCGGGGAGCAGAGTACCAG

# Questionable array : NO	 Score: 2.50
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.39, 7:0.22, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTGCGCGGTGCTTCTATCGGCTGCTGGGTGCGTGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [1,10] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-4.80,0.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [31.7-40.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_589207 flag=1 multi=3.0000 len=1105		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                                	Insertion/Deletion
==========	======	======	======	==============================	===============================================	==================
       105	    30	 100.0	    47	..............................	ATTGTATCCGTCTACATGTAGAAGGCGACATTGTAGAGTGATACAAT	
       182	    30	 100.0	     0	..............................	|                                              	
==========	======	======	======	==============================	===============================================	==================
         2	    30	 100.0	    57	ATGTCGCCTTCTACATGTAGAAGGCGACAT	                                               	       

# Left flank :   TAATATTATATAGGCCTATATGAACATTAGCCGCATATGATGATATGGCTAACATGACGTGACATAAACGTGACATAGACTTAAGGCCCGTACACACTGTCTACA
# Right flank :  TTGTAGAGGGCGACATATAGAAGACGACTTTGTATATGGACGATATTGGCGACAAATTGTAGCGTTCTACATGTAGACGCTATTGTCGAATTGTAGATACAAGGAAATACAAGTCGATCGTATTTGTTAATAATATTCATGATGTACCACGGGATAAATCACGTGCATATTTTGCATTTAGTAAGCCTACGCGTGCCTACATAGGCTTCCAGAAGTTAGTGGCTATAGGCTAATGAGACAATATGGCGGACGAAGAGGATGTGATAGATTTGTCAGCGGCTATTGTCGCTTTAGTGGCAAATAGGCGCAGAAAGCGGAAACGGGAACGGTCGTGTTGGGTGCGTTCGTGGATCAGCAGCAGGGGACAACTGGGTGGATATGAAGCCCTATTCAAGGACATCCATGGCCTGCCTTGCTCGCGCGCAGTGCTAATGAGGCTGTGCCGTAATTTGTAGGCTGTTAGGCTGGTAGCTGGCAGGATTTTTATGAATATTAATGAA

# Questionable array : NO	 Score: 2.91
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.99, 7:0.03, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATGTCGCCTTCTACATGTAGAAGGCGACAT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:53.33%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-10.50,-10.50] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [55.0-60.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_589299 flag=1 multi=11.0000 len=1186		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                  	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	=================================================================	=====================================	==================
       358	    65	  95.4	    37	.......................C.....A..........................G........	GAGCGAAGCCGCCGGCAGCGGCACCTGTGAAGCCAGA	
       460	    65	 100.0	     0	.................................................................	|                                    	
==========	======	======	======	=================================================================	=====================================	==================
         2	    65	  97.7	    38	GCCGTGGACAAGAACGGCAAGGCTCTGGCGGGTGCTGCCAAGACCAGCTTCATGAAAAAGTGCGA	                                     	       

# Left flank :   GGTGGCCTTCACGCCGGATTCACTGCGCCAGCTGTCGCTGTCCACGCGCCACCGCTTCGACGAAGCCTTCATCCGCGTGCTGGTGCGCCGGCTGCACGCGGCGCACGAGGCGCTGGCCCACCCGAGAAGAATTCTTTAGGCCGGGCGGCTGGCGGCTGCCCACCCTGGCGTGTTCACTGATTGCCATGGCACGCAGCCTCTGATGCAATCGCCCACCCGCAACACGCCGTCCACCCCGAGACAAGGAAGCTCCCCATGATCAAGACCACGACCCTGCTCACCGCCGGCCTGATGTCGCTGGCCCTGCTGGTCGGCCAAGCCCATGCCGCCAGCGCCGCCGCCGCCTGCGAGGCCAAGG
# Right flank :  AAGCCGACAGCGCTGCGGCGAAGAAGTAAATCGCCGCCCCACGCCTGCACCACCCCTGCACCACTGTTACACCAGTGCAGCACCACTGCAGCGACACGGGGCGCCCAGGATGCCCCGCCTGTCACTCAAGCCGCCGCCTGCTGCGCCCAGTAGCGCGGGTCCCCGTAGCTGTGCTTGAGGAAGTCAATCCACAGCCGCACCCGCAGCGGCAGGTGCTTGGCATGCGGAAACACGGCAAAGATGCCGTTGGCAGGCGCGGCAAAGTCCTCCAGCACCACCTGCAGGTCGCCACGCGCGACCTCGTGCTCGACCTCCCAGGTACTGCGCCACGCCAAGCCCAGGCCGCGCACGCACCAGTCGTGCAACACCTGACCGTCGCTGCAGTCGAATCTTCCGCTGGGGCGCAGGTGGGTCACTTCGCCGCCGACCACAAAGGCCCAGCCGCGGCTCTGGCTGGCGTCCGAGCTCAGCGTCAGGCACTGGTGGCGGCCCAGGTCGGC

# Questionable array : YES	 Score: 0.87
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:-1, 7:0.78, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCCGTGGACAAGAACGGCAAGGCTCTGGCGGGTGCTGCCAAGACCAGCTTCATGAAAAAGTGCGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [18,9] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-13.50,-13.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [3-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [21.7-31.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0.68   Confidence: LOW] 

# Array family : NA
//

>k141_593298 flag=1 multi=6.0000 len=3088		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                                                        	Insertion/Deletion
==========	======	======	======	=====================================	=======================================================================	==================
       327	    36	  83.8	    44	....C.GAG-..A........................	TTAGGGGCAATCTCGATCTATTTTATTTTGGGATAGCGATCGCT                           	CC [306]
       245	    37	  86.5	    71	TC.C.C...........T...................	CGTTGAAAACAGATAACTCCTGATATTCACAACAGAAACCCGGTTTCTCTACCTCAAAGACAAACCCCAAG	
       137	    37	 100.0	    58	.....................................	CGCGGAAAACAGATAACTCCTGATATTCACAACAGAAACCCGGTTTCTCTACCCCCAA             	
        42	    37	 100.0	     0	.....................................	|                                                                      	
==========	======	======	======	=====================================	=======================================================================	==================
         4	    37	  92.6	    58	GACAAACCCCAGGAACCCAGAAACCGGGTTTCTTTTT	                                                                       	       

# Left flank :   GAGAAAGTACCCTGATCCGCGATCGACGCCAAAATCGAGACTGCGATCGATGTCCGTCGGTGGCGATCTTGACCAACCTGGGGATTTTGCATTGTCTAATCGATCGGTTTTCAGGGGTTTGATGGTCTGGTCAGGAACAAATTGAAAAAAGAATCTATTCAGAAACCCGGTTTCTGGCCAAAATTTGGGTGAGGATGCAGCAATTGTTTCAAAAACCCGGTTTTTGCCGCTTTAAGGTCATCTCCTAGGTGTGAGAAACCGGGTTTCTTTGTCGCGATAAGCAGATGCACTTTGATCTCCTGAAAAGAAACCCGGTTTCTACACACAAATTAAGGAATCATATCGCCCTATTTTATGCCTGAATTCTTAACTAAAATTTTTCCCAAAAATACCTTGACAATTACCCAAAAGTGGATATATAATAAAATCATCGTAAGACTGTATAATGGAAAAAGTGTGCGCCTATATATAAAGGGCGAATTTTGAATTTTTACCACAGA
# Right flank :  TCGCAC

# Questionable array : YES	 Score: 1.41
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.63, 5:0, 6:1, 7:-1.62, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GACAAACCCCAGGAACCCAGAAACCGGGTTTCTTTTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.35%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-8.30,-9.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-13] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [1.7-66.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_595890 flag=1 multi=5.0000 len=1066		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                                    	Insertion/Deletion
==========	======	======	======	=====================================	===================================================	==================
        48	    37	  91.9	    37	.................T..A...............G	CCGAGAACTTGACAACATGACAACGCAACAACCCGAC              	
       122	    37	  89.2	    37	............A.T.....A.........C......	CTGAGAACTTGACAACATGACAACGCAACAACGCAAC              	
       196	    37	  94.6	    51	..............................C...T..	ATGATAACGTGAGAACCAGACAATTCGAAGCTAACAACCCGACAACCCGAT	
       284	    37	 100.0	     0	.....................................	|                                                  	
==========	======	======	======	=====================================	===================================================	==================
         4	    37	  93.9	    42	AACCTACCATTCGGCAGGCAGGCGCGAAGCTGACAAC	                                                   	       

# Left flank :   AGCCGACAACTTGACAACATTACAACGCAACAACCCGAGAACCCGACA
# Right flank :  CCTTGAAACTACTAACCCTCTCTGGCACGCCCTCAATCGATAAGCGGTCTGGAATAGAAATTGTGTCCCGCCATTTGATATTTCTATATATTTGATCCATTGAGTTCAATAAAACCCAAATGAAACCGTACTTCTTTTTTATCGGCTTGGTCATCTTTGCTACTTCCTGCATCAGCAACAAGCGAATTACATACTTGCAAAATCTTTCAGATAGTAGTGCTATTGATTTGGATGAATTTATTCCGTTTGCAGAGGTAGACTACGAGTACATCTTGCAACCTTTTGATATTGTACAGATTGATTTTGCGAGTTCGGACCCTGAGTTGGTGGAGGGTTTTTCCTTTCAGCGGTCACAACAGGCGGGTGTTGGCATGGTTGGTGGCGGTGGAGGTCAGGGAAGTGATCCGCTCTACATGTCCGGATTCTCCATTGATAAAGAGGGCATGGTAGAACTACCCAAACTTGGGAAGATCAAAATTGGTGGACTATCAGAAGAGGAT

# Questionable array : NO	 Score: 1.92
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.70, 5:-1.5, 6:1, 7:0.63, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AACCTACCATTCGGCAGGCAGGCGCGAAGCTGACAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-5.30,-4.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [7-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [38.3-53.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0.68   Confidence: LOW] 

# Array family : NA
//

>k141_603595 flag=1 multi=4.0000 len=1375		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                     	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	====================================	==================================	==================
       174	    36	  97.2	    34	.........................T..........	GTGATTAGGTTGCGTGGGTGTATGCCTTGTAAGT	
       104	    36	 100.0	    32	....................................	GTGATTAGGTTGCGTGGGTATGCCCTGACAGA  	
        36	    36	  97.2	     0	...........T........................	|                                 	
==========	======	======	======	====================================	==================================	==================
         3	    36	  98.1	    34	GGCTTTGGCTGCGCCGTTTTTGGTTCTTGGTTTTTG	                                  	       

# Left flank :   TTTCATGGTGTATCGGTGTTGGAGGCCTTTATTGATCCGGAACCACTCGAGCGCTGGTCGGCGAAATTTTTTTTTAAATCTGTCTTGTATTCATCGCCAGCCAGATCAATGATTTTGTTTAGAATTTCTACTTCGAGCTGTTTACGTCCAAGTGCTGCTTCAAGATCTATTATCTTCTTCTCCAGATCTTTACTTCGGTACGCTTCGCTTTTTTCTTCCACAACCAGTATTTTGTTACTACGCAAATATCGGCTATACTGATAAATCCAATTGTAAATCGTTTGGTTACTCACCGAAAGTTCTCGATGAGCTTGAGAGACCGAGCATTTACCACTTTCGATATCTGCTACGGTCTGTTTTTTAATCTGATCGCTGTAAATAATACGCTGTCGTATTCCTGTTTTCTTTGCCATTTGGGTGCTGATTTTTAGATATATTTTGACTTGGTGAAAAAGTCAACCTATTTCAGTCACGGACAATGGTTAATGTGCCCTGACAGC
# Right flank :  G

# Questionable array : YES [Potential tandem repeat]	 Score: 1.36
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.90, 5:0, 6:1, 7:1.46, 8:-3, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCTTTGGCTGCGCCGTTTTTGGTTCTTGGTTTTTG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.28%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.40,-3.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [2-2] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_609122 flag=1 multi=3.0000 len=1046		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence         	Spacer_Sequence                                                   	Insertion/Deletion
==========	======	======	======	========================	==================================================================	==================
       994	    24	 100.0	    66	........................	GCTAAGCTGTTGGTTGGAAGCAACCCATATCTCAAAGCCCTGCCCACCCTACTAACTTGCCTAGGA	
       904	    24	 100.0	    58	........................	GCTAAGCTGTTTGCTAGGAGCAACTAATCGATTAGTTGCTCTGCCCACCCTACTAACT        	
       822	    24	  91.7	    21	.G....G.................	CTACTGGGCTGTTTGCTAGGA                                             	G [817] Deletion [778]
       776	    24	 100.0	     0	........................	|                                                                 	
==========	======	======	======	========================	==================================================================	==================
         4	    24	  97.9	    49	GAATAACCTGTAGGGTGGGCAGGG	                                                                  	       

# Left flank :   TAACTAGATAGAGCTGTAGGGTGGGCTTTCTCTACTGAGCTGTTGCTTGGG
# Right flank :  CTACTGAGCTGTGGGCTAAGAGCAACTCATATCTAAAAGCCCTGCCCACCCTACTAACTCATCAGCAAAGCACGTGAAATTCAGGATAATAGCGATCCTTTATACTTGCTGACTATCTAGAACTGAATTGCATGGTAGTTGTTCGCATTACTCACTCCTGTTCCACGCGCAGGAATAAAGTGCTTTTTCACGTACCCAGGGTTTCCCCAATTCTGATCCCAGATATAGAATCCTTTGACCCCGTTTTCAGTGCCATAGCCAGCAAAAATTCCTGTATGTTGGACATATTGTCCATTAACAAGATTGTCATAGCTACCATTTGGACCAAGAAATGTTGCAATAGCAGTGCCAATTGGAATATTGCCATTAGACATGATATTTGCACCACGTTTCCAATTTCTTGTTGAGCCTGTCTGCCCTGTCACATAAGCAACCAGATCCCAGCATTGAGCGCCATAATAGCCGTCCACATCAAAATACTTGCCACTTAATTGACCATT

# Questionable array : YES	 Score: 1.00
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.90, 5:0, 6:0.25, 7:-1.75, 8:0.6, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAATAACCTGTAGGGTGGGCAGGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.90,-1.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [4-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [50.0-45.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.15   Confidence: HIGH] 

# Array family : NA
//

>k141_610959 flag=0 multi=11.0000 len=1327		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                        	Spacer_Sequence                 	Insertion/Deletion
==========	======	======	======	=======================================================	================================	==================
      1029	    55	 100.0	    24	.......................................................	CGCGATAAGCCAGGGGTTTCTGCG        	
       950	    55	  96.4	    32	........G.........G....................................	CTCTCCCAGAGCCCAGAAACCGGGTTTCTCCC	
       863	    55	  96.4	     0	.................................................T.G...	|                               	
==========	======	======	======	=======================================================	================================	==================
         3	    55	  97.6	    28	ATAACCTTCGCATCCTCACCGAGGTATTTGGGAAGAAACCCGGTTTCTTAAACCC	                                	       

# Left flank :   TTTTGACGGCTTATCAAGTCCCTGCCATCAAACCACCAAATCCAGAACCAGAACCAAAACCAGAACCAGAACCAGAACCAGGAGAACCCATACCTGTACAATCAGTCCCAGAACCAGCTTCTACGGTAAGTTTATTAGGATTTGGGATTTGGGCTGGTATTTCAAAATTGAAACGTCAACGGGAATTGTAGGGTGGGCATTGCCCCACACCCGCAGCTCAACCCAGAATTTTCATATTTTGGCAATGCCCACCCTGCGAGCTAGCCAGAACCCGAATGTCAGAAACCGGTTTTCTCC
# Right flank :  GGTTGATATTCGGGGGTCGATCGCTTTTTTTCCCAATTTACTATACAATAAATAAATCCAGACAATAACCCACAACCCGTGACCAATTTTAGTAATATAACTCCTGGTGGAGGTCTTACTTCCCATGAATCAGCAGGAGGACATACATTAGATCGGCATGTTGGTTTGACAGAAATCGAACTGGCTAAAAGACTAGCAACTCAGATAAATCTCCCTCGGGCATCATCGTTTGCTTGCTAATCGAGAAATTGCCGAACAAGCCATCTCAGAGGCGATCGATGCTAACCAAACCCAAATTACCAACTGGCTTACTGGTAGTTCCGCCCGGTGCCAAATCAGCTATACTGCGATTAGCCCCATCGGCATTACTCTGGCTCGTGGAGCCACCAAAACCGTAGCCGCTCAAAAAATACAAATCGTTTTGAAACGAGATGCAAGCGTCCCATTAAGATACTATATTCTAACAGCATACCCAGAACCATGACCCAACAAAAATTTCC

# Questionable array : NO	 Score: 1.89
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.88, 5:0, 6:-1, 7:0.61, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATAACCTTCGCATCCTCACCGAGGTATTTGGGAAGAAACCCGGTTTCTTAAACCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:52.73%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-8.20,-9.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [2-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [65.0-43.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.78   Confidence: MEDIUM] 

# Array family : NA
//

>k141_614925 flag=3 multi=17.0000 len=10363		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	============================	===================================	==================
       217	    28	 100.0	    35	............................	TCGGCTCGCACTAAACAAACTAATTAAAACTAATG	
       280	    28	  96.4	     0	...T........................	|                                  	
==========	======	======	======	============================	===================================	==================
         2	    28	  98.2	    36	TTTGTTAGCCAACAGGGGTAATTATTCA	                                   	       

# Left flank :   AAAAATCCTTCATCATCAAAGTTGGGCTCTAAGTCCAACTCCCGTGCATAGAGCGCACCGTTACAGATTGCATCAAGGAAGCACTCTCCCTCTGGGGTGCTGAACTGCCAACCGCCCACTCTACCGCAAGGGTATTCATAGGTGCAGAAGTTATCAGCATCGCCTACGTAGTGAAAGCCTATATCTATCAGGGCTTGGGCTTGTTCGAACTGGTCTT
# Right flank :  AAGGGTGCGAACCCCCATCACCACCGCCAAGCCTAAAAGATTTTCAGGGATAGAAGGATCAGGGATGAGGGTTGTTGCAGGGATTGGGGGTATCGCAGCGCCTCCTATTGCATTATTGGTCTATTCCACTAAAAAATGGTCTGAATATGTACAGTATATGTTGGGGCAATATGAAGCAACAAAAGAGCAGCTTCGAGAAAATCCCAATAGTCCTCAAAATAGAGAAGCCATGCTCAAGGCTGGTAGAGCTTATTACTTCTGCATGAGAGAGCAAGGAGTCCCAACTATTTATGACGAGCAAGCAATTAACAATGATATGACAGCGATACTTGGTAAATGAAGTTAGCTTATTAGTTACAGTTGTCGCCACGTTCCTCATAAGGTGGGTTAATCGCTTAACAGTTTTTTATCCTACAGGACAATGGCCCTTCCATACCCATAAAGCAATTGTTTTATCTACATAACCTCTGGATATATGCAATTATTTCGTCTTTGGAGGG

# Questionable array : NO	 Score: 2.81
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:1, 7:0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTTGTTAGCCAACAGGGGTAATTATTCA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:64.29%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-1.90,-0.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [50.0-48.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0   Confidence: HIGH] 

# Array family : NA
//

>k141_57483 flag=1 multi=3.0000 len=1401		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                             	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	============================================	=====================================	==================
       831	    44	  93.2	    37	.....C..........................A...G.......	AGGAGCTACCTTTTTCACTAACTTTTTAGGAGCCGGG	
       750	    44	 100.0	    22	............................................	TACTGCTTTTTTGGGAGCTGGA               	
       684	    44	  97.7	    28	...............A............................	TACTGCTTTTTTAGGTGCTGCTTTTTTT         	GAC [651]
       609	    43	  84.1	     1	....T...............C..-....GC.....GG.......	T                                    	A [577]
       564	    44	  79.5	     0	A.......C.....A..TG..........T..A..GG.......	|                                    	
==========	======	======	======	============================================	=====================================	==================
         5	    44	  90.9	    22	GCAGGTTTTTTAGCTGCAACTTTCTTTTTAGGGGCTACTTTTTT	                                     	       

# Left flank :   GATCCTAGTCCTGCTGGCAATGAATTCACATATAAAAATTCTTGATCTAACTTATTTACCAATAGCCAAATTTTACCACTAGCATTATCCCACCAATATGGAAAGAATCCTTCGTAGCGTTTCATGTTTTTGGTTTTTTCGGCAATTTTTGAATTGGTTTGAGCTGAAACTGTAAAGGCAAAAAAACAGCATAGAGCAATTAAAAGAATACGCATAATTGAAAAGTGAATAGTGTGATATGAGAACTACTTAAAGGTATTTTAAAATTTATACAATAAAAAACCCTGTCTTTATTAAGTAATAAGAACAGGGTTTTAAAAGTAAAAATTATTTATTAGTTAGAAGCTTCAGAGCTTGGTGTTTCAGGTGTAAATAGTGTTGGAGCTTCAACAGGAGCAGGAGTTGGAACAACTACAGGTGCTGGAGTAGGTTTTTTAACTGTTGTTTTTTTCTTAACAACAGGCTTTTTCTTTACTACTGGTTTTGGAACAACAACTGGA
# Right flank :  TGGAGCGGCTTTTTTCGCTGCTTTCTTTTTACTTGCTTTTGCCATGATGATATTAGTTTGTATGTTAGTTTTGAAAAAAAAGTAAGTTAGAATATTATTTTTAATATTTGGTATTTATCTATATATTTTATTTTTGAATTTTTAAATACTATTTCTTTTTTGATTGGGTGTATCCATTTTTAATTAACCATTGTAAAACTTTATCGCGCTGATCGCCTTGTACAATTGCTTCTCCATCTTTTGCACTTCCACCAGTTCCGCAAAAGTTTCTCAAATTTTTAGCCAAAGTATTCAAGTCAGATTCAGTTCCTTTAAAACCAGTTATTAGTGTTACTGTTTTTCCGCCGCGTTGTTTTTTATCTATCTGAACAAATAATCTTTGTTGATTTATGGGGGCTGTTTCAATTTGGCTTTCGTTTTCGGATTCAAATTTAAAATTTGGATCTGTACTGTAAACAAAACCTTGTTGGTCGACTTTATTTTTTTACTCATATTTAAGT

# Questionable array : NO	 Score: 2.13
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.69, 5:0, 6:0.25, 7:0.10, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCAGGTTTTTTAGCTGCAACTTTCTTTTTAGGGGCTACTTTTTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:63.64%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.10,-4.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [12-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [58.3-66.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_64250 flag=1 multi=14.9907 len=1749		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence          	Spacer_Sequence                                            	Insertion/Deletion
==========	======	======	======	=========================	===========================================================	==================
        45	    25	 100.0	    59	.........................	TATCGCGCATTGAAAAATAGGGATTCCCACTTGCGTGGGAATGACTCCAATTGAGTTAG	
       129	    25	 100.0	     0	.........................	|                                                          	
==========	======	======	======	=========================	===========================================================	==================
         2	    25	 100.0	    69	CGTCATTCCCGCGTAAGCGGGAATC	                                                           	       

# Left flank :   CGCGGCCGTGCCACTGCCATTCAGTGAACGTCAATTTGAGTGAAC
# Right flank :  CCACAATCACTTGAAAAACATGGATTCCCACTTTCGTGGGAATGACACCTATTGCTTAACTTAATAGCATTGGACCACCGGAAGTATAAATTGGCAGTAACGCAACTTTGTTGTGTCGGCGGGAGTAAAATGATTACTTGCCTTCAACCGTTAAAGTGACCGTCTTTTGGAAGTTGCTGACCAATGTTCCATCAAGTAAACGATAGCCAATCCAAAATTGGTAAGTGCCCGGTCTGTTTATTTCGCCTTCATAAATAGGGATTTCGATCAAACGAGGTAGTTTGGGTCCCGTTTTACCAGTAAAATCCATATAGACCGGCAACGTTTCCAAACTGCCATCCCATCCTCTGATTCCAAAAGCACTTTGTGGATCATCATAACCACTAAAAGCGATATATCTCGCTGGTTCACCTTCTGGAGCCGCATAGGCAACCACGAGAACTTCTCCTTTTTGACCAATATGATCTGGCGCAGGATAGATTTCTGCGGCAACGTCTACA

# Questionable array : YES	 Score: 1.13
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.34, 7:-1.1, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CGTCATTCCCGCGTAAGCGGGAATC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [5,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-6.60,-5.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [33.3-60.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0.27   Confidence: LOW] 

# Array family : NA
//

