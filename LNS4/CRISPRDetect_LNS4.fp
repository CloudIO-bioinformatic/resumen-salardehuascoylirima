>k141_5752 flag=0 multi=514.0147 len=1027		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                             	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	============================================	==================================	==================
       879	    44	 100.0	    31	............................................	GAACAGTGAGGGCGGTGGGATTCTCAACATG   	
       804	    44	 100.0	    28	............................................	GAACGGGGGCGGTGGGGTTTACAACTAC      	
       732	    44	  81.8	    34	.....T........CATCAAC.......................	AGACGGTTACGGCGGTGGGATTTACAACTACTCC	
       654	    44	  97.7	     0	....................C.......................	|                                 	
==========	======	======	======	============================================	==================================	==================
         4	    44	  94.9	    31	GGCACCCTGACCCTTGAGGGTTCCACCGTGAGCGGAAACAGCGC	                                  	       

# Left flank :   CCTCAAGGGGGCCGGAGCGGACAAGACGATTCTGGACGGAAACGGCACCGACCGGGTCTTGGAAATTTACGGAGGAAGTATAACGAAGGTAGAGGGGGTCACGATCCGGAACGGTAAGGCCGACTACGGCGGTGGGATTTACAACTC
# Right flank :  AAAAAGGGGTGGTGGGGTTTACAGTGGCATTTACCTTGTAGATTCGACAGCTAACCTTTCTTTTTCTACCATCACCAACAACCAGGCCACCTGGGAGGGTGGGGGCATTCAGCAGACCGGCTGGGGAACGGTCAAGCTCAGGGGGGTCATCCTAGCAAACAACACCGTCAACTCAGGCGGCGCCGGCCCGGACTGCAGCGGCAGCCTGACCAGCCAAGGGTATAACCTCATTAGGAGTAACAGCGATTGCGACTTTACCGTCCAAACCACTGACATCATCGGCCAGGACCCAGCCCTTGGCGACCTGAGGGACAACGGTGGCCCCACCCAGACCCACCTGCCCGCGGCAGGTAGCCCGGTGGTGGACAAGGTGCCCCCCGAAGACTGCACCGACCTGGGCGGCAACCGGCTCGCCACCGACCAGCGCGGGGTGAACCGGCCCCAGGGTGCGGCCTGTGACATCGGTTCAGTAGAGCGGTAGGGAGGCCCGAAGGGAAGCC

# Questionable array : NO	 Score: 2.41
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.75, 5:0, 6:0.25, 7:0.01, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCACCCTGACCCTTGAGGGTTCCACCGTGAGCGGAAACAGCGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,9] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-10.70,-8.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [9-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [55.0-40.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.64,0.78   Confidence: LOW] 

# Array family : NA
//

>k141_6022 flag=1 multi=11.6255 len=9249		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	===============================	=====================================	==================
      1032	    31	 100.0	    37	...............................	ATGGTGCTGGGAAAAAGGAAACATGTTTTTGGTTATA	
       964	    31	 100.0	     0	...............................	|                                    	
==========	======	======	======	===============================	=====================================	==================
         2	    31	 100.0	    38	GGTCCTAATCGAACCATGGTGGGATTGAAAG	                                     	       

# Left flank :   GCTTCAATCTCACTGCTTCCTTTATTTGCCCTCTGCTCCAGAAATATATAGGTGGATTTTCGACCCCTTTTCTGAGACGGGCATGGCTCAGATATTTATTTATGATAGCTATCCTGGGGGTGTGGGGATTTCAGAGAGGGGATATGAGGTGATAGAAAGGCTATGGGAGGCGACGCTTAAGCTTATTTCAGAATGCCCTTGTGAGGAGGGGTGTCCTGGCTGTGTTTTTTCGCCGAAATGTGGAAATAACAACCAGCCAATGGACAAGAGGGCAGCGCTTATGATTGGAGAGCATCTTATCAGGGGCTCAGGTTAATTTTTTGAGCGATTTTTCTTCATATTGCCGTCAATCCCCCGATTTTGTCCTTTTGTTACGTTTTTCTCAAATTTTTTGTAATTTGGTCAAATTGCCGTCGATCAACATTTGACTTGACGGCAACCACACATCGACGGCATAATCTTTAAAAGCTCGAACCTTAACAACCAAATACAAAACAGGG
# Right flank :  GATTAAACAATAGTGAGGTTGGTTAGGGGGACCGGGTGACCGCCCCTAAAAGGGGAGGAAAGTCCGGACCCCACCGGGCAGGCTGCCGGGTAACACCCGGGCGGGGAAACCCGACGGAAAGTGCCACAGAAACTATACCGCCAGCGGCCGAAAACGGTGCGGGCAAGGGTGAAAACGGACGGTAAGAGCGTCCGCGATGATGCAGTAATGCATCATCGGGGCAAACCCCAGCCGGGGCAAGACCAAACAGGGGACAATGGGCTGCCCGGACCAAGTCCCCGGGTAGGTCGCTTGAGGTGGCAGGTAACTGCCATCCCAGAGAAATGGTCACCACCCCTTATGGGGAACAGAATCCGGCTTATAGGTCCCCCTAACCACCCCAGCTTAAACCGATTTAAACACCCCTTGAAAAGTTTTCTGTCAAATAATATATTTCGGCTACCAGAAATGAAACCCTTCTGTTTCCTTGACCTTCGGACTTTACCCCGAATTTGGTGACA

# Questionable array : NO	 Score: 5.15
# 	Score Detail : 1:0, 2:0, 3:3, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTCCTAATCGAACCATGGTGGGATTGAAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         R Score: 4.5/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.61%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.80,-1.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [46.7-60.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,5.14   Confidence: HIGH] 

# Array family : NA
//

>k141_7179 flag=0 multi=63.7219 len=1957		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                           	Spacer_Sequence                                       	Insertion/Deletion
==========	======	======	======	==========================================	======================================================	==================
       470	    42	 100.0	    54	..........................................	CCACCAAGTGGGTGGTGGCCACCAGGTGGGTGGTGGCCAGCATAAGTAAGTTCA	
       374	    42	  92.9	     0	............A.......A..A..................	|                                                     	
==========	======	======	======	==========================================	======================================================	==================
         2	    42	  96.5	    55	GGCGGTTCTGGAGAGAAAAAGCCGGGGTGGGGAAGGTGGTGG	                                                      	       

# Left flank :   TTCAATCCTGAAGTGGTTATCGCCGCTGATAAGAACCGTATCAATCTGACGCTATGTCAGATACATTGGCAGAACGACCGGCTTGAAAAACGCGGTCTGGCGTTGCCGTGGAAGATTTGGTACGCCAAAGGACTGAAGTATGGACGCCTGTATCGTCGCAGTACATACCTGAAGAAGAAGGCTGGAAATCACGATTTGTCAGCTGATTTAACAGGGCCAGAGTAGGACAAAAATGTCCCTGCAGGTAGGACAAAAATGTCCTACACGTAGGACAAAAATGTCCTACCAAAAAAAAGGCTCAAAGCCGCATGAATACTGGGTTTTTGGCTGGTAAGGCACTAGTGTTTCTTCTCACTCTTATATATAATGTACCGTTCCTGCGAAAGCCGGAAACCCGCATGAATACTGGCTTTGCCGAAACAAGCTCAAGCGATTCTGGAAGGGAAAAAAACGCCGGAAACCCGCATGAATACTGGCTTCGATGGCAGTTTTAGTTTGAT
# Right flank :  GTCTTGTGCTTGAACCTGAGCTTGTGCGTCGTGCCCGCACAGCTGACCTTGTTTCGTTCCTGCGGGCGAAAGGATACCGGCTTAAGCACGAAGGTTCGGGAAACTACCGCGTGGTTGGCCACCAAGGTTTGATAGTGCAGGGTTCGCGGTGGTACTGGCATTCCGTCGATGAAGGCGGCAACGCGGTAGAGTTCCTGATGCGGGTGCTCGGGTACCCGTTCCGGGACGCGGTGCTGGAGCTGACCGGGAGCGTGCCGTGCGTGGAGAGTTCCCGCACGGACACGGAGCAGGTTGTTGCAAGGGATTTCGCATTGCCGGAAGCCGCGCCGGATT

# Questionable array : YES	 Score: 0.67
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.82, 5:0, 6:0.25, 7:-0.6, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCGGTTCTGGAGAGAAAAAGCCGGGGTGGGGAAGGTGGTGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [6,9] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-0.20,-3.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [3-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [45.0-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,1.42   Confidence: HIGH] 

# Array family : NA
//

>k141_8982 flag=0 multi=25.6881 len=1058		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                                 	Insertion/Deletion
==========	======	======	======	===========================	================================================	==================
       386	    27	 100.0	    48	...........................	CTCTTGCTCATTTCGCCCATCTGCCTACTGAACTCGACAGCCAAGGCG	
       311	    27	  96.3	     0	..T........................	|                                               	
==========	======	======	======	===========================	================================================	==================
         2	    27	  98.2	    49	TCGATCCTCTTGTTCGTCTCGTCGATC	                                                	       

# Left flank :   CAGATTGCTCCAAGACCACCTCGCGCGTTGGGGGCGCCGCCAAGTCCGGGAGGGCGTATGAGTCGCCTCGTCTCGGCGCCGCGGCGCTGGACGTGGCGGGATACAGCTTGGGCAGAAGGAGAGCGAAGACCTTGGTGTTGGGCTCAAGACGCCTCTATGCGGAGCCGCCGGAGGATTCGATAATATCCGGCCTAAACGCGTGCGCGTATTGGGACTCGCCGTACGACTGCGAGAGGGCCGCGGCCGTCATGGTGGCGCAGTGGAGCCGACTCGATTTGGGACATCTGAGGGCGAGGGCCGCCGAGGGATTCCTCGATGCTGCTCACCCTCTCAATACCCTTCGGCAGCCTTAGGGCGTCGACGCCCCGGTAACTTGAGGCCGTCCCACACCTCGGCCCCAGCGTCCCTACTTCGCCGATAGTTGCCTAGAAACGCCCAGAAGGACTTGGTATATGTTGGACAAGAGTTTGTACAAGTCATCTATGCGCTCGTTCGTCTCA
# Right flank :  CTCCGCCCTCATCTGCGCGATTTCCTCCCTTATCGCTCTGTCCCCTTCGGCCATCTCCGCCCTAAGCTGCGCCATCTCCCCCCTCATTGATTTGTCGCGGTCCGCCGCCTCGGCCCTCATGGCCCTGAGCTCCTCGACGATGGTGCCTAGGTATAGGAGGAGGAGATCCTCGACGCTCAGCTTCTTACCCTCCCTAAGCTTGTCGACGACCGCCTTAACCGCCTCGCTCAACGCCGCGGAGACCACGGGCTCCACGGCAAAGTCTCCGAGGAGATATAAATACTT

# Questionable array : YES	 Score: 1.37
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCGATCCTCTTGTTCGTCTCGTCGATC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [36.7-56.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.68   Confidence: LOW] 

# Array family : NA
//

>k141_9673 flag=1 multi=89.0000 len=1117		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                 	Insertion/Deletion
==========	======	======	======	=============================	================================	==================
        45	    29	 100.0	    32	.............................	CAGCGAAGGCCGCGCGCATCTTGCCCCACACA	
       106	    29	  96.6	     0	............................G	|                               	
==========	======	======	======	=============================	================================	==================
         2	    29	  98.3	    33	GTAGTCCCCACGCACGTGGGGATGGACCA	                                	       

# Left flank :   GGGATGGACCTACCGCCCTCGACCAGGGCCCAGGTGTTATCCCAG
# Right flank :  GCTTCCCTTCTCACTTCTTGAAGCTCCTCCGGGAACAAGATAAGTAAGTAGTTTCACGATTAAACCAGCTTGAGAAATAAAGCTCCACAGGACGCATGACAGGACCATAAAAGCCCTACCTGCGCATTTCCGAAGGGCTGGGATCACGCGCAGCGCACATGCCGGCCCTCCCTCTTCCGTTTAGAGACGGTTTAGGAAGAATATGCAAAAGACGCGTAGCCCAGCTCAGCCACAGCGAACCAACGGTACTGCTCGGCCCGGAACGTTTTCCATGCGGTGTAAACCTTCCGGTAGGTCGGGTCCTTGGCCGCGGTCTCCTCGTAAAGGGCAAAGGCCTCCTCTTGCGCGCGCTTCATGACCTCCAAGGGGAAGCGGCGGAGCTTAACCCCCCGTTTTGGAAGGCTTTGGAGAGCTGGAGGGTTCTTCGCATCGTACTTGCTCATCATCGTCAGGTTGACCTCGGAAGCCGCCACCTCAAAAGCCAGCTGGAACTCCTTGGG

# Questionable array : NO	 Score: 4.37
# 	Score Detail : 1:0, 2:3, 3:0, 4:0.91, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTAGTCCCCACGCACGTGGGGATGGACCA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [6,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         F [matched GTAGTCCCCACGCATGTGGGGATGGCCCG with 96% identity] Score: 4.5/4.5
# 	Secondary Structural analysis prediction:  F [-13.70,-12.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [23.3-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [5.24,0.27   Confidence: HIGH] 

# Array family : I-E [Matched known repeat from this family],   
//

>k141_9642 flag=1 multi=493.0000 len=1192		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                 	Spacer_Sequence         	Insertion/Deletion
==========	======	======	======	================================================	========================	==================
       706	    47	  93.8	    24	........................-....TG.................	CGTAGGCACCTTGACCCTTGAGGG	C [734]
       778	    48	 100.0	    24	................................................	TGAGAACACCCTGACCCTCATCAA	
       850	    48	 100.0	     0	................................................	|                       	
==========	======	======	======	================================================	========================	==================
         3	    48	  97.9	    24	CTCCACCGTGAGCGGAAACAGCGCAAGAAGCGGCGGTGGGATTTACAA	                        	       

# Left flank :   TGGGATACCCAGGTGGACCAGCACACCCTTACCCTCAAGGCCAAAGCTACCTCGAGGGGCATTGAGAGCACTGCCACCTTGACCCTTGACGTCAACCAGCCCCCCATCTACAACGTCGACACAACAGACGATACGGTTGACGCTAGGCCCGGTGATGGCCAATGTGCTGACGAGAAGGGCAAATGCTCCCTGCGGGCGGCCATCATGGAGGCCAATGCCCACAGCGCCCCGGCGGTAATCAATATCCCGGCTGGAACTTACACCCTCACCCGCACCAGCAGCAGCGTGGATGAGGAGGGTGGCGACCTGGACATCAGGAGGGACATCACCCTCAAGGGGGCCGGAAAGGACAAGACGATTCTGGACGGAAACGGCACCGACCGGGTCTTGGAAATTTACGGAGGAAGTATAACGAAGGTAGAGGGGGTCACGATCCGGAACGGTAAGGCCGACGTCAGCGGCGGTGGGATTTTCAACATAGTCACCTTGACCCTTAAGGG
# Right flank :  TAGCGGTGATGGTTCAACTGCTGACCTTTCTTTCTCTACTATCACCCTCAACCAGGCCACCTTGGAGGGTGGGGGCATTCAGATCGACTCGGGAACGGTCACCCTCAAGGGGGTCATCCTGGCAAACAACACCAGCCCGACCGGCCCGGAGTGCAGCGGCAGCCTGACCAGCCAAGGGTATAACCTCATTAGGAGTAACAGCGATTGCGCCTTTACCGTCCAAACCACTGACATCACCGGCCAAGACCCAGCCCTTGGCCCCCTGGGGGACAACGGTGGCCCCACCCAGACCCAC

# Questionable array : YES	 Score: 0.94
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.90, 5:0, 6:-0.25, 7:-0.80, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTCCACCGTGAGCGGAAACAGCGCAAGAAGCGGCGGTGGGATTTACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [14,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-8.80,-5.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [4-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [43.3-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_10771 flag=0 multi=166.5858 len=1254		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                            	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	===========================================	==================================	==================
       816	    43	  90.7	    32	.........................CG.C.......G......	TACAACTACACCGGCACCCTGACCCTTGAGGG  	
       891	    43	  86.0	    34	...........A..A..........T...A..........G.G	GGATTCTCAACGGAGGCACCCTGACCCTCAAGAG	TAC,C [916,933]
       972	    43	 100.0	    29	...........................................	CTCAACTGGGGCACCTTGACCCTCACGGA     	
      1044	    43	  95.3	     0	.............................A...T.........	|                                 	
==========	======	======	======	===========================================	==================================	==================
         4	    43	  93.0	    32	CTCCACCGTGAGCGGAAACAGCGCAGACGTGGGCGGTGGGATT	                                  	       

# Left flank :   TCCACCACCACGGATACCCCGGTGGGCCGGCACACCCTTACCCTTAAGGCCAAGGCGGGTAGCATCGAGAAGACCGCCACCTTGAACCTTATCGTCGGCCTGCTTTTCATCTACAACGTTGACACAACAGCCGATACCACCGACGCTAGGCCCGGTGATGGCCAATGTGCTGACGAGAAGGGCAGGTGCTCCCTGCGGGCGGCCATCATGGAGGCCAATGCCCTCAGCGCCCCGGCGGTGATCAACATCAACATCCCGGCTGGCACTTACGCCCTCACCAGCAGCGTGGATGAGCAGGGGAGCTACCTGGCCATCAGGAGCAACATCACCCTCAAGGGGGCCGGAGCGGACAGGACGATTTTGGACGGAAACGGCACCAACCGGGTCTTGGCGATTTACGAAGGAAGTATAACGAAGGTAGAGGGGGTCACGATCCGGAACGGTCAGGCCGACTACGGCGGCGGTGGGATTAGCAACGCGGGCACCGTGACCCTCATCAA
# Right flank :  TACAATGGTGGTGATTCAACTGCTAACCTTTCTTTCTCTACTATCACCCTCAACCAGGCCACCTCAGAGGGCGGGGGCATTGGGATCGGCTCGGGGACGGTCAAGCTCAAGGGGGTCATCCTGGCAAACAACACCGTCGGCTCGGGCGGCACCGGCCCGGACTGCAAG

# Questionable array : YES	 Score: 0.70
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.65, 5:-1.5, 6:0.25, 7:0.01, 8:0.6, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTCCACCGTGAGCGGAAACAGCGCAGACGTGGGCGGTGGGATT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [9,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-8.80,-5.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [14-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [33.3-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0.68   Confidence: LOW] 

# Array family : NA
//

>k141_9642 flag=1 multi=493.0000 len=1192		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                 	Spacer_Sequence         	Insertion/Deletion
==========	======	======	======	================================================	========================	==================
       706	    47	  93.8	    24	........................-....TG.................	CGTAGGCACCTTGACCCTTGAGGG	C [734]
       778	    48	 100.0	    24	................................................	TGAGAACACCCTGACCCTCATCAA	
       850	    48	 100.0	     0	................................................	|                       	
==========	======	======	======	================================================	========================	==================
         3	    48	  97.9	    24	CTCCACCGTGAGCGGAAACAGCGCAAGAAGCGGCGGTGGGATTTACAA	                        	       

# Left flank :   TGGGATACCCAGGTGGACCAGCACACCCTTACCCTCAAGGCCAAAGCTACCTCGAGGGGCATTGAGAGCACTGCCACCTTGACCCTTGACGTCAACCAGCCCCCCATCTACAACGTCGACACAACAGACGATACGGTTGACGCTAGGCCCGGTGATGGCCAATGTGCTGACGAGAAGGGCAAATGCTCCCTGCGGGCGGCCATCATGGAGGCCAATGCCCACAGCGCCCCGGCGGTAATCAATATCCCGGCTGGAACTTACACCCTCACCCGCACCAGCAGCAGCGTGGATGAGGAGGGTGGCGACCTGGACATCAGGAGGGACATCACCCTCAAGGGGGCCGGAAAGGACAAGACGATTCTGGACGGAAACGGCACCGACCGGGTCTTGGAAATTTACGGAGGAAGTATAACGAAGGTAGAGGGGGTCACGATCCGGAACGGTAAGGCCGACGTCAGCGGCGGTGGGATTTTCAACATAGTCACCTTGACCCTTAAGGG
# Right flank :  TAGCGGTGATGGTTCAACTGCTGACCTTTCTTTCTCTACTATCACCCTCAACCAGGCCACCTTGGAGGGTGGGGGCATTCAGATCGACTCGGGAACGGTCACCCTCAAGGGGGTCATCCTGGCAAACAACACCAGCCCGACCGGCCCGGAGTGCAGCGGCAGCCTGACCAGCCAAGGGTATAACCTCATTAGGAGTAACAGCGATTGCGCCTTTACCGTCCAAACCACTGACATCACCGGCCAAGACCCAGCCCTTGGCCCCCTGGGGGACAACGGTGGCCCCACCCAGACCCAC

# Questionable array : YES	 Score: 0.94
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.90, 5:0, 6:-0.25, 7:-0.80, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTCCACCGTGAGCGGAAACAGCGCAAGAAGCGGCGGTGGGATTTACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [14,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-8.80,-5.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [4-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [43.3-50.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_10771 flag=0 multi=166.5858 len=1254		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                              	Spacer_Sequence                     	Insertion/Deletion
==========	======	======	======	=============================================	====================================	==================
      1068	    45	  91.1	    27	.........................TC.G.........A......	CCAGTTGAGAATCCCACCGCCCACGTC         	
       996	    45	 100.0	    36	.............................................	TCCGTTGAGAATCCCACCGCCACCGCCCTCGTAGTA	
       915	    45	  88.9	    30	..........T..T............C..CA..............	GGTGTAGTTGTAAATCCCCCCGCCCAGGCG      	
       840	    45	  91.1	     0	.........................T.GA.........C......	|                                   	
==========	======	======	======	=============================================	====================================	==================
         4	    45	  92.8	    31	TGCGCTGTTTCCGCTCACGGTGGAGCTCTTGAGGGTCAGGGTGCC	                                    	       

# Left flank :   CTTGCAGTCCGGGCCGGTGCCGCCCGAGCCGACGGTGTTGTTTGCCAGGATGACCCCCTTGAGCTTGACCGTCCCCGAGCCGATCCCAATGCCCCCGCCCTCTGAGGTGGCCTGGTTGAGGGTGATAGTAGAGAAAGAAAGGTTAGCAGTTGAATCACCACCATTGTAAATCCCACCACCCTCGT
# Right flank :  CGCGTTGCTAATCCCACCGCCGCCGTAGTCGGCCTGACCGTTCCGGATCGTGACCCCCTCTACCTTCGTTATACTTCCTTCGTAAATCGCCAAGACCCGGTTGGTGCCGTTTCCGTCCAAAATCGTCCTGTCCGCTCCGGCCCCCTTGAGGGTGATGTTGCTCCTGATGGCCAGGTAGCTCCCCTGCTCATCCACGCTGCTGGTGAGGGCGTAAGTGCCAGCCGGGATGTTGATGTTGATCACCGCCGGGGCGCTGAGGGCATTGGCCTCCATGATGGCCGCCCGCAGGGAGCACCTGCCCTTCTCGTCAGCACATTGGCCATCACCGGGCCTAGCGTCGGTGGTATCGGCTGTTGTGTCAACGTTGTAGATGAAAAGCAGGCCGACGATAAGGTTCAAGGTGGCGGTCTTCTCGATGCTACCCGCCTTGGCCTTAAGGGTAAGGGTGTGCCGGCCCACCGGGGTATCCGTGGTGGTGGAGATGGTGAGTTTTTGACGGG

# Questionable array : YES	 Score: 0.44
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.64, 5:-1.5, 6:0.25, 7:-0.04, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGCGCTGTTTCCGCTCACGGTGGAGCTCTTGAGGGTCAGGGTGCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [12,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-4.60,-8.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [9-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [31.7-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,1.05   Confidence: MEDIUM] 

# Array family : NA
//

>k141_11378 flag=1 multi=7.3376 len=2653		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence                    	Spacer_Sequence                                    	Insertion/Deletion
==========	======	======	======	===================================	===================================================	==================
       658	    35	 100.0	    51	...................................	TAAGGCCAGGGGTGTGAGGATTGAAAGCCCCTATCCCCCCTTGCCAAAAAC	
       744	    35	 100.0	     0	...................................	|                                                  	
==========	======	======	======	===================================	===================================================	==================
         2	    35	 100.0	    52	TTGCGGCGGTGTGCAAAGCACCGGGCCGGCGGTCT	                                                   	       

# Left flank :   AGCCTCAGCAGCCTGAAGGCCTGGGTAGCGGTTCTGTCGCCTATTGCCTTCACCCTTGCGGGTTCCTGCCAGGTAGCCACAGGTACGGTTTCGGCTATAGCCTGCCTCTCTTCCTCAAGCTGGGCCAGTTCTCGTTTCATTAGCGGATACTGCAGCAAAACGTCCTTACAAAACCGCCTCACGTGGGCAGGAAGTGCCGTCAAAAGCCTCACCTCACAAAGAAGTCTAGTAGTAACAGCATAGCCCCCACTGCCGCCAGTGCCCAGCTAAGAGGATAAAGCACCACGTCGGTGTTTCAGTGTTTCATTTTGTCTCGTTCTGTCCTAGCCCGGTGAGGTGTTAAGGTGTTAAACTTTTTGCCTTTAACGTTAACGTTGTTAAACTTTAACGTTGAAGGGGAAAGCGTGGAGATGTACGGTGTTCCAGTGTTTCATTTTGTCCCGTTCTGTCCCGGCCATGTTTCACACCACCCCGAGAGAGACCGTTTTTTTTACCCCATT
# Right flank :  TACATGCTGGCGGTTCCAGGATTTTACCCCCCCCTACCCGTTGTCACGGGTGTCAAGCTAAATCCAACCGGTGTCAAGGTTGACAAGGCTCATGCCTTCATCTCCGCTCCCGGTGCTGGTGGGTGGCAGGTTCGGCAAACTCTAAGGCCCCACCTTGATTGCCAAAAATCGCTACCTCCACAAGTGGGGCAGGCACCAGGAAGTCGTGGCCTGCATACCTGGCAAACAAGGTTGACGTGAAGTGTTCCCCGCTGGCAGGTGGGGCAAAACAGCAGCCGGGGAGCAAGAGCCTCCTCAAGGGCCTTTTCATTTTCGGCTACCCATTCAAGGAGGCCCGCTGCCTCGGCAGGAGCTTCTTCCGGGGGCCAGGGAGCTTTTAAGCGGAGGCCTCCCCCCTCCCACCACAGGGAGAGGCCAGCCTGCTTAAGTCTTTCTAAGGCCTCGGAGATACTCATTCAGGGTTCCCTCCCAGGTGGGTGTTTTGGTCTTGTCGTCGTTGC

# Questionable array : NO	 Score: 1.84
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-0.3, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TTGCGGCGGTGTGCAAAGCACCGGGCCGGCGGTCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-9.30,-6.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [46.7-43.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_124 flag=1 multi=3.0000 len=1124		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	===========================	=======================================	==================
         1	    27	 100.0	    39	...........................	GGGCTACATAGGGCAGGTTGAAGAGGTAGAGGGGTAGGC	
        67	    27	 100.0	     0	...........................	|                                      	
==========	======	======	======	===========================	=======================================	==================
         2	    27	 100.0	    49	CCACTGTATAGACCACAGCCCCCCAGA	                                       	       

# Left flank :   |
# Right flank :  AAGAATAGGTGGGAGGGTACCCCGTACTCTTTCACTGCAATATATAGCGGAATACCAGCTATGGGTTCAAAGGTGTAGCCTGCCACTATCAGTCCTAAGCCTAGGAAGTTCCCCTCAGCAGCCAAGGGAATACCAGGTAGGGTAATGGCCATTGCCAGTATGGATGAAACCCAAAAGAGACTCTTCCTGGGTATAGCGGACCTACCGTAGAATTCCACTGCTGGGATTGCAAGGAGCCACACAAGGAAGAACTGTACTGCACCAGCCACAGTGAGCCCGAAGCTCACACCGTAGAGATCCAGCTCCAGGAGGATTGTAAATATGGCTACAGCTGCTATTATGCTTACTATATTTCCTCTAATGAATTTAGAGTCCATACTTAGTGCTATCCATGAGGGTTGATTTAGTTTTATGAGGTAAATTGATACAAAATTGACTTATAAGGTGTCCCGTACCGTCACAATCACTCCAGATAGGCTGCTCTCGGAGGCCCACAACAT

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCACTGTATAGACCACAGCCCCCCAGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [8,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [0.00,-1.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_11950 flag=1 multi=3.0000 len=1049		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                        	Insertion/Deletion
==========	======	======	======	===========================	=======================================	==================
       103	    27	 100.0	    39	...........................	TTTTAGCGCGTTCTATGCGTTCTACGCACCCTTAGAACC	
        37	    27	 100.0	     0	...........................	|                                      	
==========	======	======	======	===========================	=======================================	==================
         2	    27	 100.0	    40	GTCTTAACTCTAGTTTGAGGAAGAGAA	                                       	       

# Left flank :   CACCAACCGCTGGGCCAATTGCCCACCCTGCGTTTATACCTACCCTCAACCTACTGAACGCCTTTATGAGGCTGGTAAGATTCCTCTGTATGTCCCCAACCAGAGTGGTGTTGGCCACATTGTAAACGCCGTTGAAAAACGTCTGTACAAGGATGAGGGAGACCACAGCCCAGGAGAACCCTAGGACAGAGGCAAGTAGAAGGGAGAGTGATGACAAGGAAGCTGAGAGAATCATCACCTTTACCCTCCCCAGGTAGTCTGTCATGTATCCCCCTAGGAGATAGGCAAGGATCCCTATCACCCCCTGAGCCAGGGAGAATATGGAGACAAAAGTCAGGCTTAGCCCATACACTCTATAGAGGGCAAAGCTTGTGAATGGCCATATTATTGAACCCGCTAGGGCCCTTCCAGCACCTACGAGGGCCAGTCTGTCTATCAACTCCATTCGGAGAACTGAGGGGTCATGATAAAAAAGTTAGGTATATAATGAACAGTGCGCT
# Right flank :  ACCAACTCCAT

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTCTTAACTCTAGTTTGAGGAAGAGAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:62.96%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.40,-2.60] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [8.3-60.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.64   Confidence: HIGH] 

# Array family : NA
//

>k141_12749 flag=0 multi=18.7284 len=3959		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                               	Insertion/Deletion
==========	======	======	======	=============================	==============================================	==================
       238	    29	 100.0	    46	.............................	CCTTGGCGGCCGAGCTGGGCAGACAAGTGGGCGAGGTGAACAAGAG	
       313	    29	 100.0	     0	.............................	|                                             	
==========	======	======	======	=============================	==============================================	==================
         2	    29	 100.0	    47	GATCGACGAGACGAACAAGAGGATAGACG	                                              	       

# Left flank :   GGACAGAGCGATAAGGGAGGAGATAGGCGCCATAAGGACGGAGATGGCTGAGCGCGATAAGGCCATGAGGGCCGAGGCGGCTGAGCGCGACAGAGCCGTGAGAGAGGAGATCGCGCAGCTTAGGGCGGAGATGGCCGAAAGGGACAGAGCGATAAGGGAGGAGATAGGCGCCATAAGGACGGAGATGGCTGAGCGCGATAAGGCCATGAGGGCCGAGGCGGCGCAGATGAGGGCGGAG
# Right flank :  GACCTCTACAAGCTTCTGTCCAGCATCCACCAAGTCCTCCTGGACATCTCCAGACAGCTGTCCGCCGCCGGGTGAGCCGCCGGCGTTTATTCGACCGCTTCCTCGCCGCTAAGGGCCGCGACGGCCGCCGTTGCCATCAGAGCCCGGGGGCGGGCCGTCGCCGGCGCCCTAGGGCTGGGCCGAGCTCAGGAGGTCGTCCAGCCTCTTGGCCTCACTCTCCATCTCGAGCATGTAGGGTGGCGCGGACTTCTCCCACTCCTCCCGCGCGAGCACGATGAACTCGACGCCGGGCTCTAGGTACTTAGACATCGCGTTGAGCCTATCGATGTATCTAACCCCCCTCCCCATCTCGTCGTAGATGATCACCAGATCTGTGTCGCCGTCCTCCGAGCAGTCGCCCCTCGCCCTGGAGCCCACCACGTAAACCTCGTCGATTTTGAACTCCCCCTCAGCTTCTCCACGAACGGTCCTCCTCAGCATGACCTCTTGGCTCCTCAACG

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GATCGACGAGACGAACAAGAGGATAGACG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [12,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [31.7-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0.27   Confidence: LOW] 

# Array family : NA
//

>k141_14339 flag=0 multi=22.3778 len=2539		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                                 	Spacer_Sequence                                   	Insertion/Deletion
==========	======	======	======	================================================================	==================================================	==================
         1	    64	 100.0	    50	................................................................	GCGGTATCGGTGGCGCCATCGGAAGCTTCCTAAGCGGGGACGTTGGCTCT	
       115	    64	 100.0	     0	................................................................	|                                                 	
==========	======	======	======	================================================================	==================================================	==================
         2	    64	 100.0	    60	GGTATTAATGGCATTGCCGGTACCATTGGCAACCTCATCGGTGGGGATGCTGGGTCTGCCATTA	                                                  	       

# Left flank :   |
# Right flank :  AAAGGTATCGGTGGCGCCATCGGAGGCATCCTAAAAGGGGATTTCGGTTCTGCTGTTGGCAACCTTGCCAATGCTGTTGGTAGCTTCTTTGGCGTTGGTAATGTTGGCAGCGCTGTTAAGAACTTCCTAGACGGAAACGTTGAGGGTCTCATAAGCGATGTTGGTGGCTTCGTTGGGAGCCTCATAGGAGGCCCCGTCGGAGATGTAGTTGGAAGGCTCGCTGGAAATCTCGTCGGAGGCGTTGCCAAATCTGCCATTGGTTCCATTGGCAACTTCCTATCTGGTATTTTCGGCGGCGGAGGTAGCAAAGGTAATAGCAAAGATAATAGCGCGAAAGACAAGTTGTTTGCAGACTCCGAGCCTCTCATCAATACCATTCTTAGCGACAACAAAAATGAGAGCACAATTGAAAAAATCTGGTCTGCCGTCGATTCTACTGCCGGCAATAACTGTTCTTCCGCAAATGCCCAGGCCAATGGGTTGAACTGGGCTTGCGCATT

# Questionable array : YES	 Score: 0.69
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-1, 7:-0.2, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTATTAATGGCATTGCCGGTACCATTGGCAACCTCATCGGTGGGGATGCTGGGTCTGCCATTA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [12,18] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-16.60,-12.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_15003 flag=1 multi=2.0000 len=1055		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                	Spacer_Sequence                          	Insertion/Deletion
==========	======	======	======	===============================	=========================================	==================
       665	    31	 100.0	    41	...............................	ATCGGCTGCCTTATCAGTTTTCATCTTCTCAAGTCCTTTTG	
       737	    31	 100.0	     0	...............................	|                                        	
==========	======	======	======	===============================	=========================================	==================
         2	    31	 100.0	    51	CCGCTTTGCTGGGTTCCATTTCCTCAAATAC	                                         	       

# Left flank :   AACATCGCCCTGGACAAAGTTTTCGGTGGTATTGACACTTTGGTTAGTAGCCTCAACCCAACCCTTGCAGTCCTTACACCAGTAATAAAGCCTTAAGGTTGTCTCATCAATGCCAGCTACTTCAGCATCGGTATAGTATAGCTTAATTGTAGCTTTGCCTTCAGTGAATTCAGAAGGAACTTCCAGATCGATGACCTTTACTGGTATTTTCCCTGTTTTCTCTTGGATGCCAACCGAGCCCTTTAGCTCTTCAGGATACTTTCTTACTTTTATATCACCGCTCCCCTTTTCTCCTCCAGTATCTTTAAAGCTCACTTCAGCTTCTTTTTCTACCTTGAAAGGAGACTCTTTAGTGAATTCCTGAGCCAGATCCTTATTTGTAACAGATACCGTTTTCACAGGCTCAAGCATTTCCTTAATTGCTTCGACAGCTTTACCAGGCTCCATCTTCTTAGTTATTTCAGCCACTTTTGGAAGTTCTATTTCCTTAAGCCTCTCAC
# Right flank :  TTTAGCTGCTACCTTAGGAGGCATCCTTTCAGCTTCAGAAGCAACTTCTTCTGGTTTTTTCTTTTCCCACTCGCGGGCAAGCTGTGAAATTGTAATTCCAGTGCCGCTCAGGGAAATGCTCACTGTCGCTTCGTCAGGGTCAGGGTCATTTGAAGGAATTGATAAGCTTGCTGACTTTGCCCCCTCTGATGTCGGGCTGAATATAACCTGAAGGGTAGCCAAACCACCCGGGGCTATGGTTTTACCTGAGACATTATCATTCTTGATGCTAAACTCGGAGGCATTTGC

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCGCTTTGCTGGGTTCCATTTCCTCAAATAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.61%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-1.80,-0.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [60.0-58.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.37,0   Confidence: MEDIUM] 

# Array family : NA
//

>k141_14412 flag=0 multi=284.4979 len=2286		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                           	Insertion/Deletion
==========	======	======	======	=============================	==========================================================	==================
      1380	    29	 100.0	    58	.............................	GGAGGTTCAGGGTGCCCTCGCCAAAGATGCCCTCGTTGTAAATCCCACCACCTTCGTC	
      1293	    29	 100.0	    46	.............................	CGAGGGCCAGGGTGCCCTCGTTGTAAATTCCACCGCCGTCATAGCT            	
      1218	    29	  89.7	    46	..........T.A..........C.....	TGAGGGCCAGGGCGTCATAGTGATTGTAAATCCCACCGCCCTCGCC            	
      1143	    29	  96.6	    58	..........T..................	TGAGGACCAGGGCGCCCGCGTTGTAAATCCCACCGCCGTCATCGCTGCCGTAACCGCT	
      1056	    29	  86.2	     0	.........................CCCT	|                                                         	
==========	======	======	======	=============================	==========================================================	==================
         5	    29	  94.5	    52	TGCGCTGTTTCCGCTCACGGTGGAGTTGA	                                                          	       

# Left flank :   AAAAAGCCCCCGCAAAGGGGGCTTCCCTTCGGGCCTCCCTACCGCTCTACTGAACCGATGTCACAGGCCGCACCCCGGGGCCGGTTCACCTCGCGCTGGTCGGTGGCGAGCGGGTCGCCGTCCGGGTCGGTGCAGTCCCTGGGGGGCACCTTGTCCACCACCGGGCTACCTGCCTGGGGCAGGTGGGTCTGGGTGGGGCCACCGTTGTTCCCCAGGGGGCCAAGGGCTGGGTCTTTGCCGGTGATGTCAGTGGTTTGGACGGTAAAGGTGCAATCTCTGTTACTCCCAATGAGGTTATACCCTTGGCTGGTCAGGCTGCCGCTGCACTCCGGGCCGGTCGGGCTGGTGTTGTTTGCCAGGATGACCCCCCTGAGCTTGACCGTTCCCGAGTCGATCCCAATGCCCCCACCCTCTGAGGTGGCCTGGTTGAGGGTGATAGTAGAGAAAAAAAGGTTAGCAGTTGAACTGTAACCGATATTGTAAATTCCACCGCCTTTGCT
# Right flank :  CAAGGGTCAGGATGCCCTTGTTGTAAATACCGCCGCCGTCGTCGGCCTGACCGTTCCGGATCGTGACCCCCTCTACCTTCGTTTCAATTCCTTCGTAAATCTCCAAGACCCGGTCCGTGCCGTTTCCGTCCAGAATCGTCTTGTCCGCTCCGGCCCCCTTGAGGATGATGTTGCTCCCGATGTCCAGGTCGCCCCCCTGCTCATCCACCCTGCTGGTGCGGGTGAGGGTGTAAGTGCCAGCCGGGATGTTGATCACCGCCGGGGAGCTGAGGGCATTGGCCTCCATGATGGCCGCCCGCAGGGAGCACCTGTCGTTCTCGTCAGCACATCGGCCATCACCTGGCCTAGCGTCAATGGTGTCGTCTGTTGTGTTGACGTTGTAGATGGGAGGCTGGTTGACGATAAGGGTCAACTTGGTGGTTTTCTCAATGCTCTTCGAGGTAGCTTTGGCCTTGAGGGTGAGGGTGTACTTGCCCGGCGGGGTATCCCAGGTGGTGGAG

# Questionable array : NO	 Score: 1.83
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.72, 5:0, 6:0.25, 7:-0.66, 8:0.6, 9:0.92,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGCGCTGTTTCCGCTCACGGTGGAGTTGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [9,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.70,-5.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [5-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [38.3-61.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,1.05   Confidence: MEDIUM] 

# Array family : NA
//

>k141_15154 flag=1 multi=15.0000 len=1851		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                                                                  	Insertion/Deletion
==========	======	======	======	===========================	=================================================================================	==================
       699	    27	 100.0	    33	...........................	GAGTTGAGGAGTCCTTCCACACACAGCTATACG                                                	
       639	    27	 100.0	    33	...........................	ACATTGAGGGGATTCCCCACGTACAGCCATACG                                                	
       579	    27	  81.5	    21	...A....GG.T..G............	GCACTGGGACGTATTCCCACC                                                            	Deletion [532]
       531	    27	  85.2	    33	....G.........G..G.....A...	GCGTTGAAAGCCGCTCCTACGTACAACTATACC                                                	
       471	    27	  70.4	    32	...AG...GA....G..G.....T.C.	CACTGCAGGGCCCTTTCGTATATAGTTCCACG                                                 	
       411	    27	  70.4	    33	....GC...G....GA.T..C..T...	ATACTGAAGGCTATTCCCACGTATCGCTATACA                                                	
       351	    27	  85.2	    81	....GC...A....G............	GCGTTGAAGGGGTTACGTGGGTATTTGGTCACGATGCAGGAGTCCTATTCCATCCCGCAGACACCTACGTACAGCTACACA	
       243	    27	  81.5	     0	....GC..G.....G...........G	|                                                                                	
==========	======	======	======	===========================	=================================================================================	==================
         8	    27	  84.3	    38	GTCGATGTATCCGAATCACTTGCCGTT	                                                                                 	       

# Left flank :   CGGCCGCGCTTGCCAGCGCCCCGATATGGCTTGCTGCCGTCGTCATGCAGTATATTGTCAAACCATTAGTGAGTTTTATATTGAATGCACTTAAACAAATATTTTCAGTCGTTTACAAATTTGTATGTACTGTGCTTGTGCCCATCGTAAGGGTGTCCATAATGGCCTATGTCGATTATCACGCGGTTAAACACGTATACAGGCATGCGCACGACATGATAGCTTCGTGGTTCTCGTCCTGGTCGCCTAAAAGCACGTTTAAGGCGATCGGAAAGACCGTGGCCACTTTCGCCGTGCCCCTGGCTTCCGGCTTCATCGCCGCCCATATGTACGACTATATACTGGGTATGGTCGGCGCCTGCGGCCTCGCGCCGCCCGTCCTAGCGCCCAGTGCTCCCGTCGTTCCCACCACCCCGCCGTCTAACATGTTTGTCAAGTCGTTCACCGTGACTGAATCGCTTGCCGTTGAGTTGAGGGGTCCTTCCACACACAGCTATACG
# Right flank :  GACGATCAGAAAAACCCGATAATTGAGAAATATCTTTGGTCCGATCTAGAGGTTTGCCGCGGAGACGTTCGCAACGTCAATGCCCAACATGAGGATCGTTGCCGAGGCGATGGAAACTTGTAATTTCCCGCCGTTGGTGAACGCATTGGCCGTGATGCGGGAATTCTATATTACACGGCAACAGACTGTGTGATTCTCTTCAAGTTTGGCGTGGCTG

# Questionable array : YES	 Score: 1.27
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.21, 5:0, 6:0.25, 7:-0.66, 8:1, 9:0.47,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTCGATGTATCCGAATCACTTGCCGTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.85%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [17-9] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [56.7-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.27,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_19078 flag=0 multi=417.0261 len=1981		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                               	Spacer_Sequence                    	Insertion/Deletion
==========	======	======	======	==============================================	===================================	==================
      1040	    46	 100.0	    35	..............................................	TTGTTGTAAATCCCACCGCCACTGAACAGAAAGCC	
       959	    46	  95.7	     0	..............................A....T..........	|                                  	
==========	======	======	======	==============================================	===================================	==================
         2	    46	  97.8	    36	TGCGCTGTTTCCGCTCACGGTGGAGTTGATGAGGGCCAGGGTGCCC	                                   	       

# Left flank :   CCCCCGCAAAGGGGGCTTCCCTTCGGGCCTCTTTACTGCTCTCGCTCTACTGAACCGATGTCACAGGCCGCACCCTGGGGCCGGTTCACCCCGCGCTGGTCGGTGGTAAGGCGGTCGTCGTCCAGGTCGGTGCCGTCCCCGGGGGGTACCTTGTCCACCACCGGGCTACCTAGCTGGGGCAGGTGGGTCTGGGTGGGGCCACCGTTGTCCCCCAGATCGCCAAGGGCTGGGTCTTGGCCGGTGATGTCAGTGGTTTGGACGGTAAAGGCGCAATCGCTGTTACTCCCAATGAGGTTATACCCTTGGCTGGTCAGGCTGCCGCTGCACTCCGGGCCGGTCGGGCTGGTGTTGTTTGCCAGGATGACCCCCTTGAGGGCGACCGTTCCCGAGTCGATCCGAATGCCCCCACCCTCTGAGGTGGCCTGGTTGAGGGTGATAGTAGAGAAAGAGAGGTTAGCAGCTGAACCGTAACCGATATTGTAAATACCACCGCCGCTTAT
# Right flank :  GCATTGTAAATCCCGCCGCCGCCGTAGTCGGCCTGACCGTTCTGGATCGTGACCCCCTCTACCTCCGTTTCAATTCCTTCGTAAATCTCCAAGACCCGGTCCGTGCCGTTTCCGTCCAAAATCGTCCCGTCCGCTCCGGCCCCCTTGAGGGTGATGTCCCTCCTGATGTCCAGGTCGCCCCCCTGCTCATCCACCCTGCTGCTGGTGCGGGTGAGGGTGTAAGTGCCAGCCGGGATGTTGATCACCGCCGGGGTGCCGCGGACATTGGCCTCCATGATGGCCGCCCGCAGGGAGCATTTGCCGTTCTCGTCAGCACATCGGCCATCACCGGGCCTAGCGTCAATGGTGTCGTCTGTTGTGTTGACGTTGTAGAGCTGGCCGACGATAAGGGTCAAGGTGGCGGTCCTCTCAAGGCCGCTCGAGGTGGCTTTGGCCTTGAGGGTGAGGGTGTACTGGCCCACCGGGGTATCCGGGGTAGTGGAGATGGTGAGTTCTTGACG

# Questionable array : YES	 Score: 1.35
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGCGCTGTTTCCGCTCACGGTGGAGTTGATGAGGGCCAGGGTGCCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-5.80,-6.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [36.7-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,1.05   Confidence: MEDIUM] 

# Array family : NA
//

>k141_1962 flag=1 multi=315.0521 len=1447		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                             	Spacer_Sequence                                  	Insertion/Deletion
==========	======	======	======	============================================	=================================================	==================
      1167	    44	 100.0	    31	............................................	CCTCGTTGTAAATCCCACCGCCGTAATGGTA                  	
      1092	    44	  86.4	    49	.........................CC.TC....A...A.....	CGGAGTAGGCGTTGTAAACCCCACCGCCGTCACCCTCGAGGAAGCTACT	
       999	    44	  86.4	    34	...A......TT.............C..T.....A.........	TGGAGTAGTCGTTGTAAACCCCACCGCCCCTATC               	
       921	    44	  81.8	    31	...............T.........CC.TC....A...A...A.	CCTTAATGTTGTAAATCCCCCCGCCAACGCT                  	
       846	    44	  88.6	     0	...T.....A........A.....................A.A.	|                                                	
==========	======	======	======	============================================	=================================================	==================
         5	    44	  88.6	    36	TGCGCTGTTTCCGCTCACGGTGGAGTTCGTGAGGGTCAGGGTGC	                                                 	       

# Left flank :   GGTCTTGGCCGGTGATGTCAGTGGTTTGGACGGTAAAGGCGCAATTGCTGTTACTCCTAATGAGGTTGTACCCTTTGCTGGTCAGGCTGCCGCTACACTCCGGGCCGGCCTTGCTGGTGTTGTTTGCCAGGATGACCCCCTTGAGGGTGACCGTTCCCGAGTCGATCTGCTGAATGCCCCCACCCTCCCAGGTGGCCTGGTTGTTGGTGATGGTAGAAAAAGAAAGGTTAGCTGTCGAATCTGCAAGGTAAATGCCACTGTAAACCCCACCACCCCTTT
# Right flank :  CCTTAAAGTTGAAAATCCCACCGCCAGGGTCAATTGAAGTCTTACCATTCCGGATCGTGCCCCCCTCTACCTTCGTTACGCTTCCCTCGTCAATGTACAAGACCCGGTAGGTGCCGTTTCCGTCCAGAATCGTCTTGTCCGCTCCGGCCCCCTTGAGGATGATGCTGCTCTTGATGTTCAGGTCGTCCCCCTGCTCATCCACCCTGCTGGTGCGGGTGAGGGTGTAAGTGCCAGCCGGGATGTTGATCACCGCCGAGGTGCGACGGGCATTGGCCTCCATGATGGCCGCCCGCGGGGAGCATTTGCCCTTCTCGTCAGCACATTGGCCATCACCGGGCCTAACGTCAACCGTATCGTCTGTTGTGTCAACGTTGTAGAGAGCCTGGACGTCAAGGGTCAAGGTGGCGGTGCTCTCAATATTGCCCGAGGTGGCCTTGAGGGTAAGGGTGTACTGGCCCACCGGGGTATCCCGGGTGGTGGAGATGGTAAGTTCCCGAGGG

# Questionable array : YES	 Score: 0.27
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.43, 5:-1.5, 6:0.25, 7:-0.02, 8:0.6, 9:0.51,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGCGCTGTTTCCGCTCACGGTGGAGTTCGTGAGGGTCAGGGTGC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [12,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-11.50,-12.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [13-6] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [51.7-53.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.78   Confidence: MEDIUM] 

# Array family : NA
//

>k141_21327 flag=1 multi=631.8149 len=1070		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                       	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	======================================	=====================================	==================
       635	    38	  94.7	    37	....................A..G..............	AGGGCTCCACCGTGAGCGGAAACAGCGCAGACTATTA	
       710	    38	 100.0	     0	......................................	|                                    	
==========	======	======	======	======================================	=====================================	==================
         2	    38	  97.3	    38	CGGCGGCGGGATTTACAACGTGGACACCCTGACCCTTA	                                     	       

# Left flank :   GTGGAGGGCTCCGGAGAGACCAGTCAAGAACTCACCATCTCCACCACCACGGATACCCCGGTGGGCCAGTACACCCTTACCCTCAAGGCCAGAGCTACCTCGAGCAACATTGAGGATGCCACCACGTTGAGGCTTACCGTCAACCAGCTTTCCATCTACAACGTTGACACAACAGACGACACCATTGACGCTAGGCCTGGTGATGGCCAATGTGCTGACGAGAAGGGCAGGTGCTCCCTGCGGGCGGCCATCATGGAGGCCAATGCCCTCAGCTCCCCGGCGGTGATCAACATCCCGGCTGGCACTTACACCCTCACCCGCACCAGCAGCGTGGATGAGCAGGGGGGCGACCTGGACATCGGGAGCAACATCACCCTCAAGGGGGCCGGAGCGGACGGGACGATTCTGGACGGAAACGGCACGGACCGGGTCTTGGAGATTTACGAAGGAATTGAAACGAAGGTAGAGGGGGTCACGATCCAGAACGGTCAGGCCGACGC
# Right flank :  TCAACTCCACCGTGAGCGGAAACAGCGCAGACGAGGGCGGTGGGATTTACAATGGTGGTGATTCAACTGCTAACCTTTCTTTCTCTACTATCACCCTCAACCAGGCCACCTCAGAGGGTGGGGGCATTCGGATCGACTCGGGAACGGTCGCCCTCAAGGGGGTCATCCTGGCAAACAACACCAGCCCAACCGGCCCGGAGTGCAGCGGCAGCCTGACCAGCCATGGGTACAACCTCATTAAGAGTAACAGCGATTGCGACTTTACCGTCCAAACCACTGACATCACCGGCCAAGACCCAGCCCTTGGCCCCCTGGGGGACAAC

# Questionable array : YES	 Score: 1.32
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CGGCGGCGGGATTTACAACGTGGACACCCTGACCCTTA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [8,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-3.40,-2.50] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [45.0-41.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.74,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_2439 flag=0 multi=362.6812 len=1377		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence          	Spacer_Sequence                                                  	Insertion/Deletion
==========	======	======	======	=========================	=================================================================	==================
       410	    25	 100.0	    59	.........................	TTGATGAGGACCAGGGTGCTGTAGTAGTAGTTGTAAATCCCACCGCCGCTGGAGGCGGT      	
       494	    25	 100.0	    50	.........................	TTGATGAGGGTCATGGTGCCTGCGTTGTCAATCCCACCGCCGCTCCAACG               	
       569	    25	  96.0	    38	..........T..............	CCCTCAAGGGTCAAGGTGCCCGCGTTGTAAATCCCACC                           	Deletion [632]
       632	    25	  76.0	    50	GC........TT.T....A......	CCCTTAAGGGTCAGGGTGCCGTAGGAGTTGTAAATCCCGCCGCCCCAACG               	
       707	    25	  92.0	    50	..........T.......A......	CCCTTAAGGGTCTGGGTGCCGGAGTAGTTGTAAATCCCACCACCCTGGCG               	
       782	    25	  92.0	    65	C..T.....................	CCCTCAAGGGTCAGGGTGCCAAGGGTCAGGGTGCCCGCGTTGTAAATCCCACCGCCATCATAGCT	
       872	    25	  92.0	    50	..................A.....A	TTGATGAGGACCAGGGCGCTGTAGTAGTTGTAAATCCCACCGCCCTCGTC               	
       947	    25	  88.0	     0	....A.....T.............A	|                                                                	
==========	======	======	======	=========================	=================================================================	==================
         8	    25	  92.0	    52	TGCGCTGTTTCCGCTCACGGTGGAG	                                                                 	       

# Left flank :   CCCCTCGCTGGTCGGTGGTAAGGCGGTTGCCGCTTAGGTCGGTGCAGTCCCTGGGGGGCACCTTGTCCACCACCGGGCTACCTGGCTGGGGCATGTGGGTCTGGGTGGGGCCACCGTTGTCCCCCAGGGGGCCAAGGGCTGGGTCTTGGCCGGTGATGTCAGTGGTTTGGACGGTAAAGGCGCAATCGCTGTTACTCCCAATGAGGTTGTACCCTTGGCCCGTCAGGCTGCCCTTGCAGTCCGGGCCGGTCGGGCTGGTGTTGTTTGCCAGGATGACCCCCTTGAGCTTGACCGTTCCCGAGTCGTCCTGAATGCCCCCACCCGCCGAGGTGGCCTGGTTGCGGGTGATAGTAGAGAAAGAAAGGTTAGCAGTTGAATTATAACCATTGTAAATCCCACCACCCTGGCGT
# Right flank :  CCCTCAAGGGTCAAGGTGCCCGCGTTGTAAATCCCACCGCCGGCGTTGGCCTGACCGTTCCGGATCGTGACCCCCTCTACCTTCGTTATACTTCCTTTGTAAATCTCCAAGACCCGGTAGGTGCCGTTTCCGTCCAGAATCGTCTTGTCCTTTCCGGCCCCCTTGAGGGTGATGTTGCTCCTGATGTCCAGGTCGTCCCCCTGCTCATCCACCCTGCTGCTGGTGCGGGTGAGGGTGTAAGTGCCAGCCGGGATGTTGATCACCGCCGGGGTGCCGCGGGCATTGGCCTCCATGATAGCCGCCCGCAGGGAGCACCTGCCGTTCTCGTCAGCACATTGGCCATCACCAGGCCTAGCGTCAATGGTATCGGCTGTTGTGTCGACGTTGTAGATGGGTTCGCTGTTGC

# Questionable array : NO	 Score: 1.52
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.60, 5:0, 6:0.25, 7:-0.60, 8:0.8, 9:0.47,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TGCGCTGTTTCCGCTCACGGTGGAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [2,7] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-2.20,-2.20] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [1-9] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [58.3-35.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0.37   Confidence: LOW] 

# Array family : NA
//

