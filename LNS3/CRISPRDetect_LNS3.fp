>k141_3745 flag=0 multi=300.8116 len=1245		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                      	Spacer_Sequence                       	Insertion/Deletion
==========	======	======	======	=====================================	======================================	==================
       991	    37	 100.0	    38	.....................................	TCAACTCCACCGTGAGCGGAAACAGCGCATATTACAAC	
      1066	    37	  97.3	    38	...................T.................	CGGGCTCCACCGTGAGCGGAAATAGCGCAGGCCCTTGG	
      1141	    37	  89.2	     0	............CT....G........T.........	|                                     	TTT [1157]
==========	======	======	======	=====================================	======================================	==================
         3	    37	  95.5	    38	GGCGGTGGGATTTACAACAAGGGCACCCTGACCCTCA	                                      	       

# Left flank :   GACCAATTACCGGGCCTTCCTCTAAAGATGAAAGCCCGGCATCTTGGAGGCGGCGTATGAGAGGATACAAACGGCCTTGGCCAATGGTGTTCCTGCCCCTCGTGGCCCTGTTGTTCAGCGCTTGTGGACCAATCCCAATCCCAAGCATCGGCTTCTACAACGTCAACACAACAGCCGATACGGTTGACGCTAGCCCCGGTGATGGCCAATGTGCTGACGAGAAGGGCAGGTGCTCCCTGCGGGCGGCCATCATGGAGGCCAATGCCCGCGGCACCCCGGCGGTGATCAACATCCCGGCTGGCACTTACACCCTCACCCGCACCAGCAGCGTGGATGAGCAGGGGGGCGACCTGGACATCAGGATCAAAATCATCCTCAAGGGGGCCGGAGCGGACCAGACGATTCTGGACGGAAACGGCACCGACCGGGTCTTGGAAATCCACGAAGGAAGTATAACGAATGTAGAAGGGGTCACGATCCGGAAGGGCAAGACCTCCTAC
# Right flank :  CGGGCTCCACCGTGAGCGGAAATAGCGCAGAATACGGCGGTGGGATTTTCAACTACTCCGGCACC

# Questionable array : YES	 Score: 0.95
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.78, 5:-1.5, 6:0.25, 7:0.02, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCGGTGGGATTTACAACAAGGGCACCCTGACCCTCA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [9,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.60,-3.90] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-8] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [48.3-45.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_4831 flag=1 multi=14.9894 len=2677		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                        	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	=======================================	==================================	==================
       209	    39	  94.9	    32	A.....................................C	ATAGCAAAACGACAGCCGGATGCGCATCCCGC  	
       138	    39	 100.0	    34	.......................................	CCTGGTTCCTCGACGTAGCGGCGTGCGGGTATGT	
        65	    39	 100.0	     0	.......................................	|                                 	
==========	======	======	======	=======================================	==================================	==================
         3	    39	  98.3	    34	CGTCTCGCCCGGATGAAACCCCGGGCGCGGATTGAAACT	                                  	       

# Left flank :   GACATACGACGTTTCCACCGAAACTGCGGCCGGCCGCAAGCGATTGCGGCGCGTTGCCAAAGCCTGCGAACGCGTGGGGCAGCGAGTGCAGAAATCGGTATTCGAATGCGAAGTTGACGCCATGCGCTACGAGGCTCTGGAGCGAGAGCTGCTCTCCGAGATCAATCTCGAGGAAGATAACCTGCGCTTTTATCGCATTACCGAACCGGCCAACATGCGCGTCAAACAGTATGGCGTCTTTCGCTCCACCGATTTCAACGGCCCGCTTGTGGTGTAAGGCTGCGCCCAGATCAACCCCTGTGTCTGGGTTGCAACATGCTGTCGGCCGCGAACCTGGATCACAGCCAAACGCCTGGTCGGTTCGCGCATCATGTAAGTCCTTGATCTTTAACAAATTGATGAGAATTTCTTTTTCCCAGACCCGGCTGCGACGCACCACAAGCCCGGGTTCGCGCAAGGGAGCCAAAACTTCGTTGTGCGGCAAAATGTTACCCCGACGG
# Right flank :  TTCGTCGAGTAAGGCAAGCTGCGTGGT

# Questionable array : NO	 Score: 2.27
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:0.25, 7:0.02, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CGTCTCGCCCGGATGAAACCCCGGGCGCGGATTGAAACT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         R Score: 4.5/4.5
# 	A,T distribution in repeat prediction:     R [6,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-16.60,-15.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [18.3-40.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.78,5.14   Confidence: HIGH] 

# Array family : NA
//

>k141_6550 flag=1 multi=5.0000 len=1180		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                  	Insertion/Deletion
==========	======	======	======	============================	=================================	==================
       139	    28	 100.0	    33	............................	GTATACCTTTTTCTGCCTGCCATGCAACCCTCC	
        78	    28	 100.0	    32	............................	CACAACGCAAAAACACGCTGGGTGAGCCGGGG 	
        18	    18	  64.3	     0	..................----------	|                                	
==========	======	======	======	============================	=================================	==================
         3	    28	  88.1	    33	GGTGGCCCCATGCGCGTGGGGGAGGACC	                                 	       

# Left flank :   AGGCATCTTTTGGCTGGCGTGCGGTCACCATCAGGTTGGCGCCAAAGATAAACAGGATGGCGCAGATTGCAGGCAGGATGCTTTGTACCAAATAACGCTGTAAACGTCGCATCAAGACTCCTTGAACACAAAGAAAACATACATGAAAGATACAGGTTTTCTATTGCAATTGCAAGACCTGTGTGCTACGATGTGTTTTCAGAATAACCCAAAGGAGCCATCATGTATCTTCGTCCGGATGACATTGCGCTGCATGCGCCAGTGCATGAACAGTGGCAATATGCGCTGGCTAATACCTTCAGAGGCCAAGTGAGCGATGTGCGCCATTTACATGTAACAGCATGGTGCGATCCATCAGGTGTGCTGCATATGCGTGCGACATCCGAACAAGGCCAATGGGTAAACCTCAATGTGCCGCCGACGGACTGGGGACTGATAAAGGCATCGTGAAAGGATGCTGGCTGGTTTATTTGAGGATTTTCAATGACTTACACCCTAGA
# Right flank :  G

# Questionable array : NO	 Score: 1.76
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.40, 5:0, 6:0.25, 7:0.02, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTGGCCCCATGCGCGTGGGGGAGGACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [3,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-7.50,-9.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,0.78   Confidence: HIGH] 

# Array family : NA
//

>k141_973 flag=1 multi=4.0000 len=1227		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                              	Spacer_Sequence                                             	Insertion/Deletion
==========	======	======	======	=============================================	============================================================	==================
         1	    45	 100.0	    60	.............................................	GATGGCCGAATTAGCCGAAGCCCAGCGGCGCACCGAAGAGCGGGTATCCCGCCTGGAAAC	
       106	    45	  95.6	     0	.................................C.....A.....	|                                                           	
==========	======	======	======	=============================================	============================================================	==================
         2	    45	  97.8	    61	CGCCCTGGCGGAACTGGCCGAGGCCCAGCGGCGTACCGATGAGCG	                                                            	       

# Left flank :   |
# Right flank :  GCCTCTCCGGCATGGCAGAGCGCCTCTCCCGCCTGGAGGCCGCCGTCCAGTCCCTGATAGAGCAGGTGGATAGATTAGTGGGCTGGCAGCAGGGGGAATCCAGACGCCGGGAAGGGGAGCAGTACGAGCGGCTCCTGGTCAAACGAGCCCCGGCTCTGTTCAACCGTGGCACCGGTGGCGCGACTGACAACCCCTTTGTCCAGCAGTGGTTGTCTGAGAAACTCCAGCATCTGCTGGCCGACGGGGAACTCTCCGATGAGGAGAACCCCTTCCTGGCCGACCTGATCTGGCGGAAAGGGGAGCAGGTCCTGGTTGTAGAGGCATCCATCCACGTGGACCGTCGGGACGTCCAGCGGGCCGCCCGGCGGGCAGAAGTCTTGCGTAGGGCTGGCCTGCAGGCCAGGGGAATCGTCGTCGGGGAGGCATGGTTTGGCCCGAGTGTGCGGGAGCAGGCACTGGCCCAGGGCATAGAGTGGAAAGTGGGGGACGACTGCTCCGAG

# Questionable array : YES	 Score: 0.14
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.89, 5:0, 6:0.25, 7:-1.2, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CGCCCTGGCGGAACTGGCCGAGGCCCAGCGGCGTACCGATGAGCG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [7,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-10.20,-9.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.15,0   Confidence: HIGH] 

# Array family : NA
//

>k141_8452 flag=0 multi=21.0000 len=2093		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                  	Spacer_Sequence                                    	Insertion/Deletion
==========	======	======	======	=================================	===================================================	==================
       940	    33	 100.0	    51	.................................	GCCGCGAACGAAGAGCTTCAAAGCTCAAACGAGGAGCTACAAGCTACAAAC	
      1024	    33	  93.9	     0	..A..........................T...	|                                                  	
==========	======	======	======	=================================	===================================================	==================
         2	    33	  97.0	    52	GAGGAGCTTGAGACATCAAACGAGGAGCTACAA	                                                   	       

# Left flank :   CGAGACAAAAAAATAGGCAAAAAGAGGGTGCGAGAGATAAAAAAGGTATAAATATTGAAGAGATAATCTTGTCTGTTTTATATAATGAGTTTGTTCAAAAATGTGTCGTAATTGACGAAAATACAAATCTTCTCTTTATAAAAGGGAATCTAGGCGGGATAGTGTCGTTGCCAGAAGGGGCGGTAAATTTGGCGTTTAATAAGATGATTTGCGATGAGCTATCGGTTGATTTTCGCTCGCTTCTATTTAAAGCCTCAAAGAGTGGCGAGCCTCAAAGGGGGAGGCCAAAAACGGTAAATATAGCAGGCGAAAGGCGAAAAATTGCAATATCTATATTTCCTCTAAGAGCCTATATCAGCGGATATTTGGTTGTTTTTGATGAGTTTGCGGCAGATGAAGAGCAAAGCTCGCAAACAGATGAGGGGTTAGTTCTTAGCGCTAGATACAAAGAGCTAGAAGAGGAGCTAAATGTAACAAGAGAGCATCTGCAAACCGTTATA
# Right flank :  AAGCTCAAACGAGGAGCTACAGACTGCCTATATCGAGCTTAAGATGGCGTACGAAGAACAGGCAAGGCAAAGAGAGAGCCTTGCAGAGGCAAATAGAGAGCTTGTAAAGCTAAACGAGGAGTTAAGCTCCAAAGAGAGATTTATAAACGCTCTTTTAGATGCAGAGCAAGCAATAGTTATCGTAACTAGGCGGGGCGAAGAGATATTGGATGCAAATGGGGCCTTTTTTAGGTTTTTTGATGAGTATGCTTCGCTGGATGAGTTTAAAGCGTTCCACAAATCCATTTGCGAGATGTTTGAGCCGGTTGAGGGGGATGAGGAATTTTTGCCGCCCGGATACTCCAAGAGAGATGGCAAAAGCTGGCTTGAACTTGTAATGAGCAATACCAAAAAGCGCTACAAAGCGCTAATCAAAAAAAATGGAAAAGGGTATGTTTTTTCGGTCCGTGCAAGCGACCTTGATGTAGCAAACGAAAAATATGTAGTAATACTAAACAACA

# Questionable array : YES	 Score: 1.00
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.85, 5:0, 6:0.25, 7:-0.3, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GAGGAGCTTGAGACATCAAACGAGGAGCTACAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:51.52%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.50,-3.70] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [61.7-51.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.68,0.37   Confidence: LOW] 

# Array family : NA
//

>k141_8884 flag=0 multi=23.0156 len=1420		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                     	Insertion/Deletion
==========	======	======	======	=============================	====================================	==================
      1283	    29	 100.0	    36	.............................	CCCCTCAGCATCTTCGATTTTTACATCCCACCCAAA	
      1348	    29	 100.0	     0	.............................	|                                   	
==========	======	======	======	=============================	====================================	==================
         2	    29	 100.0	    46	ATTCGAACAACCCCTATGGGGATTGAAAC	                                    	       

# Left flank :   GCTATTTGCACGAACCAAACTACAAAAGTTTGAGTCTTCATCTTGACTTAGCAGAAATATTTAAGCCTATCATTGGCGATAACATGATATTTTCTGCCTTAAATCGTAACGAAATAACGGCAAAAGATTTTGAGAACGACGGAGGTAGATTGAGGCTTACCAAAGATGCAATCAAAAAGATAGAACTAAAATTTATCGCCAAAATGACCGAACAGATAGAGGTGGCAGGTACAGGCGGACAGATGCTAACTTGGCGGCAAATCATTCGCAGAGAGGCAAATAGACTCAAAAAATGCATATGTGAAATGAGCGAATATGAACCGTTCGGAGCGTGACGATAAAGCTTTTTTAAAGCTTTAACATAAACTCATAAATCCCCACTAGCATCGTAATTTCGTAACGGCATAATGTAGTTTAGCTAGTCTGGTTAAGCTGTTTTTAAACTAAAAAACAAAACAAAAAAATCCTCTTAAGCAATGTTTTAAGGGCTTTTTGCTAGG
# Right flank :  TGTGATAAGGGAGTTGAATAATGAAAGAGTGCCTTATTCGAACA

# Questionable array : NO	 Score: 5.15
# 	Score Detail : 1:0, 2:0, 3:3, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATTCGAACAACCCCTATGGGGATTGAAAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         F Score: 4.5/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:55.17%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-6.00,-5.90] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [71.7-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [4.77,0   Confidence: HIGH] 

# Array family : NA
//

>k141_1003 flag=1 multi=12.0000 len=1340		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                                                                                                                                              	Insertion/Deletion
==========	======	======	======	=============================	=============================================================================================================================================================================	==================
       566	    29	 100.0	    32	.............................	GCGCGGTCGTCCACCCTTGCGTCCGCGTGCAC                                                                                                                                             	
       505	    29	  75.9	    32	.TCAG.......T..........AA....	CGCAGTGGCCAGAAGGGCCGGAGATGGGGATG                                                                                                                                             	
       444	    29	  82.8	    26	.TCAG.......................G	CTGGATCGCCTGACGCTGCTGGCTTG                                                                                                                                                   	
       389	    29	  75.9	    32	A.........CCTA..........A...G	TCTCGGTCTCGCTCATAGCTGATGCCCCTTTT                                                                                                                                             	
       328	    29	  82.8	    32	.TC.G............A.........T.	ATCCGCTCGTCCTCGCGCACACCGCCGCGCAA                                                                                                                                             	
       267	    29	  89.7	   173	.T..G.......................A	TACCCCGTGTTAGCCTTGCCTATACAGGGGACCGCCAACTGGTGAACGAAAAAGAAGTTGCTTTCATAGTTGCATCAAGATTGGTTTGCTGTTATCATTCAACGCACGCCCAAAAAGAGATTATCAGGTCCGAAAGTAAGAAGTCTTGATTTTTCAAAGAATTGCACCATAGA	
        65	    29	  86.2	     0	..........CAT...............T	|                                                                                                                                                                            	
==========	======	======	======	=============================	=============================================================================================================================================================================	==================
         7	    29	  84.8	    55	GGTGTCCCCGTGCGCGCGGGGGAGGACCC	                                                                                                                                                                             	       

# Left flank :   CTGATGGCTCGAAAATAAAGCGCTGGTTTCCAATATCTGCGTACCTTTTCATGGATAAGGACGGTCGAGTCTGGGCCGACTTGATTGATACGGTCAAAGGAATAGACGAGAAGCTAGAGATAGATTTGGCAGCGAGAAGAATCCTTTGCTGGAGACTTTCACCAGCAGCAGATGGCGAGAGCCTAATTGAAGTGCTCACCTCAGAAAAGGTCAAGTCTTTGCTAGAGCGTGTGCATGCAGGACACGCCGCCGCTTACAACAATCTTTTTTGGTGCAGATATCTCGGCGATGATGCAGAAGCGGCTTACAAAGAACTCAAAGAATATTTGAATTGCGATTCACTCTGTTTTACGCCTTATAAAAAGCGCGACCTTCCCATGACAATTCTTAGTCGTAGTTCGCGCAAATTTGCTCATGAAAATCAGACGCCCCCAATGAGTCAAGACAGCGATGATTCGCAATCTTTTTCTCCATGCTCATGAATGAGTTTTCTGTTTGAA
# Right flank :  TCGGCGCACTTTGGGGTCAGCAGCGCACCCAGCGGTG

# Questionable array : YES	 Score: 0.80
# 	Score Detail : 1:0, 2:3, 3:0, 4:0.24, 5:-1.5, 6:0.25, 7:-2.53, 8:1, 9:0.34,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTGTCCCCGTGCGCGCGGGGGAGGACCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [1,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         R [matched GTGGTCCCCGCGCACGCGGGGGTGGACCC with 87% identity] Score: 4.5/4.5
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [3-8] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [18.3-60.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,5.14   Confidence: HIGH] 

# Array family : I-E [Matched known repeat from this family],   
//

>k141_1003 flag=1 multi=12.0000 len=1340		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                                                                                                                                              	Insertion/Deletion
==========	======	======	======	=============================	=============================================================================================================================================================================	==================
       566	    29	  89.7	    32	.G..T.......................C	GCGCGGTCGTCCACCCTTGCGTCCGCGTGCAC                                                                                                                                             	
       505	    29	  79.3	    32	..CA........T..........AA...C	CGCAGTGGCCAGAAGGGCCGGAGATGGGGATG                                                                                                                                             	
       444	    29	  89.7	    26	..CA........................G	CTGGATCGCCTGACGCTGCTGGCTTG                                                                                                                                                   	
       389	    29	  69.0	    32	AG..T.....CCTA..........A...G	TCTCGGTCTCGCTCATAGCTGATGCCCCTTTT                                                                                                                                             	
       328	    29	  86.2	    32	..C..............A.........TC	ATCCGCTCGTCCTCGCGCACACCGCCGCGCAA                                                                                                                                             	
       267	    29	 100.0	   173	.............................	TACCCCGTGTTAGCCTTGCCTATACAGGGGACCGCCAACTGGTGAACGAAAAAGAAGTTGCTTTCATAGTTGCATCAAGATTGGTTTGCTGTTATCATTCAACGCACGCCCAAAAAGAGATTATCAGGTCCGAAAGTAAGAAGTCTTGATTTTTCAAAGAATTGCACCATAGA	
        65	    29	  79.3	     0	.G..T.....CAT...............T	|                                                                                                                                                                            	
==========	======	======	======	=============================	=============================================================================================================================================================================	==================
         7	    29	  84.7	    55	GTTGGCCCCGTGCGCGCGGGGGAGGACCA	                                                                                                                                                                             	       

# Left flank :   CTGATGGCTCGAAAATAAAGCGCTGGTTTCCAATATCTGCGTACCTTTTCATGGATAAGGACGGTCGAGTCTGGGCCGACTTGATTGATACGGTCAAAGGAATAGACGAGAAGCTAGAGATAGATTTGGCAGCGAGAAGAATCCTTTGCTGGAGACTTTCACCAGCAGCAGATGGCGAGAGCCTAATTGAAGTGCTCACCTCAGAAAAGGTCAAGTCTTTGCTAGAGCGTGTGCATGCAGGACACGCCGCCGCTTACAACAATCTTTTTTGGTGCAGATATCTCGGCGATGATGCAGAAGCGGCTTACAAAGAACTCAAAGAATATTTGAATTGCGATTCACTCTGTTTTACGCCTTATAAAAAGCGCGACCTTCCCATGACAATTCTTAGTCGTAGTTCGCGCAAATTTGCTCATGAAAATCAGACGCCCCCAATGAGTCAAGACAGCGATGATTCGCAATCTTTTTCTCCATGCTCATGAATGAGTTTTCTGTTTGAA
# Right flank :  TCGGCGCACTTTGGGGTCAGCAGCGCACCCAGCGGTG

# Questionable array : YES	 Score: 0.80
# 	Score Detail : 1:0, 2:3, 3:0, 4:0.24, 5:-1.5, 6:0.25, 7:-2.53, 8:1, 9:0.34,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTTGGCCCCGTGCGCGCGGGGGAGGACCA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [1,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         R [matched GTGGTCCCCGCGCACGCGGGGGTGGACCC with 87% identity] Score: 4.5/4.5
# 	Secondary Structural analysis prediction:  NA [0.00,0.00] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [3-8] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [18.3-60.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.41,5.14   Confidence: HIGH] 

# Array family : I-E [Matched known repeat from this family],   
//

>k141_11475 flag=0 multi=19.7691 len=4623		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                              	Insertion/Deletion
==========	======	======	======	==============================	=============================================	==================
       282	    30	 100.0	    45	..............................	TTAAGCTCCGAGCGTACCTGCTCCACTTCCAGCCGTACCTGCTCG	
       207	    30	 100.0	     0	..............................	|                                            	
==========	======	======	======	==============================	=============================================	==================
         2	    30	 100.0	    46	ATCTCCTTGGTGAGCTTAAGCTCAGTCTCC	                                             	       

# Left flank :   ATTCGGTCCGGATGACGGCGCGCGGTCTCGATTACGCCGCAGGCCGAGGCATTGATGACGGCGGTCACCCCGCCGGATTGGGCATAGAAGGCGTTCTTGGCTGACATACAGGTGTCCTCGATGCGTTTTTAAGTAGGGATGATCAAGGTGGATGCGCTTAAGCCGATCTTAGAACGCGCTGAGTAATTCGTCATGCCCAAGCGGGCATCCAGGGACCTGATGCGACTAGACTTGCGCTGTCGCGAGAGTGACGAAACCCCAATGGATCAGCGCGTCCCTAAACCCCTCTCCTGCAAGGGCTGGGTGTGAGGGGCCAAGCACCCAACCAGAGACGACCTGGTCCCCCGGCAGGCCGGTCACCGCTCTATGGCGTCGGCCACAGCCGCCACAGGAGCGTGAGCAATACCGCGAGCTGACTGATCCAAAAGCCAAAGCTCCACAGCAAGATGTTGCTGCGCGCGCGCTCGATATCGCGGCTTAAGTCGGCACGAACCTGAGCA
# Right flank :  CGACAGGTGCGTGCGCGTGGCCAGCTCCGAGATGTGCGGATAGCGCGTCTCCAGCGCCTCGAATGCCTGCGCTAGGATGCGCGCCCGCGTGCGCTCATCCGGCGCATTGGCCAGTTGATCGTAGAGGTCGACGGCTGAGGCCACCATAACGCACTTGTCCTCTCAAAGAGGGTCTTGG

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATCTCCTTGGTGAGCTTAAGCTCAGTCTCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [10,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-5.50,-6.10] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [30.0-40.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_13375 flag=1 multi=62.1170 len=7541		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                                         	Insertion/Deletion
==========	======	======	======	============================	========================================================	==================
       995	    28	 100.0	    56	............................	AAACAGCGTGCTGGACGCATAAAACGCTGTGCGGGTTTGCCACACCCCAAACCCCG	
       911	    28	 100.0	     0	............................	|                                                       	
==========	======	======	======	============================	========================================================	==================
         2	    28	 100.0	    57	AACATCCCGACACATCCCGAAATCCCGA	                                                        	       

# Left flank :   ACCCCACTTTTATACCTGATTATAAGAGGGGCGATGCGTCTTTTGTCAAGCGGTGGTGTTTAACCTCTATGCGCACAGCTTTGTATCCTTCGTACGGTCAAACCCGCGGCGGGCTCGAGGGTGTTTGTTGTTCTCCCCGCTTTCCGCTATAGACGCCCAGTGGGTGTTCAAGCGCCGGTTTTGATGCCGGCCCGCCTTCGGTGGGGCGCCGGATCTCTCGGAGAAAGCGCGGGGTCCAAGCGCCCCCATCTTGGGTCACACCTCGAGGAAACGGCGCCCCCCACCCCGGCTTAAGTGGTAAACCGCGCAACGCCCCTGCGGTATGGGCCGGCGGGATGTTCTTTCCCAACATCCCGCCGGCGTTTTTATCGTCTGGGACGCAATTTTCGGGATGTCGGGATTTGTCGGGATTTTGTTTTTACCTAAAAGGGTAAAGAATGGGTAGCTAAAAAAGCGATACCCCGAGAAGACGTCTAACCCCAACGGAAGTTTTAAAGAAC
# Right flank :  CAGGCGCCCGGCACCCCTCCTTGGTATAGGCGGCCGCCTCGAGGATGGTGGGTATTGAGGGGTTATGTGAACGCACGCGTCAGTGATGTATCCTCCCCGGATGAGCCGCTGGGGAAGTTCCGTATCCCGGTCGAATGGGACCCTGAGGCCAGGGTCGTCTTCGTCCCGGTCCCTCCCGTTGCTTCCCGAAGGGTACACCTTGAGGACCCCGAGCGCACCCATGCCCTCCCCCTCCTCAGAGGTGCCCTCGAGGCGTGCATCGGGTTTGGGGAGGATGGACTCCTCGAGTTCTATGGGGAAAAGAGAAAACCCTACAGGGGAGGAGACCCGTAGGGTCCAGAAAGGTGGGGGGCCTATGGCGAACGAACTTCAAGGGCCATCTTAAGGTGTTCCCTACGAGGTACGCTACCGCGTGACCGGGAAGCAGCTAGGTCCTCCCTCCGTGTGGGGGGTGTCCAATGGTTCGTCCATTGGACGTCCAACTCTCCCCGTCCAACCGT

# Questionable array : YES	 Score: 1.34
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:-0.8, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AACATCCCGACACATCCCGAAATCCCGA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,10] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-1.60,0.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [31.7-55.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.64   Confidence: LOW] 

# Array family : NA
//

>k141_17654 flag=0 multi=305.5178 len=1155		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                  	Insertion/Deletion
==========	======	======	======	=============================	=================================	==================
      1042	    29	 100.0	    33	.............................	CACGGCGTGGTTGGCGTGGAAGACCGTTTGCTC	
       980	    29	  96.6	     0	............................G	|                                	
==========	======	======	======	=============================	=================================	==================
         2	    29	  98.3	    34	GTAGTCCCCACGCACGTGGGGATGGACCT	                                 	       

# Left flank :   CATCGGCTTAGCGCCTTCGGCCGTTCCTCGCGGGGTGACCCGTCTTTTTGGACTGTTGCCCGATAGGACACATAACCGGGGGGTAGGTGAGGTTTTTGCCGTCCCCGTCATG
# Right flank :  ATTACGCCCAGCATGACGGTCTGGTAGGAGCCTCGGCCCCAGGGGCCCTTCTTCCCGTCCCAGGAAAGAAACCCCACCTCGAGGACGTCCCCCGTGCGCAAGAGGCTCGTGACGGGCTTCCCTCGGATGTAGAGGGGGAGGCTCAGGCTCCAGGTGCCCATGCCCTGAACGGGGTGGCGGTCAGCATAGCTCAGCACGTAGTCCGAAAGGTCCAGCGCCCCCCCGTCCGTGTGGGCGATGGCCCGGATGTGGGCCTCAAACGTCCCCATCCCCCTTAGAATGCCTAGGGATGAGGGTCATCCTCTTGGGGCTGGCGCTCCTCCTCGCGGGGTGCTTCCAGGACCCCATCGCCCAGGTCAAGCGCCTCTTGGGGCAACACGTGTCCCCTGGCGCTGCCGAGGAAGAGGCCTACCGCATGGCCTGCGCCTTCGCCTTCACGCAGAGCGAGACCATGCAGGAGAAGCTCCGCCAAATCGTGGGGGCGGGGGGCATCCTTCCCG

# Questionable array : NO	 Score: 4.37
# 	Score Detail : 1:0, 2:3, 3:0, 4:0.91, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTAGTCCCCACGCACGTGGGGATGGACCT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [4,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         R [matched GTAGTCCCCACGCATGTGGGGATGGCCCG with 96% identity] Score: 4.5/4.5
# 	Secondary Structural analysis prediction:  NA [-11.10,-11.10] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: R [33.3-43.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0,5.14   Confidence: HIGH] 

# Array family : I-E [Matched known repeat from this family],   
//

>k141_17632 flag=1 multi=22.9549 len=1028		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                              	Spacer_Sequence                           	Insertion/Deletion
==========	======	======	======	=============================================	==========================================	==================
       787	    45	  86.7	    42	....C.....C....G...A.....T...............C...	GGTATGGGCGGTGGGATTGAAAACGGCGGGCTTTTTGACACG	
       874	    45	 100.0	    30	.............................................	AGCGGTTGGGGCGGTGGGATTGACAACGAT            	
       949	    45	  86.7	     0	..C...........CG..A.............C........C...	|                                         	
==========	======	======	======	=============================================	==========================================	==================
         3	    45	  91.1	    36	GGTATCCTGATCCTTAAGGGCTCCACCGTGAGTGGAAACAGGGCA	                                          	       

# Left flank :   AGACCCGTCAAGAACTTACCATCTCCACCACCTGGGATACCCCGCTGGAGCAGCACACCCTCACCCTTAAGGCCAAAGCTCCCTGGAGCAGCATTGAGAGGACCACCAAGTTGACCCTTAACGTCAACCAGCCTTCCACCTACAACGTTAACGCAACAGACGATACGGTTGACGCTAGCCCCGGTGATGGCCGATGTGCTGACTCAGACGGCAAATGCTCCCTACGGGCGGCCATCATGGAGGCCAATGCCCGCGGCACCCCGGCGGTGATCAACATCCCGGCTGGCACCTACACCCTCACCAGCGAGGCGGATGGGGATGGGAGCGACCTGGACATCGAGAGCAGCATTACCCTCAAGGGAGCCGGAGCGGACAAGACGATTCTGGACGGAAACGGCACCGGCCGGGTCTTGTACATTTACGAGGGAAGCATAACGAAGGTAGAGGGGGTCACGATCCAGAACGGTAAGGACATAATCGGCGGTGGGATTTACAACGAG
# Right flank :  AGCGGTTGGGGCGGTGGGATTGACAACGCAGGCAC

# Questionable array : YES	 Score: 0.72
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.55, 5:-1.5, 6:0.25, 7:0.02, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTATCCTGATCCTTAAGGGCTCCACCGTGAGTGGAAACAGGGCA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [11,9] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-7.90,-7.70] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [6-6] Score: 0/0.41
# 	AT richness analysis in flanks prediction: F [46.7-20.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.64,0   Confidence: HIGH] 

# Array family : NA
//

>k141_18829 flag=1 multi=14.8341 len=4440		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence            	Spacer_Sequence                                                                                                                                                                	Insertion/Deletion
==========	======	======	======	===========================	===============================================================================================================================================================================	==================
       877	    27	  70.4	    66	.CA......TGTT......A...G...	CATCTCGGTCTTGCTCAACACAAGAATGCGCAATAGCTTGTTCTGTTCAGCAGTTCAAGCCCTTTT                                                                                                             	
       970	    27	  81.5	    26	..A.......GTG..........G...	CGTATATCTAAGGTGGCGTAGCCCCG                                                                                                                                                     	C [971]
      1024	    27	  81.5	    24	.......T.TG.G..........G...	CTCGAGAGCCGCCAGAATCGTGTT                                                                                                                                                       	Deletion [1075]
      1075	    27	  81.5	   175	...G.....TG.G..........G...	ATACCCCGTGTTAGCCTTGCCTATACAGGGGACCGCCAACTGGTGAGCGAAAAAGAAGTTGCTTTCATAGTTGCATCAAGATTGGTTTGCTGCTATCATTCAATTCACGCTCAAAAAGAAATTATCAGGGACCGAAGTAAGAGGTTCTGATTTTTCAAGGGCTTGCACCATAGAG	C [1076]
      1278	    27	 100.0	    34	...........................	TTGTGGATAACCCTCCGCGGCCGCACGATTGTGG                                                                                                                                             	
      1339	    27	 100.0	    34	...........................	ATTGAACAAGATGCGGACTGCGTGATTTTCATTG                                                                                                                                             	
      1400	    27	 100.0	    12	...........................	CAAATACCCTAG                                                                                                                                                                   	Deletion [1439]
      1439	    27	  96.3	    35	.......................G...	CGAGCGACCTCCGCCATTGGCAAACCGGGCATGTG                                                                                                                                            	
      1501	    27	  77.8	    29	..........GTGT.........GG..	CAGAAATTGGCGCGGCGGGATTTTTTTGT                                                                                                                                                  	
      1557	    27	  74.1	    26	CG.C.....TG.G..........G...	CCTACTACAAGGCGATCTTCACTCAG                                                                                                                                                     	
      1610	    27	  81.5	    36	A..G.......TG..........G...	CGAACGGAGGGGCCGGAGCTGGAGCAAGAGCCGGAG                                                                                                                                           	C [1612]
      1674	    27	  77.8	     0	..........T.G........TTTC..	|                                                                                                                                                                              	
==========	======	======	======	===========================	===============================================================================================================================================================================	==================
        12	    27	  85.2	    45	GTGTCCCCGCACACGCGGGGGAGAACC	                                                                                                                                                                               	       

# Left flank :   AAAATCCGCGCTCTTGGTGATTGATGTACAAAAAGGCTTTATCAATGAGCACACCGCCCACATTCCTGCTTTGGTTCAACAGCTTTTGCCATCCTATGAAACCATCTTCGCTACCCGTTTCTTTAATCCAGAAGGCTCAAACTATCGACGGCTTTTGAAATGGTCGCGATTTTCTGAAGGAAGCGAAGATACGGAATTTGCTTTCGAGCTGACCCCGAACGTGAGGGTGATCGACAAGACTGTTTACACTTGCGTCAATCCGCGATTTCTTGCGGCGCTGGATGAATTGAGTATTGAGAATGTTCATATTGTTGGCGTCGATACGGATGCATGCGTAATGAAGTGTGCCGTGGACTTGTTTGAATACGGGCGAACGCCTTTTGTGCTAGCTAAATACTGCGCCAGTTGTGCGGGGCCCGGCATTCACCAGGCCGCCCTTCAGATCATTTCAGGGCTAATTGGTGAGGGGCAAGTTATCAGGTAGAAATAACGTCGCCCCA
# Right flank :  CACTTTGCCCCCTCCCTCACATATCCGTTTGCCCATCATTTTTTTGTAAAGGAGAACTCATGTCACAAGACAGCCCCTTTCCCACCGTCAACAGCATCATGACGGTTGACCGCTTTCATGCGCTGGGACTGCATTTGCGCGATGTGCAACTCCAGGTTGCGCGCGACGTGGAAAAAGTCCTTGCCCACAAGCCTCTGGAGCCCAAGGTCATTGCGTTGCAGTCCGGTGAGAACGCGGAACTGCGCCGAAGTGTTGTGGGCTTGATCGAAGCGGGCACCGGAACAGGCAAAACCCTGGGATACCTGGTGCCGCTGCTGCTGTACCAAATGCGCACCGGCAAGCAAGTGGGGGTGAGCACCTTCACACGTGCCCTGCAGCGCCAAATCGCTGCAGACGACCTCCAAAAGGCGCTTGCGCTTACCGGCTCTCAGGCACGCGCAGCACTTCTGTTGGGCCGCACCAATTTCATCTCTCGCAGTCGTGTTCTTGCCTTGCATGAG

# Questionable array : YES	 Score: 0.36
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.33, 5:0, 6:0.25, 7:-1.67, 8:1, 9:0.45,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTGTCCCCGCACACGCGGGGGAGAACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [5,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-9.30,-9.10] Score: 0/0.37
# 	Array degeneracy analysis prediction:      F [17-26] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [55.0-56.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0   Confidence: HIGH] 

# Array family : NA
//

>k141_19125 flag=0 multi=8.8626 len=1160		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                  	Insertion/Deletion
==========	======	======	======	============================	=================================	==================
       970	    28	 100.0	    33	............................	TGGTGTCACCCCCTCGCACGCCGCGCTTGTCGC	
      1031	    28	 100.0	    33	............................	CACGTCCGCACCCCAGAAGCCTGGGTCGATGTG	
      1092	    28	  96.4	     0	..T.........................	|                                	
==========	======	======	======	============================	=================================	==================
         3	    28	  98.8	    34	GTCGGCCCCGTGCGCGCGGGGGAGGACC	                                 	       

# Left flank :   AGCCCAAAATTCAGACCCGCTCTACGCAAGAGCCATCGCCCTTGAAGGAAAAGGGGCTGTACCTCATGTCGTGCCGGTTTGCCCCGATCAAGCGTCGCAAACATATGGAAATACCCGTGCCTATGGGGCAATTTCAGGATTGGCTGCGCGATTTGCTGAGGCGCAATGGCATGACCCTGGTTCGCGTGGATGCAGCGCGCCCCTACGTCATGGCGCTTGGACGTCGGGGTGAGCAAAAAAGTCCCAAGGAAAACTTGATGGGGTCAATACTTCGCACCGTAAAAGCCCGCTTCGTGGTCTATGTCCACGACGCATCCCTGGCAGGTCACGCTTACCAATATGGCATTGGCCGACACAAAGCGTGGGGGTGCGGCATGATCCATGTGCTCGATGGTGTTGAGCCCAATAGGGGATAGATAAGGTTGCATTTGCGGTTCATCTCTGCGCCCTGTAATCTCCGCCCCATGGTTCTATATTGATTTTCAACGACTTACGATTGA
# Right flank :  CAGTACCAAAAGGCCGGCGAGTCCAGCTGTTAGGTTGGCCC

# Questionable array : NO	 Score: 2.30
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.94, 5:0, 6:0.25, 7:0.02, 8:0.4, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GTCGGCCCCGTGCGCGCGGGGGAGGACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [2,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-10.50,-8.80] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [55.0-26.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.05,0   Confidence: HIGH] 

# Array family : NA
//

>k141_18829 flag=1 multi=14.8341 len=4440		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                                                                                                                                              	Insertion/Deletion
==========	======	======	======	=============================	=============================================================================================================================================================================	==================
       876	    29	  72.4	    65	A.CA......TGTT......A........	ATCTCGGTCTTGCTCAACACAAGAATGCGCAATAGCTTGTTCTGTTCAGCAGTTCAAGCCCTTTT                                                                                                            	
       970	    29	  79.3	    24	.TCA.......GTG...............	GTATATCTAAGGTGGCGTAGCCCC                                                                                                                                                     	
      1023	    29	  86.2	    23	........T.TG.G...............	TCGAGAGCCGCCAGAATCGTGTT                                                                                                                                                      	
      1075	    29	  75.9	   173	.TC.G.....TG.G..............A	TACCCCGTGTTAGCCTTGCCTATACAGGGGACCGCCAACTGGTGAGCGAAAAAGAAGTTGCTTTCATAGTTGCATCAAGATTGGTTTGCTGCTATCATTCAATTCACGCTCAAAAAGAAATTATCAGGGACCGAAGTAAGAGGTTCTGATTTTTCAAGGGCTTGCACCATAGA	
      1277	    29	  93.1	    32	........................A...T	TGTGGATAACCCTCCGCGGCCGCACGATTGTG                                                                                                                                             	
      1338	    29	  93.1	    32	........................A...A	TTGAACAAGATGCGGACTGCGTGATTTTCATT                                                                                                                                             	
      1399	    29	  96.6	    10	........................A....	AAATACCCTA                                                                                                                                                                   	Deletion [1438]
      1438	    29	 100.0	    33	.............................	GAGCGACCTCCGCCATTGGCAAACCGGGCATGT                                                                                                                                            	
      1500	    29	  82.8	    27	...........GTGT..........G...	AGAAATTGGCGCGGCGGGATTTTTTTG                                                                                                                                                  	
      1556	    29	  75.9	    25	TCG.C.....TG.G...............	CTACTACAAGGCGATCTTCACTCAG                                                                                                                                                    	
      1610	    29	  79.3	     0	ATC.G.......TG...............	|                                                                                                                                                                            	
==========	======	======	======	=============================	=============================================================================================================================================================================	==================
        11	    29	  85.0	    44	GGTGTCCCCGCACACGCGGGGGAGGACCC	                                                                                                                                                                             	       

# Left flank :   AAAAATCCGCGCTCTTGGTGATTGATGTACAAAAAGGCTTTATCAATGAGCACACCGCCCACATTCCTGCTTTGGTTCAACAGCTTTTGCCATCCTATGAAACCATCTTCGCTACCCGTTTCTTTAATCCAGAAGGCTCAAACTATCGACGGCTTTTGAAATGGTCGCGATTTTCTGAAGGAAGCGAAGATACGGAATTTGCTTTCGAGCTGACCCCGAACGTGAGGGTGATCGACAAGACTGTTTACACTTGCGTCAATCCGCGATTTCTTGCGGCGCTGGATGAATTGAGTATTGAGAATGTTCATATTGTTGGCGTCGATACGGATGCATGCGTAATGAAGTGTGCCGTGGACTTGTTTGAATACGGGCGAACGCCTTTTGTGCTAGCTAAATACTGCGCCAGTTGTGCGGGGCCCGGCATTCACCAGGCCGCCCTTCAGATCATTTCAGGGCTAATTGGTGAGGGGCAAGTTATCAGGTAGAAATAACGTCGCCCC
# Right flank :  CGAACGGAGGGGCCGGAGCTGGAGCAAGAGCCGGAGGTGTCCCCGCTCGCGCGGGGGTTTCCCACTTTGCCCCCTCCCTCACATATCCGTTTGCCCATCATTTTTTTGTAAAGGAGAACTCATGTCACAAGACAGCCCCTTTCCCACCGTCAACAGCATCATGACGGTTGACCGCTTTCATGCGCTGGGACTGCATTTGCGCGATGTGCAACTCCAGGTTGCGCGCGACGTGGAAAAAGTCCTTGCCCACAAGCCTCTGGAGCCCAAGGTCATTGCGTTGCAGTCCGGTGAGAACGCGGAACTGCGCCGAAGTGTTGTGGGCTTGATCGAAGCGGGCACCGGAACAGGCAAAACCCTGGGATACCTGGTGCCGCTGCTGCTGTACCAAATGCGCACCGGCAAGCAAGTGGGGGTGAGCACCTTCACACGTGCCCTGCAGCGCCAAATCGCTGCAGACGACCTCCAAAAGGCGCTTGCGCTTACCGGCTCTCAGGCACGCG

# Questionable array : YES	 Score: 1.28
# 	Score Detail : 1:0, 2:3, 3:0, 4:0.25, 5:-1.5, 6:0.25, 7:-1.92, 8:1, 9:0.20,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTGTCCCCGCACACGCGGGGGAGGACCC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [4,1] Score: 0.37/0.37
# 	Reference repeat match prediction:         F [matched GTGGTCCCCGCGCACGCGGGGGTGGACCC with 93% identity] Score: 4.5/4.5
# 	Secondary Structural analysis prediction:  NA [-2.70,-2.50] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [19-13] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [51.7-26.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [5.14,0.41   Confidence: HIGH] 

# Array family : I-E [Matched known repeat from this family],   
//

>k141_22620 flag=1 multi=3.0000 len=1035		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                       	Spacer_Sequence                     	Insertion/Deletion
==========	======	======	======	======================================	====================================	==================
       915	    38	 100.0	    36	......................................	AACCGCCTTCTCCAGGCGCTCAGCATCGAGCCGGCA	
       989	    38	  94.7	     0	.............................C...A....	|                                   	
==========	======	======	======	======================================	====================================	==================
         2	    38	  97.3	    37	AGCGTCAATGCGCGGATACCCAGCCCCGCAAGGCGCAA	                                    	       

# Left flank :   TCTGTGATGACGACAAAAGTATTATCGCTCTGTCTTTCTATTCCAACAGTTGGTGTTGGGGTGGCGGTGGGCTCAGGAGTGGGTGTTGGTATTGGTGTTTCGGTTGGGATTGGAGTTAGAGTTGGGGTTGGAGTTGGGGTTGGAGTTGGTGTTGCTGGCGGTTCTGTTCTAACCCCGCAGGCGGTTAAGAGGAAAACGAACAGGACGACGCCAAGCCATCCTTTACGAGAGGGTGGATACGGGTGTCTTTCGTTCATATTCAACCTCCGTTTTTCAAGAATGGGGCAACCGCTCAGCCGGGCGCCCCATTGACAGCAGAGAACGCGGAGTTCACAGAGATTTCCCTTGATTTCTCCGCCTTCTCAGCGATCTCTGCGGTGAGATCCGAGGAGACCTGAGCAGTGACAGAATGGGCCTCCTTGACGCTGCCGCTTTGCGCCAGGGAGCGCCATCCCGCCAGAGTTTCGCGAGCGACTGATAGTATATGGTGCGCTGCTGAC
# Right flank :  TGCCAGCAC

# Questionable array : YES	 Score: 1.32
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.86, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AGCGTCAATGCGCGGATACCCAGCCCCGCAAGGCGCAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [10,3] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-7.00,-8.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-2] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [40.0-5.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.05,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_23237 flag=1 multi=2.7171 len=1442		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                           	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	==========================================	=====================================	==================
      1208	    42	 100.0	    37	..........................................	GCCGTCAAGATGTGCCCAATCGTCGATCATGGAAAGA	
      1129	    42	  95.2	     0	..........T............................T..	|                                    	
==========	======	======	======	==========================================	=====================================	==================
         2	    42	  97.6	    38	GCGTCAGACCAGGCTGCAAGCGCTCCCCGTGGCGCAGGCCTC	                                     	       

# Left flank :   CGTATCGTCGGTTGGTGGTGCGTTATGATCGTCATCGGCATCGCTCTCGTGCATGGTGTCTCGTCGCTATCATGGTGTGGTGCATTGACCGAATTTTGAAATAAGTCCTAGCACGTGATTCAAGATGTACCCTTCCGCCACACCTGCGTTCGTCGCATCCTTAACGTATGAGAATGTAAACGGGCACATGCACCGTCCGTGGTAGTAGGCTATGCATTACCCACATGTCACGC
# Right flank :  CAGCCTGCGTGGAGGTATTCCTCTCAACCGCCGTCAGGTATTGGCCGTGGCCGCTGGTGCGCAGGCACGAATCTCTTGCAACATAACCGTCTACGCGCAGGGATCGCATGCATGCCTGACCGACTTCGATGATCGGCACGAGCACATTCAACACCCTCGTGACCAGGATGGATAACGGTATGCCACGCGCCGGATCGTGAGTACGGCTGGCGCGGCAGCATGGCTGCCGCACTCCAAACTGTGCGACACGCGCATAACGAGCGGGTACGGCAATCCATCCAGCCCAGCACGTGCCGGCACGGCTGGAGCGGCGTGCTACCACCGGGCCAGTTGTCCACAACAGCACCCATAGTGATCATGCCTGCTGATATGCACGTGGTTGACACCAGTTATGGGTAATGCATACCCGCGGGGGGCGGGAACCGCCGGTGCTGGATGGCACATGCTGTCGCGTCCTGAATGATACACCGCTGGCAGCAGGTACAGCACGTTGTTGCAAG

# Questionable array : YES	 Score: 1.34
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.88, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GCGTCAGACCAGGCTGCAAGCGCTCCCCGTGGCGCAGGCCTC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [5,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-7.80,-7.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [2-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: R [35.0-46.7]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,1.05   Confidence: MEDIUM] 

# Array family : NA
//

>k141_25335 flag=1 multi=13.5357 len=1709		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence                           	Spacer_Sequence                                       	Insertion/Deletion
==========	======	======	======	==========================================	======================================================	==================
       478	    42	 100.0	    54	..........................................	CCACCAAGTGGGTGGTGGCCACCAGGTGGGTGGTGGCCAGCATAAGTAAGTTCA	
       574	    42	  92.9	     0	............A.......A..A..................	|                                                     	
==========	======	======	======	==========================================	======================================================	==================
         2	    42	  96.5	    55	GGCGGTTCTGGAGAGAAAAAGCCGGGGTGGGGAAGGTGGTGG	                                                      	       

# Left flank :   CTACCCTATTTCGGATAATCCTCAACGCCGACCATCTTGAGCGCAAGGGTATAAAGCTGCCGTGGAAAGTCTGGCTCAAGGAAGGCGCAGAATACGGCCGTCTCTACCGCCGCAAGACCTATCTGAAGTACCGGGAGCAGGCGCAGGCAAAACGATCCGCAAAAAAGGACCGGAAAAGTGCGAGACCGGAAAGCCAGAAATGAAGCGGCTTTGCGAGGGGGGTAGGAAGTATAGTTCCTACATGTAGGAAGTATAGTTCCTACCCCAAAAGTGGCTCAAACCCAGTAGTGATGCTCATTTCCGCGCTCGGGGGGTATGGTTTTTCTTCTCACTCTTATATATAATGTACCGTTCCTGCGAAAGCCGGAAACCCGCATGAATACTGGCTTTGCCGAAACAAGCTCAAGCGATTCTGGAAGGGAAAAAAACGCCGGAAACCCGCATGAATACTGGCTTCGATGGCAGTTTTAGTTTGATG
# Right flank :  GTCTTGTGCTTGAACCTGAGCTTGTGCGTCGTGCCCGCACAGCTGACCTTGTTTCGTTCCTGCGGGCGAAAGGATACCGGCTTAAGCACGAAGGTTCGGGAAACTACCGCGTGGTTGGCCACCAAGGTTTGATAGTGCAGGGTTCGCGGTGGTACTGGCATTCCGTCGATGAAGGCGGCAACGCGGTAGAGTTCCTGATGCGGGTGCTCGGGTACCCGTTCCGGGACGCGGTGCTGGAGCTGACCGGGAGCGTGCCGTGCGTGGAGAGTTCCCGTACGGACACGGAGCAGGTTGTTGCAAGGGATTTCGCATTGCCGGAAGCCGCGCCGGATTACCGACGGCTCTACGGGTATCTAATTCGGAGCCGCAACCTGCCGGTGGTGCTGGTGCAGGATGTGGTTCGCCGGAAGCTGGCTTACCAGGACCTGCGTGGCAACGTGATTTGGGTCTGTAGAGACAGTCAAGAGGTAGTCCGTGGCGCGTTCCTGGAAGGCACTGGC

# Questionable array : YES	 Score: 0.67
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.82, 5:0, 6:0.25, 7:-0.6, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGCGGTTCTGGAGAGAAAAAGCCGGGGTGGGGAAGGTGGTGG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [9,6] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-3.60,-0.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-3] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [55.0-45.0]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [1.42,0   Confidence: HIGH] 

# Array family : NA
//

>k141_27463 flag=0 multi=8.3864 len=1637		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence           	Spacer_Sequence                                              	Insertion/Deletion
==========	======	======	======	==========================	=============================================================	==================
       360	    26	  96.2	    61	...........G..............	AATTTCAATCTCTTTGCGAATCTTGCCCTCAAGCTCCCGTATCTCCAGCCTCACACCCTCA	
       447	    26	 100.0	    40	..........................	ACTCTCGATCTCTTTGGTAAGCTTTAGCCTCCCACTCTCA                     	
       513	    26	 100.0	     7	..........................	CCCTTCT                                                      	Deletion [546]
       546	    25	  84.6	     0	........T..-.....A......T.	|                                                            	T,G [551,568]
==========	======	======	======	==========================	=============================================================	==================
         4	    26	  95.2	    36	ATCTCTTTGGTAAGCTCCAGCCTCAC	                                                             	       

# Left flank :   CAAGAGCTATCGCCGCCGCCGTATTTCTAGCCGCACCTTCAAGCACAAAAGAGGCATCAACCCCGCACTCCACCGTCTGATCAAGCGCCAAAAAATATTGCGACTCGTTTGACACCACACATATCTTAGCCCCAAGCTTTAAGTTTCTCATAAGTGTTTTTTGATAAAGCGAGCCATCTTCAAAAAATTTTACAAACTGTTTAGGCAAAAGCGCCCTACTTACAGGCCACAATCTAGTCCCGCTTCCGCCGCAAAGTATTACGGTTGTCAAATTTTATGCTCCAAATAGTTTAAATATAGCTGCTATTATGGTGATGTACCCGCCAAGTATCATTACCGTCTTATTACTTTGTCGCCCTA
# Right flank :  CTCGCTCACATGACTCTTTGTTGCCATGTCCGAAAGGGGATATCTACCCTCCGCCTCGTCGAACGCCTCGGCTATTATTCTGGCCAGATCTGTATTATTCGTAGCTTTAGCTATCTCTTCATATATTTTTATAGGACTTGACACCGCCGCTCCTTTCCGAAATCTCTTCCAAAATTATAGCCCAATTATCCTAGTTTTTGAACTTCTCAGCCTCTTTTTGGTACAATCCAAACTATGATGTCGAGTGTTTTCTCATACAGAAACTTCATTTTATCATCCGTAAAGTCCGAATTTAGGTCAAAGTTCGCAGGTAGCAAGCTAGGTACTCTATGGGCTTTTTTGCATCCTCTTGCCCAAGTGGCAGTGTATGCATTTATTCTATCATCCATCATGTCCGCAAAGCTCCCGGGTGTCAAAAGTCAGTACGGCTACCCTATTTACCTTATGTCAGGGATGCTCTTTTGGTCTCTTTTTTCAGAGGTCTTTATGAGATCCGTCAC

# Questionable array : YES	 Score: 0.71
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.76, 5:0, 6:0.25, 7:-1.70, 8:0.4, 9:1,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ATCTCTTTGGTAAGCTCCAGCCTCAC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [5,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.30,-1.30] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [1-6] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [55.0-46.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_25335 flag=1 multi=13.5357 len=1709		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence               	Spacer_Sequence                      	Insertion/Deletion
==========	======	======	======	==============================	=====================================	==================
       360	    30	 100.0	    37	..............................	TGCCGAAACAAGCTCAAGCGATTCTGGAAGGGAAAAA	
       427	    30	  96.7	     0	..C...........................	|                                    	
==========	======	======	======	==============================	=====================================	==================
         2	    30	  98.3	    38	AAAGCCGGAAACCCGCATGAATACTGGCTT	                                     	       

# Left flank :   CTACCCTATTTCGGATAATCCTCAACGCCGACCATCTTGAGCGCAAGGGTATAAAGCTGCCGTGGAAAGTCTGGCTCAAGGAAGGCGCAGAATACGGCCGTCTCTACCGCCGCAAGACCTATCTGAAGTACCGGGAGCAGGCGCAGGCAAAACGATCCGCAAAAAAGGACCGGAAAAGTGCGAGACCGGAAAGCCAGAAATGAAGCGGCTTTGCGAGGGGGGTAGGAAGTATAGTTCCTACATGTAGGAAGTATAGTTCCTACCCCAAAAGTGGCTCAAACCCAGTAGTGATGCTCATTTCCGCGCTCGGGGGGTATGGTTTTTCTTCTCACTCTTATATATAATGTACCGTTCCTGCGA
# Right flank :  TCGATGGCAGTTTTAGTTTGATGGCGGTTCTGGAGAGAAAAAGCCGGGGTGGGGAAGGTGGTGGCCACCAAGTGGGTGGTGGCCACCAGGTGGGTGGTGGCCAGCATAAGTAAGTTCAGGCGGTTCTGGAAAGAAAAAACCAGGGTGGGGAAGGTGGTGGTCTTGTGCTTGAACCTGAGCTTGTGCGTCGTGCCCGCACAGCTGACCTTGTTTCGTTCCTGCGGGCGAAAGGATACCGGCTTAAGCACGAAGGTTCGGGAAACTACCGCGTGGTTGGCCACCAAGGTTTGATAGTGCAGGGTTCGCGGTGGTACTGGCATTCCGTCGATGAAGGCGGCAACGCGGTAGAGTTCCTGATGCGGGTGCTCGGGTACCCGTTCCGGGACGCGGTGCTGGAGCTGACCGGGAGCGTGCCGTGCGTGGAGAGTTCCCGTACGGACACGGAGCAGGTTGTTGCAAGGGATTTCGCATTGCCGGAAGCCGCGCCGGATTACCGACGG

# Questionable array : YES	 Score: 1.37
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     AAAGCCGGAAACCCGCATGAATACTGGCTT
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [10,5] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-2.70,-3.00] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      F [0-1] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [51.7-45.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.78,0.37   Confidence: MEDIUM] 

# Array family : NA
//

>k141_28187 flag=1 multi=7.0000 len=1002		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                	Insertion/Deletion
==========	======	======	======	============================	===============================	==================
       463	    28	 100.0	    31	............................	TTAGGGGGGCGGGCAGGGCTTATCCTTTTCA	
       522	    28	 100.0	     0	............................	|                              	
==========	======	======	======	============================	===============================	==================
         2	    28	 100.0	    41	TCCAGCTCCCCCCGCATGAGGCTGACCA	                               	       

# Left flank :   TCACCACCCTCACAGGATCCGCGGCCTTCGGGTTTATGGGGATGCTCCCTAAGGTCTCGGCTTTTTCCAGACGCTTCTCGTCCGGGTCGATGGCCAGTACCACCCCTGCCCCCGGAGTGTGGGCCACGCGTTGGGCCAATAGGGCCACCGGCCCTGAACCCACGACCGCCACGCTTTGGCCGGGCTCGAGGTAGGGCCGCACCCCCCGTAGGCGGTGGAGAGGATATCCCCAGCCAAAATCGCCTCCTCCGCCGGGAGGTCGTCCGGGATGGGGAAGAGGTAGGTTGTTTCCTCTATAAGGCGGGGTTCCTCTATAAGGCGGGGTTCCTCCACATCCTGGACCACAACCTGGAAATGCTCCTGGTAGACCACCGCCTTCACAGGACCTCCCGGGCTACGCGGACCAGCTCCTGCGCCGCCTGGACATAGGGGGCAAGGGCGGCCAGGCTGCCCTTCTTAAGCT
# Right flank :  GGGGTTCGGCCCGGCCTTCCAAGACCTCCTGCCAGATGACGAGATCCCCTTCGATGACGAAGTCCGTAGCCATGGCTTCCTCTTCTGAAGCCGCCTTTGCGCCCCGGCAGTGGCCATGCCAGAGGTCTAGGACCACCGCTAGAGGTCGTGCTACCCCCATGGCGGGATCGGCCCGCAAGAGGAAGGCTGGGCTTCCTTCCCAAGGTGGCCGCCGCCTTCTTGTAGGCCTCGCTCTCGTTCAGCTTCCGGCAATAGGCCTGGGCCCATTCCTCCGAAAAGAGTTCCCTCCCTCCCCCTTTGAACTCAGTATAAGGGGGATGGATCCCTTCCCCGTCAAGTTCGCTCGGCGCCCTGGGGGAGGCCGGCTTGGGAGGGCTCTTCGAAGCCCCCAGGGGCCTGGCGGTTCAGAAGGGGCGGGGTTTGCTTCTCTCTGTTATATGTCGAGCTGGGCAA

# Questionable array : NO	 Score: 2.15
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     TCCAGCTCCCCCCGCATGAGGCTGACCA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [5,4] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  R [-1.30,-4.40] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [31.7-36.7]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0.37,0.37   Confidence: NA] 

# Array family : NA
//

>k141_31786 flag=1 multi=11.5275 len=1686		Array_Orientation: Forward

  Position	Repeat	   %id	Spacer	Repeat_Sequence             	Spacer_Sequence                   	Insertion/Deletion
==========	======	======	======	============================	==================================	==================
      1380	    28	  67.9	    12	A.AAG........GTGA..A........	CCAGACATACCG                      	Deletion [1420]
      1420	    28	  71.4	    23	.............GT.TA.....CTGA.	GTGCGGCAGCGCGCCTAGCGATG           	
      1471	    28	  82.1	    33	...........A.GTGT...........	TGGGGCCGCCAGGCTTTCATGGCCTCAGAAATG 	
      1532	    28	 100.0	    33	............................	CCCAAGAGGGGGATTGCCTTCAAACTGGACTCT 	
      1593	    28	 100.0	    34	............................	TCAAAGGATGCTTCACCTCTTTTGGCGTCTGAAG	
      1655	    28	  85.7	     0	.............GTGT...........	|                                 	
==========	======	======	======	============================	==================================	==================
         6	    28	  84.5	    27	GGTCCTCCCCCGCACACGCGGGGACACC	                                  	       

# Left flank :   CTTGCAGGCTTTCGTGGGCTTTTTTCTTCTTGGCTTTTTCCGCTGCAGGTGCAGCATTGCTGTTGGCGCTGCTTTCATCGCTGGGCGCGCCATTTTCTGTCGGGCTTGCGTCTTCTGGCTTTTGGGCAGAAGCATCGCTCTTTTTGCCCTTGCTGCCTTTTTTGGGGTTCTTTTTTTGTGGCACGAAGCCCTTGTCGAGCGCTAGCAGTTGCTCCAGGGTCAAGTGTCTGCCATGCTGCGCAAGCGCAGCTTGCACCACACGCGCAATGCCAGACCGAATGACATGGCGCATGGTGGACGCCGGAATATAGCGCACCGCAGACTGCCCATAGCCAAAGGATGCCAGGCGCAAGGGTGAGCCGTCATTTTTCTTGTCGCTAGCCCAGGCAGCCCCCGGCGGCGAGACGGCCAACGGCTGGCGCGCCCAAAGTGGGCCAGTGACTTGCAGTGTCTGAATCGTGGTGTTCATGAAGTCTCCTAAAAGGTTGAGAGAAGGAAAA
# Right flank :  ACCA

# Questionable array : NO	 Score: 1.65
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.23, 5:0, 6:0.25, 7:-0.52, 8:1, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     GGTCCTCCCCCGCACACGCGGGGACACC
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     F [4,2] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-12.40,-12.60] Score: 0/0.37
# 	Array degeneracy analysis prediction:      R [17-4] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: F [58.3-3.3]%AT Score: 0.27/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         F [0.64,0.41   Confidence: LOW] 

# Array family : NA
//

>k141_32666 flag=1 multi=7.0000 len=2017		Array_Orientation: Unconfirmed

  Position	Repeat	   %id	Spacer	Repeat_Sequence                                  	Spacer_Sequence                                   	Insertion/Deletion
==========	======	======	======	=================================================	==================================================	==================
         1	    49	 100.0	    50	.................................................	GAGCAATAACAAACAAACAGTGGTATTATCTTCTTACCTTCTTTCAAAAT	
       100	    49	 100.0	     0	.................................................	|                                                 	
==========	======	======	======	=================================================	==================================================	==================
         2	    49	 100.0	    60	CCAGAAAACCAAAAAGGTAGAGAAACATTGGTAGATGAAGCTATACAAG	                                                  	       

# Left flank :   |
# Right flank :  GAATTAAAAAGAGAAGAGCGGTGGGATTATCTTCTTACCTTCTTTAAAAATCCGGAAAATCAGCCGAAATTCCCTGAGAAATTTAAACAATTAATTATTTTAAAACTTTTGCGTTCAATTGAAACTGTAGCTGATAAAAATTTGCACCTAAAGATAATAGAGTTCTATTGGGAATATTTTCAATCAAAAAAACCTTTAAGTTTGATTCACTTTACTCCCACCTCCGAACTACCCCTAATTCTCTTATCTTCTTTTTTGAATATCAGACCTGAGATAATTAAAAGCTGGAAACTTAATGACAAAGAAAGAAATACTCTTTTGCCAATATTTTTTAGGCTAAAAGAAAAAGGTATTTCTTTACCATTTCACTTACCCTTTTTCCCCAGAGAAGGAAGACCGTTTAAAAAACAAAGACTGATAGAGTATTTTTCGCTGCTTGATTTTATCTCTTCTTTACCAGAACAATTAGAACCAAAAAAACTTATTAATCAACACCTTAG

# Questionable array : YES	 Score: 1.19
# 	Score Detail : 1:0, 2:0, 3:0, 4:1.00, 5:0, 6:-0.5, 7:-0.2, 8:0.2, 9:0.69,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CCAGAAAACCAAAAAGGTAGAGAAACATTGGTAGATGAAGCTATACAAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:63.27%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  NA [-2.90,-3.10] Score: 0/0.37
# 	Array degeneracy analysis prediction:      NA [0-0] Score: 0/0.41
# 	AT richness analysis in flanks prediction: NA [0.0-0.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         NA [0,0   Confidence: NA] 

# Array family : NA
//

>k141_33132 flag=1 multi=4.0000 len=1862		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence              	Spacer_Sequence                                        	Insertion/Deletion
==========	======	======	======	=============================	=======================================================	==================
       208	    29	 100.0	    55	.............................	TGTCAATATGAATAACGTCGCTGCAGCCATGGAGCAGGCCTCTACCAATGTGAGT	
       124	    29	  96.6	     0	.....G.......................	|                                                      	
==========	======	======	======	=============================	=======================================================	==================
         2	    29	  98.3	    56	ACGGTAGCCGCGGCATCGGAAGAGATGAG	                                                       	       

# Left flank :   GTCCCATTCGTCTGACCCAGGAGTGTCTCTACTGCCATGGCGACCCCAAAGGCGAAAAAGATCCCACCGGCGGCACCAAGGAAGGTTGGAAGGAGGGAGAGATACATGGCGCGTTTTCCGTCATCATGTCCATGGAGGCATCCAATGCTGCGGTGCGTGCGAGTGGGCTGCGCTTGGGGGTGGAGGTCTTGGGTATCCTGGCAGTGGTTGGTATCGCGGCCTGGTTTTTGGTGCACCGCGGCGTCATCCAGCCGTTGATGCAGACAGGCGCCTATCTGGATCGTCTGGCAACTGGTGATATGACCTCGACGATCCCCGTGGAGCGCGAGGACGAGATCGGCCTTATGCAGCGCAATCTCAACGCCATGGCCCAGGGACTGCGCTCCATTGTCCAGGATATTGTGACCCGTTCCCGCACCCTTATTGGTTCTGCCACGGAGCTAGAGACCATGGCGGATACTTTGCTGGTCCGTTCTCAGGATTTAGCGGGCCGCTCCCGC
# Right flank :  GCGCCACCATCCACGAGATCGCTGGCAATACCGAAAAGGCCCGGGAAGTGACTCAGCGGGCGGTGACCACCGCTACATCGGCATCGCAGCGGGTGT

# Questionable array : YES	 Score: 0.66
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.91, 5:0, 6:0.25, 7:-0.7, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     ACGGTAGCCGCGGCATCGGAAGAGATGAG
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     R [3,8] Score: 0.37/0.37
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-3.00,-2.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [36.7-40.0]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.78   Confidence: MEDIUM] 

# Array family : NA
//

>k141_385 flag=1 multi=7.0000 len=1052		Array_Orientation: Reverse

  Position	Repeat	   %id	Spacer	Repeat_Sequence                  	Spacer_Sequence                     	Insertion/Deletion
==========	======	======	======	=================================	====================================	==================
       879	    33	 100.0	    36	.................................	CTTGCTAACATTTTTGCTAACATTCTTGCTAACAAA	
       810	    33	  97.0	     0	............T....................	|                                   	
==========	======	======	======	=================================	====================================	==================
         2	    33	  98.5	    37	CTTGCTAACAAACTTGCTAACATTGCTAACAAA	                                    	       

# Left flank :   TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTATTGTACCAGGTACAAATATACAAACGCGCCTTTCCGCTCGAACGCGCTTCCGCCGCGCTTCCGCCGTGCTTTCCTGCCGCGCCGCGCCGCTCGGAGTTTGTTAGCAGCTATGATAGCAAATGTTAGCTGCTAACAT
# Right flank :  TGCTGAGAACGGAAGAAGGAAAGGACTTGGCGGAGTCGTGCTCCGTACTTCGAGATTTGAGCTTTACTTCGGCACCTGCGTTCTGGGCCGGAAGGAGATTTTGGGGTTTTTGGAGGAGGAATCTGACCGCTTTCCCGCCGCCGCACCTTGCCGACCGGCCCCGGGATACCCTTCCAAATTCTTCCTTTTCCACTTTTTGCCAGATTTAACGACTTCTTCCGGAACAGGATGCAATCCCCACTTCTGCCGTCTTCTACCCAATTTAGGCCTGCTTCCCAGTTTTGCCAGCATCTTCCCGATTAAGGCAGATTTCCCATTTCTGCCAGCTCTCCGCCAGTTTAGCCGCATTCTTCCAAATTTGTCAGCTTCTGCACAATCTGAGCATATTCCCCAGATTTGCCGTTTTCGTCCAGATAAAGCACATTCTTCCAGATTTGTCGACTTTTGCCTAATTTAGGCAGATTTCCCACTTTTGTCGGATTCTGTCTAATCAGGACGTA

# Questionable array : YES	 Score: 1.39
# 	Score Detail : 1:0, 2:0, 3:0, 4:0.93, 5:0, 6:0.25, 7:0.01, 8:0.2, 9:0.00,
# 	Score Legend : 1: cas, 2: likely_repeat, 3: motif_match, 4: overall_repeat_identity, 5: one_repeat_cluster, 6: exp_repeat_length, 7: exp_spacer_length, 8: spacer_identity, 9: log(total repeats) - log(total mutated repeats),
# Primary repeat :     CTTGCTAACAAACTTGCTAACATTGCTAACAAA
# Alternate repeat :   NA

# Directional analysis summary from each method:
# 	Motif ATTGAAA(N) match prediction:         NA Score: 0/4.5
# 	A,T distribution in repeat prediction:     NA [Repeat is AT rich:66.67%AT]
# 	Reference repeat match prediction:         NA 
# 	Secondary Structural analysis prediction:  F [-2.00,-0.20] Score: 0.37/0.37
# 	Array degeneracy analysis prediction:      R [1-0] Score: 0.41/0.41
# 	AT richness analysis in flanks prediction: NA [48.3-48.3]%AT Score: 0/0.27
# 	Longer leader analysis prediction:         NA 
# 	----------------------------------------------------------------------------
# 	Final direction:         R [0.37,0.41   Confidence: LOW] 

# Array family : NA
//

